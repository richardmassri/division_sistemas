@extends("panelAdmin")

@section('content')
<div class="box-header with-border">
   <h3 class="box-title">Formulario de Usuario de Actualizacion</h3>
</div>

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<?php 
  if(isset($descripcion)){?>
    <div class="<?php echo $descripcion['clase'];?>">
      <?php echo $descripcion['mensaje'];?>
    <div>
<?php
  }
?>    
 
{!! Form::open(array('url' => ['admin/post',$dataForm->id], 'method' => 'put', 'files'=> true)) !!}﻿

<link rel="stylesheet" href="{{asset('bootstrap/css/bootstrap-multiselect.css')}}" type="text/css">

            <div class="box-body">
            <div class="col-md-12">
                <div class="col-md-6">
                <label class="col-md-12"> Nombre </label>
                  <input class="form-control col-md-4" placeholder="Nombre" name="nombre" type="text" maxlength="30" value="{{ $dataForm->nombre }}">
                </div>
                <div class="col-md-6">
                <label class="col-md-12"> Descripción Breve </label>
                  <input class="form-control col-md-4" type="text" placeholder="Descipción Breve" name="descripcion_breve" maxlength="30" value="{{ $dataForm->descripcion_breve }}">
                </div>
              </div>
              <div class="row"> </div>
              <br>
              <div class="col-md-12">
                  <section class="content">
                      <div class="row">
                        <div class="col-md-12">
                          <div class="box box-info">
                            <div class="box-header">
                              <h3 class="box-title">Descripción
                              </h3>
                              <!-- tools box -->
                              <div class="pull-right box-tools">
                                <button type="button" class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                  <i class="fa fa-minus"></i></button>
                                <button type="button" class="btn btn-info btn-sm" data-widget="remove" data-toggle="tooltip" title="Remove">
                                  <i class="fa fa-times"></i></button>
                              </div>
                              <!-- /. tools -->
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body pad">
                              <textarea id="descripcion" class="descripcion" name="descripcion" rows="10" cols="80" value="{{ $dataForm->descripcion }}">
                              <?php echo $dataForm->descripcion;?>
                              </textarea>
                            </div>
                          </div>
                          <!-- /.box -->

                        </div>
                        <!-- /.col-->
                      </div>
                      <!-- ./row -->
                    </section>
                </div>
              <div class="row"> </div>
              <br>

                  <?php
                      $posT="";$tagsConcat="";?>
                      @foreach($posTags as $posT)
                        <?php
                                $tagsConcat=$tagsConcat.$posT->tags_id.",";
                              ?>
                      @endforeach
                      <?php
                      $maximoCadena=strlen($tagsConcat);
                      $cadenaMaxima=$tagsConcat[$maximoCadena-1];
                      if($cadenaMaxima==','){
                       $reemplazarCadena=substr($tagsConcat,0,(int)$cadenaMaxima-1);
                      }
                    ?>


                          
              <input type="text" class="hidden posTags" value="{{$reemplazarCadena}}"> </input>
              <div class="col-md-12">
                  <div class="form-group col-md-4">
                    <label for="exampleInputFile">
                      <img src="{{asset($dataForm->foto)}}" class="col-md-3" style="cursor:pointer;width:200px;height: 150px " alt="Injaz Msila" style="float:right;margin:7px">
                      <input id="exampleInputFile" class="col-md-4" type="file" name="foto" style="cursor: pointer;  display: none">
                  </label>
                  <label class="col-md-12 imagen_mensaje hidden"> </label>
                  </div>
                    <div class="col-md-4 form-group">
                      <label class="col-md-12"> Tags </label>
                      <select id="tagsR" name="tags[]" multiple="multiple" class="col-md-4 form-control tagsR">
                      @foreach($tags as $ta)
                      <option value="{{$ta->id}}">{{$ta->nombre}} </option>
                      @endforeach
                      </select>
                    </div>

                <div class="col-md-4">
                <label class="col-md-12">Categoria</label>
                  <select class="col-md-4 form-control" name="categoria_id" required="required">
                  <option value="">Seleccione</option>
                  @foreach ($categorias as $categoria)
                    <option value="{{$categoria->id}}"<?php echo ($dataForm->categoria_id==$categoria->id)?"selected":"";?>>{{$categoria->nombre}}</option>
                  @endforeach
                  </select>
                </div>
              </div>

    

                <div class="row"></div>
                <br>

                <div class="col-md-4">
                  <a href="{{asset('admin/post')}}" class="btn btn-danger">Volver </a>
                  </div>
                <div class="col-md-4">
               </div>
               <div class="col-md-4 text-right">
               <button class="btn  btn-success text-right">Actualizar</button>
               </div>
              </div>


                <!-- /input-group -->
              </div>
            <form>

            @stop

            {!! Form::close() !!} 

          @section('scripts')
          <script type="text/javascript" src="{{asset('bootstrap/js/bootstrap-multiselect.js')}}"></script>
            <script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>

            <script>
              $(document).ready(function(){
                var values=$('.posTags').val();
                    $.each(values.split(","), function(i,e){
                    $(".tagsR option[value='" + e + "']").prop("selected", true);
                $(function () {
                  CKEDITOR.replace('descripcion');
                  $(".descripcion").wysihtml5();
                });
              });
                $('#tagsR').multiselect({
                  buttonWidth: '300px',
                  enableHTML: true,
                  nonSelectedText: 'Selecione el/los tags',
              });
            });

          $('#exampleInputFile').on("change",function(){
            $('.imagen_mensaje').removeClass('hidden');
            $('.imagen_mensaje').html($('#exampleInputFile').val());
          })

            </script>


          @stop
