@extends("panelAdmin")

@section('content')
<div class="box-header with-border">
   <h3 class="box-title">Crear Nuevo Post</h3>
</div>

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>

            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<?php 
  if(isset($descripcion)){?>
    <div class="<?php echo $descripcion['clase'];?>">
      <?php echo $descripcion['mensaje'];?>
    </div>
<?php
  }
?>    
<link rel="stylesheet" href="{{asset('bootstrap/css/bootstrap-multiselect.css')}}" type="text/css">
<form action="{{asset('admin/post')}}" method="post" name="vieja" accept-charset="UTF-8" enctype="multipart/form-data">
  <div class="box-body">
            <div class="col-md-12">
                <div class="col-md-6">
                <label class="col-md-12"> Nombre </label>
                  <input class="form-control col-md-4" placeholder="Nombre" name="nombre" type="text" maxlength="30" value="{{ old('nombre') }}" required="required">
                </div>

                <div class="col-md-6">
                <label class="col-md-12"> Descripción Breve </label>
                <textarea class="form-control col-md-4" name="descripcion_breve" placeholder="Descripción Breve" cols="40" rows="5" value="{{ old('descripcion_breve') }}" required="required" maxlength="200"></textarea>
                </div>
              </div>

              <div class="row"> </div>
              <br>

              <div class="col-md-12">
                  <section class="content">
                      <div class="row">
                        <div class="col-md-12">
                          <div class="box box-info">
                            <div class="box-header">
                              <h3 class="box-title">Descripción
                              </h3>
                              <!-- tools box -->
                              <div class="pull-right box-tools">
                                <button type="button" class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                  <i class="fa fa-minus"></i></button>
                                <button type="button" class="btn btn-info btn-sm" data-widget="remove" data-toggle="tooltip" title="Remove">
                                  <i class="fa fa-times"></i></button>
                              </div>
                              <!-- /. tools -->
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body pad">
                              <textarea id="descripcion" name="descripcion" rows="10" cols="80" required="required">
                              </textarea>
                            </div>
                          </div>
                          <!-- /.box -->

                        </div>
                        <!-- /.col-->
                      </div>
                      <!-- ./row -->
                    </section>
                </div>

                <div class="row"></div>
                <br>
                <div class="col-md-12">
                  <div class="form-group col-md-4">
                    <label for="exampleInputFile">Seleccione la foto</label>
                    <input id="exampleInputFile col-md-4" type="file" name="foto" value="{{ old('foto') }}" required="required">
                  </div>
                  <div class="col-md-4 form-group">
                  <label class="col-md-12"> Tags </label>
                      <select id="tags" name="tags[]" multiple="multiple" class="col-md-4 form-control" required="required">
                      @foreach($tags as $tag)
                      <option value="{{$tag->id}}">{{$tag->nombre}} </option>
                      @endforeach
                      </select>
                  </div>

                <div class="col-md-4 form-group">
                  <label class="col-md-12"> Categoria </label>
                      <select id="categoria_id" name="categoria_id" class="col-md-4 form-control" required="required">
                      @foreach($categorias as $categoria)
                      <option value="{{$categoria->id}}">{{$categoria->nombre}} </option>
                      @endforeach
                      </select>
                  </div>

              </div>
              </div>
                <input type="text" class="hidden" name="_token" value="{{ csrf_token() }}"></input>


              <div class="row"></div>
              <br>

              <div class="col-md-12"> 

                <div class="col-md-4">
                  <a href="{{asset('admin/post')}}" class="btn btn-danger">Volver </a>
               </div>

                <div class="col-md-4">
               </div>
               <div class="col-md-4 text-right">
               <button class="btn btn-success text-right">Guardar</button>
               </div>
              </div>

            </form>

            @stop

@section('scripts')
<script src="http://code.jquery.com/ui/1.10.1/jquery-ui.js"></script>
<script type="text/javascript" src="{{asset('bootstrap/js/bootstrap-multiselect.js')}}"></script>
<script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
<script src="{{asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
<script type="text/javascript">
    $(function () {
// Replace the <textarea id="editor1"> with a CKEditor
// instance, using default configuration.
      CKEDITOR.replace('descripcion');
      //bootstrap WYSIHTML5 - text editor
      $(".textarea").wysihtml5();
    });
    $(document).ready(function(){
    $('#tags').multiselect({
    buttonWidth: '300px',
    enableHTML: true,
    nonSelectedText: 'Selecione el/los tags',
  });
})
</script>
@stop