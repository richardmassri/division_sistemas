@extends("panelAdmin")

@section('content')


<div class="box-header with-border">
   <h3 class="box-title">Formulario de Usuario de Vista</h3>
</div>

<link rel="stylesheet" href="{{asset('bootstrap/css/bootstrap-multiselect.css')}}" type="text/css">
  <div class="box-body">
            <div class="col-md-12">
                <div class="col-md-6">
                <label class="col-md-12"> Nombre </label>
                  <input class="form-control col-md-4" placeholder="Nombre" name="nombreForm" type="text" maxlength="30" value="{{ $dataForm->nombre }}" readonly="readonly">
                </div>
                <div class="col-md-6">
                <label class="col-md-12"> Descripción Breve </label>
                  <input class="form-control col-md-4" type="text" placeholder="Descripción Breve" name="descripcion_breve" maxlength="30" value="{{ $dataForm->descripcion_breve}}" readonly="readonly">
                </div>
              </div>
              <div class="row"> </div>
              <br>
              <div class="col-md-12">
                <div class="col-md-12">
                 <label class="col-md-12">Descripción</label>
                  <textarea class="form-control col-md-4" name="vision" cols="40" rows="5" value="{{ $dataForm->descripcion }}" required="required" readonly> <?php echo $dataForm->descripcion;?></textarea>
                </div>
              </div>
              <div class="row"> </div>
              <br>

                  <?php
                      $posT="";$tagsConcat="";?>
                      @foreach($posTags as $posT)
                        <?php
                                $tagsConcat=$tagsConcat.$posT->tags_id.",";
                              ?>
                      @endforeach
                      <?php
                      $maximoCadena=strlen($tagsConcat);
                      $cadenaMaxima=$tagsConcat[$maximoCadena-1];
                      if($cadenaMaxima==','){
                       $reemplazarCadena=substr($tagsConcat,0,(int)$cadenaMaxima-1);
                      }
                    ?>


                          
                      <input type="text" class="hidden posTags" value="{{$reemplazarCadena}}"> </input>
              <div class="col-md-12">
                <div class="form-group col-md-4">
                  <label class="col-md-12"> Foto </label>
                  <img width="120" height="80" src="{{asset($dataForm->foto)}}">
                </div>
                    <div class="col-md-4 form-group">
                      <label class="col-md-12"> Tags </label>
                      <select id="tagsR" name="tags[]" multiple="multiple" class="col-md-4 form-control tagsR">
                      @foreach($tags as $ta)
                      <option value="{{$ta->id}}">{{$ta->nombre}} </option>
                      @endforeach
                      </select>
                    </div>

                  <div class="col-md-4">
                <label class="col-md-12"> Categoria </label>
                  <input class="form-control col-md-4" name="categoria"  type="text" maxlength="50" value="{{ $dataForm->categoria->nombre}}" readonly="readonly">
                </div>
              </div>

    

                <div class="row"></div>
                <br>


                <div class="col-md-12">
                @if($dataForm->usuario_ini_id)
                <div class="col-md-6">
                <label class="col-md-12"> Creado por: </label>
                  <input class="form-control col-md-4" name="contrasenaForm"  type="text" maxlength="50" value="{{ $dataForm->metodo_usuario_ini_id->nombre}} {{ $dataForm->metodo_usuario_ini_id->apellido}}" readonly="readonly">
                </div>
                @endif


                @if($dataForm->usuario_act_id)
                <div class="col-md-6">
                <label class="col-md-12"> Actualizado por: </label>
                  <input class="form-control col-md-4" name="contrasenaForm"  type="text" maxlength="50" value="{{ $dataForm->metodo_usuario_act_id->nombre}} {{ $dataForm->metodo_usuario_act_id->apellido}}" readonly="readonly">
                </div>
                @endif

              </div>
              <div class="row"> </div>
              <br>

              <div class="col-md-12">
                <div class="col-md-6">
                <label class="col-md-12"> Fecha de Creación </label>
                  <input class="form-control col-md-4" placeholder="Rpetir Contraseña" name="contrasena_nuevaForm" type="text" maxlength="50" value="{{ $dataForm->updated_at }}" readonly="readonly">
                </div>
                <div class="col-md-6">
                <label class="col-md-12"> Fecha de Actualización </label>
                  <input class="form-control col-md-4" name="contrasenaForm"  type="text" placeholder="Contraseña" maxlength="50" value="{{ $dataForm->created_at }}" readonly="readonly">
                </div>

              </div>


                <input type="text" class="hidden" name="_token" value="{{ csrf_token() }}"></input>


              <div class="row"></div>

              <br>

              <div class="col-md-12"> 

                <div class="col-md-4">
                  <a href="{{asset('admin/post')}}" class="btn btn-danger">Volver </a>
               </div>

                <div class="col-md-4">
               </div>
              </div>


                <!-- /input-group -->
              </div>
            <form>


            @stop

            {!! Form::close() !!} 
          @section('scripts')
          <script type="text/javascript" src="{{asset('bootstrap/js/bootstrap-multiselect.js')}}"></script>
            <script>

              $(document).ready(function(){
                var values=$('.posTags').val();
                    $.each(values.split(","), function(i,e){
                    $(".tagsR option[value='" + e + "']").prop("selected", true);
              });
                $('#tagsR').multiselect({
                  buttonWidth: '300px',
                  enableHTML: true,
                  nonSelectedText: 'Selecione el/los tags',
              });
            });

            </script>
          @stop