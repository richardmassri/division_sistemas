@extends("panelAdmin")

@section('content')
<div class="box-header with-border">
   <h3 class="box-title">Formulario de Vista de operación</h3>
</div>


  <div class="box-body">
            <div class="col-md-12">
                <div class="col-md-4">
                <label class="col-md-12"> Nombre </label>
                  <input class="form-control col-md-4" name="nombreForm" type="text" maxlength="30" value="{{ $dataForm->nombre }}" readonly="readonly">
                </div>

                <div class="col-md-4">
                <label class="col-md-12"> Fecha de Actualización </label>
                  <input class="form-control col-md-4" name="contrasena_nuevaForm" type="text" maxlength="50" value="{{ $dataForm->updated_at }}" readonly="readonly">
                </div>
                <div class="col-md-4">
                <label class="col-md-12"> Fecha de Creación </label>
                  <input class="form-control col-md-4" name="contrasenaForm"  type="text" maxlength="50" value="{{ $dataForm->created_at }}" readonly="readonly">
                </div>
              </div>
                <div class="row">
                </div>
                <br>
              <div class="col-md-12">
                @if($dataForm->usuario_ini_id)
                <div class="col-md-4">
                <label class="col-md-12"> Creado por: </label>
                  <input class="form-control col-md-4" name="contrasenaForm"  type="text" maxlength="50" value="{{ $dataForm->metodo_usuario_ini_id->nombre}} {{ $dataForm->metodo_usuario_ini_id->apellido}}" readonly="readonly">
                </div>
                @endif


                @if($dataForm->usuario_act_id)
                <div class="col-md-4">
                <label class="col-md-12"> Actualizado por: </label>
                  <input class="form-control col-md-4" name="contrasenaForm"  type="text" maxlength="50" value="{{ $dataForm->metodo_usuario_act_id->nombre}} {{ $dataForm->metodo_usuario_act_id->apellido}}" readonly="readonly">
                </div>
                @endif

              </div>

                <input type="text" class="hidden" name="_token" value="{{ csrf_token() }}"></input>


              <div class="row"></div>

              <br>

              <div class="col-md-12"> 

                <div class="col-md-4">
                  <a href="{{asset('admin/operacion')}}" class="btn btn-danger">Volver </a>
               </div>

                <div class="col-md-4">
               </div>
              </div>


                <!-- /input-group -->
              </div>
            <form>

            @stop

            {!! Form::close() !!} 
