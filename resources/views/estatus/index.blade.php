@extends("panelAdmin")

@section('content')
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">

  <link rel="stylesheet" href="{{ asset('dist/css/skins/_all-skins.min.css')}}">



          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Gestión de Estatus</h3>
            </div>

            <!-- /.box-header -->
            <div class="box-body table-responsive">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Nombre</th>
                  <th>Estatus</th>
                  <th>Accion</th>
                </tr>
                </thead>
                
                  <tbody>
                  @foreach ($consultarEstatus as $consultarEstatus)
                  <tr>
                    <td><?php echo $consultarEstatus->nombre;?></td>
                    <td><?php if($consultarEstatus->estatus=='A'){ 
                      echo 'Activo';}else{ echo 'Inactivo'; }?></td>
                    <td>
                    <a href="estatus/{{$consultarEstatus->id}}" class="fa fa-fw fa-search"> </a>
                    <?php if($consultarEstatus->estatus=='A'){?>
                    <a href="estatus/{{$consultarEstatus->id}}/edit" class="fa fa-fw fa-pencil"> </a>
                    <?php } ?>
                    <?php if($consultarEstatus->estatus=='A'){?>
                    <a href="#" class="fa fa-fw fa-trash elim" onclick="eliminar(<?php echo $consultarEstatus->id;?>)
                    ">
                    <?php }else if($consultarEstatus->estatus=='E'){?>
                    <a href="#" class="fa fa-fw fa-check-square-o activar" onclick="activar(<?php echo $consultarEstatus->id;?>)
                    ">
                    <?php }?> 

                    </a>
                    </td>
                  </tr>
                @endforeach
               
                </thead>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
    <!-- /.content -->
 
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<div id="dialog1" title="Dialog Title" hidden="hidden">I'm a dialog</div>

<div id="dialog-confirm"> Esta seguro que desea eliminar este registro?
</div>

<div id="activar"> Esta seguro que desea activar este registro?
</div>
<input type="text" class="hidden" name="_token" id="token" value="{{ csrf_token() }}"></input>


@stop
@section('scripts')
  <script src="http://code.jquery.com/ui/1.11.1/jquery-ui.min.js"></script>
  <link rel="stylesheet" href="https://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css" />
    <script>
    function eliminar(id){
      var _token = $("#token").val();
    $( "#dialog-confirm" ).dialog({
            resizable: false,
            modal: true,
            buttons: {
                "Eliminar": function() {
                  var _token = $("#token").val();

                  $.ajax({
                  type: "DELETE",
                  url: "{{asset('admin/estatus')}}/"+id+'-'+'e',
                  data: { id: id,_token:_token},
                  dataType: "json",
                  success: function (resultado) {
                    //$( this ).dialog( "close" );
                    $( "#dialog-confirm" ).dialog("close");
                    window.location.href = "{{asset('admin/estatus')}}"; 
                        }
                  })
                },
                Cancel: function() {
                    $( this ).dialog( "close" );
                }
            }
        });
  }

   function activar(id){
      var _token = $("#token").val();
    $( "#activar" ).dialog({
            resizable: false,
            modal: true,
            buttons: {
                "Activar": function() {
                  var _token = $("#token").val();
                  var tipo="a";
                  $.ajax({
                  type: "DELETE",
                  url: "{{asset('admin/estatus')}}/"+id+'-'+'a',
                  data: { id: id,_token:_token},
                  dataType: "json",
                  success: function (resultado) {
                    //$( this ).dialog( "close" );
                    $( "#activar" ).dialog("close");
                    window.location.href = "{{asset('admin/estatus')}}";
                        }
                  })
                },
                Cancel: function() {
                    $( this ).dialog( "close" );
                }
            }
        });
  }


</script>


<!-- page script -->
<script>

    $(function () {
    $("#example1").DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
        language: {
        "sProcessing":     "Procesando...",
        "sLengthMenu":     "Mostrar _MENU_ registros",
        "sZeroRecords":    "No se encontraron resultados",
        "sEmptyTable":     "Ningún dato disponible en esta tabla",
        "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix":    "",
        "sSearch":         "Buscar:",
        "sUrl":            "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst":    "Primero",
            "sLast":     "Último",
            "sNext":     "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
    }
});
});



</script>


@stop