
<!DOCTYPE html>
<html>
<head>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="robots" content="index, follow">
<title>Invermaster</title>


<!-- Page loader -->
<script src="{{asset('tribu/assets/pace/js/pace.min.js')}}"></script>
<link rel="stylesheet" href="{{asset('tribu/assets/pace/css/pace-minimal.css')}}" type="text/css">
<link rel="stylesheet" href="{{asset('plugins/select2/select2.min.css')}}">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.css">

<!-- Bootstrap -->
<link rel="stylesheet" href="{{asset('tribu/assets/bootstrap/css/bootstrap.min.css')}}" type="text/css">

<!-- CSS animation library -->
<link rel="stylesheet" href="{{asset('tribu/assets/animate/animate.min.css')}}" type="text/css">

<link href="https://fonts.googleapis.com/css?family=Quicksand:400,500,700" rel="stylesheet">

<!-- Plugins -->
<link rel="stylesheet" href="{{asset('tribu/assets/bootstrap-select/css/bootstrap-select.min.css')}}">
<link rel="stylesheet" href="{{asset('tribu/assets/swiper/css/swiper.min.css')}}">

<!-- Light Gallery -->
<link rel="stylesheet" href="{{asset('tribu/assets/lightgallery/css/lightgallery.min.css')}}">

<!-- Icons -->
<link rel="stylesheet" href="{{asset('tribu/fonts/font-awesome/css/font-awesome.min.css')}}" type="text/css">
<link rel="stylesheet" href="{{asset('tribu/fonts/ionicons/css/ionicons.min.css')}}" type="text/css">
<link rel="stylesheet" href="{{asset('tribu/fonts/line-icons/line-icons.css')}}" type="text/css">
<link rel="stylesheet" href="{{asset('tribu/fonts/line-icons-pro/line-icons-pro.css')}}" type="text/css">

<!-- Global style (main) -->
<link id="stylesheet" type="text/css" href="{{asset('tribu/css/global-style.min.css')}}" rel="stylesheet" media="screen">
<!-- Custom style - Remove if not necessary -->
<link type="text/css" href="{{asset('tribu/css/custom-style.css')}}" rel="stylesheet">

<!-- Favicon -->
<link href="{{asset('tribu/images/logo/logo.png')}}" rel="icon" type="image/png">


<!-- RS5.0 -->
<link rel="stylesheet" type="text/css" href="{{asset('tribu/assets/revolution/css/settings.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('tribu/assets/revolution/css/layers.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('tribu/assets/revolution/css/navigation.css')}}">

<!-- Cube Portfolio -->
<link rel="stylesheet" href="{{asset('tribu/assets/cubeportfolio/css/cubeportfolio.min.css')}}">

</head>
<body>


<!-- MAIN WRAPPER -->
@include('plantillaCliente.plantilla_principal')




                    <!-- Properties listing -->
                    <section class="slice sct-color-2">
                        <div class="container">

                        <div class="row">
                        <div class="alert alert-success container enviarMensaje hidden">
                            <strong>Estimado usuario, su mensaje fue enviado de forma exitosa. En breves momentos será atendida su solicitud. </strong>
                    </div>
                        </div>
                            <div class="row">
                                <div class="col-md-9">
                                    <div class="mb-2">
                                        <h3 class="heading heading-3 strong-600 text-normal">
                                            {{$consultaPropiedadPrincipal[0]->nombre_proyecto}}
                                        </h3>
                                        <ul class="inline-links inline-links--style-2">
                                        <li>
                                            <a href="#">
                                            <i class="fa fa-heart meGusta{{$consultaPropiedadPrincipal[0]->id}}"
                                            <?php 
                                            if(\Cache::get($consultaPropiedadPrincipal[0]->id)==$consultaPropiedadPrincipal[0]->id){ ?>  style="color: blue;" 
                                            <?php } ?>
                                            onclick="meGusta({{$consultaPropiedadPrincipal[0]->id}})"></i>
                                            </a>
                                            </li>
                                            <li>
                                                <i class="fa fa-eye"></i> <?php 
                                                    if(count($consultaCantidadPropiedadesVistas)>0){
                                                        echo $consultaCantidadPropiedadesVistas[0]->cantidad_vistas;}else{ echo "0";}?>
                                            </li>
                                            <li>
                                            <i class="fa fa-envelope"></i> <?php
                                            if(count($consultaCantidadPropiedadesVistasContactadas)>0){
                                                        echo $consultaCantidadPropiedadesVistasContactadas[0]->cantidad_vistas;}else{ echo "0";}?>
                                            </li>
                                        </ul>
                                    </div>

                                    <!-- Gallery -->
                                    <div class="gallery-top">
                                        <img src="{{asset($consultaPropiedadPrincipal[0]->nombre_galeria)}}">
                                    </div>
                                    
                                    <div class="gallery-bottom" id="light_gallery">
                                        <div class="row">
                                        <?php $c=0;?>
                                        @foreach($consultaPropiedadAll as $consultaPropiedadAll)
                                            <div class="col-sm-2">
                                                <div class="gallery-thumb">
                                                    <a class="item" href="{{asset($consultaPropiedadAll->nombre_galeria)}}">
                                                        <img src="{{asset($consultaPropiedadAll->nombre_galeria)}}" width="130" height="100">
                                                    </a>
                                                </div>
                                                <?php if($c==5){ $c=0;?> <br> <?php } ?>
                                            </div>
                                            <?php $c++;?>
                                        @endforeach
                                        </div>
                                    </div>

                                    <input type="text" name="latitud" class="hidden" id="latitud" value="{{$consultaPropiedadPrincipal[0]->latitud}}">

                                    <input type="text" name="longitud" class="hidden" id="longitud" value="{{$consultaPropiedadPrincipal[0]->longitud}}">


                                    <span class="space-md-md"></span>

                                    <!-- Description -->
                                    <div class="card z-depth-2-top">
                                        <div class="card-title b-xs-bottom">
                                            <h3 class="heading heading-sm text-uppercase" >Descripción</h3>
                                        </div>
                                        <div class="card-body" style="text-align: justify;">
                                            <p>
                                            <?php echo $consultaPropiedadPrincipal[0]->descripcion;?>
                    
                                            </p>


                                        </div>
                                    </div>

                                    <!--<span class="space-md-md"></span>

                                    <div class="card z-depth-2-top">
                                        <div class="card-title b-xs-bottom">
                                            <h3 class="heading heading-sm text-uppercase">Ubicación</h3>
                                                <div id="googleMap" style="width:100%;height:400px;"></div>
                                        </div>
                                    </div>-->

                                    <span class="space-md-md"></span>

                                    <!-- Property details -->
                                    <div class="card z-depth-2-top">
                                        <div class="card-title b-xs-bottom">
                                            <h3 class="heading heading-sm text-uppercase">Detalles de la propiedad</h3>
                                        </div>
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <table class="table table-no-border table-striped table-responsive">
                                                        <tbody>

                                                            <tr>
                                                                <td><strong>Nombre:</strong> 
                                                                {{$consultaPropiedadPrincipal[0]->nombre_proyecto}}</td>
                                                                 <td><strong>País:</strong> {{$consultaPropiedadPrincipal[0]->nombre_pais}}</td>
                                                            </tr>
                                                            

                                                            <tr>
                                                                <td><strong>Ciudad:</strong> {{$consultaPropiedadPrincipal[0]->nombre_ciudad}}</td>
                                                                <td><strong>Sector:</strong> {{$consultaPropiedadPrincipal[0]->nombre_sector}}</td>
                                                            </tr>

                                                                 <?php 
                                                                $convertirFecha=new DateTime($consultaPropiedadPrincipal[0]->fecha_publicacion);?>

                                                            <tr> 
                                                                <td><strong>Fecha Publicación: </strong><?php echo $convertirFecha->format('d-m-Y');?> </td>
                                                                  <td colspan=3><strong>Tipo:</strong> {{$consultaPropiedadPrincipal[0]->nombre_tipo}}</td>
                
                                                            </tr>
                                                                  <tr>
                                                                <td><strong>Precio:</strong> 
                                                                <?php $precio_dede=number_format((float)$consultaPropiedadPrincipal[0]->precio, 2, ',', '.');?>

                                                                USD {{$precio_dede}}</td>
                                                            </tr>

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <span class="space-md-md"></span>

                                    <input type="text" class="hidden" id="propiedad_id" name="propiedad_id" value="{{$consultaPropiedadPrincipal[0]->id}}">
                                    <?php if(count($consultaCaracteristicaPropiedad)>0){?>
                                    <div class="card z-depth-2-top">
                                        <div class="card-title b-xs-bottom">
                                            <h3 class="heading heading-sm text-uppercase">Características de la propiedad</h3>
                                        </div>
                                        <div class="card-body">
                                        <table>
                                          @foreach($consultaCaracteristicaPropiedad as $consultaCaracteristicaPropiedad)
                                            <tr>
                                               <td>
                                                 <i class="fa fa-circle-o"> </i>
                                               {{$consultaCaracteristicaPropiedad->nombre_caracteristica}} 
                                               </td>
                                            </tr>
                                            @endforeach
                                        </table>
                            
                                        </div>
                                    </div>

                                    <?php }?>

                                    <span class="space-md-md"></span>

                                    <?php if(count($consultaOperacionesPropiedad)>0){?>
                                    <div class="card z-depth-2-top">
                                        <div class="card-title b-xs-bottom">
                                            <h3 class="heading heading-sm text-uppercase">Operaciones de la propiedad</h3>
                                        </div>
                                        <div class="card-body">
                                        <table>
                                          @foreach($consultaOperacionesPropiedad as $consultaOperacionesPropiedad)
                                            <tr>
                                               <td>
                                                 <i class="fa fa-circle-o"> </i>
                                               {{$consultaOperacionesPropiedad->nombre_operacion}} 
                                               </td>
                                            </tr>
                                            @endforeach
                                        </table>
                            
                                        </div>
                                    </div>
                                    <?php }?>

                                    <span class="space-md-md"></span>
                                    
                                    <!-- Video -->
                                    <?php if($consultaPropiedadPrincipal[0]->video){ ?>
                                    <div class="card z-depth-2-top">
                                        <div class="card-title b-xs-bottom">
                                            <h3 class="heading heading-sm text-uppercase">Video</h3>
                                        </div>
                                        
                                        <div class="card-body">
                                            <!-- 16:9 aspect ratio -->
                                           <div class="embed-responsive embed-responsive-16by9">
                                               <iframe width="560" height="315" src="{{$consultaPropiedadPrincipal[0]->video}}" frameborder="0" allowfullscreen></iframe>
                                           </div>
                                        </div>
                                    </div>
                                    <?php }?>
                                </div>

                                <div class="col-md-3">
                                    <div class="sidebar">
                <br><br><br><br>
                <img src="{{asset($consultaPropiedadPrincipal[0]->foto)}}" style="width: 100%;height: 250px;margin-left: auto;margin-right: auto;">
                <div class="text-xs-center">
                </div>

    <div class="sidebar-object">
        <div class="card z-depth-2-top">
            <div class="card-title b-xs-bottom">
                <h3 class="heading heading-sm text-uppercase">Agente inmobiliario</h3>
            </div>
            <div class="list-group-item">
            <i class="fa fa-user"></i>
                <span class="strong-500">NOMBRE</span>
                <span class="pull-right">{{$consultaPropiedadPrincipal[0]->nombre_usuario}} {{$consultaPropiedadPrincipal[0]->apellido_usuario}}</span>
            </div>
            <div class="list-group-item">
                <i class="fa fa-phone"></i>
                <span class="strong-500">OFICINA</span>
                <span class="pull-right">{{$consultaPropiedadPrincipal[0]->telefono_local}}</span>
            </div>


            <div class="list-group-item">
                <i class="fa fa-phone"></i>
                <span class="strong-500">CELULAR</span>
                <span class="pull-right">{{$consultaPropiedadPrincipal[0]->telefono_celular}}</span>
            </div>


            <div class="card-title b-xs-bottom">
                <h3 class="heading heading-sm text-uppercase">Contáctanos</h3>
            </div>

        <div class="col-sm-12">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                <input type="text" class="form-control" id="nombreContacto" placeholder="Nombre y Apellido"> </input>
            </div>
            <label class="col-md-12 nombreContactoMensaje hidden text-danger"> Debe ingresar el nombre </label>
            <br>

            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                <input type="text" class="form-control" id="emailContacto" placeholder="Email"> </input>
            </div>
            <label class="col-md-12 nombreContactoEmail hidden text-danger"> Debe ingresar el email </label>
            <br>

            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                <input type="text" class="form-control" id="telefonoContacto" placeholder="Telefono"> </input>
            </div>
            <label class="col-md-12 telefonoContactoMensaje hidden text-danger"> Debe ingresar el teléfono </label>
            <br>
            <div>
            <textarea class="form-control" cols="20" rows="10" id="mensajeContacto" placeholder="Mensaje"></textarea>
            </div>
            <label class="col-md-12 telefonoContactoMensajeText hidden text-danger"> Debe ingresar el mensaje </label>
            <br>
        </div>  

            <div class="card-body">
                <a href="#" class="btn btn-block btn-base-2 enviar_mensaje">
                    Enviar mensaje
                </a>
                <img class="hidden" id="loading" src="{{asset('tribu/images/loading.gif')}}">
            </div>

            <div>
                <div id="mensajeDiv"> 
                <p id="contenidoMensaje" class="bg-base-1"> </p>
                </div>
            </div>
        </div>
    </div>

    <div class="sidebar-object b-xs-bottom pb-2 mb-2">
        <div class="card z-depth-2-top">
            <div class="card-title b-xs-bottom">
                <h3 class="heading heading-sm text-uppercase">Búsqueda Avanzada</h3>
            </div>
            <div class="card-body">
                  <form class="form-default mt-2" action="{{asset('invermaster/proyecto')}}" method="post" data-toggle="validator" role="form">
                <div class="row">
                <div class="col-sm-12">
                    <div class="col-sm-12">
                        <div class="form-group form-group-lg has-feedback">
                            <label for="" class="text-uppercase">Ciudad</label>
                            <select class="form-control select2" style="width: 100%;" name="sector_ciudad_nombre">
                              <option value="">Selecione</option>
                                @foreach($ciudad as $ciudad)
                                    <option value="{{$ciudad->id}}">{{$ciudad->nombre}}</option>
                                @endforeach
                            </select>
                            <span class="help-block with-errors"></span>
                        </div>
                    </div>
                </div>
                    <div class="col-sm-12">
                    <div class="col-sm-6">
                        <div class="form-group form-group-lg has-feedback">
                            <label for="" class="text-uppercase">Tipo</label>
                            <select class="form-control selectpicker" name="tipo" id="tipo">
                                <option value="">Selecione</option>
                            @foreach($tipo as $tipo)
                                <option value="{{$tipo->id}}">{{$tipo->nombre}}</option>
                            @endforeach
                            </select>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                          <input class="form-control hidden" type="text" id="precio_desde" placeholder="City, Country" name="precio_desde">

                         <input class="form-control hidden" type="text" id="precio_hasta" placeholder="Nombre" name="precio_hasta">

                    <div class="col-sm-6">
                        <div class="form-group form-group-lg has-feedback">
                            <label for="" class="text-uppercase">Operaciones</label>
                            <select class="form-control selectpicker" name="operaciones" id="operaciones">
                                <option value="">Selecione</option>
                                @foreach($operaciones as $operaciones)
                                    <option value="{{$operaciones->id}}">{{$operaciones->nombre}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="help-block with-errors"></div>
                    </div>
                    </div>

                <div class="col-sm-12">
                    <div class="col-sm-6">
                        <div class="form-group form-group-lg has-feedback">
                            <label for="" class="text-uppercase">dormitorios</label>
                            <select class="form-control selectpicker" name="dormitorios" id="banios">
                                <option value="">Selecione</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                            </select>
                        </div>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group form-group-lg has-feedback">
                            <label for="" class="text-uppercase">Baños</label>
                            <select class="form-control selectpicker" name="banios" id="banios">
                                <option value="">Selecione</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                            </select>
                        </div>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="col-sm-6">
                    <div class="form-group form-group-lg has-feedback">
                        <label for="" class="text-uppercase">Parqueos</label>
                            <select class="form-control selectpicker" name="estacionamiento" id="banios">
                                <option value="">Selecione</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                            </select>
                        </div>
                        </div>

                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <div class="col-sm-12">
                            <div class="form-group has-feedback">
                                <label class="text-uppercase" for="">Rango de Precios</label>
                                <div class="range-slider-wrapper mt-1">
                                    <!-- Range slider container -->
                                    <div id="filterPrinceRange"></div>

                                    <!-- Range slider values -->
                                    <div class="row mt-1">
                                        <div class="col-xs-6">
                                            <small>USD </small>
                                            <span class="range-slider-value value-low" id="filterPrinceValueLow"></span>
                                        </div>

                                        <div class="col-xs-6 text-right">
                                            <small>USD </small>
                                            <span class="range-slider-value value-hight" id="filterPrinceValueHigh"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
                </div>
                    <input type="text" class="hidden" id="_token" name="_token" value="{{ csrf_token() }}"></input>
                <button class="btn btn-block btn-lg btn-base-1 mt-2" id="boton">Buscar</button>
            </form>
        </div>
    </div>
</div>

      <div class="sidebar-object">
        <div class="section-title section-title--style-1 mt-3">
            <h4 class="heading heading-xs strong-300 c-gray-light">Propiedades</h4>
            <h3 class="section-title-inner heading-4 strong-400 mt-0 text-uppercase">
                Vistas Recientes
            </h3>
        </div>
        <?php $clase="";$claseNueva="";?>
         @foreach($consultaPropiedadAllDestacadaSector as $consultaPropiedadAllDestacadaSector)

            <?php
                $fecha_i=date('Y-m-d');
                $fecha_publicacion=date('Y-m-d',strtotime($consultaPropiedadAllDestacadaSector->fecha_publicacion));
                $cantidad=$parametro_seguridad[0]->cantidad;
                $fecha_f = strtotime ('+'.$parametro_seguridad[0]->cantidad.' days', strtotime($fecha_publicacion));
                $diasO = ($fecha_f-strtotime($fecha_i))/86400;
                $barInt = (int) $diasO;
                $nueva="";
                if($barInt>=0){
                    $claseNueva='block-ribbon block-ribbon-right bg-purple';
                    $nueva="Nueva";
                }
            ?>


        <div class="block block--style-3 z-depth-2-top z-depth-3--hover <?php echo $clase;?>">
            <div class="block-image relative">
                <a href="">
                    <img src="{{asset($consultaPropiedadAllDestacadaSector->nombre_galeria)}}" class="img-responsive" style="width:100%; height:100%">
                </a>
                <span class="{{$claseNueva}}">{{$nueva}}</span>
            </div>
            <br>
            <div class="block-body">
                <h3 class="heading heading-sm text-uppercase"> <strong>
                {{$consultaPropiedadAllDestacadaSector->nombre_proyecto}} </strong> </h3>
                <p class="description" style="text-align: justify;">
                    {{$consultaPropiedadAllDestacadaSector->descripcion_breve}}
                </p>
                <div class="mt-1"> <span class="block-price heading-6"> 
                    <?php $precio=number_format((float)$consultaPropiedadAllDestacadaSector->precio, 2, ',', '.');
                    echo  'USD '.$precio;?>
                 </span>
            </div>
            </div>


        </div>
        <?php $clase="mt-3";?>

@endforeach

        </div>
        </section>

        @include('plantillaCliente.plantilla_principal_footer')

                </div>
            </div>
        </div><!-- END: st-pusher -->
    </div><!-- END: st-container -->
</div><!-- END: body-wrap -->
<input type="text" class="hidden" name="_token" id="token" value="{{ csrf_token() }}"></input>

<!-- SCRIPTS -->

<?php 
$precio_minimo=number_format((float)$consultaMinimoPrecio[0]->precio_minimo, 2, ',', '.');
$precio_minimo_co=(float)$precio_minimo;


$precio_maximo=number_format((float)$consultaMaximoPrecio[0]->precio_maximo, 2, ',', '.');
$precio_minimo_co_max=(float)$precio_maximo;

?>

<input type="text" name="" class="hidden" id="minimo_precio" value="<?php echo $precio_minimo;?>">

<input type="text" name="" class="hidden" id="maximo_precio" value="{{$precio_maximo}}">



<!-- Required JSs -->

<script src="{{asset('plugins/select2/select2.full.min.js')}}"></script>

<script type="text/javascript">
$(".select2").select2({
    theme: "bootstrap",

});
</script>

<!-- Essential JSs -->
<script src="{{asset('tribu/assets/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('tribu/js/bootstrap-extensions/bootstrap-dropdowneffect.js')}}"></script>
<script src="{{asset('tribu/js/bootstrap-extensions/bootstrap-validator.js')}}"></script>
<script src="{{asset('tribu/js/modernizr.custom.js')}}"></script>
<script src="{{asset('tribu/js/jquery.mousewheel-3.0.6.pack.js')}}"></script>
<script src="{{asset('tribu/js/jquery.easing.js')}}"></script>
<script src="{{asset('tribu/js/classie.js')}}"></script>
<script src="{{asset('tribu/js/slidebar/slidebar.js')}}"></script>

<!-- Assets - Required -->
<script src="{{asset('tribu/assets/headroom/headroom.min.js')}}"></script>
<script src="{{asset('tribu/assets/headroom/jquery.headroom.min.js')}}"></script>
<script src="{{asset('tribu/assets/footer-reveal/footer-reveal.min.js')}}"></script>
<script src="{{asset('tribu/assets/bootstrap-select/js/bootstrap-select.min.js')}}"></script>
<script src="{{asset('tribu/assets/swiper/js/swiper.min.js')}}"></script>
<script src="{{asset('tribu/assets/parallax/parallax.min.js')}}"></script>
<script src="{{asset('tribu/assets/viewport-checker/viewportchecker.min.js')}}"></script>
<script src="{{asset('tribu/assets/milestone-counter/jquery.countTo.js')}}"></script>

<!-- Light Gallery -->
<script src="{{asset('tribu/assets/lightgallery/js/lightgallery.min.js')}}"></script>
<script src="{{asset('tribu/assets/lightgallery/js/lg-thumbnail.min.js')}}"></script>
<script src="{{asset('tribu/assets/lightgallery/js/lg-video.js')}}"></script>

<!-- Cross-browser responsivity scripts -->
<!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
<![endif]-->

<!-- Google Analitycs JS -->
<script type="text/javascript" src="{{asset('tribu/js/wpx.ga.js')}}"></script>


<!-- RS5.0 Core JS Files -->
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/jquery.themepunch.tools.min.js?rev=5.0')}}"></script>
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/jquery.themepunch.revolution.min.js?rev=5')}}"></script>
<script type="text/javascript" src="{{asset('tribu/js/revolution/revolution-slider-real-estate-1.js')}}"></script>
<!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/extensions/revolution.extension.actions.min.js')}}"></script>
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/extensions/revolution.extension.carousel.min.js')}}"></script>
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/extensions/revolution.extension.kenburn.min.js')}}"></script>
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/extensions/revolution.extension.layeranimation.min.js')}}"></script>
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/extensions/revolution.extension.migration.min.js')}}"></script>
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/extensions/revolution.extension.navigation.min.js')}}"></script>
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/extensions/revolution.extension.parallax.min.js')}}"></script>
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/extensions/revolution.extension.slideanims.min.js')}}"></script>
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/extensions/revolution.extension.video.min.js')}}"></script>

<!-- Cube Portfolio -->
<script src="{{asset('tribu/assets/cubeportfolio/js/jquery.cubeportfolio.min.js')}}"></script>
<script src="{{asset('tribu/assets/cubeportfolio/js/main.4-col.space.grid.js')}}"></script>

<!-- No UI slider -->
<script type="text/javascript" src="{{asset('tribu/assets/nouislider/js/nouislider.min.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function() {
                var resultado=[];
        var resultado2="";
        var resultado=0;
        var snapSlider = document.getElementById('filterPrinceRange');
        var precio_minimo=parseFloat($('#minimo_precio').val().replace(/\./g,"").replace(',','.'));
        var precio_maximo=parseFloat($('#maximo_precio').val().replace(/\./g,"").replace(',','.'));
        noUiSlider.create(snapSlider, {
            start: [ precio_minimo, precio_maximo ],

            connect: true,
            range: {
                'min': precio_minimo,
                'max': precio_maximo
            }
        });

        var snapValues = [
            document.getElementById('filterPrinceValueLow'),
            document.getElementById('filterPrinceValueHigh')
        ];

        snapSlider.noUiSlider.on('update', function( values, handle ) {
           values[handle]=values[handle].replace(',','');
            resultado=parseFloat(values[handle]).toLocaleString('de-DE');
            snapValues[handle].innerHTML = resultado;
        });
    });
</script>

<!-- App JS -->
<script src="{{asset('tribu/js/wpx.app.js')}}"></script>

<script type="text/javascript">

    function meGusta(id){
    var _token = $("#token").val();
    $(".meGusta"+id).css({
        'color':'blue'});
    //console.log(_token);
    $.ajax({
    type: "POST",
    url: "{{asset('propiedad/meGusta')}}",
    data: {
    id: id,_token:_token},
    dataType: "json",
    success: function (resultado) {
      console.log(resultado);


    }
    });
  }
    
$('#boton').on('click',function(){
    var precio_desde=$('#filterPrinceValueLow').text();
    var precio_hasta=$('#filterPrinceValueHigh').text();
    $('#precio_desde').val(precio_desde);
    $('#precio_hasta').val(precio_hasta);
   /*var precio_desde=$('#filterPrinceValueLow').text();
   var precio_hasta=$('#filterPrinceValueHigh').text();
   var nombre=$('#nombre').val();
   var tipo=$('#tipo').val();
   var operacion=$('#operaciones').val();
   var sector=$('#sector').val();
   var _token=$('#_token').val();
      $.ajax({                        
           type: "POST",                 
           url: '/invermaster/proyecto',                     
           data: {'precio_desde':precio_desde,'precio_hasta':precio_hasta,'nombre':nombre,'tipo':tipo,'operacion':operacion,'sector':sector,'_token':_token},
       success: function (result) {
        window.location.href = '/invermaster/proyecto';
           //do somthing here
      }
       });*/





})
</script>

<!--<script>
      function myMap() {
      var latitud=$('#latitud').val();
      var longitud=$('#longitud').val();
      var myLatLng = {lat:parseFloat(latitud) , lng:parseFloat(longitud)};

  var map = new google.maps.Map(document.getElementById('googleMap'), {
    zoom: 15,
    center: myLatLng,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  });

  var marker = new google.maps.Marker({
    position: myLatLng,
    map: map,
    draggable:true,
    title: 'Hello World!'
  });
}
</script>-->

<!--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBpd76B1_f4lp75gkMLbthkxeZjfW2FUVU&callback=myMap"></script>-->

<script type="text/javascript">

    function validarFormuSolicitud(nombre,email,telefono,mensaje){
        var comprobar=0;
        if(nombre==''){
            comprobar=1;
            $(".nombreContactoMensaje").removeClass('hidden');
        }else{
            $(".nombreContactoMensaje").addClass('hidden');
        }
        if(email==''){
            comprobar=1;
            $(".nombreContactoEmail").removeClass('hidden');
        }else{
            $(".nombreContactoEmail").addClass('hidden');
        }
        
        if(telefono==''){
            comprobar=1;
            $(".telefonoContactoMensaje").removeClass('hidden');
        }else{
            $(".telefonoContactoMensaje").addClass('hidden');
        }
        if(mensaje==''){
            comprobar=1;
            $(".telefonoContactoMensajeText").removeClass('hidden');
        }else{
            $(".telefonoContactoMensajeText").addClass('hidden');
        }
        return comprobar;
    }
    $(".enviar_mensaje").on("click",function(){
        var tipo = "propiedad";
        var _token = $("#token").val();
        var propiedad_id = $("#propiedad_id").val();
        var nombre = $("#nombreContacto").val();
        var email = $("#emailContacto").val();
        var telefono = $("#telefonoContacto").val();
        var mensaje = $("#mensajeContacto").val();
        var comprobarValor=validarFormuSolicitud(nombre,email,telefono,mensaje);
        if(comprobarValor==0){
            $('#loading').removeClass('hidden');
            $('.enviar_mensaje').addClass('hidden');
            $.ajax({
                type: "POST",
                url: "{{asset('invermaster/contacto')}}",
                data: { propiedad_id: propiedad_id,_token:_token,tipo:tipo,nombre:nombre,email:email,telefono:telefono,mensaje:mensaje},
                dataType: "json",
                success: function (resultado) {
                    if(resultado['statusCode']=='exito'){
                        $('#loading').addClass('hidden');
                        $('html, body').animate({scrollTop: 0},500);
                        $("#nombreContacto").val('');
                        $("#emailContacto").val('');
                        $("#telefonoContacto").val('');
                        $("#mensajeContacto").val('');
                        $( ".enviarMensaje" ).removeClass('hidden');
                        $( ".enviar_mensaje" ).removeClass('hidden');
                        //$('#contenidoMensaje').html(resultado['menaje']);
                        setTimeout(function() {
                            $( ".enviarMensaje" ).addClass('hidden');
                        }, 10000);
                        
                    }
                }
            });
        }
    });
    </script>

</body>
</html>