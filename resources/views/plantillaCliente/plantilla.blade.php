
<!DOCTYPE html>
<html>
<head>


<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="robots" content="index, follow">
<title>Invermaster</title>


<!-- Page loader -->
<link rel="stylesheet" href="{{asset('tribu/assets/pace/css/pace-minimal.css')}}" type="text/css">

<!-- Bootstrap -->
<link rel="stylesheet" href="{{asset('tribu/assets/bootstrap/css/bootstrap.min.css')}}" type="text/css">

<!-- CSS animation library -->
<link rel="stylesheet" href="{{asset('tribu/assets/animate/animate.min.css')}}" type="text/css">

<link href="https://fonts.googleapis.com/css?family=Quicksand:400,500,700" rel="stylesheet">

<!-- Plugins -->
<link rel="stylesheet" href="{{asset('tribu/assets/bootstrap-select/css/bootstrap-select.min.css')}}">
<link rel="stylesheet" href="{{asset('tribu/assets/swiper/css/swiper.min.css')}}">

<!-- Light Gallery -->
<link rel="stylesheet" href="{{asset('tribu/assets/lightgallery/css/lightgallery.min.css')}}">

<link rel="stylesheet" href="{{asset('plugins/select2/select2.min.css')}}">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.css">

<!-- Icons -->
<link rel="stylesheet" href="{{asset('tribu/fonts/font-awesome/css/font-awesome.min.css')}}" type="text/css">
<link rel="stylesheet" href="{{asset('tribu/fonts/ionicons/css/ionicons.min.css')}}" type="text/css">
<link rel="stylesheet" href="{{asset('tribu/fonts/line-icons/line-icons.css')}}" type="text/css">
<link rel="stylesheet" href="{{asset('tribu/fonts/line-icons-pro/line-icons-pro.css')}}" type="text/css">

<!-- Global style (main) -->
<link id="stylesheet" type="text/css" href="{{asset('tribu/css/global-style.min.css')}}" rel="stylesheet" media="screen">


<!-- Favicon -->
<link href="{{asset('tribu/images/logo/logo.png')}}" rel="icon" type="image/png">


<!-- RS5.0 -->
<link rel="stylesheet" type="text/css" href="{{asset('tribu/assets/revolution/css/settings.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('tribu/assets/revolution/css/layers.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('tribu/assets/revolution/css/navigation.css')}}">

<!-- Cube Portfolio -->
<link rel="stylesheet" href="{{asset('tribu/assets/cubeportfolio/css/cubeportfolio.min.css')}}">
<!-- Custom style - Remove if not necessary -->
<link type="text/css" href="{{asset('tribu/css/custom-style.css')}}" rel="stylesheet">
</head>
<body>

@include('plantillaCliente.plantilla_principal')

<!--<input type="text" id="input" placeholder="Message…" />
<hr />
<pre id="output"></pre>-->


                    <!-- REVOLUTION SLIDER -->
<div class="relative">
    <div id="rev_slider_1_wrapper" class="rev_slider_wrapper fullscreen-container" data-alias="revolution-slider-real-estate-2" style="background-color:transparent;padding:0px;">
    <!-- START REVOLUTION SLIDER 5.2.5.1 fullscreen mode -->
        <div id="rev_slider_1" class="rev_slider fullscreenbanner" style="display:none;" data-version="5.2.5.1">
            <ul>


            @foreach($galeriaEmpresa as $galeriaEmpresa)
                <!-- SLIDE  -->
                <li data-index="rs-2" data-transition="random" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="default"  data-rotate="0"  data-saveperformance="off"  data-title="" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                    <!-- MAIN IMAGE -->
                    <img src="{{asset($galeriaEmpresa->nombre)}}"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
                </li>
            @endforeach
            </ul>
            <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
        </div>
    </div><!-- END REVOLUTION SLIDER -->

    <!-- Search form -->
    <div class="overlayed-form-wrapper">
        <div class="overlayed-form overlayed-form--style-1">
            <h3 class="heading heading-5 strong-600 text-capitalize">Buscar Propiedades</h3>

            <form class="form-default mt-2" action="{{asset('invermaster/proyecto')}}" method="post" data-toggle="validator" role="form">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group form-group-lg has-feedback">
                            <label for="" class="text-uppercase">Sector/Ciudad</label>
                            <select class="form-control select2 ddl-custom" name="sector_ciudad_nombre">
                              <option value="">Seleccione</option>
                                @foreach($ciudad as $ciudad)
                                    <option value="{{$ciudad->id}}">{{$ciudad->nombre}}</option>
                                @endforeach
                            </select>
                            <span class="help-block with-errors"></span>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group form-group-lg has-feedback">
                            <label for="" class="text-uppercase">Tipo</label>
                            <select class="form-control selectpicker" name="tipo" id="tipo">
                                <option value="">Seleccione</option>
                            @foreach($tipo as $tipo)
                                <option value="{{$tipo->id}}">{{$tipo->nombre}}</option>
                            @endforeach
                            </select>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
   
                    </div>
                       <input class="form-control hidden" type="text" id="precio_desde" placeholder="City, Country" name="precio_desde">

                         <input class="form-control hidden" type="text" id="precio_hasta" placeholder="Nombre" name="precio_hasta">



                <div class="row">

                    <div class="col-sm-6">
                        <div class="form-group form-group-lg has-feedback">
                            <label for="" class="text-uppercase">dormitorios</label>
                            <select class="form-control selectpicker" name="dormitorios" id="banios">
                                <option value="">Seleccione</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                            </select>
                        </div>
                        <div class="help-block with-errors"></div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group form-group-lg has-feedback">
                            <label for="" class="text-uppercase">Baños</label>
                            <select class="form-control selectpicker" name="banios" id="banios">
                                <option value="">Seleccione</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                            </select>
                        </div>
                        <div class="help-block with-errors"></div>
                    </div>

                    <div class="col-sm-6">

                    <div class="form-group form-group-lg has-feedback">
                        <label for="" class="text-uppercase">Parqueos</label>
                            <select class="form-control selectpicker" name="estacionamiento" id="banios">
                                <option value="">Seleccione</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                            </select>
                        </div>
                        </div>

                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group has-feedback">
                            <label class="text-uppercase" for="">Rango de Precios</label>
                            <div class="range-slider-wrapper mt-1">
                                <!-- Range slider container -->
                                <div id="filterPrinceRange"></div>

                                <!-- Range slider values -->
                                <div class="row mt-1">
                                    <div class="col-xs-6">
                                        <small>USD </small>
                                        <span class="range-slider-value value-low" id="filterPrinceValueLow"></span>
                                    </div>

                                    <div class="col-xs-6 text-right">
                                        <small>USD </small>
                                        <span class="range-slider-value value-hight" id="filterPrinceValueHigh"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                    <input type="text" class="hidden" id="_token" name="_token" value="{{ csrf_token() }}"></input>
                <button class="btn btn-block btn-lg btn-base-1 mt-2" id="boton">Buscar</button>
            </form>
        </div>
    </div>
</div>

                    <!-- Properties listing -->
                    <section class="slice sct-color-2">
                        <div class="container">
                            <div class="section-title section-title--style-1 text-xs-center">
                            <?php
                            if(count($consultaPropiedades)>0 and count($consultaProyecto)>0){?>
                            <h4 class="heading heading-sm strong-400 c-gray-light"> PROYECTOS Y PROPIEDADES </h4>
                                <h3 class="section-title-inner heading-4 strong-600 mt-0 text-capitalize">
                                    LOS MÁS DESTACADOS
                                </h3>

                            <?php }else if(count($consultaPropiedades)>0){?>
                                <h3 class="section-title-inner heading-4 strong-600 mt-0 text-capitalize">
                                    LOS MÁS DESTACADOS
                                </h3>

                            <?php }else if(count($consultaProyecto)>0){?>
                                <h3 class="section-title-inner heading-4 strong-600 mt-0 text-capitalize">
                                    LOS MÁS DESTACADOS
                                </h3>
                            <?php }else{?>
                                <h3 class="section-title-inner heading-4 strong-600 mt-0">
                                    No se encontraron registros
                                </h3>
                            <?php }?>
           
                            </div>
                        <div class="row">




                        @foreach($consultaProyecto as $consultaProyecto)
                             <div class="col-md-3 col-sm-6" >
                                        <div class="block block--style-3 z-depth-1-bottom z-depth-2-bottom--hover">
                                            <div class="block-image relative" style="height: 200px;">
                                                <a href="{{asset('invermaster/proyecto/detalle')}}/{{$consultaProyecto->id}}"><img src="{{asset($consultaProyecto->nombre_galeria)}}" style="height: 100%; width: 100%;" class="img-responsive"></a>
                                                <span class="block-price-over bg-base-4" style="
                                                    margin-left: -100px;
                                                    width: 200px;">
                                                    <?php $precio=number_format((float)$consultaProyecto->precio_desde, 2, ',', '.');
                                                     echo 'USD '. $precio;?>
                                                </span>
                                            </div>

                                            <div class="block-body text-xs-center mt-1" style="height: 200px;">
                                                <h3 class="heading heading-6 strong-400 mb-1 text-uppercase">
                                                <strong>
                                                    {{$consultaProyecto->nombre_proyecto}}
                                                </strong>
                                                </h3>
                                                <h3 class="heading heading-6 strong-400 text-normal mb-1">
                                                    <br>
                                                    {{$consultaProyecto->descripcion_breve}}
                                                </h3>
                    
                                                <span class="clearfix"></span>
                                            </div>

           
                                        </div>
                                    </div>

                                    @endforeach
                                    <div class="row"> </div> <br>



                                    @foreach($consultaPropiedades as $consultaPropiedades)
                                        <?php
                                            $fecha_i=date('Y-m-d');
                                            $fecha_publicacion=date('Y-m-d',strtotime($consultaPropiedades->fecha_publicacion));
                                            $cantidad=$parametro_seguridad[0]->cantidad;
                                            $fecha_f = strtotime ('+'.$parametro_seguridad[0]->cantidad.' days', strtotime($fecha_publicacion));
                                            $diasO = ($fecha_f-strtotime($fecha_i))/86400;
                                            $barInt = (int) $diasO;
                                            $clase="";
                                            $nueva="";
                                            if($barInt>=0){
                                                $clase='block-ribbon block-ribbon-right bg-purple';
                                                $nueva="Nueva";
                                            }
                                        ?>

                                        <div class="col-md-3 col-sm-6">
                                            <div class="block block--style-3 z-depth-1-bottom z-depth-2-bottom--hover">
                                                <div class="block-image relative" style="height: 200px;">
                                                    <a href="{{asset('invermaster/propiedad/detalle')}}/{{$consultaPropiedades->id}}"><img src="{{asset($consultaPropiedades->nombre_galeria)}}" style="height: 100%; width: 100%;" class="img-responsive"></a>
                                                    <span class="{{$clase}}">{{$nueva}}</span>
              
                                                    <span class="block-price-over bg-base-4" style="
                                                    margin-left: -100px;
                                                    width: 200px;">

                                                    <?php
                                                        $precio=number_format((float)$consultaPropiedades->precio_desde, 2, ',', '.');
                                                     echo 'USD '. $precio;?></span>
                                                </div>

                                            <div class="block-body text-xs-center mt-1" style="height: 200px;">
                                                <h3 class="heading heading-6 strong-400 mb-1 text-uppercase">
                                                <strong>
                                                    <?php echo $consultaPropiedades->nombre_propiedad;?>
                                                    </strong>
                                                    <br>
                                                </h3>
                                                 <h3 class="heading heading-6 strong-400 mb-1">
                                                    
                                                    <?php echo $consultaPropiedades->descripcion_breve;?>
                                                </h3>
 
                                            </div>
                                            <center>
                                            <div style="margin: 10px;">
                                            <?php 
                                             foreach ($operacionesPropiedad as $operacionesProp) {
                                                if($operacionesProp->propiedad_id==$consultaPropiedades->id){
                                                ?>
                                                 <div class="badge" style="font-size: 12px;"> 
                                                 <strong>{{$operacionesProp->nombre_operacion}} </strong></div>
                                                    <?php
                                                }
                                            }?>
                                            </div>
                                            </center>
                                                <span class="clearfix"></span>
                                                <ul class="aux-info aux-info--style-2 b-xs-top">
                                                    <li class="heading strong-400 text-uppercase text-xs-center">
                                                        <i class="fa fa-home mr-0"></i>
                                                        <span class="clearfix"></span>
                                                        {{$consultaPropiedades->metros_cuadrado}}
                                                        <sup>2</sup> <span class="">mtc</span>
                                                    </li>
                                                    <li class="heading strong-400 text-uppercase text-xs-center">
                                                        <i class="fa fa-bed"></i>
                                                        <span class="clearfix"></span>
                                                        {{$consultaPropiedades->dormitorio}}
                                                    </li>
                                                    <li class="heading strong-400 text-uppercase text-xs-center">

                                                        <i class="fa fa-fw fa-car"></i>
                                                        <span class="clearfix"></span>
                                                          {{$consultaPropiedades->estacionamiento}}
                                                        </li>

                                                    <li class="heading strong-400 text-uppercase text-xs-center">
                                                        <i class="fa fa-tint"></i>
                                                        <span class="clearfix"></span>
                                                        {{$consultaPropiedades->banios}}
                                                    </li>
                                                    </ul>
                                            </div>
                                        </div>
                                        
                                @endforeach
                                </div>
                        </div>
                    </section>

                    <!-- PORTFOLIO -->

                    <!-- FOOTER -->
          
@include('plantillaCliente.plantilla_principal_footer')

                </div>
            </div>
        </div><!-- END: st-pusher -->
    </div><!-- END: st-container -->
</div><!-- END: body-wrap -->

<!-- SCRIPTS -->

<!--input de maximo de precio hide -->
<input type="text" name="" class="hidden" id="maximo_precio" value="{{$consultaMaximoPrecio[0]->precio_maximo}}">
<input type="text" name="" class="hidden" id="minimo_precio" value="{{$consultaMinimoPrecio[0]->precio_minimo}}">




<!--Finnnnnnnnnnnnnnnnnnnnnnnnnn -->
<!-- Required JSs -->

<!-- Essential JSs -->
<script src="{{asset('tribu/assets/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('tribu/js/bootstrap-extensions/bootstrap-dropdowneffect.js')}}"></script>
<script src="{{asset('tribu/js/bootstrap-extensions/bootstrap-validator.js')}}"></script>
<script src="{{asset('tribu/js/modernizr.custom.js')}}"></script>
<script src="{{asset('tribu/js/jquery.mousewheel-3.0.6.pack.js')}}"></script>
<script src="{{asset('tribu/js/jquery.easing.js')}}"></script>
<script src="{{asset('tribu/js/classie.js')}}"></script>
<script src="{{asset('tribu/js/slidebar/slidebar.js')}}"></script>

<!-- Assets - Required -->
<script src="{{asset('tribu/assets/headroom/headroom.min.js')}}"></script>
<script src="{{asset('tribu/assets/headroom/jquery.headroom.min.js')}}"></script>
<script src="{{asset('tribu/assets/footer-reveal/footer-reveal.min.js')}}"></script>
<script src="{{asset('tribu/assets/bootstrap-select/js/bootstrap-select.min.js')}}"></script>
<script src="{{asset('tribu/assets/swiper/js/swiper.min.js')}}"></script>
<script src="{{asset('tribu/assets/parallax/parallax.min.js')}}"></script>
<script src="{{asset('tribu/assets/viewport-checker/viewportchecker.min.js')}}"></script>
<script src="{{asset('tribu/assets/milestone-counter/jquery.countTo.js')}}"></script>

<!-- Light Gallery -->
<script src="{{asset('tribu/assets/lightgallery/js/lightgallery.min.js')}}"></script>
<script src="{{asset('tribu/assets/lightgallery/js/lg-thumbnail.min.js')}}"></script>
<script src="{{asset('tribu/assets/lightgallery/js/lg-video.js')}}"></script>

<!-- Cross-browser responsivity scripts -->
<!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
<![endif]-->

<!-- Google Analitycs JS -->
<script type="text/javascript" src="{{asset('tribu/js/wpx.ga.js')}}"></script>


<!-- RS5.0 Core JS Files -->
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/jquery.themepunch.tools.min.js?rev=5.0')}}"></script>
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/jquery.themepunch.revolution.min.js?rev=5')}}"></script>
<script type="text/javascript" src="{{asset('tribu/js/revolution/revolution-slider-real-estate-1.js')}}"></script>
<!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/extensions/revolution.extension.actions.min.js')}}"></script>
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/extensions/revolution.extension.carousel.min.js')}}"></script>
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/extensions/revolution.extension.kenburn.min.js')}}"></script>
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/extensions/revolution.extension.layeranimation.min.js')}}"></script>
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/extensions/revolution.extension.migration.min.js')}}"></script>
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/extensions/revolution.extension.navigation.min.js')}}"></script>
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/extensions/revolution.extension.parallax.min.js')}}"></script>
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/extensions/revolution.extension.slideanims.min.js')}}"></script>
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/extensions/revolution.extension.video.min.js')}}"></script>

<!-- Cube Portfolio -->
<script src="{{asset('tribu/assets/cubeportfolio/js/jquery.cubeportfolio.min.js')}}"></script>
<script src="{{asset('tribu/assets/cubeportfolio/js/main.4-col.space.grid.js')}}"></script>

<!-- No UI slider -->
<script type="text/javascript" src="{{asset('tribu/assets/nouislider/js/nouislider.min.js')}}"></script>
<script src="{{asset('plugins/select2/select2.full.min.js')}}"></script>

<script type="text/javascript">
$(".select2").select2({
    theme: "bootstrap"
});
    $(document).ready(function() {
        var snapSlider = document.getElementById('filterPrinceRange');
        var precio=parseFloat($('#maximo_precio').val());
        var precio_minimo=parseFloat($('#minimo_precio').val());
        var resultado=[];
        var resultado2="";
        noUiSlider.create(snapSlider, {
            start: [ precio_minimo, precio ],

            connect: true,
            range: {
                'min': precio_minimo,
                'max': precio
            }
        });

        var snapValues = [
            document.getElementById('filterPrinceValueLow'),
            document.getElementById('filterPrinceValueHigh')
        ];

        snapSlider.noUiSlider.on('update', function( values, handle ) {
            values[handle]=values[handle].replace(',','');
            resultado=parseFloat(values[handle]).toLocaleString('de-DE');
            snapValues[handle].innerHTML = resultado;
        });
    });
</script>

<!-- App JS -->
<script src="{{asset('tribu/js/wpx.app.js')}}"></script>

<script type="text/javascript">
    
$('#boton').on('click',function(){
    var precio_desde=$('#filterPrinceValueLow').text();
    var precio_hasta=$('#filterPrinceValueHigh').text();
    $('#precio_desde').val(precio_desde);
    $('#precio_hasta').val(precio_hasta);
   /*var precio_desde=$('#filterPrinceValueLow').text();
   var precio_hasta=$('#filterPrinceValueHigh').text();
   var nombre=$('#nombre').val();
   var tipo=$('#tipo').val();
   var operacion=$('#operaciones').val();
   var sector=$('#sector').val();
   var _token=$('#_token').val();
      $.ajax({                        
           type: "POST",                 
           url: '/invermaster/proyecto',                     
           data: {'precio_desde':precio_desde,'precio_hasta':precio_hasta,'nombre':nombre,'tipo':tipo,'operacion':operacion,'sector':sector,'_token':_token},
       success: function (result) {
        window.location.href = '/invermaster/proyecto';
           //do somthing here
      }
       });*/





})
</script>

<script src="{{asset('js/smart-chat-ui/smart.chat.ui.min.js')}}"></script>
<script src="{{asset('js/smart-chat-ui/smart.chat.manager.min.js')}}"></script>

<script src="{{asset('js/smart-chat-ui/app.config.js')}}"></script>
<script src="{{asset('js/smart-chat-ui/app.min.js')}}"></script>




</body>
</html>
