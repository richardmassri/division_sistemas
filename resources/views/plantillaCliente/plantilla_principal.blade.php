<!-- MAIN WRAPPER -->
<link rel="stylesheet" type="text/css" media="screen" href="{{ asset('css/smartadmin-production-plugins.min.css')}}">

<div id="google_translate_element" style="margin-top: 20px;"></div><script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'es', includedLanguages: 'de,en,fr,it,ru', layout: google.translate.TranslateElement.InlineLayout.HORIZONTAL}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>

<div class="body-wrap">
    <div id="st-container" class="st-container">

        <div class="st-pusher">
            <div class="st-content">
                <div class="st-content-inner">
                    <!-- HEADER -->
                    <div class="header-1 header-inner-wrapper" id="header">
    <!-- TOP HEADER -->
    <div class="header-inner header-inner--style-1">
        <div class="container">
            <div class="col-sm-3 col-xs-12">
                    <!-- Logo: Dark layout -->
                    <img src="{{asset('tribu/images/logo/logo.png')}}"  width="130" height="100" class="hide"/>

                    <!-- Logo: Light layout -->
                    <img src="{{asset('tribu/images/logo/logo.png')}}"  width="130" height="100"/>
            </div>
            <div class="col-sm-9 text-right pr-0">
                <div class="header-col hidden-xs" style="padding-right: 90px;">
                    <div class="icon-block--style-3 icon-block--style-3-v1">
                        <i class="icon ion-ios-telephone-outline"></i>
                        <span class="icon-block-text heading heading-6 strong-400">
                            {{$consultaEmpresa[0]->telefono}}
                        </span>
                        <small class="icon-block-subtext c-gray-light">
                            {{$consultaEmpresa[0]->correo}}
                        </small>
                    </div>
                </div>
                 <div class="header-col hidden-xs" style="padding-right: 10px;">
                    <div class="icon-block--style-3 icon-block--style-3-v1">
                        <i class="ion-ios-location-outline"></i>
                        <span class="icon-block-text heading heading-6 strong-400" style="padding-right: 40px;">
                            {{$consultaEmpresa[0]->direccion1}}
                        </span>
                        <small class="icon-block-subtext c-gray-light">{{$consultaEmpresa[0]->direcccion}}</small>
                    </div>
                </div>
                <div class="header-col hidden-xs hidden-sm hidden-md" >
                    <ul class="social-media social-media--style-1-v3 social-media-circle">
                        <li>
                            <a href="https://www.facebook.com/{{$consultaEmpresa[0]->facebook}}" class="facebook" target="_blank" title="Facebook">
                                <i class="fa fa-facebook"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.instagram.com/{{$consultaEmpresa[0]->instagram}}" class="instagram" target="_blank">
                                <i class="fa fa-instagram"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://twitter.com/{{$consultaEmpresa[0]->twitter}}" class="dribbble" target="_blank">
                                <i class="fa fa-fw fa-twitter"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <!-- NAVBAR -->
<!-- NAVBAR -->
    <nav class="navbar navbar-main navbar--sm navbar--shadow navbar--caps navbar--strong" role="navigation">
        <div class="container relative">
            <div class="navbar-header">     
                <!-- NAVBAR TOGGLE -->
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">
                    <span class="sr-only">Navegación</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- LOGO -->
                <a class="navbar-logo" href="index.html">
                    <!-- Logo: Dark layout -->
                    <img src="{{asset('tribu/images/logo/logo.png')}}"  width="130" height="100" class="hide"/>

                    <!-- Logo: Light layout -->
                    <img src="{{asset('tribu/images/logo/logo.png')}}" width="130" height="100"/>
                </a>

                                <!-- SLIDEBAR TOGGLE -->
            </div>

            <div id="navbar-collapse" class="collapse navbar-collapse">
                <ul class="nav navbar-nav" data-hover="dropdown" data-animations="zoomIn zoomIn zoomIn zoomIn">
                    <!-- Navbar items/links -->
                     <!-- Navbar links -->
<li class="dropdown">
    <a href="{{asset('invermaster/proyecto')}}">
       <span>Inicio</span>
    </a>

</li>
<li class="dropdown">
    <a href="{{asset('invermaster/proyecto/general')}}">
       <span>Proyectos</span>
    </a>
  
</li>
<li class="dropdown">
    <a href="{{asset('invermaster/propiedad/general')}}">
       <span>Propiedades</span>
    </a>
   
</li>
<li class="dropdown">
    <a href="{{asset('invermaster/quinesomos/empresa')}}">
       <span>Quiénes Somos</span>
    </a>
    
</li>

<li class="dropdown">
    <a href="{{asset('invermaster/equipo')}}">
       <span>Equipo</span>
    </a>
   
</li>
<li class="dropdown">
    <a href="{{asset('plantilla/block')}}">
       <span>Blog</span>
    </a>
   
</li>


<!-- Shop -->
<li class="dropdown mega-dropdown">
        <a href ="{{asset('invermaster/contacto/empresa')}}">Contáctanos
    </a>
   
</li>

                </ul>

                <ul class="nav navbar-nav navbar-right" data-hover="dropdown" data-animations="zoomIn zoomIn zoomIn zoomIn">
                    
                    <!-- Login button -->
                    <li class="navbar-btn-lg">
                        <a href="{{asset('invermaster/equipo')}}" class="btn text-uppercase">
                            Contacta un agente
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <!-- GLOBAL SEARCH -->
    <section id="sctGlobalSearch" class="global-search">
    <div class="relative">
        <!-- Backdrop for global search -->
        <div class="global-search-backdrop"></div>

        <!-- Search form -->
        <form class="form-horizontal form-global-search" role="form">
            <div class="container relative">
                <div class="row">
                    <div class="col-xs-12">
                        <input type="text" class="search-input" placeholder="Type and hit enter ...">
                    </div>
                </div>

                <!-- Close button -->
                <button type="button" class="global-search-close-btn css-animate" data-target="#sctGlobalSearch" title="Close search bar">
                    <i class="icon ion-close-round"></i>
                </button>
            </div>
        </form>
    </div>
</section>

</div>
<script src="{{asset('tribu/js/jquery.js')}}"></script>
<script src="{{asset('tribu/js/jquery-ui.min.js')}}"></script>
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.0.3/socket.io.js"></script>-->
