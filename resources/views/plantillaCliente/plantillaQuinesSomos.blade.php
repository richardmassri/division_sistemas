<!DOCTYPE html>
<html>
<head>


<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="robots" content="index, follow">
<title>Invermaster</title>
<style type="text/css">

.ui-dialog-titlebar-close {
    visibility: hidden;
}

    /*.ui-dialog .ui-dialog-titlebar {
    background:transparent;
    border:none;
}*/
</style>


<!-- Page loader -->
<script src="{{asset('tribu/assets/pace/js/pace.min.js')}}"></script>
<link rel="stylesheet" href="{{asset('tribu/assets/pace/css/pace-minimal.css')}}" type="text/css">

<link rel="stylesheet" href="https://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css" />

<!-- Bootstrap -->
<link rel="stylesheet" href="{{asset('tribu/assets/bootstrap/css/bootstrap.min.css')}}" type="text/css">

<!-- CSS animation library -->
<link rel="stylesheet" href="{{asset('tribu/assets/animate/animate.min.css')}}" type="text/css">

<link href="https://fonts.googleapis.com/css?family=Quicksand:400,500,700" rel="stylesheet">

<!-- Plugins -->
<link rel="stylesheet" href="{{asset('tribu/assets/bootstrap-select/css/bootstrap-select.min.css')}}">
<link rel="stylesheet" href="{{asset('tribu/assets/swiper/css/swiper.min.css')}}">

<!-- Light Gallery -->
<link rel="stylesheet" href="{{asset('tribu/assets/lightgallery/css/lightgallery.min.css')}}">

<link rel="stylesheet" href="{{asset('plugins/select2/select2.min.css')}}">

<!-- Icons -->
<link rel="stylesheet" href="{{asset('tribu/fonts/font-awesome/css/font-awesome.min.css')}}" type="text/css">
<link rel="stylesheet" href="{{asset('tribu/fonts/ionicons/css/ionicons.min.css')}}" type="text/css">
<link rel="stylesheet" href="{{asset('tribu/fonts/line-icons/line-icons.css')}}" type="text/css">
<link rel="stylesheet" href="{{asset('tribu/fonts/line-icons-pro/line-icons-pro.css')}}" type="text/css">

<!-- Global style (main) -->
<link id="stylesheet" type="text/css" href="{{asset('tribu/css/global-style.min.css')}}" rel="stylesheet" media="screen">
<!-- Custom style - Remove if not necessary -->
<link type="text/css" href="{{asset('tribu/css/custom-style.css')}}" rel="stylesheet">

<!-- Favicon -->
<link href="{{asset('tribu/images/logo/logo.png')}}" rel="icon" type="image/png">


<!-- RS5.0 -->
<link rel="stylesheet" type="text/css" href="{{asset('tribu/assets/revolution/css/settings.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('tribu/assets/revolution/css/layers.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('tribu/assets/revolution/css/navigation.css')}}">

<!-- Cube Portfolio -->
<link rel="stylesheet" href="{{asset('tribu/assets/cubeportfolio/css/cubeportfolio.min.css')}}">

</head>
<body>


<!-- MAIN WRAPPER -->
@include('plantillaCliente.plantilla_principal')


                    <section class="parallax-section parallax-section-lg" style="background-image: url('{{asset('tribu/images/backgrounds/page-title/page-title-img-1.jpg')}}'); background-position: center 13px; min-height: 320px;">

                        <div class="mask mask-base-1--style-1"></div>

                        <div class="container">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="py-3">
                                        <h1 class="heading heading-1 c-white strong-400 text-normal">
                                            {{$consultaEmpresa[0]->nombre}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>

                   <section class="ss-slice">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="section-title--style-1">
                                        <h3 class="section-title-inner heading-5 strong-400 text-uppercase">
                                            <strong>Quienes Somos</strong>
                                        </h3>
                                        <span class="section-title-delimiter clearfix"></span>
                                    </div>

                                    <p class="mt-1">
                                        <?php echo $consultaEmpresa[0]->quines_somos;?>
                                    </p>

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="section-title--style-1">
                                        <h3 class="section-title-inner heading-5 strong-400 text-uppercase">
                                            <strong>Mision</strong>
                                        </h3>
                                        <span class="section-title-delimiter clearfix"></span>
                                    </div>

                                    <p class="mt-1">
                                        <?php echo $consultaEmpresa[0]->mision;?>
                                    </p>

                                </div>

                                 <div class="col-md-6">
                                    <div class="section-title--style-1">
                                        <h3 class="section-title-inner heading-5 strong-400 text-uppercase">
                                            <strong>Vision</strong>
                                        </h3>
                                        <span class="section-title-delimiter clearfix"></span>
                                    </div>

                                    <p class="mt-1">
                                        <?php echo $consultaEmpresa[0]->vision;?>
                                    </p>

                                </div>
                            </div>
                        </div>
                    </section>
                

                    <!-- FOOTER -->


        @include('plantillaCliente.plantilla_principal_footer')



<input type="text" class="hidden" name="_token" id="token" value="{{ csrf_token() }}"></input>
<div id="dialog-confirm" class="hidden">


<!--Finnnnnnnnnnnnnnnnnnnnnnnnnn -->

<!-- Required JSs -->
<!-- Essential JSs -->
<script src="{{asset('tribu/assets/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('tribu/js/bootstrap-extensions/bootstrap-dropdowneffect.js')}}"></script>
<script src="{{asset('tribu/js/bootstrap-extensions/bootstrap-validator.js')}}"></script>
<script src="{{asset('tribu/js/modernizr.custom.js')}}"></script>
<script src="{{asset('tribu/js/jquery.mousewheel-3.0.6.pack.js')}}"></script>
<script src="{{asset('tribu/js/jquery.easing.js')}}"></script>
<script src="{{asset('tribu/js/classie.js')}}"></script>
<script src="{{asset('tribu/js/slidebar/slidebar.js')}}"></script>

<!-- Assets - Required -->
<script src="{{asset('tribu/assets/headroom/headroom.min.js')}}"></script>
<script src="{{asset('tribu/assets/headroom/jquery.headroom.min.js')}}"></script>
<script src="{{asset('tribu/assets/footer-reveal/footer-reveal.min.js')}}"></script>
<script src="{{asset('tribu/assets/bootstrap-select/js/bootstrap-select.min.js')}}"></script>
<script src="{{asset('tribu/assets/swiper/js/swiper.min.js')}}"></script>
<script src="{{asset('tribu/assets/parallax/parallax.min.js')}}"></script>
<script src="{{asset('tribu/assets/viewport-checker/viewportchecker.min.js')}}"></script>
<script src="{{asset('tribu/assets/milestone-counter/jquery.countTo.js')}}"></script>

<!-- Light Gallery -->
<script src="{{asset('tribu/assets/lightgallery/js/lightgallery.min.js')}}"></script>
<script src="{{asset('tribu/assets/lightgallery/js/lg-thumbnail.min.js')}}"></script>
<script src="{{asset('tribu/assets/lightgallery/js/lg-video.js')}}"></script>

<!-- Cross-browser responsivity scripts -->
<!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
<![endif]-->

<!-- Google Analitycs JS -->
<script type="text/javascript" src="{{asset('tribu/js/wpx.ga.js')}}"></script>


<!-- RS5.0 Core JS Files -->
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/jquery.themepunch.tools.min.js?rev=5.0')}}"></script>
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/jquery.themepunch.revolution.min.js?rev=5')}}"></script>
<script type="text/javascript" src="{{asset('tribu/js/revolution/revolution-slider-real-estate-1.js')}}"></script>
<!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/extensions/revolution.extension.actions.min.js')}}"></script>
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/extensions/revolution.extension.carousel.min.js')}}"></script>
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/extensions/revolution.extension.kenburn.min.js')}}"></script>
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/extensions/revolution.extension.layeranimation.min.js')}}"></script>
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/extensions/revolution.extension.migration.min.js')}}"></script>
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/extensions/revolution.extension.navigation.min.js')}}"></script>
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/extensions/revolution.extension.parallax.min.js')}}"></script>
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/extensions/revolution.extension.slideanims.min.js')}}"></script>
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/extensions/revolution.extension.video.min.js')}}"></script>

<!-- Cube Portfolio -->
<script src="{{asset('tribu/assets/cubeportfolio/js/jquery.cubeportfolio.min.js')}}"></script>
<script src="{{asset('tribu/assets/cubeportfolio/js/main.4-col.space.grid.js')}}"></script>


<!-- App JS -->
<script src="{{asset('tribu/js/wpx.app.js')}}"></script>



<script src="{{asset('tribu/js/jquery-ui.min.js')}}"></script>


</script>
</body>
</html>

