<style type="text/css">
    #messages{
        border: 1px solid black;
        height: 300px;
        margin-bottom: 8px;
        overflow: scroll;
        padding: 5px;
    }
</style>



<!--<div class="container spark-screen hidden" style="position: fixed; bottom: 0%; top: 38%; left: 72%;height: 40px">
    <div class="row" style="height: 40px;">
        <div class="col-md-3 col-md-offset-1">
            <div class="panel panel-default">
            <i class="fa fa-close" aria-hidden="true" id="close"></i>
                <div class="panel-heading">Chat</div>
                    <div class="chatReal hidden">
                    <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-8">
                          <div id="messages" style="height: 100px;"></div>
                        </div>
                        <div class="col-lg-8" >
                                <form action="sendmessage" method="POST">
                                    <input type="text" class="hidden" id="_tokenA" name="_tokenA" value="{{ csrf_token() }}"></input>
                                    <input type="hidden" name="user" id="nombreApellidoChat">
                                    <textarea class="form-control msg"></textarea>
                                    <br/>
                                    <input type="button" value="Send" class="btn btn-success send-msg">
                                </form>
                        </div>
                    </div>
                </div>
                </div>
                <div class="chatForm">
                    <div class="col-md-12 formularioChat panel panel-default">
                    <div class="col-md-12">
                    <br>
                    <label class="col-md-12"> Nombre </label>
                    <input type="text" name="nombre" class="col-md-12" id="nombreChat" placeholder="Nombre">
                    </input>
                    </div>
                    <div class="col-md-12">
                    <br>
                    <label class="col-md-12"> Apellido </label>
                    <input type="text" name="apellido" class="col-md-12" id="apellidoChat" placeholder="Apellido">
                    </input>
                    </div>
                    <div class="col-md-12">
                    <br>
                    <label class="col-md-12"> Correo </label>
                    <input type="text" name="correo" class="col-md-12" id="correoChat" placeholder="Correo">
                    </input>
                    </div>
                    <div class="col-md-12">
                    <br>
                    <input type="text" class="hidden" id="_tokenC" name="_tokenC" value="{{ csrf_token() }}"></input>
                    <button class="btn btn-success guardarPersonaChat"> Guardar </button>
                    </input>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var socket = io.connect('172.25.171.85:9000');
    socket.on('message', function (data) {
        data = jQuery.parseJSON(data);
        console.log(data.user);
        $(".spark-screen").removeClass('hidden');
        $( "#messages" ).append( "<strong>"+data.user+":</strong><p>"+data.message+"</p>" );
      });

    $(".send-msg").click(function(e){
        e.preventDefault();
        var user = $("#nombreApellidoChat").val();
        var msg = $(".msg").val();
        if(msg != ''){
            $.ajax({
                type: "POST",
                url: '{!! URL::to("sendmessage") !!}',
                dataType: "json",
                data: {'_token':$('#_tokenA').val(),'message':msg,'user':user},
                success:function(data){
                    console.log(data);
                    $(".msg").val('');
                }
            });
        }else{
            alert("Please Add Message.");
        }
    })

        $(".guardarPersonaChat").click(function(e){
        e.preventDefault();
        var nombre = $("#nombreChat").val();
        var apellido = $("#apellidoChat").val();
        var correo = $("#correoChat").val();
        var _tokenC = $("#_tokenC").val();
            $.ajax({
                type: "POST",
                url: '{!! URL::to("sendmessageForm") !!}',
                dataType: "json",
                data: {'nombre':nombre,'_token':_tokenC,'apellido':apellido,'correo':correo},
                success:function(data){
                    console.log(data);
                    $(".chatReal").removeClass('hidden');
                    $("#nombreApellidoChat").val(data.nombreApellido);
                    $(".chatForm").addClass('hidden');
                }
            });
    })

    
</script>

<script type="text/javascript">
  $('#botonChat').on("click",function(){
      $(".spark-screen").removeClass('hidden');
  });

    $('#close').on("click",function(){
      $(".spark-screen").addClass('hidden');
  });
</script>

<style type="text/css">
    a:hover {
    background-color: white; 
}
</style>-->

<footer style="height: 55px;">
    <div class="footer-bottom" style="padding: 0.8rem 0;margin-bottom: 10px;">
        <div class="container">
            <div class="row flex flex-items-xs-middle">
                    <div class="copyright col-md-6">
                        &copy; 2017 
                        <strong style="color: blue;"> {{$consultaEmpresa[0]->nombre}} </strong>
                    </div>  
                    <div class="col-md-3 text-right"> Desarrollado por: <a target="_black" href="http://key-core.com" style="font-size: 12px; color: gray;"><img src="{{asset('tribu/images/keycore.png')}}" style="height:30px;"> </a>
                    </div>
                    <div class="col-md-offset-3"> </div>
            </div>
        </div>
    </div>
</footer>



<!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
(function(){ var widget_id = '9laYG5vS99';var d=document;var w=window;function l(){
var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);}if(d.readyState=='complete'){l();}else{if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();</script>
<!-- {/literal} END JIVOSITE CODE -->
