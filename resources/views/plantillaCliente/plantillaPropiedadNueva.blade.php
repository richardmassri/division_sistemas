<!DOCTYPE html>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=euc-jp">


<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="robots" content="index, follow">
<title>Invermaster</title>

<!-- Page loader -->
<script src="{{asset('tribu/assets/pace/js/pace.min.js')}}"></script>
<link rel="stylesheet" href="{{asset('tribu/assets/pace/css/pace-minimal.css')}}" type="text/css">

<!-- Bootstrap -->
<link rel="stylesheet" href="{{asset('tribu/assets/bootstrap/css/bootstrap.min.css')}}" type="text/css">


<!-- CSS animation library -->
<link rel="stylesheet" href="{{asset('tribu/assets/animate/animate.min.css')}}" type="text/css">

<link href="https://fonts.googleapis.com/css?family=Quicksand:400,500,700" rel="stylesheet">

<!-- Plugins -->
<link rel="stylesheet" href="{{asset('tribu/assets/bootstrap-select/css/bootstrap-select.min.css')}}">
<link rel="stylesheet" href="{{asset('tribu/assets/swiper/css/swiper.min.css')}}">

<!-- Light Gallery -->
<link rel="stylesheet" href="{{asset('tribu/assets/lightgallery/css/lightgallery.min.css')}}">

<!-- Icons -->
<link rel="stylesheet" href="{{asset('tribu/fonts/font-awesome/css/font-awesome.min.css')}}" type="text/css">
<link rel="stylesheet" href="{{asset('tribu/fonts/ionicons/css/ionicons.min.css')}}" type="text/css">
<link rel="stylesheet" href="{{asset('tribu/fonts/line-icons/line-icons.css')}}" type="text/css">
<link rel="stylesheet" href="{{asset('tribu/fonts/line-icons-pro/line-icons-pro.css')}}" type="text/css">

<!-- Global style (main) -->
<link id="stylesheet" type="text/css" href="{{asset('tribu/css/global-style.min.css')}}" rel="stylesheet" media="screen">


<!-- Favicon -->
<link href="{{asset('tribu/images/logo/logo.png')}}" rel="icon" type="image/png">
<link rel="stylesheet" href="{{asset('plugins/select2/select2.min.css')}}">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.css">

<!-- Custom style - Remove if not necessary -->
<link type="text/css" href="{{asset('tribu/css/custom-style.css')}}" rel="stylesheet">

</head>
<body>


<!-- MAIN WRAPPER -->
@include('plantillaCliente.plantilla_principal')

                    <section class="has-bg-cover bg-size-cover flex flex-items-xs-top" style="background-image: url('{{ asset('tribu/images/prv/real-estate/page-title-1.jpg')}}');background-position: center bottom;">

                        <span class="mask mask-base-1--style-2"></span>

                        <div class="flex-wrap-item">
                            <div class="container">
                                <div class="page-title page-title--style-2">
                                    <div class="">
                                        <h3 class="heading heading-1 strong-700 text-uppercase c-white">
                                            Propiedades
                                        </h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>

                    <!-- Properties listing -->
                    <section class="slice sct-color-2">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-9">
                                @foreach($consultaPropiedades as $consultaPropiedad)
                                    <div class="card card-horizontal no-padding z-depth-2-top">
                                        <div class="row row-no-padding flex flex-items-xs-middle">
                                            <div class="col col-md-4">
                                                <div class="">
                                                <a href="{{asset('invermaster/propiedad/detalle/'.$consultaPropiedad->id)}}">
                                                    <img src="{{asset($consultaPropiedad->nombre_galeria) }}" class="img-responsive" style="width: 100%; height: 100%;">
                                                </a>
                                                </div>
                                            </div>

                                            <div class="col col-md-8">
                                                <div class="card-body">
                                                    <h4 class="heading heading-5 text-uppercase">
                                                        <a href="{{asset('invermaster/propiedad/detalle/'.$consultaPropiedad->id)}}">{{$consultaPropiedad->nombre_propiedad}}</a>
                                                    </h4>
                                                    <h5 class="text-uppercase">
                                                        {{$consultaPropiedad->nombre_sector}}
                                                    </h5>

                                                    <p class="card-text mt-1">
                                                    {{$consultaPropiedad->descripcion_breve}}
                                                        
                                                    </p>

                                                    <ul class="inline-links inline-links--style-2">
                                                        <li>
                                                            <a href="#">
                                                                <i class="fa fa-heart meGusta{{$consultaPropiedad->id}}"
                                                                <?php 
                                                                if(\Cache::get($consultaPropiedad->id)==$consultaPropiedad->id){ ?>  style="color: blue;" 
                                                                <?php } ?>
                                                                onclick="meGusta({{$consultaPropiedad->id}})"></i>
                                                            </a>
                                                        </li>
                                                        <?php $contador_vistas=0;?>
                                                            @foreach($consultaCantidadPropiedadesVistas as $vista)
                                                                @if($consultaPropiedad->id==$vista->propiedad_id)
                                                                    <?php $contador_vistas=$contador_vistas+1;?>
                                                                @endif                        
                                                            @endforeach
                                                        <li>
                                                            <i class="fa fa-eye"></i> {{$contador_vistas}}
                                                        </li>
                                                        <li>
                                                            <a href="#">
                                                                <i class="fa fa-comment"></i>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>

                                                <div class="card-footer b-xs-top">
                                                    <div class="row flex flex-items-xs-middle">
                                                        <div class="col col-xs-7">
                                                            <span class="block-price strong-600">
                                                             <?php $precio=number_format((float)$consultaPropiedad->precio_desde, 2, ',', '.');
                                                                echo 'USD '. $precio;?></span>
                                                            <span class="block-price-text"></span>
                                                        </div>
               
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <span class="space-xs-lg"></span>
                                    @endforeach

                                    <div class="pagination-wrapper mt-3">
                                        <ul class="pagination pagination--style-2 pagination-lg">
                                        
                                            <li>{!! $consultaPropiedades->render()!!}</li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="sidebar">
    <div class="sidebar-object b-xs-bottom pb-2 mb-2">
        <div class="card z-depth-2-top">
            <div class="card-title b-xs-bottom">
                <h3 class="heading heading-sm text-uppercase">Buscar Propiedades</h3>
            </div>
            <div class="card-body">
                <form class="form-default mt-2" action="{{asset('invermaster/proyecto')}}" method="post" data-toggle="validator" role="form">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group form-group-lg has-feedback">
                            <label for="" class="text-uppercase">Ciudad</label>
                            <select class="form-control select2" style="width: 100%;" name="sector_ciudad_nombre">
                              <option value="" class="small-text">Seleccione</option>
                                @foreach($ciudad as $ciudad)
                                    <option value="{{$ciudad->id}}">{{$ciudad->nombre}}</option>
                                @endforeach
                            </select>
                            <span class="help-block with-errors"></span>

                        </div>
                    </div>
                <div class="row"> </div>
                    <div class="col-sm-6">
                        <div class="form-group form-group-lg has-feedback">
                            <label for="" class="text-uppercase">Tipo</label>
                            <select class="form-control selectpicker" name="tipo" id="tipo">
                                <option value="">Seleccione</option>
                            @foreach($tipo as $tipo)
                                <option value="{{$tipo->id}}">{{$tipo->nombre}}</option>
                            @endforeach
                            </select>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
   
                       <input class="form-control hidden" type="text" id="precio_desde" placeholder="City, Country" name="precio_desde">

                         <input class="form-control hidden" type="text" id="precio_hasta" placeholder="Nombre" name="precio_hasta">

                    <div class="col-sm-6">
                        <div class="form-group form-group-lg has-feedback">
                            <label for="" class="text-uppercase">Operaciones</label>
                            <select class="form-control selectpicker" name="operaciones" id="operaciones">
                                <option value="">Seleccione</option>
                                @foreach($operaciones as $operaciones)
                                    <option value="{{$operaciones->id}}">{{$operaciones->nombre}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="help-block with-errors"></div>
                    </div>

                     <div class="row"> </div>

                    <div class="col-sm-6">
                        <div class="form-group form-group-lg has-feedback">
                            <label for="" class="text-uppercase">dormitorios</label>
                            <select class="form-control selectpicker" name="dormitorios" id="banios">
                                <option value="">Seleccione</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                            </select>
                        </div>
                        <div class="help-block with-errors"></div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group form-group-lg has-feedback">
                            <label for="" class="text-uppercase">Baños</label>
                            <select class="form-control selectpicker" name="banios" id="banios">
                                <option value="">Seleccione</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                            </select>
                        </div>
                        <div class="help-block with-errors"></div>
                    </div>

                    <div class="col-sm-6">

                    <div class="form-group form-group-lg has-feedback">
                        <label for="" class="text-uppercase">Estacionamiento</label>
                            <select class="form-control selectpicker" name="estacionamiento" id="banios">
                                <option value="">Seleccione</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                            </select>
                        </div>
                        </div>


                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group has-feedback">
                            <label class="text-uppercase" for="">Rango de Precios</label>
                            <div class="range-slider-wrapper mt-1">
                                <!-- Range slider container -->
                                <div id="filterPrinceRange"></div>

                                <!-- Range slider values -->
                                <div class="row mt-1">
                                    <div class="col-xs-6">
                                        <small>USD </small>
                                        <span class="range-slider-value value-low" id="filterPrinceValueLow"></span>
                                    </div>

                                    <div class="col-xs-6 text-right">
                                        <small>USD </small>
                                        <span class="range-slider-value value-hight" id="filterPrinceValueHigh"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                    <input type="text" class="hidden" id="token" name="_token" value="{{ csrf_token() }}"></input>
                <button class="btn btn-block btn-lg btn-base-1 mt-2" id="boton">Buscar</button>
            </form>
        </div>
    </div>
</div>

    </section>

@include('plantillaCliente.plantilla_principal_footer')
   
                </div>
            </div>
        </div><!-- END: st-pusher -->
    </div><!-- END: st-container -->
</div><!-- END: body-wrap -->

             


<!-- SCRIPTS -->

<!-- Required JSs -->

<!-- Essential JSs -->
<script src="{{asset('tribu/assets/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('tribu/js/bootstrap-extensions/bootstrap-dropdowneffect.js')}}"></script>
<script src="{{asset('tribu/js/bootstrap-extensions/bootstrap-validator.js')}}"></script>
<script src="{{asset('tribu/js/modernizr.custom.js')}}"></script>
<script src="{{asset('tribu/js/jquery.mousewheel-3.0.6.pack.js')}}"></script>
<script src="{{asset('tribu/js/jquery.easing.js')}}"></script>
<script src="{{asset('tribu/js/classie.js')}}"></script>
<script src="{{asset('tribu/js/slidebar/slidebar.js')}}"></script>

<!-- Assets - Required -->
<script src="{{asset('tribu/assets/headroom/headroom.min.js')}}"></script>
<script src="{{asset('tribu/assets/headroom/jquery.headroom.min.js')}}"></script>
<script src="{{asset('tribu/assets/footer-reveal/footer-reveal.min.js')}}"></script>
<script src="{{asset('tribu/assets/bootstrap-select/js/bootstrap-select.min.js')}}"></script>
<script src="{{asset('tribu/assets/swiper/js/swiper.min.js')}}"></script>
<script src="{{asset('tribu/assets/parallax/parallax.min.js')}}"></script>
<script src="{{asset('tribu/assets/viewport-checker/viewportchecker.min.js')}}"></script>
<script src="{{asset('tribu/assets/milestone-counter/jquery.countTo.js')}}"></script>

<!-- Light Gallery -->
<script src="{{asset('tribu/assets/lightgallery/js/lightgallery.min.js')}}"></script>
<script src="{{asset('tribu/assets/lightgallery/js/lg-thumbnail.min.js')}}"></script>
<script src="{{asset('tribu/assets/lightgallery/js/lg-video.js')}}"></script>

<!-- Cross-browser responsivity scripts -->
<!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
<![endif]-->

<!-- Google Analitycs JS -->
<input type="text" name="" class="hidden" id="maximo_precio" value="{{$consultaMaximoPrecio[0]->precio_maximo}}">

<input type="text" name="" class="hidden" id="minimo_precio" value="{{$consultaMinimoPrecio
[0]->precio_minimo}}">

<script type="text/javascript" src="{{asset('tribu/js/wpx.ga.js')}}"></script>


<!-- No UI slider -->
<script type="text/javascript" src="{{asset('tribu/assets/nouislider/js/nouislider.min.js')}}"></script>
<script src="{{asset('plugins/select2/select2.full.min.js')}}"></script>

<script type="text/javascript">
$(".select2").select2({
    theme: "bootstrap"
});
</script>
<script type="text/javascript">
    $(document).ready(function() {
        var snapSlider = document.getElementById('filterPrinceRange');
        var precio=parseFloat($('#maximo_precio').val());
        var precio_minimo=parseFloat($('#minimo_precio').val());
        var resultado=[];
        var resultado2="";
        var resultado=0;

        noUiSlider.create(snapSlider, {
            start: [precio_minimo, precio ],

            connect: true,
            range: {
                'min': precio_minimo,
                'max': precio
            }
        });

        var snapValues = [
            document.getElementById('filterPrinceValueLow'),
            document.getElementById('filterPrinceValueHigh')
        ];

        snapSlider.noUiSlider.on('update', function( values, handle ) {
             values[handle]=values[handle].replace(',','');
            resultado=parseFloat(values[handle]).toLocaleString('de-DE');
            snapValues[handle].innerHTML = resultado;
        });
    });
</script>
</script>

<!-- App JS -->
<script src="{{asset('tribu/js/wpx.app.js')}}"></script>

<script type="text/javascript">
$('#boton').on('click',function(){
    var precio_desde=$('#filterPrinceValueLow').text();
    var precio_hasta=$('#filterPrinceValueHigh').text();
    $('#precio_desde').val(precio_desde);
    $('#precio_hasta').val(precio_hasta);

})
    function meGusta(id){
    var _token = $("#token").val();
    $(".meGusta"+id).css({
        'color':'blue'});
    //console.log(_token);
    $.ajax({
    type: "POST",
    url: "{{asset('propiedad/meGusta')}}",
    data: {
    id: id,_token:_token},
    dataType: "json",
    success: function (resultado) {
      console.log(resultado);


    }
    });
  }
</script>

</body>
</html>