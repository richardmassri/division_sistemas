<!DOCTYPE html>
<html>
<head>


<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="robots" content="index, follow">
<title>Invermaster</title>
<style type="text/css">

.ui-dialog-titlebar-close {
    visibility: hidden;
}

    /*.ui-dialog .ui-dialog-titlebar {
    background:transparent;
    border:none;
}*/
</style>


<!-- Page loader -->
<script src="{{asset('tribu/assets/pace/js/pace.min.js')}}"></script>
<link rel="stylesheet" href="{{asset('tribu/assets/pace/css/pace-minimal.css')}}" type="text/css">

<link rel="stylesheet" href="https://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css" />

<!-- Bootstrap -->
<link rel="stylesheet" href="{{asset('tribu/assets/bootstrap/css/bootstrap.min.css')}}" type="text/css">

<!-- CSS animation library -->
<link rel="stylesheet" href="{{asset('tribu/assets/animate/animate.min.css')}}" type="text/css">

<link href="https://fonts.googleapis.com/css?family=Quicksand:400,500,700" rel="stylesheet">

<!-- Plugins -->
<link rel="stylesheet" href="{{asset('tribu/assets/bootstrap-select/css/bootstrap-select.min.css')}}">
<link rel="stylesheet" href="{{asset('tribu/assets/swiper/css/swiper.min.css')}}">

<!-- Light Gallery -->
<link rel="stylesheet" href="{{asset('tribu/assets/lightgallery/css/lightgallery.min.css')}}">

<link rel="stylesheet" href="{{asset('plugins/select2/select2.min.css')}}">

<!-- Icons -->
<link rel="stylesheet" href="{{asset('tribu/fonts/font-awesome/css/font-awesome.min.css')}}" type="text/css">
<link rel="stylesheet" href="{{asset('tribu/fonts/ionicons/css/ionicons.min.css')}}" type="text/css">
<link rel="stylesheet" href="{{asset('tribu/fonts/line-icons/line-icons.css')}}" type="text/css">
<link rel="stylesheet" href="{{asset('tribu/fonts/line-icons-pro/line-icons-pro.css')}}" type="text/css">

<!-- Global style (main) -->
<link id="stylesheet" type="text/css" href="{{asset('tribu/css/global-style.min.css')}}" rel="stylesheet" media="screen">
<!-- Custom style - Remove if not necessary -->
<link type="text/css" href="{{asset('tribu/css/custom-style.css')}}" rel="stylesheet">

<!-- Favicon -->
<link href="{{asset('tribu/images/logo/logo.png')}}" rel="icon" type="image/png">


<!-- RS5.0 -->
<link rel="stylesheet" type="text/css" href="{{asset('tribu/assets/revolution/css/settings.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('tribu/assets/revolution/css/layers.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('tribu/assets/revolution/css/navigation.css')}}">

<!-- Cube Portfolio -->
<link rel="stylesheet" href="{{asset('tribu/assets/cubeportfolio/css/cubeportfolio.min.css')}}">

</head>
<body>
<!-- MAIN WRAPPER -->
@include('plantillaCliente.plantilla_principal')


                    <section class="parallax-section parallax-section-lg" style="background-image: url('{{asset('tribu/images/backgrounds/page-title/page-title-img-1.jpg')}}'); background-position: center 13px; min-height: 320px;">

                        <div class="mask mask-base-1--style-1"></div>

                        <div class="container">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="py-3">
                                        <h1 class="heading heading-1 c-white strong-400 text-normal">
                                            {{$consultaEmpresa[0]->nombre}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>


                    <input type="text" name="latitud" class="hidden" id="latitud" value="{{$consultaEmpresa[0]->latitud}}">

                    <input type="text" name="longitud" class="hidden" id="longitud" value="{{$consultaEmpresa[0]->longitud}}">

                    <section class="slice sct-color-2">
                        <div class="container container-over-top">
                            <div class="container-inner sct-color-1">
                                <!-- Map canvas -->
                                <div id="googleMap" style="width:100%;height:400px;">
                                    <!-- #google-container will contain the map  -->
                                    <div id="map_canvas" class="map-canvas"></div>
                                    <!-- #cd-zoom-in and #zoom-out will be used to create our custom buttons for zooming-in/out -->
                                    <div id="map-zoom-in"></div>
                                    <div id="map-zoom-out"></div>
                                </div>
                            </div>
                        </div>
                    </section>

                    <div class="alert alert-success container hidden enviarMensaje">
                            <strong>Estimado usuario, su mensaje fue enviado de forma exitosa. En breves momentos será atendida su solicitud. </strong>
                    </div>
                    <section class="slice pt-0 sct-color-2">
                        <div class="container">
                            <div class="block-card-wrapper">
                                <div class="block-card-section z-depth-1-bottom">
                                    <div class="" id="same_height_1">
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="px-2 py-2">
                                                    <h3 class="heading heading-6 strong-600 text-uppercase">
                                                        Contáctanos
                                                    </h3>

                                                    <!-- Contact form -->
                                                    <form class="form-default form-material mt-3" role="form">
                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <div class="form-group">
                                                                    <label for="" class="text-uppercase  c-gray-light">Nombre y Apellido</label>
                                                                    <input type="text" class="form-control input-lg" placeholder="" id="nombreContacto">
                                                                    <span class="bar"></span>
                                                                </div>
                                                                <label class="col-md-12 nombreContactoMensaje hidden text-danger"> Debe ingresar el nombre </label>

                                                            </div>
                                                            <div class="col-sm-6">
                                                                <div class="form-group">
                                                                    <label for="" class="text-uppercase c-gray-light">E-mail</label>
                                                                    <input type="email" class="form-control input-lg" placeholder="" id="emailContacto">
                                                                    <span class="bar"></span>
                                                                </div>
                                                                <label class="col-md-12 nombreContactoEmail hidden text-danger"> Debe ingresar el email </label>

                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <div class="form-group">
                                                                    <label for="" class="text-uppercase c-gray-light">Teléfono</label>
                                                                    <input type="text" class="form-control input-lg" placeholder="" id="telefonoContacto">
                                                                    <span class="bar"></span>
                                                                </div>
                                                                <label class="col-md-12 telefonoContactoMensaje hidden text-danger"> Debe ingresar el teléfono </label>

                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <div class="form-group">
                                                                    <label for="" class="text-uppercase c-gray-light">Mensaje</label>
                                                                    <textarea class="form-control no-resize" rows="5" placeholder="" id="mensajeContacto"></textarea>
                                                                    <span class="bar"></span>
                                                                </div>
                                                                <label class="col-md-12 telefonoContactoMensajeText hidden text-danger"> Debe ingresar el mensaje </label>
                                                            </div>
                                                        </div>

                                                        <div  class="btn btn-sm btn-base-1 mt-1 mensaje_boton" onclick="contactarAgente()">Enviar mensaje</div>
                                                        <img class="hidden" id="loading" src="{{asset('tribu/images/loading.gif')}}">
                                                    </form>
                                                </div>
                                            </div>

 

                                            <div class="col-md-4">
                                                <div class="same-height bg-base-1">
                                                    <div class="px-2 py-2">
                                                        <h3 class="mt-1 mb-1 text-uppercase">
                                                            Información de contacto
                                                        </h3>

                                                        <div class="icon-block--style-3 mb-1">
                                                            <i class="icon ion-ios-location bg-base-4"></i>
                                                            <span class="heading heading-6 strong-400">
                                                                {{$consultaEmpresa[0]->direccion1}} <p  style="padding-left:55px">{{$consultaEmpresa[0]->direcccion}} </p>
                                                            </span>
                                                        </div>

                                                        <div class="icon-block--style-3 mb-1">
                                                            <i class="icon ion-ios-telephone bg-base-4"></i>
                                                                <span class="heading heading-6 strong-400" style="width:120px;">
                                                                {{$consultaEmpresa[0]->telefono}}
                                                            </span>
                                                        </div>

                                                        <div class="icon-block--style-3 mb-1">
                                                            <i class="icon ion-ios-email bg-base-4"></i>
                                                            <span class="heading strong-400" style="font-size:0.9rem">
                                                                {{$consultaEmpresa[0]->correo}}
                                                            </span>
                                                        </div>

                                                        <span class="clearfix"></span>
                                                        <br>
                                                        <div class="text-xs-center">
                                                            <ul class="social-media social-media--style-1-v3 social-media-circle">
                                                                <li>
                                                                    <a href="https://www.facebook.com/{{$consultaEmpresa[0]->facebook}}" class="facebook" target="_blank" title="Facebook">
                                                                        <i class="fa fa-facebook"></i>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="https://www.instagram.com/{{$consultaEmpresa[0]->instagram}}" class="instagram" target="_blank">
                                                                        <i class="fa fa-instagram"></i>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="https://twitter.com/{{$consultaEmpresa[0]->twitter}}" class="dribbble" target="_blank">
                                                                        <i class="fa fa-fw fa-twitter"></i>
                                                                        </li>
                                                                    </a>
                                                                
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <br>

                    <!-- FOOTER -->


                        <br>
@include('plantillaCliente.plantilla_principal_footer')
                </div>
            </div>
        </div><!-- END: st-pusher -->
    </div><!-- END: st-container -->
</div><!-- END: body-wrap -->

<input type="text" class="hidden" name="_token" id="token" value="{{ csrf_token() }}"></input>
<div id="dialog-confirm" class="hidden">


<!--Finnnnnnnnnnnnnnnnnnnnnnnnnn -->
<a href="#" class="back-to-top"></a>

<!-- Required JSs -->
<!-- Essential JSs -->
<script src="{{asset('tribu/assets/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('tribu/js/bootstrap-extensions/bootstrap-dropdowneffect.js')}}"></script>
<script src="{{asset('tribu/js/bootstrap-extensions/bootstrap-validator.js')}}"></script>
<script src="{{asset('tribu/js/modernizr.custom.js')}}"></script>
<script src="{{asset('tribu/js/jquery.mousewheel-3.0.6.pack.js')}}"></script>
<script src="{{asset('tribu/js/jquery.easing.js')}}"></script>
<script src="{{asset('tribu/js/classie.js')}}"></script>
<script src="{{asset('tribu/js/slidebar/slidebar.js')}}"></script>

<!-- Assets - Required -->
<script src="{{asset('tribu/assets/headroom/headroom.min.js')}}"></script>
<script src="{{asset('tribu/assets/headroom/jquery.headroom.min.js')}}"></script>
<script src="{{asset('tribu/assets/footer-reveal/footer-reveal.min.js')}}"></script>
<script src="{{asset('tribu/assets/bootstrap-select/js/bootstrap-select.min.js')}}"></script>
<script src="{{asset('tribu/assets/swiper/js/swiper.min.js')}}"></script>
<script src="{{asset('tribu/assets/parallax/parallax.min.js')}}"></script>
<script src="{{asset('tribu/assets/viewport-checker/viewportchecker.min.js')}}"></script>
<script src="{{asset('tribu/assets/milestone-counter/jquery.countTo.js')}}"></script>

<!-- Light Gallery -->
<script src="{{asset('tribu/assets/lightgallery/js/lightgallery.min.js')}}"></script>
<script src="{{asset('tribu/assets/lightgallery/js/lg-thumbnail.min.js')}}"></script>
<script src="{{asset('tribu/assets/lightgallery/js/lg-video.js')}}"></script>

<!-- Cross-browser responsivity scripts -->
<!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
<![endif]-->

<!-- Google Analitycs JS -->
<script type="text/javascript" src="{{asset('tribu/js/wpx.ga.js')}}"></script>


<!-- RS5.0 Core JS Files -->
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/jquery.themepunch.tools.min.js?rev=5.0')}}"></script>
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/jquery.themepunch.revolution.min.js?rev=5')}}"></script>
<script type="text/javascript" src="{{asset('tribu/js/revolution/revolution-slider-real-estate-1.js')}}"></script>
<!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/extensions/revolution.extension.actions.min.js')}}"></script>
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/extensions/revolution.extension.carousel.min.js')}}"></script>
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/extensions/revolution.extension.kenburn.min.js')}}"></script>
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/extensions/revolution.extension.layeranimation.min.js')}}"></script>
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/extensions/revolution.extension.migration.min.js')}}"></script>
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/extensions/revolution.extension.navigation.min.js')}}"></script>
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/extensions/revolution.extension.parallax.min.js')}}"></script>
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/extensions/revolution.extension.slideanims.min.js')}}"></script>
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/extensions/revolution.extension.video.min.js')}}"></script>

<!-- Cube Portfolio -->
<script src="{{asset('tribu/assets/cubeportfolio/js/jquery.cubeportfolio.min.js')}}"></script>
<script src="{{asset('tribu/assets/cubeportfolio/js/main.4-col.space.grid.js')}}"></script>


<!-- App JS -->
<script src="{{asset('tribu/js/wpx.app.js')}}"></script>


<!-- Google maps -->
<script>
      function myMap() {
      var latitud=$('#latitud').val();
      var longitud=$('#longitud').val();
      var myLatLng = {lat:parseFloat(latitud) , lng:parseFloat(longitud)};

  var map = new google.maps.Map(document.getElementById('googleMap'), {
    zoom: 15,
    center: myLatLng,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  });

  var marker = new google.maps.Marker({
    position: myLatLng,
    map: map,
    title: 'Hello World!'
  });
}
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBpd76B1_f4lp75gkMLbthkxeZjfW2FUVU&callback=myMap"></script><script src="{{asset('tribu/js/google-maps/google-maps-custom.js')}}"></script>


<script src="{{asset('tribu/js/jquery-ui.min.js')}}"></script>

    <script>
    function validarFormuSolicitud(nombre,email,telefono,mensaje){
        var comprobar=0;
        if(nombre==''){
            comprobar=1;
            $(".nombreContactoMensaje").removeClass('hidden');
        }else{
            $(".nombreContactoMensaje").addClass('hidden');
        }
        if(email==''){
            comprobar=1;
            $(".nombreContactoEmail").removeClass('hidden');
        }else{
            $(".nombreContactoEmail").addClass('hidden');
        }
        
        if(telefono==''){
            comprobar=1;
            $(".telefonoContactoMensaje").removeClass('hidden');
        }else{
            $(".telefonoContactoMensaje").addClass('hidden');
        }
        if(mensaje==''){
            comprobar=1;
            $(".telefonoContactoMensajeText").removeClass('hidden');
        }else{
            $(".telefonoContactoMensajeText").addClass('hidden');
        }
        return comprobar;
    }
    function contactarAgente(){
        var _token = $("#token").val();
        var tipo = "contacto";
        var _token = $("#token").val();
        var nombre = $("#nombreContacto").val();
        var email = $("#emailContacto").val();
        var telefono = $("#telefonoContacto").val();
        var mensaje = $("#mensajeContacto").val();
        var comprobarValor=validarFormuSolicitud(nombre,email,telefono,mensaje);
        if(comprobarValor==0){
            $('#loading').removeClass('hidden');
            $('.mensaje_boton').addClass('hidden');
            $.ajax({
                type: "POST",
                url: "{{asset('invermaster/contacto')}}",
                data: { usuario_id: '',_token:_token,tipo:tipo,nombre:nombre,email:email,telefono:telefono,mensaje:mensaje},
                dataType: "json",
                success: function (resultado) {
                    if(resultado['statusCode']=='exito'){
                        $( ".enviarMensaje" ).removeClass('hidden');
                        $('.mensaje_boton').removeClass('hidden');
                        $('#loading').addClass('hidden');
                        $("#proyecto_id").val('');
                        $("#nombreContacto").val('');
                        $("#emailContacto").val('');
                        $("#telefonoContacto").val('');
                        $("#mensajeContacto").val('');
                        $('html, body').animate({scrollTop: '300px'});
                        setTimeout(function() {
                            $( ".enviarMensaje" ).addClass('hidden');
                        }, 10000);
                    }
                }
            });
        }
    }
  </script>


</script>
</body>
</html>

