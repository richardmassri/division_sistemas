
<!DOCTYPE html>
<html>
<head>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="robots" content="index, follow">
<title>Invermaster</title>

<!-- Page loader -->
<script src="{{asset('tribu/assets/pace/js/pace.min.js')}}"></script>
<link rel="stylesheet" href="{{asset('tribu/assets/pace/css/pace-minimal.css')}}" type="text/css">
<link rel="stylesheet" href="{{asset('plugins/select2/select2.min.css')}}">

<!-- Bootstrap -->
<link rel="stylesheet" href="{{asset('tribu/assets/bootstrap/css/bootstrap.min.css')}}" type="text/css">

<!-- CSS animation library -->
<link rel="stylesheet" href="{{asset('tribu/assets/animate/animate.min.css')}}" type="text/css">

<link href="https://fonts.googleapis.com/css?family=Quicksand:400,500,700" rel="stylesheet">

<!-- Plugins -->
<link rel="stylesheet" href="{{asset('tribu/assets/bootstrap-select/css/bootstrap-select.min.css')}}">
<link rel="stylesheet" href="{{asset('tribu/assets/swiper/css/swiper.min.css')}}">

<!-- Light Gallery -->
<link rel="stylesheet" href="{{asset('tribu/assets/lightgallery/css/lightgallery.min.css')}}">

<!-- Icons -->
<link rel="stylesheet" href="{{asset('tribu/fonts/font-awesome/css/font-awesome.min.css')}}" type="text/css">
<link rel="stylesheet" href="{{asset('tribu/fonts/ionicons/css/ionicons.min.css')}}" type="text/css">
<link rel="stylesheet" href="{{asset('tribu/fonts/line-icons/line-icons.css')}}" type="text/css">
<link rel="stylesheet" href="{{asset('tribu/fonts/line-icons-pro/line-icons-pro.css')}}" type="text/css">

<!-- Global style (main) -->
<link id="stylesheet" type="text/css" href="{{asset('tribu/css/global-style.min.css')}}" rel="stylesheet" media="screen">
<!-- Custom style - Remove if not necessary -->
<link type="text/css" href="{{asset('tribu/css/custom-style.css')}}" rel="stylesheet">

<!-- Favicon -->
<link href="{{asset('tribu/images/logo/logo.png')}}" rel="icon" type="image/png">


<!-- RS5.0 -->
<link rel="stylesheet" type="text/css" href="{{asset('tribu/assets/revolution/css/settings.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('tribu/assets/revolution/css/layers.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('tribu/assets/revolution/css/navigation.css')}}">

<!-- Cube Portfolio -->
<link rel="stylesheet" href="{{asset('tribu/assets/cubeportfolio/css/cubeportfolio.min.css')}}">

</head>
<body>


<!-- MAIN WRAPPER -->
@include('plantillaCliente.plantilla_principal')

<div class="row"> </div>
<div class="slice sct-color-2"> </div>

                    <form method="post" action="{{asset('invermaster/proyecto/general')}}" name="form">
                    <section class="slice sct-color-2">
                        <div class="container container-over-top">
                            <div class="container-inner">
                                <div class="card card-advanced-search z-depth-2-top px-1 py-2">
                                    <div class="card-body">
                       
                                            <div class="advanced-search-visible">
                                                <div class="row">
                                                    <div class="col-md-3 col-sm-6">
                                                        <div class="form-group form-group-lg">
                                                        <label class="text-uppercase" for=""></label>

                                                            <select class="form-control selectpicker select2" name="sector" id="sector">
                                                                <option value="">Selecione</option>
                                                                @foreach($sector as $sector)
                                                                    <option value="{{$sector->id}}">{{$sector->nombre}}</option>
                                                                @endforeach
                                                            </select>
                                                            <span class="bar"></span>
                                                        </div>
                                                    </div>

                    <div class="col-md-3 col-sm-6">
                        <div class="form-group has-feedback">
                            <label class="text-uppercase" for="">Rango de Precios</label>
                            <div class="range-slider-wrapper mt-1">
                                <!-- Range slider container -->
                                <div id="filterPrinceRange"></div>

                                <!-- Range slider values -->
                                <div class="row mt-1">
                                    <div class="col-xs-6">
                                        <small>USD </small>
                                        <span class="range-slider-value value-low" id="filterPrinceValueLow"></span>
                                    </div>

                                    <div class="col-xs-6 text-right">
                                        <small>USD </small>
                                        <span class="range-slider-value value-hight" id="filterPrinceValueHigh"></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <input class="form-control hidden" type="text" id="precio_desde" placeholder="City, Country" name="precio_desde">

                         <input class="form-control hidden" type="text" id="precio_hasta" placeholder="Nombre" name="precio_hasta">
                    </div>

                    <div class="col-md-3 col-sm-6">
                        <label for="" class=""></label>
                        <button type="submit" class="btn btn-block btn-base-4" id="boton">Buscar</button>
                    </div>

                         
                            <input type="text" class="hidden" id="_token" name="_token" value="{{ csrf_token() }}"></input>
                                <div class="col-md-3 col-sm-6">
                            </div>

                    </section>
                    </form>
    
                        <h3 class="heading heading-1 strong-700 text-uppercase c-white">
                        <?php if(count($consultaProyectoPrincipal)>0){?>
                            {{$consultaProyectoPrincipal[0]->nombre_proyecto}}
                        <?php }?>
                        </h3>
   
                    <!-- Properties listing -->
                    <section>
                        <div class="container">
                            <div class="row">
                                <div class="alert alert-success container enviarMensaje hidden">
                                    <strong>Estimado usuario, su mensaje fue enviado de forma exitosa. En breves momentos será atendida su solicitud. </strong>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-9">
                                    <div class="mb-2">
                                        <h3 class="heading heading-3 strong-600 text-uppercase">
                                            {{$consultaProyectoPrincipal[0]->nombre_proyecto}}
                                        </h3>
                                        <ul class="inline-links inline-links--style-2">
    
                                            <li>
                                                <i class="fa fa-eye"></i> 
                                                <?php 
                                                    if(count($consultaCantidadProyectosVistos)>0){
                                                    echo $consultaCantidadProyectosVistos[0]->cantidad_vistas;}else{ echo "0";}?>
                                            </li>
                                            <li>
                                                <i class="fa fa-envelope"></i> <?php 
                                                if(count($consultaCantidadProyectosVistosContactados)>0){
                                                        echo $consultaCantidadProyectosVistosContactados[0]->cantidad_vistas;}else{ echo "0";}?>
                                            </li>
                                        </ul>
                                    </div>

                                    <!-- Gallery -->
                                    <div class="gallery-top">

                                        <img src="{{asset($consultaProyectoPrincipal[0]->nombre_galeria)}}">
                                    </div>
                                    
                                    <div class="gallery-bottom" id="light_gallery">
                                        <div class="row">
                                        <?php $c=0;?>
                                        @foreach($consultaProyectoAll as $consultaProyectoAll)
                                            <div class="col-sm-2">
                                                <div class="gallery-thumb">
                                                    <a class="item" href="{{asset($consultaProyectoAll->nombre_galeria)}}">
                                                        <img src="{{asset($consultaProyectoAll->nombre_galeria)}}" style='width:500px;height:100px'>
                                                    </a>
                                                </div>
                                                <?php if($c==5){ $c=0;?> <br> <?php } ?>
                                            </div>
                                            <?php $c++;?>

                                        @endforeach
                                        </div>
                                    </div>

                                    <span class="space-md-md"></span>

                                    <!-- Description -->
                                    <div class="card z-depth-2-top">
                                        <div class="card-title b-xs-bottom">
                                            <h3 class="heading heading-sm text-uppercase">Descripcion</h3>
                                        </div>
                                        <div class="card-body" style="text-align: justify;">
                                        <?php echo $consultaProyectoPrincipal[0]->descripcion;?>
                                            


                                        </div>
                                    </div>

                                   <!-- <span class="space-md-md"></span>

                                    <div class="card z-depth-2-top">
                                        <div class="card-title b-xs-bottom">
                                            <a href="#" class="btn-aux">
                                                <i class="ion-location"></i>
                                                <span class="aux-text">Ubicacion</span>
                                            </a>
                                        </div>
                                        <div class="card-body">
                                            <div class="row">
                                            <div id="googleMap" style="width:100%;height:400px;"></div>
                                            </div>
                                        </div>
                                    </div>-->

                                    <span class="space-md-md"></span>

                                    <!-- Property details -->
                                    <div class="card z-depth-2-top">
                                        <div class="card-title b-xs-bottom">
                                            <h3 class="heading heading-sm text-uppercase">Detalles del proyecto</h3>
                                        </div>
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <table class="table table-no-border table-striped table-responsive">
                                                        <tbody>
                                                            <tr>
                                                                <td><strong>Nombre:</strong> {{$consultaProyectoPrincipal[0]->nombre_proyecto}}</td>
                                                                <td><strong>Fecha Publicacion:</strong> 
                                                              

                                                                <?php 
                                                                $convertirFecha=new DateTime($consultaProyectoPrincipal[0]->fecha_publicacion);
                                                                echo $convertirFecha->format('d-m-Y');?></td>
                                                                <td><strong>¿Está en contrucción?</strong> {{$consultaProyectoPrincipal[0]->construccion}}</td>
                                                            </tr>

                                                            <tr>
                                                                <td><strong>Pais:</strong> {{$consultaProyectoPrincipal[0]->nombre_pais}}</td>
                                                                <td><strong>Ciudad:</strong> {{$consultaProyectoPrincipal[0]->nombre_ciudad}}</td>
                                                                <td><strong>Sector:</strong> {{$consultaProyectoPrincipal[0]->nombre_sector}}</td>
                                                            </tr>

                                                            <tr>
                                                                <td><strong>Precio Desde:</strong> 
                                                                <?php $precio_dede=number_format((float)$consultaProyectoPrincipal[0]->precio_desde, 2, ',', '.');
                                                                $precio_hasta=number_format((float)$consultaProyectoPrincipal[0]->precio_hasta, 2, ',', '.');
                                                                ?>

                                                                USD {{$precio_dede}}</td>
                                                                <td colspan="2"><strong>Precio Hasta:</strong> USD {{$precio_hasta}}</td>
                                                            </tr>

                   
                                                            <?php 
                                                                $cantidad_proyecto=count($consultaTipoProyecto);
                                                                $contador_comas=', ';
                                                                $contador=0;
                                                            ?>
                                                                <tr>
                                                                <td colspan="3"><strong> Tipo: </strong> 
                                                                @foreach($consultaTipoProyecto as $consultaTipoProyecto)
                                                                    <?php 
                                                                    if($cantidad_proyecto-1==$contador){
                                                                            $contador_comas='';
                                                                    }
                                                                    echo $consultaTipoProyecto->nombre_tipo.$contador_comas;?>
                                                                    <?php $contador++;?>
                                                                @endforeach
                                                                </td>
                                                                </tr>

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <span class="space-md-md"></span>

                                    <!-- Caracteristicas -->
                                    <?php if(count($consultaCaracteristicaProyecto)>0){?>
                                    <div class="card z-depth-2-top">
                                        <div class="card-title b-xs-bottom">
                                            <h3 class="heading heading-sm text-uppercase">Caracteristicas del proyecto</h3>
                                        </div>
                                        <div class="card-body">
                                        <table>
                                            @foreach($consultaCaracteristicaProyecto as $consultaCaracteristicaProyecto)
                                            <tr>
                                               <td>
                                               <?php if(!empty($consultaCaracteristicaProyecto->icono)){?>
                                                <i> <img src="{{asset($consultaCaracteristicaProyecto->icono)}}" style="width: 5%; height: 2%"> </i>
                                                <?php }else{?>
                                                 <i class="fa fa-circle-o"> </i>
                                                    <?php }?>
                                               {{$consultaCaracteristicaProyecto->nombre_caracteristica}} 
                                               </td>
                                            </tr>
                                            @endforeach
                                        </table>
                                        </div>
                                    </div>
                                    <?php }?>

                                    <input type="text" name="latitud" class="hidden" id="latitud" value="{{$consultaProyectoPrincipal[0]->latitud}}">

                                    <input type="text" name="longitud" class="hidden" id="longitud" value="{{$consultaProyectoPrincipal[0]->longitud}}">

                                    <span class="space-md-md"></span>

                                    <!-- Video -->
                                    @if($consultaProyectoPrincipal[0]->video)
                                    <div class="card z-depth-2-top">
                                        <div class="card-title b-xs-bottom">
                                            <h3 class="heading heading-sm text-uppercase">Video</h3>
                                        </div>
                                        <div class="card-body">
                                            <!-- 16:9 aspect ratio -->
                                           <div class="embed-responsive embed-responsive-16by9">
                                               <iframe width="560" height="315" src="{{$consultaProyectoPrincipal[0]->video}}" frameborder="0" allowfullscreen></iframe>
                                           </div>
                                        </div>
                                    </div>
                                    @endif

                   
                                </div>

                                <div class="col-md-3">
                                    <div class="sidebar">
                                    <br><br><br><br>

                    <img src="{{asset($consultaProyectoPrincipal[0]->foto)}}" style="height: 150px; width: 100%;height: 250px;margin-left: auto;margin-right: auto;">
                <div class="text-xs-center">
                </div>


    <div class="sidebar-object">
        <div class="card z-depth-2-top">
            <div class="card-title b-xs-bottom">
                <h3 class="heading heading-sm text-uppercase">Agente inmobiliario</h3>
            </div>

            <div class="list-group-item">
                <i class="fa fa-user"></i>
                <span class="strong-500">NOMBRE</span>
                <span class="pull-right">{{$consultaProyectoPrincipal[0]->nombre_usuario}} {{$consultaProyectoPrincipal[0]->apellido_usuario}}</span>
            </div>


            <div class="list-group-item">
                <i class="fa fa-phone"></i>
                <span class="strong-500">OFICINA</span>
                <span class="pull-right">{{$consultaProyectoPrincipal[0]->telefono_local}}</span>
            </div>

            <div class="list-group-item">
                <i class="fa fa-phone"></i>
                <span class="strong-500">CELULAR</span>
                <span class="pull-right">{{$consultaProyectoPrincipal[0]->telefono_celular}}</span>
            </div>


            <br>
            <div class="card-title b-xs-bottom">
                <h3 class="heading heading-sm text-uppercase">Contáctanos</h3>
            </div>


<div class="col-sm-12">

            <input type="text" class="hidden" id="proyecto_id" name="proyecto_id" value="{{$consultaProyectoPrincipal[0]->proyecto_id}}">

            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                <input type="text" class="form-control" id="nombreContacto" placeholder="Nombre y Apellido"> </input>
            </div>
            <label class="col-md-12 nombreContactoMensaje hidden text-danger"> Debe ingresar el nombre </label>
            <br>
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                <input type="text" class="form-control" id="emailContacto" placeholder="Email"> </input>
            </div>
            <label class="col-md-12 nombreContactoEmail hidden text-danger"> Debe ingresar el email </label>
            <br>

            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                <input type="text" class="form-control" id="telefonoContacto" placeholder="Telefono"> </input>
            </div>
            <label class="col-md-12 telefonoContactoMensaje hidden text-danger"> Debe ingresar el teléfono </label>
            <br>
            <div>
            <textarea class="form-control" cols="20" rows="10" id="mensajeContacto" placeholder="Mensaje"></textarea>
            </div>
            <label class="col-md-12 telefonoContactoMensajeText hidden text-danger"> Debe ingresar el mensaje </label>
            <br>
        </div>  
            <div class="card-body">
                <a href="#" class="btn btn-block btn-base-2 enviar_mensaje">
                    Enviar mensaje
                </a>
                <img class="hidden" id="loading" src="{{asset('tribu/images/loading.gif')}}">
                <br>
                <div>
                <div id="mensajeDiv"> 
                <p id="contenidoMensaje" class="bg-base-1"> </p>
                </div>
            </div>
        </div>
    </div>

   

    <div class="sidebar-object">
        <div class="section-title section-title--style-1 mt-3">
            <h4 class="heading heading-xs strong-300 c-gray-light">Proyectos</h4>
            <h3 class="section-title-inner heading-4 strong-400 mt-0 text-uppercase">
                Vistas Recientes
            </h3>
        </div>

        <?php $clase="";?>

        @foreach($consultaProyectoAllDestacadaSector as $consultaProyectoAllDestacadaSector)

        <div class="block block--style-3 z-depth-2-top z-depth-3--hover <?php echo $clase;?>">
            <div class="block-image relative">
                <a href="">
                    <img src="{{asset($consultaProyectoAllDestacadaSector->nombre_galeria)}}" class="img-responsive" style="width:100%; height:100%">
                </a>
            </div>
            <br>
            <div class="block-body">
                <h3 class="heading heading-sm text-uppercase"> <strong>
                {{$consultaProyectoAllDestacadaSector->nombre_proyecto}} </strong> </h3>
                <p class="description" style="text-align: justify;">
                    {{$consultaProyectoAllDestacadaSector->descripcion_breve}}
                </p>
                <div class="mt-1"> <span class="block-price heading-6"> 
                    <?php $precio=number_format((float)$consultaProyectoAllDestacadaSector->precio_desde, 2, ',', '.');
                    echo  'USD '.$precio;?>
                 </span>
            </div>
            </div>


        </div>
        <?php $clase="mt-3";?>
@endforeach

        </div>
        </section>





    @include('plantillaCliente.plantilla_principal_footer')

                </div>
            </div>
        </div><!-- END: st-pusher -->
    </div><!-- END: st-container -->
</div><!-- END: body-wrap -->

<input type="text" class="hidden" name="_token" id="token" value="{{ csrf_token() }}"></input>



<input type="text" name="" class="hidden" id="maximo_precio" value="{{$consultaMaximoPrecio[0]->precio_maximo}}"><!-- SCRIPTS -->
<input type="text" name="" class="hidden" id="minimo_precio" value="{{$consultaMinimoPrecio[0]->precio_minimo}}">

<!-- Required JSs -->

<!-- Essential JSs -->
<script src="{{asset('tribu/assets/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('tribu/js/bootstrap-extensions/bootstrap-dropdowneffect.js')}}"></script>
<script src="{{asset('tribu/js/bootstrap-extensions/bootstrap-validator.js')}}"></script>
<script src="{{asset('tribu/js/modernizr.custom.js')}}"></script>
<script src="{{asset('tribu/js/jquery.mousewheel-3.0.6.pack.js')}}"></script>
<script src="{{asset('tribu/js/jquery.easing.js')}}"></script>
<script src="{{asset('tribu/js/classie.js')}}"></script>
<script src="{{asset('tribu/js/slidebar/slidebar.js')}}"></script>

<!-- Assets - Required -->
<script src="{{asset('tribu/assets/headroom/headroom.min.js')}}"></script>
<script src="{{asset('tribu/assets/headroom/jquery.headroom.min.js')}}"></script>
<script src="{{asset('tribu/assets/footer-reveal/footer-reveal.min.js')}}"></script>
<script src="{{asset('tribu/assets/bootstrap-select/js/bootstrap-select.min.js')}}"></script>
<script src="{{asset('tribu/assets/swiper/js/swiper.min.js')}}"></script>
<script src="{{asset('tribu/assets/parallax/parallax.min.js')}}"></script>
<script src="{{asset('tribu/assets/viewport-checker/viewportchecker.min.js')}}"></script>
<script src="{{asset('tribu/assets/milestone-counter/jquery.countTo.js')}}"></script>

<!-- Light Gallery -->
<script src="{{asset('tribu/assets/lightgallery/js/lightgallery.min.js')}}"></script>
<script src="{{asset('tribu/assets/lightgallery/js/lg-thumbnail.min.js')}}"></script>
<script src="{{asset('tribu/assets/lightgallery/js/lg-video.js')}}"></script>

<!-- Cross-browser responsivity scripts -->
<!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
<![endif]-->

<!-- Google Analitycs JS -->
<script type="text/javascript" src="{{asset('tribu/js/wpx.ga.js')}}"></script>


<!-- RS5.0 Core JS Files -->
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/jquery.themepunch.tools.min.js?rev=5.0')}}"></script>
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/jquery.themepunch.revolution.min.js?rev=5')}}"></script>
<script type="text/javascript" src="{{asset('tribu/js/revolution/revolution-slider-real-estate-1.js')}}"></script>
<!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/extensions/revolution.extension.actions.min.js')}}"></script>
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/extensions/revolution.extension.carousel.min.js')}}"></script>
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/extensions/revolution.extension.kenburn.min.js')}}"></script>
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/extensions/revolution.extension.layeranimation.min.js')}}"></script>
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/extensions/revolution.extension.migration.min.js')}}"></script>
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/extensions/revolution.extension.navigation.min.js')}}"></script>
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/extensions/revolution.extension.parallax.min.js')}}"></script>
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/extensions/revolution.extension.slideanims.min.js')}}"></script>
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/extensions/revolution.extension.video.min.js')}}"></script>

<!-- Cube Portfolio -->
<script src="{{asset('tribu/assets/cubeportfolio/js/jquery.cubeportfolio.min.js')}}"></script>
<script src="{{asset('tribu/assets/cubeportfolio/js/main.4-col.space.grid.js')}}"></script>

<!-- No UI slider -->
<script type="text/javascript" src="{{asset('tribu/assets/nouislider/js/nouislider.min.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        var snapSlider = document.getElementById('filterPrinceRange');
        var precio=parseFloat($('#maximo_precio').val());
        var minimo=parseFloat($('#minimo_precio').val());
        var resultado=[];
        var resultado2="";
        var resultado=0;

        noUiSlider.create(snapSlider, {
            start: [ minimo, precio ],

            connect: true,
            range: {
                'min': minimo,
                'max': precio
            }
        });

        var snapValues = [
            document.getElementById('filterPrinceValueLow'),
            document.getElementById('filterPrinceValueHigh')
        ];

        snapSlider.noUiSlider.on('update', function( values, handle ) {
           values[handle]=values[handle].replace(',','');
            resultado=parseFloat(values[handle]).toLocaleString('de-DE');
            snapValues[handle].innerHTML = resultado;
        });
    });
</script>

<!-- App JS -->
<script src="{{asset('tribu/js/wpx.app.js')}}"></script>
<script src="{{asset('plugins/select2/select2.full.min.js')}}"></script>
<script type="text/javascript">
$(".select2").select2();
</script>

<script type="text/javascript">
$('#boton').on('click',function(){
    var precio_desde=$('#filterPrinceValueLow').text();
    var precio_hasta=$('#filterPrinceValueHigh').text();
    $('#precio_desde').val(precio_desde);
    $('#precio_hasta').val(precio_hasta);
   /*var precio_desde=$('#filterPrinceValueLow').text();
   var precio_hasta=$('#filterPrinceValueHigh').text();
   var nombre=$('#nombre').val();
   var tipo=$('#tipo').val();
   var operacion=$('#operaciones').val();
   var sector=$('#sector').val();
   var _token=$('#_token').val();
      $.ajax({                        
           type: "POST",                 
           url: '/invermaster/proyecto',                     
           data: {'precio_desde':precio_desde,'precio_hasta':precio_hasta,'nombre':nombre,'tipo':tipo,'operacion':operacion,'sector':sector,'_token':_token},
       success: function (result) {
        window.location.href = '/invermaster/proyecto';
           //do somthing here
      }
       });*/





})
</script>

<!--<script>
    function myMap() {
      var latitud=$('#latitud').val();
      var longitud=$('#longitud').val();
      var myLatLng = {lat:parseFloat(latitud) , lng:parseFloat(longitud)};

  var map = new google.maps.Map(document.getElementById('googleMap'), {
    zoom: 15,
    center: myLatLng,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  });

  var marker = new google.maps.Marker({
    position: myLatLng,
    map: map,
    title: 'Hello World!'
  });
}
</script>-->

<!--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBpd76B1_f4lp75gkMLbthkxeZjfW2FUVU&callback=myMap"></script>-->


<script type="text/javascript">
    function validarFormuSolicitud(nombre,email,telefono,mensaje){
        var comprobar=0;
        if(nombre==''){
            comprobar=1;
            $(".nombreContactoMensaje").removeClass('hidden');
        }else{
            $(".nombreContactoMensaje").addClass('hidden');
        }
        if(email==''){
            comprobar=1;
            $(".nombreContactoEmail").removeClass('hidden');
        }else{
            $(".nombreContactoEmail").addClass('hidden');
        }
        
        if(telefono==''){
            comprobar=1;
            $(".telefonoContactoMensaje").removeClass('hidden');
        }else{
            $(".telefonoContactoMensaje").addClass('hidden');
        }
        if(mensaje==''){
            comprobar=1;
            $(".telefonoContactoMensajeText").removeClass('hidden');
        }else{
            $(".telefonoContactoMensajeText").addClass('hidden');
        }
        return comprobar;
    }
    $(".enviar_mensaje").on("click",function(){
        var tipo = "proyecto";
        var _token = $("#token").val();
        var proyecto_id = $("#proyecto_id").val();
        var nombre = $("#nombreContacto").val();
        var email = $("#emailContacto").val();
        var telefono = $("#telefonoContacto").val();
        var mensaje = $("#mensajeContacto").val();
        var comprobarValor=validarFormuSolicitud(nombre,email,telefono,mensaje);
        if(comprobarValor==0){
            $('#loading').removeClass('hidden');
             $('.enviar_mensaje').addClass('hidden');
            $.ajax({
                type: "POST",
                url: "{{asset('invermaster/contacto')}}",
                data: { proyecto_id: proyecto_id,_token:_token,tipo:tipo,nombre:nombre,email:email,telefono:telefono,mensaje:mensaje},
                dataType: "json",
                success: function (resultado) {
                    if(resultado['statusCode']=='exito'){
                        $('#loading').addClass('hidden');
                        $('.mensaje_boton').removeClass('hidden');
                        $('html, body').animate({scrollTop: '200px'});
                        $("#nombreContacto").val('');
                        $("#emailContacto").val('');
                        $("#telefonoContacto").val('');
                        $("#mensajeContacto").val('');
                        $( ".enviar_mensaje" ).removeClass('hidden');
                        $( ".enviarMensaje" ).removeClass('hidden');
                        setTimeout(function() {
                             $( ".enviarMensaje" ).addClass('hidden');
                        }, 10000);
                    }
                }
            });
        }
    });
    </script>

</body>
</html>