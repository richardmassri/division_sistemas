<!DOCTYPE html>
<html>
<head>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="robots" content="index, follow">
<title>Invermaster</title>

<!-- Page loader -->
<script src="{{asset('tribu/assets/pace/js/pace.min.js')}}"></script>
<link rel="stylesheet" href="{{asset('tribu/assets/pace/css/pace-minimal.css')}}" type="text/css">
<link rel="stylesheet" href="{{asset('plugins/select2/select2.min.css')}}">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.css">


<!-- Bootstrap -->
<link rel="stylesheet" href="{{asset('tribu/assets/bootstrap/css/bootstrap.min.css')}}" type="text/css">

<!-- CSS animation library -->
<link rel="stylesheet" href="{{asset('tribu/assets/animate/animate.min.css')}}" type="text/css">

<link href="https://fonts.googleapis.com/css?family=Quicksand:400,500,700" rel="stylesheet">

<!-- Plugins -->
<link rel="stylesheet" href="{{asset('tribu/assets/bootstrap-select/css/bootstrap-select.min.css')}}">
<link rel="stylesheet" href="{{asset('tribu/assets/swiper/css/swiper.min.css')}}">

<!-- Light Gallery -->
<link rel="stylesheet" href="{{asset('tribu/assets/lightgallery/css/lightgallery.min.css')}}">

<!-- Icons -->
<link rel="stylesheet" href="{{asset('tribu/fonts/font-awesome/css/font-awesome.min.css')}}" type="text/css">
<link rel="stylesheet" href="{{asset('tribu/fonts/ionicons/css/ionicons.min.css')}}" type="text/css">
<link rel="stylesheet" href="{{asset('tribu/fonts/line-icons/line-icons.css')}}" type="text/css">
<link rel="stylesheet" href="{{asset('tribu/fonts/line-icons-pro/line-icons-pro.css')}}" type="text/css">

<!-- Global style (main) -->
<link id="stylesheet" type="text/css" href="{{asset('tribu/css/global-style.min.css')}}" rel="stylesheet" media="screen">
<!-- Custom style - Remove if not necessary -->
<link type="text/css" href="{{asset('tribu/css/custom-style.css')}}" rel="stylesheet">

<!-- Favicon -->
<link href="{{asset('tribu/images/logo/logo.png')}}" rel="icon" type="image/png">


</head>
<body>


@include('plantillaCliente.plantilla_principal')


    <!-- GLOBAL SEARCH -->
    <section id="sctGlobalSearch" class="global-search">
    <div class="relative">
        <!-- Backdrop for global search -->
        <div class="global-search-backdrop"></div>

        <!-- Search form -->
        <form class="form-horizontal form-global-search" role="form">
            <div class="container relative">
                <div class="row">
                    <div class="col-xs-12">
                        <input type="text" class="search-input" placeholder="Type and hit enter ...">
                    </div>
                </div>

                <!-- Close button -->
                <button type="button" class="global-search-close-btn css-animate" data-target="#sctGlobalSearch" title="Close search bar">
                    <i class="icon ion-close-round"></i>
                </button>
            </div>
        </form>
    </div>
</section>
</div>

    <!-- GLOBAL SEARCH -->
    <section id="sctGlobalSearch" class="global-search">
    <div class="relative">
        <!-- Backdrop for global search -->
        <div class="global-search-backdrop"></div>

        <!-- Search form -->
        <div class="form-horizontal form-global-search" role="form">
            <div class="container relative">
                <div class="row">
                    <div class="col-xs-12">
                        <input type="text" class="search-input" placeholder="Type and hit enter ...">
                    </div>
                </div>

                <!-- Close button -->
                <button type="button" class="global-search-close-btn css-animate" data-target="#sctGlobalSearch" title="Close search bar">
                    <i class="icon ion-close-round"></i>
                </button>
            </div>
        </div>
    </div>
</section>
</div>

                    <section class="has-bg-cover bg-size-cover flex flex-items-xs-top" style="background-image: url('{{ asset('tribu/images/prv/real-estate/page-title-1.jpg')}}');background-position: center bottom">
                        <span class="mask mask-base-1--style-2"></span>

                        <div class="flex-wrap-item">
                            <div class="container">
                                <div class="page-title page-title--style-2">
                                    <div class="">
                                        <h3 class="heading heading-1 strong-700 text-uppercase c-white">
                                            Proyectos
                                        </h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>

                    <!-- Search properties -->
                <form method="post" action="{{asset('invermaster/proyecto/general')}}" name="form">
                    <section class="slice sct-color-2">
                        <div class="container container-over-top">
                            <div class="container-inner">
                                <div class="card card-advanced-search z-depth-2-top px-1 py-2">
                                    <div class="card-body">
                       
                                            <div class="advanced-search-visible">
                                                <div class="row">
                                                    <div class="col-md-3 col-sm-6">
                                                        <div class="form-group form-group-lg">
                                                        <label class="text-uppercase" for=""></label>

                                                            <select class="form-control selectpicker select2" name="sector" id="sector">
                                                                <option value="">Seleccione</option>
                                                                @foreach($sector as $sector)
                                                                    <option value="{{$sector->id}}">{{$sector->nombre}}</option>
                                                                @endforeach
                                                            </select>
                                                            <span class="bar"></span>
                                                        </div>
                                                    </div>

                    <div class="col-md-3 col-sm-6">
                        <div class="form-group has-feedback">
                            <label class="text-uppercase" for="">Rango de Precios</label>
                            <div class="range-slider-wrapper mt-1">
                                <!-- Range slider container -->
                                <div id="filterPrinceRange"></div>

                                <!-- Range slider values -->
                                <div class="row mt-1">
                                    <div class="col-xs-6">
                                        <small>USD </small>
                                        <span class="range-slider-value value-low" id="filterPrinceValueLow"></span>
                                    </div>

                                    <div class="col-xs-6 text-right">
                                        <small>USD </small>
                                        <span class="range-slider-value value-hight" id="filterPrinceValueHigh"></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <input class="form-control hidden" type="text" id="precio_desde" placeholder="City, Country" name="precio_desde">

                         <input class="form-control hidden" type="text" id="precio_hasta" placeholder="Nombre" name="precio_hasta">
                    </div>

                         
                            <input type="text" class="hidden" id="_token" name="_token" value="{{ csrf_token() }}"></input>
                                <div class="col-md-3 col-sm-6">
                                                        <label for="" class=""></label>
                                                        <button type="submit" class="btn btn-block btn-base-4" id="boton">Buscar</button>
                                                    </div>
                                                </div>
                                            </div>

                                            </div>

                                            <a href="#" id="btn_advanced_search_open" class="btn-advanced-search-open">
                                                <i class="ion-ios-arrow-down"></i>
                                            </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    </form>

                    <!-- Properties listing -->
                    <section class="slice pt-0 sct-color-2">
                        <div class="container">
                            <div class="section-title section-title--style-1 text-xs-center">
                                <h3 class="section-title-inner heading-4 strong-400 mt-0 text-uppercase">
                                    <strong class="c-base-1"> Nuestros Proyectos</strong>
                                </h3>
                            </div>
                            <?php if(count($consultaProyecto)<=0){?>
                            <br>
                                <h3 class="section-title-inner heading-4 strong-400 mt-0 text-uppercase">
                                     <center> <strong class="c-base-1">No se encontraron resultados</strong> </center>
                                </h3>
                                <?php }?>

                            <div class="row">
                            <?php $c=0;?>
                            @foreach($consultaProyecto as $proyecto)
                                <div class="col-md-4">
                                    <div class="block block--style-3 z-depth-2-top z-depth-3--hover">
                                        <div class="block-image relative">
                                            <a href="{{asset('invermaster/proyecto/detalle/'.$proyecto->id)}}">
                                                 <img src="{{asset($proyecto->nombre_galeria)}}" class="img-responsive" style="width: 100%; height: 250px;">
                                            </a>
                                           <!-- <span class="block-ribbon block-ribbon-right bg-red">For rent</span>-->
                                        </div>

                                        <div class="block-body" style="height: 170px;">
                                            <h3 class="heading heading-6 text-uppercase">
                                               {{$proyecto->nombre_proyecto}}
                                            </h3>
                                            <table>
                                            <tr>
                                                    
                                                @foreach($consultaTipoProyecto as $value)
                                                    @if($value->proyecto_id==$proyecto->id)
                                                    <td>
                                                        <span class="badge bg-gren" style="margin-bottom:10px;margin-right:5px"> <?php echo $value->nombre_tipo; ?></span>
                                                        </td> 
                                                    @endif
                                                @endforeach                    
                                                    
                                          </tr>
                                                </table>
                                            <p class="description" style="text-align: justify;">
                                                {{$proyecto->descripcion_breve}}
                                            </p>
                                        </div>

                                     <div class="block-footer b-xs-top">
                                            <div class="row flex flex-items-xs-middle">
                                                <div class="col col-xs-12">
                                                    <span class="block-price" style="font-size: 14px;"><?php $precio=number_format((float)$proyecto->precio_desde, 2, ',', '.');
                                                    echo 'USD '. $precio;?></span>
                                                    <span class="block-price-text"></span>
                                                </div>
       
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php $c++; if($c==3){$c=0?>
                                <div class="row"> </div><br>
                                    <?php } ?>
                                @endforeach
                            </div>

                                <div class="pagination-wrapper mt-3">
                                        <ul class="pagination pagination--style-2 pagination-lg">
                                <ul class="pagination pagination--style-2 pagination-lg">
                                            <li>{!! $consultaProyecto->render()!!}</li>
                                </ul>
                                </ul>
                                </div>
                        </div>
                    </section>

                    <!-- FOOTER -->
       @include('plantillaCliente.plantilla_principal_footer')

                </div>
            </div>
        </div><!-- END: st-pusher -->
    </div><!-- END: st-container -->
</div><!-- END: body-wrap -->


<input type="text" name="" class="hidden" id="maximo_precio" value="{{$consultaMaximoPrecio[0]->precio_maximo}}">
<input type="text" name="" class="hidden" id="minimo_precio" value="{{$consultaMinimoPrecio[0]->precio_minimo}}">

<!-- SCRIPTS -->

<!-- Required JSs -->

<!-- Essential JSs -->
<script src="{{asset('tribu/assets/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('tribu/js/bootstrap-extensions/bootstrap-dropdowneffect.js')}}"></script>
<script src="{{asset('tribu/js/bootstrap-extensions/bootstrap-validator.js')}}"></script>
<script src="{{asset('tribu/js/modernizr.custom.js')}}"></script>
<script src="{{asset('tribu/js/jquery.mousewheel-3.0.6.pack.js')}}"></script>
<script src="{{asset('tribu/js/jquery.easing.js')}}"></script>
<script src="{{asset('tribu/js/classie.js')}}"></script>
<script src="{{asset('tribu/js/slidebar/slidebar.js')}}"></script>

<!-- Assets - Required -->
<script src="{{asset('tribu/assets/headroom/headroom.min.js')}}"></script>
<script src="{{asset('tribu/assets/headroom/jquery.headroom.min.js')}}"></script>
<script src="{{asset('tribu/assets/footer-reveal/footer-reveal.min.js')}}"></script>
<script src="{{asset('tribu/assets/bootstrap-select/js/bootstrap-select.min.js')}}"></script>
<script src="{{asset('tribu/assets/swiper/js/swiper.min.js')}}"></script>
<script src="{{asset('tribu/assets/parallax/parallax.min.js')}}"></script>
<script src="{{asset('tribu/assets/viewport-checker/viewportchecker.min.js')}}"></script>
<script src="{{asset('tribu/assets/milestone-counter/jquery.countTo.js')}}"></script>

<!-- Light Gallery -->
<script src="{{asset('tribu/assets/lightgallery/js/lightgallery.min.js')}}"></script>
<script src="{{asset('tribu/assets/lightgallery/js/lg-thumbnail.min.js')}}"></script>
<script src="{{asset('tribu/assets/lightgallery/js/lg-video.js')}}"></script>

<!-- Cross-browser responsivity scripts -->
<!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
<![endif]-->

<!-- Google Analitycs JS -->
<script type="text/javascript" src="{{asset('tribu/js/wpx.ga.js')}}"></script>


<!-- App JS -->
<script src="{{asset('tribu/js/wpx.app.js')}}"></script>

<script type="text/javascript" src="{{asset('tribu/assets/nouislider/js/nouislider.min.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        var resultado=[];
        var resultado1="";
        resultado2="";
        resultado3="";
        var resultadoFinal="";
        var resultado=0;
        //alert(resultado2);
        var snapSlider = document.getElementById('filterPrinceRange');
        var precio=parseFloat($('#maximo_precio').val());
        var minimo=parseFloat($('#minimo_precio').val());


        noUiSlider.create(snapSlider, {
            start: [minimo, precio ],

            connect: true,
            range: {
                'min': minimo,
                'max': precio
            }
        });


        var snapValues = [
            document.getElementById('filterPrinceValueLow'),
            document.getElementById('filterPrinceValueHigh')
        ];

        snapSlider.noUiSlider.on('update', function( values, handle ) {
             values[handle]=values[handle].replace(',','');
            resultado=parseFloat(values[handle]).toLocaleString('de-DE');
            snapValues[handle].innerHTML = resultado;
        });
    });
</script>

<script type="text/javascript">
    
$('#boton').on('click',function(){
    var precio_desde=$('#filterPrinceValueLow').text();
    var precio_hasta=$('#filterPrinceValueHigh').text();
    $('#precio_desde').val(precio_desde);
    $('#precio_hasta').val(precio_hasta);
   /*var precio_desde=$('#filterPrinceValueLow').text();
   var precio_hasta=$('#filterPrinceValueHigh').text();
   var nombre=$('#nombre').val();
   var tipo=$('#tipo').val();
   var operacion=$('#operaciones').val();
   var sector=$('#sector').val();
   var _token=$('#_token').val();
      $.ajax({                        
           type: "POST",                 
           url: '/invermaster/proyecto',                     
           data: {'precio_desde':precio_desde,'precio_hasta':precio_hasta,'nombre':nombre,'tipo':tipo,'operacion':operacion,'sector':sector,'_token':_token},
       success: function (result) {
        window.location.href = '/invermaster/proyecto';
           //do somthing here
      }
       });*/





})
</script>

<script src="{{asset('plugins/select2/select2.full.min.js')}}"></script>
<script type="text/javascript">
$(".select2").select2({
    theme: "bootstrap"
});
</script>
</body>
</html>