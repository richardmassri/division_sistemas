
<!DOCTYPE html>
<html>
<head>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="robots" content="index, follow">
<title>Invermaster</title>

<!-- Page loader -->
<script src="{{asset('tribu/assets/pace/js/pace.min.js')}}"></script>
<link rel="stylesheet" href="{{asset('tribu/assets/pace/css/pace-minimal.css')}}" type="text/css">

<!-- Bootstrap -->
<link rel="stylesheet" href="{{asset('tribu/assets/bootstrap/css/bootstrap.min.css')}}" type="text/css">

<!-- CSS animation library -->
<link rel="stylesheet" href="{{asset('tribu/assets/animate/animate.min.css')}}" type="text/css">

<link href="https://fonts.googleapis.com/css?family=Quicksand:400,500,700" rel="stylesheet">

<!-- Plugins -->
<link rel="stylesheet" href="{{asset('tribu/assets/bootstrap-select/css/bootstrap-select.min.css')}}">
<link rel="stylesheet" href="{{asset('tribu/assets/swiper/css/swiper.min.css')}}">

<!-- Light Gallery -->
<link rel="stylesheet" href="{{asset('tribu/assets/lightgallery/css/lightgallery.min.css')}}">

<!-- Icons -->
<link rel="stylesheet" href="{{asset('tribu/fonts/font-awesome/css/font-awesome.min.css')}}" type="text/css">
<link rel="stylesheet" href="{{asset('tribu/fonts/ionicons/css/ionicons.min.css')}}" type="text/css">
<link rel="stylesheet" href="{{asset('tribu/fonts/line-icons/line-icons.css')}}" type="text/css">
<link rel="stylesheet" href="{{asset('tribu/fonts/line-icons-pro/line-icons-pro.css')}}" type="text/css">

<!-- Global style (main) -->
<link id="stylesheet" type="text/css" href="{{asset('tribu/css/global-style.min.css')}}" rel="stylesheet" media="screen">
<!-- Custom style - Remove if not necessary -->
<link type="text/css" href="{{asset('tribu/css/custom-style.css')}}" rel="stylesheet">

<!-- Favicon -->
<link href="{{asset('tribu/images/favicon.png')}}" rel="icon" type="image/png">


</head>
<body>


<!-- MAIN WRAPPER -->
<div class="body-wrap" data-template-mode="cards">
    <div id="st-container" class="st-container">


<!-- MAIN WRAPPER -->
@include('plantillaCliente.plantilla_principal')

                <section class="slice sct-color-2">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-8 col-md-push-4">
                                    <div class="block-card-wrapper">
                                        <div class="block-card-section z-depth-1-bottom">
                                            <div class="block-card-section-inner">
                                                <div class="text-xs-center">
                                                    <h5 class="heading heading-xs c-gray-dark text-uppercase strong-500 letter-spacing-2 mb-0 mt-1">{{$postUsuario[0]->nombre_categoria}}</h5>

                                                    <span class="short-delimiter short-delimiter-center short-delimiter--style-1 short-delimiter-light short-delimiter-center short-delimiter-lg"></span>

                                                     <h5 class="">
                                                        {{$postUsuario[0]->nombre_post}}                                     
                                                        </h5>

                                                    <h3 class="heading heading-4 text-normal">
                                                        {{$postUsuario[0]->descripcion_breve}}                                     
                                                        </h3>

        
                                                </div>
                                            </div>

                                            <span class="clearfix"></span>

                                            <div class="block block-post">
                                                <div class="block-image mt-1">
                                                        <img src="{{asset($postUsuario[0]->foto_post)}}">
                                                </div>

                                                <div class="block-body block-post-body">
                                                    <p>
                                                        <?php echo $postUsuario[0]->descripcion;?>
                                                    </p>
                                                    <div class="tagcloud tagcloud--style-1 clearfix">
                                                    @foreach($posTags as $posTag)
                                                        <a href="#"><span>{{$posTag->nombre}}</span></a>
                                                    @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


      

               
                                </div>

                                <div class="col-md-4 col-md-pull-8">
                                    <div class="sidebar">
    <!-- About me -->
    <div class="sidebar-object">
        <div class="block-card-wrapper">
            <div class="block-card-section z-depth-1-bottom">
                <div class="block-card-section-inner">
                    <div class="card-section-title">
                        <h3 class="heading heading-6 strong-700 text-uppercase">
                            Sobre mi
                        </h3>
                    </div>

                    <div class="block">
                        <div class="block-image">
                            <img src="{{asset($postUsuario[0]->foto_usuario)}}" style="height: 300px; width: 300px;margin-left: auto;margin-right: auto;">
                        </div>

                        <div class="text-xs-center">
                            <h3 class="heading heading-5 strong-700 text-uppercase mt-2">
                                <a href="#">
                                    {{$postUsuario[0]->nombre_usuario}}
                                </a>
                            </h3>
                            <h4 class="heading heading-xs strong-400 text-uppercase c-gray-light letter-spacing-2 mt-1">
                                Consultor
                            </h4>
                            <p class="mt-2">
                                {{$postUsuario[0]->correo}}
                            </p>
                            <p class="mt-2">
                                {{$postUsuario[0]->telefono_celular}}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Facebook plugin -->

    <!-- Photo feed -->
</div>
                                </div>
                            </div>
                        </div>
                    </section>


@include('plantillaCliente.plantilla_principal_footer')   
</div>
            </div>
        </div><!-- END: st-pusher -->
    </div><!-- END: st-container -->
</div><!-- END: body-wrap -->

<!-- SCRIPTS -->
<a href="#" class="back-to-top"></a>

<!-- Required JSs -->

<!-- Essential JSs -->
<script src="{{asset('tribu/assets/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('tribu/js/bootstrap-extensions/bootstrap-dropdowneffect.js')}}"></script>
<script src="{{asset('tribu/js/bootstrap-extensions/bootstrap-validator.js')}}"></script>
<script src="{{asset('tribu/js/modernizr.custom.js')}}"></script>
<script src="{{asset('tribu/js/jquery.mousewheel-3.0.6.pack.js')}}"></script>
<script src="{{asset('tribu/js/jquery.easing.js')}}"></script>
<script src="{{asset('tribu/js/classie.js')}}"></script>
<script src="{{asset('tribu/js/slidebar/slidebar.js')}}"></script>

<!-- Assets - Required -->
<script src="{{asset('tribu/assets/headroom/headroom.min.js')}}"></script>
<script src="{{asset('tribu/assets/headroom/jquery.headroom.min.js')}}"></script>
<script src="{{asset('tribu/assets/footer-reveal/footer-reveal.min.js')}}"></script>
<script src="{{asset('tribu/assets/bootstrap-select/js/bootstrap-select.min.js')}}"></script>
<script src="{{asset('tribu/assets/swiper/js/swiper.min.js')}}"></script>
<script src="{{asset('tribu/assets/parallax/parallax.min.js')}}"></script>
<script src="{{asset('tribu/assets/viewport-checker/viewportchecker.min.js')}}"></script>
<script src="{{asset('tribu/assets/milestone-counter/jquery.countTo.js')}}"></script>

<!-- Light Gallery -->
<script src="{{asset('tribu/assets/lightgallery/js/lightgallery.min.js')}}"></script>
<script src="{{asset('tribu/assets/lightgallery/js/lg-thumbnail.min.js')}}"></script>
<script src="{{asset('tribu/assets/lightgallery/js/lg-video.js')}}"></script>

<!-- Cross-browser responsivity scripts -->
<!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
<![endif]-->

<!-- Google Analitycs JS -->
<script type="text/javascript" src="{{asset('tribu/js/wpx.ga.js')}}"></script>

<!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->


<!-- App JS -->
<script src="{{asset('tribu/js/wpx.app.js')}}"></script>

</body>
</html>
