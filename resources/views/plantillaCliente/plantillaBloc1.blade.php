
<!DOCTYPE html>
<html>
<head>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="robots" content="index, follow">
<title>Invermaster</title>

<!-- Page loader -->
<script src="{{asset('tribu/assets/pace/js/pace.min.js')}}"></script>
<link rel="stylesheet" href="{{asset('tribu/assets/pace/css/pace-minimal.css')}}" type="text/css">

<!-- Bootstrap -->
<link rel="stylesheet" href="{{asset('tribu/assets/bootstrap/css/bootstrap.min.css')}}" type="text/css">

<!-- CSS animation library -->
<link rel="stylesheet" href="{{asset('tribu/assets/animate/animate.min.css')}}" type="text/css">

<link href="https://fonts.googleapis.com/css?family=Quicksand:400,500,700" rel="stylesheet">

<!-- Plugins -->
<link rel="stylesheet" href="{{asset('tribu/assets/bootstrap-select/css/bootstrap-select.min.css')}}">
<link rel="stylesheet" href="{{asset('tribu/assets/swiper/css/swiper.min.css')}}">

<!-- Light Gallery -->
<link rel="stylesheet" href="{{asset('tribu/assets/lightgallery/css/lightgallery.min.css')}}">

<!-- Icons -->
<link rel="stylesheet" href="{{asset('tribu/fonts/font-awesome/css/font-awesome.min.css')}}" type="text/css">
<link rel="stylesheet" href="{{asset('tribu/fonts/ionicons/css/ionicons.min.css')}}" type="text/css">
<link rel="stylesheet" href="{{asset('tribu/fonts/line-icons/line-icons.css')}}" type="text/css">
<link rel="stylesheet" href="{{asset('tribu/fonts/line-icons-pro/line-icons-pro.css')}}" type="text/css">

<!-- Global style (main) -->
<link id="stylesheet" type="text/css" href="{{asset('tribu/css/global-style.min.css')}}" rel="stylesheet" media="screen">
<!-- Custom style - Remove if not necessary -->
<link type="text/css" href="{{asset('tribu/css/custom-style.css')}}" rel="stylesheet">

<!-- Favicon -->
<link href="{{asset('tribu/images/favicon.png')}}" rel="icon" type="image/png">


<!-- RS5.0 -->
<link rel="stylesheet" type="text/css" href="{{asset('tribu/assets/revolution/css/settings.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('tribu/assets/revolution/css/layers.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('tribu/assets/revolution/css/navigation.css')}}">

</head>
<body>
@include('plantillaCliente.plantilla_principal')

                    <!-- REVOLUTION SLIDER -->
 <div id="rev_slider_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="media-carousel-autoplay30" data-source="gallery" style="margin:0px auto;background-color:#26292b;padding:0px;margin-top:0px;margin-bottom:0px;">
<!-- START REVOLUTION SLIDER 5.3.0.2 fullwidth mode -->
	<div id="rev_slider_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.3.0.2">
		<ul>

		@foreach ($consultaPost as $foto)
			<!-- SLIDE  -->
			<!-- SLIDE  -->
			<li data-index="rs-2" data-transition="fade" data-slotamount="7" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="Power3.easeInOut" data-easeout="Power3.easeInOut" data-masterspeed="1500"  data-thumb="{{asset('asset($foto->foto)')}}"  data-rotate="0"  data-saveperformance="off"  data-title="Calbuco" data-param1="Vimeo Video" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
				<!-- MAIN IMAGE -->
				<img src="{{asset($foto->foto)}}"  data-bgposition="center center" data-bgfit="cover" class="rev-slidebg" data-no-retina>

				<!-- BACKGROUND VIDEO LAYER -->
				<!-- LAYER NR. 3 -->

			</li>

		@endforeach

		</ul>
		<div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
	</div>
</div><!-- END REVOLUTION SLIDER -->

                    <section class="sct-color-2">
                        <div class="container">
                            <span class="space-xs-md"></span>
                            <!-- Section title -->
                            <div class="section-title section-title--style-1 text-xs-center mb-0">
                                <h3 class="section-title-inner heading-6 strong-600 text-uppercase">
                                    Últimas publicaciones
                                </h3>
                            </div>

                            <!-- Articles -->
                            
	                            <div class="mt-2 py-2 b-xs-top">
	                                <div class="row">
	                                <?php $i=0;?>
	                                @foreach ($consultaPost as $consulta)
	                                <?php $i++;?>
	                                    @if($i==4)
	                                    <?php $i=0;?>
	                                    <div class="row"> </div>
	                                    	<br>
	                                    @endif
	                                    <div class="col-md-4">
	                                        <div class="block block--style-3 z-depth-1-bottom">
	                                            <div class="block-image">
	                                             <img src="{{asset($consulta->foto)}}">
	                                            </div>
	                                            <div class="block-body text-xs-center">
	                                                <h5 class="heading heading-xs c-gray-light text-uppercase strong-500 letter-spacing-2 mb-0">{{$consulta->nombre_categoria}}</h5>

	                                                <span class="short-delimiter short-delimiter--style-1 short-delimiter-light  short-delimiter-center short-delimiter-lg"></span>

	                                                <h3 class="">
	                                                    <a href="{{asset('plantilla/block/'.$consulta->id)}}">
	                                                        {{$consulta->nombre_post}} 
	                                                     </a>
	                                                </h3>

	                                                <h3 class="heading heading-6 strong-500 text-normal">
	                                                    <a href="{{asset('plantilla/block/'.$consulta->id)}}">
	                                                        {{$consulta->descripcion_breve}} 
	                                                     </a>
	                                                </h3>
	                                            </div>
	                                        </div>
	                                    </div>

	 
	                                    
	                                @endforeach
	                                </div>
	                            </div>
	                            
                        </div>
                    </section>


                                    <span class="space-xs-md"></span>

                                    <!-- Pagination -->
                                   <div class="pagination-wrapper mt-3">
                                        <ul class="pagination pagination--style-2 pagination-lg">
                                        
                                            <li>{!! $consultaPost->render()!!}</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>

                    <!-- FOOTER -->

    @include('plantillaCliente.plantilla_principal_footer')

                </div>
            </div>
        </div><!-- END: st-pusher -->
    </div><!-- END: st-container -->
</div><!-- END: body-wrap -->

<!-- SCRIPTS -->
<a href="#" class="back-to-top"></a>

<!-- Required JSs -->

<!-- Essential JSs -->
<script src="{{asset('tribu/assets/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('tribu/js/bootstrap-extensions/bootstrap-dropdowneffect.js')}}"></script>
<script src="{{asset('tribu/js/bootstrap-extensions/bootstrap-validator.js')}}"></script>
<script src="{{asset('tribu/js/modernizr.custom.js')}}"></script>
<script src="{{asset('tribu/js/jquery.mousewheel-3.0.6.pack.js')}}"></script>
<script src="{{asset('tribu/js/jquery.easing.js')}}"></script>
<script src="{{asset('tribu/js/classie.js')}}"></script>
<script src="{{asset('tribu/js/slidebar/slidebar.js')}}"></script>

<!-- Assets - Required -->
<script src="{{asset('tribu/assets/headroom/headroom.min.js')}}"></script>
<script src="{{asset('tribu/assets/headroom/jquery.headroom.min.js')}}"></script>
<script src="{{asset('tribu/assets/footer-reveal/footer-reveal.min.js')}}"></script>
<script src="{{asset('tribu/assets/bootstrap-select/js/bootstrap-select.min.js')}}"></script>
<script src="{{asset('tribu/assets/swiper/js/swiper.min.js')}}"></script>
<script src="{{asset('tribu/assets/parallax/parallax.min.js')}}"></script>
<script src="{{asset('tribu/assets/viewport-checker/viewportchecker.min.js')}}"></script>
<script src="{{asset('tribu/assets/milestone-counter/jquery.countTo.js')}}"></script>

<!-- Light Gallery -->
<script src="{{asset('tribu/assets/lightgallery/js/lightgallery.min.js')}}"></script>
<script src="{{asset('tribu/assets/lightgallery/js/lg-thumbnail.min.js')}}"></script>
<script src="{{asset('tribu/assets/lightgallery/js/lg-video.js')}}"></script>

<!-- Cross-browser responsivity scripts -->
<!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
<![endif]-->

<!-- Google Analitycs JS -->
<script type="text/javascript" src="{{asset('tribu/js/wpx.ga.js')}}"></script>


<!-- RS5.0 Core JS Files -->
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/jquery.themepunch.tools.min.js?rev=5.0')}}"></script>
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/jquery.themepunch.revolution.min.js?rev=5.0')}}"></script>
<script type="text/javascript" src="{{asset('tribu/js/revolution/revolution-slider-blog-cards-1.js')}}"></script>
<!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/extensions/revolution.extension.actions.min.js')}}"></script>
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/extensions/revolution.extension.carousel.min.js')}}"></script>
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/extensions/revolution.extension.kenburn.min.js')}}"></script>
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/extensions/revolution.extension.layeranimation.min.js')}}"></script>
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/extensions/revolution.extension.migration.min.js')}}"></script>
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/extensions/revolution.extension.navigation.min.js')}}"></script>
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/extensions/revolution.extension.parallax.min.js')}}"></script>
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/extensions/revolution.extension.slideanims.min.js')}}"></script>
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/extensions/revolution.extension.video.min.js')}}"></script>

<!-- App JS -->
<script src="{{asset('tribu/js/wpx.app.js')}}"></script>

</body>
</html>
