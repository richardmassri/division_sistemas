<!DOCTYPE html>
<html>
<head>


<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="robots" content="index, follow">
<title>Invermaster</title>
<style type="text/css">

.ui-dialog-titlebar-close {
    visibility: hidden;
}

.ui-dialog > .ui-widget-header {background: white;}

    /*.ui-dialog .ui-dialog-titlebar {
    background:transparent;
    border:none;
}*/
</style>


<!-- Page loader -->
<script src="{{asset('tribu/assets/pace/js/pace.min.js')}}"></script>
<link rel="stylesheet" href="{{asset('tribu/assets/pace/css/pace-minimal.css')}}" type="text/css">

<link rel="stylesheet" href="https://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css" />

<!-- Bootstrap -->
<link rel="stylesheet" href="{{asset('tribu/assets/bootstrap/css/bootstrap.min.css')}}" type="text/css">

<!-- CSS animation library -->
<link rel="stylesheet" href="{{asset('tribu/assets/animate/animate.min.css')}}" type="text/css">

<link href="https://fonts.googleapis.com/css?family=Quicksand:400,500,700" rel="stylesheet">

<!-- Plugins -->
<link rel="stylesheet" href="{{asset('tribu/assets/bootstrap-select/css/bootstrap-select.min.css')}}">
<link rel="stylesheet" href="{{asset('tribu/assets/swiper/css/swiper.min.css')}}">

<!-- Light Gallery -->
<link rel="stylesheet" href="{{asset('tribu/assets/lightgallery/css/lightgallery.min.css')}}">

<link rel="stylesheet" href="{{asset('plugins/select2/select2.min.css')}}">

<!-- Icons -->
<link rel="stylesheet" href="{{asset('tribu/fonts/font-awesome/css/font-awesome.min.css')}}" type="text/css">
<link rel="stylesheet" href="{{asset('tribu/fonts/ionicons/css/ionicons.min.css')}}" type="text/css">
<link rel="stylesheet" href="{{asset('tribu/fonts/line-icons/line-icons.css')}}" type="text/css">
<link rel="stylesheet" href="{{asset('tribu/fonts/line-icons-pro/line-icons-pro.css')}}" type="text/css">

<!-- Global style (main) -->
<link id="stylesheet" type="text/css" href="{{asset('tribu/css/global-style.min.css')}}" rel="stylesheet" media="screen">
<!-- Custom style - Remove if not necessary -->
<link type="text/css" href="{{asset('tribu/css/custom-style.css')}}" rel="stylesheet">

<!-- Favicon -->
<link href="{{asset('tribu/images/logo/logo.png')}}" rel="icon" type="image/png">


<!-- RS5.0 -->
<link rel="stylesheet" type="text/css" href="{{asset('tribu/assets/revolution/css/settings.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('tribu/assets/revolution/css/layers.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('tribu/assets/revolution/css/navigation.css')}}">

<!-- Cube Portfolio -->
<link rel="stylesheet" href="{{asset('tribu/assets/cubeportfolio/css/cubeportfolio.min.css')}}">

</head>
<body>


<!-- MAIN WRAPPER -->
@include('plantillaCliente.plantilla_principal')

    <div class="container">
    <br>
                            <div class="section-title section-title--style-1 text-xs-center">
                                <h3 class="section-title-inner heading-3 strong-600">
                                    Contactar a un agente
                                </h3>
                            </div>
                            <div id="mensajeDiv" class="alert alert-success hidden">
                            <strong class="box-title" id="contenidoMensaje"></strong>
                            </div>
                            <br>
                            <div class="row">
                            @foreach($consultaEquipo as $consultaEquipo)
                                <div class="col-md-4 col-sm-6" onclick="contactarAgente('<?php echo $consultaEquipo->id;?>','<?php echo $consultaEquipo->nombre." ".$consultaEquipo->apellido;?>')">
                                    <div class="block block--style-3 z-depth-1-bottom z-depth-3--hover">
                                        <div class="block block-image">
                                            <div class="view view-second">
                                                <img src="{{asset($consultaEquipo->foto)}}" style="height: 300px; width: 300px; border-radius: 50%;margin-left: auto;margin-right: auto;">
                                                <div class="mask mask-base--style-2">
                          
                                                </div>
                                            </div>
                                        </div>
                                        <div class="block-body" style="height: 260px;">
                                        <h4 class="heading heading-xs strong-400 text-uppercase letter-spacing-2 c-gray-light"> Agente inmobiliario </h4>
                                            <h3 class="heading heading-5">{{$consultaEquipo->nombre}} {{$consultaEquipo->apellido}}</h3>
                                            <p class="mt-1">
                                            <i class="fa fa-phone"> </i>
                                                {{$consultaEquipo->telefono_celular}}
                                            </p>
                                            <p class="mt-1">
                                            <i class="fa fa-envelope"> </i>             
                                                {{$consultaEquipo->correo}}
                                            </p>
                                            <p class="mt-1 btn btn-block btn-base-2" onclick="contactarAgente(<?php echo $consultaEquipo->id;?>)">
                                                Contactar    
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
    @include('plantillaCliente.plantilla_principal_footer')

<div id="dialog-confirm" class="hidden">
    <div class="sidebar-object">
        <div class="card z-depth-2-top">

  
       <div class="col-sm-12">
       <br>
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                <input type="text" class="form-control" id="nombreContacto" placeholder="Nombre y Apellido"> </input>
            </div>
            <label class="col-md-12 nombreContactoMensaje hidden text-danger"> Debe ingresar el nombre </label>
            <br>

            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                <input type="text" class="form-control" id="emailContacto" placeholder="Email"> </input>
            </div>
            <label class="col-md-12 nombreContactoEmail hidden text-danger"> Debe ingresar el email </label>
            <br>
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                <input type="text" class="form-control" id="telefonoContacto" placeholder="Teléfono "> </input>
            </div>
            <label class="col-md-12 telefonoContactoMensaje hidden text-danger"> Debe ingresar el teléfono </label>
            <br>
            <div>
             <textarea class="form-control" cols="20" rows="10" id="mensajeContacto" placeholder="Mensaje"></textarea>
             </div>

            <img class="hidden" id="loading" src="{{asset('tribu/images/loading.gif')}}">

            <label class="col-md-12 telefonoContactoMensajeText hidden text-danger"> Debe ingresar el mensaje </label> 
            <br>
        </div>  
    </div>
</div>
</div>

<input type="text" class="hidden" name="_token" id="token" value="{{ csrf_token() }}"></input>


<!--Finnnnnnnnnnnnnnnnnnnnnnnnnn -->

<!-- Required JSs -->

    <script>

    function validarFormuSolicitud(nombre,email,telefono,mensaje){
        var comprobar=0;
        if(nombre==''){
            comprobar=1;
            $(".nombreContactoMensaje").removeClass('hidden');
        }else{
            $(".nombreContactoMensaje").addClass('hidden');
        }
        if(email==''){
            comprobar=1;
            $(".nombreContactoEmail").removeClass('hidden');
        }else{
            $(".nombreContactoEmail").addClass('hidden');
        }
        
        if(telefono==''){
            comprobar=1;
            $(".telefonoContactoMensaje").removeClass('hidden');
        }else{
            $(".telefonoContactoMensaje").addClass('hidden');
        }
        if(mensaje==''){
            comprobar=1;
            $(".telefonoContactoMensajeText").removeClass('hidden');
        }else{
            $(".telefonoContactoMensajeText").addClass('hidden');
        }
        return comprobar;
    }
    function contactarAgente(id,nombre_apelido_contacto){
    $( "#dialog-confirm" ).removeClass('hidden');
      var _token = $("#token").val();
      //var bombre_apellido_contacto=$('.nombre_apelido_contacto').html();
    $( "#dialog-confirm" ).dialog({
            resizable: false,
            modal: true,
            title:"Contactar con "+nombre_apelido_contacto,
            width:700,
            height:600,
            buttons:[
                        {
                            class: 'btn btn-danger',
                            text: "Cancelar",
                            click: function() {
                                $("#dialog-confirm").dialog("close");
                                //$( this ).dialog( "close" );
                            }
                        },
                        {
                            class: 'btn btn-base-2 enviar_mensaje',
                            text: "Enviar",
                            click: function() { 
                                var tipo = "usuario";
                                var _token = $("#token").val();
                                var usuario_id = id;
                                var nombre = $("#nombreContacto").val();
                                var email = $("#emailContacto").val();
                                var telefono = $("#telefonoContacto").val();
                                var mensaje = $("#mensajeContacto").val();
                                var comprobarValor=validarFormuSolicitud(nombre,email,telefono,mensaje);
                                if(comprobarValor==0){
                                    $('body, html,div').animate({ scrollTop: $("#dialog-confirm").offset().top }, 800);
                                    $('#loading').removeClass('hidden');
                                    $('.enviar_mensaje').addClass('hidden');
                                    $.ajax({
                                        type: "POST",
                                        url: "{{asset('invermaster/contacto')}}",
                                        data: { usuario_id: usuario_id,_token:_token,tipo:tipo,nombre:nombre,email:email,telefono:telefono,mensaje:mensaje},
                                        dataType: "json",
                                        success: function (resultado) {
                                            $( "#dialog-confirm" ).addClass('hidden');
                                            if(resultado['statusCode']=='exito'){
                                                $('#loading').addClass('hidden');
                                                $( ".enviar_mensaje" ).removeClass('hidden');
                                                $("#proyecto_id").val('');
                                                $("#nombreContacto").val('');
                                                $("#emailContacto").val('');
                                                $("#telefonoContacto").val('');
                                                $("#mensajeContacto").val('');
                                                $('#mensajeDiv').removeClass('hidden');
                                                $('#contenidoMensaje').html(resultado['menaje']);
                                                $("#dialog-confirm").dialog("close");
                                                setTimeout(function() {
                                                    $('#mensajeDiv').addClass('hidden');
                                                }, 10000);
                                            }
                                        }
                                    });
                                }
                            }
    
                        }
                    ]
        });
  }
  </script>




<!-- Essential JSs -->
<script src="{{asset('tribu/assets/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('tribu/js/bootstrap-extensions/bootstrap-dropdowneffect.js')}}"></script>
<script src="{{asset('tribu/js/bootstrap-extensions/bootstrap-validator.js')}}"></script>
<script src="{{asset('tribu/js/modernizr.custom.js')}}"></script>
<script src="{{asset('tribu/js/jquery.mousewheel-3.0.6.pack.js')}}"></script>
<script src="{{asset('tribu/js/jquery.easing.js')}}"></script>
<script src="{{asset('tribu/js/classie.js')}}"></script>
<script src="{{asset('tribu/js/slidebar/slidebar.js')}}"></script>

<!-- Assets - Required -->
<script src="{{asset('tribu/assets/headroom/headroom.min.js')}}"></script>
<script src="{{asset('tribu/assets/headroom/jquery.headroom.min.js')}}"></script>
<script src="{{asset('tribu/assets/footer-reveal/footer-reveal.min.js')}}"></script>
<script src="{{asset('tribu/assets/bootstrap-select/js/bootstrap-select.min.js')}}"></script>
<script src="{{asset('tribu/assets/swiper/js/swiper.min.js')}}"></script>
<script src="{{asset('tribu/assets/parallax/parallax.min.js')}}"></script>
<script src="{{asset('tribu/assets/viewport-checker/viewportchecker.min.js')}}"></script>
<script src="{{asset('tribu/assets/milestone-counter/jquery.countTo.js')}}"></script>

<!-- Light Gallery -->
<script src="{{asset('tribu/assets/lightgallery/js/lightgallery.min.js')}}"></script>
<script src="{{asset('tribu/assets/lightgallery/js/lg-thumbnail.min.js')}}"></script>
<script src="{{asset('tribu/assets/lightgallery/js/lg-video.js')}}"></script>

<!-- Cross-browser responsivity scripts -->
<!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
<![endif]-->

<!-- Google Analitycs JS -->
<script type="text/javascript" src="{{asset('tribu/js/wpx.ga.js')}}"></script>


<!-- RS5.0 Core JS Files -->
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/jquery.themepunch.tools.min.js?rev=5.0')}}"></script>
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/jquery.themepunch.revolution.min.js?rev=5')}}"></script>
<script type="text/javascript" src="{{asset('tribu/js/revolution/revolution-slider-real-estate-1.js')}}"></script>
<!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/extensions/revolution.extension.actions.min.js')}}"></script>
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/extensions/revolution.extension.carousel.min.js')}}"></script>
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/extensions/revolution.extension.kenburn.min.js')}}"></script>
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/extensions/revolution.extension.layeranimation.min.js')}}"></script>
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/extensions/revolution.extension.migration.min.js')}}"></script>
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/extensions/revolution.extension.navigation.min.js')}}"></script>
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/extensions/revolution.extension.parallax.min.js')}}"></script>
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/extensions/revolution.extension.slideanims.min.js')}}"></script>
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/extensions/revolution.extension.video.min.js')}}"></script>

<!-- Cube Portfolio -->
<script src="{{asset('tribu/assets/cubeportfolio/js/jquery.cubeportfolio.min.js')}}"></script>
<script src="{{asset('tribu/assets/cubeportfolio/js/main.4-col.space.grid.js')}}"></script>


<!-- App JS -->
<script src="{{asset('tribu/js/wpx.app.js')}}"></script>

</script>
</body>
</html>

