@extends("panelAdmin")
@section('css')

<!-- Estilo para select Multiple -->
<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.0.2/css/bootstrap.min.css">
<link rel="stylesheet" href="{{asset('bootstrap/css/bootstrap-multiselect.css')}}" type="text/css">

<!-- Fin __________________ -->

@stop
@section('content')

<div class="box-header with-border">
   <h3 class="box-title">Crear Nueva Propiedad</h3>
</div>

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<?php 
  if(isset($descripcion)){?>
    <div class="<?php echo $descripcion['clase'];?>">
      <?php echo $descripcion['mensaje'];?>
    </div>
<?php
  }
?>    
<form action="{{asset('admin/propiedad')}}" method="post" name="vieja" accept-charset="UTF-8">
  <div class="box-body">

            <div class="col-md-12">
                <div class="col-md-4">
                <label class="col-md-12"> Nombre </label>
                  <input class="form-control col-md-4" placeholder="Nombre" name="nombre" type="text" maxlength="200" value="{{ old('nombre') }}" required="required">
                </div>

                  <div class="col-md-4">
                  <label class="col-md-12"> Metros Cuadrados </label>
                  <input class="form-control col-md-4" type="number" placeholder="Metros Cuadrados" name="metros_cuadrado" maxlength="30" value="{{ old('metros_cuadrado') }}" required="required" onkeypress="return validaNumero(event)">
                </div>

                 <div class="col-md-4">
                 <label class="col-md-12"> Descripción Breve </label>
                  <textarea class="form-control col-md-4" name="descripcion_breve" placeholder="Descripción Breve" cols="40" rows="5" value="{{ old('descripcion_breve') }}" required="required" maxlength="107"></textarea>
                </div>


              </div>

                <div class="row"></div>
                <br>


              <div class="col-md-12">



              <div class="col-md-4">
              <label class="col-md-12"> Dormitorio/s </label>
                 <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-fw fa-hospital-o"></i>
                      </div>
                    <input class="form-control col-md-4" placeholder="Dormitorio/s" name="dormitorio" type="number" maxlength="50" value="{{ old('dormitorio') }}" onkeypress="return validaNumero(event)">
                  </div>
                </div>

  

                <div class="col-md-4">
                <label class="col-md-12"> Parqueo/s </label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-fw fa-car"></i>
                    </div>
                  <input class="form-control col-md-4" type="number" placeholder="Parqueo/s" name="estacionamiento" maxlength="30" value="{{ old('estacionamiento') }}" required="required" onkeypress="return validaNumero(event)">
                  </div>
                </div>

                <div class="col-md-4">
                <label class="col-md-12"> Baños </label>
                     <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-fw fa-tint"></i>
                    </div>
                  <input class="form-control col-md-4" name="banios"  type="number" placeholder="Baños"  maxlength="50" value="{{ old('banios') }}" required="required" onkeypress="return validaNumero(event)">
                  </div>
                </div>
                  <!-- /.input group -->



              </div>


                <div class="row">
                </div>
                <br>

                  <div class="col-md-12">
                    <div class="col-md-4">
                    <label class="col-md-12"> Fecha Inicial </label>
                      <input class="form-control col-md-4 datepicker" placeholder="Fecha Inicial" name="fecha_inicial" type="text" maxlength="50" value="{{ old('fecha_inicial') }}" required="required">
                    </div>
                    <div class="col-md-4">
                    <label class="col-md-12"> Fecha Final </label>
                        <input class="form-control col-md-4 datepicker" placeholder="Fecha Final" name="fecha_final" type="text" maxlength="50" value="{{ old('fecha_final')}}" required="required">
                      </div>

                      @if($es_agente=="SI")
                        <div class="form-group col-md-4">
                        <label class="col-md-12"> Agente </label>
                          <select class="col-md-4 form-control select2 select2-hidden-accessible" name="agente_id" required="required" readonly >
                          <option value="{{$usuario_id}}">{{$usuario}}</option>
                          </select>
                        </div>
                      @else
                        <div class="form-group col-md-4">
                        <label class="col-md-12"> Agente </label>
                          <select class="col-md-4 form-control select2 select2-hidden-accessible" name="agente_id" required="required" value="{{ old('agente_id')}}">
                          <option value="">Seleccione el Agente</option>
                           @foreach($agentes as $agente):
                              <option value="{{ $agente->id }}">{{$agente->nombre}}</option>
                            @endforeach
                          </select>
                        </div>
                    @endif
                  </div>
                  <div class="row">
                  </div>
                <br>

              <div class="col-md-12">
              <div class="col-md-4">
                <label class="col-md-12"> precio Usd </label>
                  <input class="form-control col-md-4" placeholder="precio Usd" name="precio_en_dolares" id="precio_en_dolares" type="text" maxlength="50" value="{{ old('precio_en_dolares') }}" onkeypress="return validaNumero(event)">
                </div>

              <div class="col-md-4">
                <label class="col-md-12"> Tasa </label>
                  <input class="form-control col-md-4" placeholder="Tasa" name="tasa" id="tasa" type="text" maxlength="50" value="{{ old('tasa') }}" required="required" onkeypress="return validaNumero(event)">
                </div>

                <div class="col-md-4">
                <label class="col-md-12"> Peso/s </label>
                  <input class="form-control col-md-4" name="precio"  type="text" placeholder="Peso"  maxlength="50" value="{{ old('precio_desde') }}" id="precio" required="required" onkeypress="return validaNumero(event)" readonly="readonly">
                </div>

                </div>
                <div class="row"> </div>
                <br>


              <div class="col-md-12">

                  <div class="form-group col-md-4">
                  <label class="col-md-12"> Tipo </label>
                    <select class="col-md-4 form-control select2 select2-hidden-accessible" style="width: 100%;" tabindex="-1" aria-hidden="true" name="tipo_id" required="required">
                    <option value="">Seleccione el Tipo</option>
                     @foreach($tipos as $tipo)
                      <option value="{{ $tipo->id }}"> {{$tipo->nombre}}</option>
                      @endforeach
                    </select>
                  </div>

                 <div class="col-md-4 form-group">
                 <label class="col-md-12"> Caracteristica </label>
                        <select id="demo" name="caracteristica[]" multiple="multiple" class="col-md-4 form-control">
                        @foreach($caracteristica as $caracteristica)
                        <option value="{{$caracteristica->id}}">
                        <?php if(!empty($caracteristica->icono)){?>
                         &lt;img src="{{asset($caracteristica->icono)}}" width="10%" height="10%" class=&quot;fa fa-user&quot;&gt;&lt;/i&gt; <?php }?>  {{$caracteristica->nombre}} </option>
                        }
                        }
                        @endforeach
                        </select>
                    </div>


                <div class="form-group col-md-4">
                <label class="col-md-12"> País </label>
                    <select class="col-md-4 form-control select2 select2-hidden-accessible" style="width: 100%;" tabindex="-1" aria-hidden="true" name="pais_id" id="pais" required="required">
                    <option value="">País --Seleccione --</option>
                     @foreach($pais as $pais)
                      <option value="{{ $pais->id }}"> {{$pais->nombre}}</option>
                      @endforeach
                    </select>
                  </div>
                <br>
              </div>

                <div class="col-md-12">
                  <div class="form-group col-md-4">
                  <label class="col-md-12"> Ciudad </label>
                  <select class="col-md-4 form-control" name="ciudad_id" id="ciudad" required="required" disabled>
                  <option  value="">Ciudad --Seleccione--</option>
                  </select>
                </div>
                


                  <div class="form-group col-md-4">
                  <label class="col-md-12"> Sector </label>
                  <select class="col-md-4 form-control" name="sector_id" id="sector" required="required" disabled>
                   <option  value="">Seleccione el sector</option>
                  </select>
                </div>

                  <div class="col-md-4 form-group">
                  <label class="col-md-12"> Operaciones </label>
                      <select id="operaciones" name="operaciones[]" multiple="multiple" class="col-md-4 form-control" required="required">
                      @foreach($operaciones as $operaciones)
                      <option value="{{$operaciones->id}}">{{$operaciones->nombre}} </option>
                      @endforeach
                      </select>
                  </div>
                </div>
                      <br>

                <!-- Select Multiple -->
                  <!-- Fin del selct Multiple -->
                <div class="col-md-12">
                <div class="col-md-4 form-group">
                <label class="col-md-12"> Video </label>
                    <input class="form-control col-md-4" name="video" placeholder="Video" type="text" maxlength="200" value="{{ old('video') }}">
                  <!-- /.input group -->
                </div>
                  <div class="col-md-4 form-group">
                  <label class="col-md-12"> Mantenimiento </label>
                    <input class="form-control col-md-4" name="mantenimiento" placeholder="Mantenimiento" type="number" maxlength="200" value="{{ old('mantenimeinto') }}" required="required" onkeypress="return validaNumero(event)">
                  <!-- /.input group -->
                </div>
                <div class="col-md-4 form-group">
                <label class="col-md-12"> Estatus de Invermaster </label>
                      <select class="col-md-4 form-control" name="estatus_invermaste" required="required" value="{{ old('estatus_invermaste') }}">
                       <option  value="">Estatus de invermaster</option>
                       <option value="A">Activo</option>
                       <option value="I">Inactivo</option>
                      </select>
                    </div>
              </div>
              <div class="row"> </div>
                    <br>
                <div class="col-md-12"> 
                <div class="col-md-4">
                <label class="col-md-12"> antigüedad </label>
                  <input class="form-control col-md-4" type="text" placeholder="Antigüedad" name="antiguedad" maxlength="30" value="{{ old('antiguedad') }}" required="required">
                </div>



                <div class="form-group col-md-4">
                <label class="col-md-12"> Fecha de la Publicación </label>
                      <input class="form-control col-md-4 datepicker" name="fecha_publicacion" placeholder="Fecha de la Publicación" type="text" maxlength="11" value="{{ old('fecha_publicacion') }}" required="required">
                  <!-- /.input group -->
                </div>
              </div>
                  <div class="row"> </div>
                  <br>

                <div class="col-md-12">
                <div class="form-group col-md-8">
                <label class="col-md-12"> Ubicación </label>
                  <input class="col-md-10 form-control" name="ubicacion" placeholder="Ubicacion" type="text" maxlength="500" value="{{ old('ubicacion') }}" required="required" id="address">
                </div>
                <div class="col-md-4" style="padding:25px;">
                  <input class="btn  btn-info text-right" id="search" type="button" value ="Buscar"/>
                  </div>
                </div>
                  <br>                    
                  
                    <div class="clear"></div>
                    <div class="col-md-12 container">
                    <div id="map_canvas" class="col-md-12" style="width:800px;
      height:400px;"></div>
                    </div>
              <input type="text" class="hidden" id="latitud" name="latitud">
              <input type="text" class="hidden" id="longitud" name="longitud">
              <br>




              <!-- Descripcion con editor de texto -->
                <div class="col-md-12">
                  <section class="content">
                      <div class="row">
                        <div class="col-md-12">
                          <div class="box box-info">
                            <div class="box-header">
                              <h3 class="box-title">Descripción
                              </h3>
                              <!-- tools box -->
                              <div class="pull-right box-tools">
                                <button type="button" class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                  <i class="fa fa-minus"></i></button>
                                <button type="button" class="btn btn-info btn-sm" data-widget="remove" data-toggle="tooltip" title="Remove">
                                  <i class="fa fa-times"></i></button>
                              </div>
                              <!-- /. tools -->
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body pad">
                                    <textarea required="required" id="descripcion" name="descripcion" rows="10" cols="80">
                                    </textarea>
                            </div>
                          </div>
                          <!-- /.box -->

                        </div>
                        <!-- /.col-->
                      </div>
                      <!-- ./row -->
                    </section>
                </div>
            <!-- Fin  del editor de texto -->


              <input type="text" class="hidden" id="token" name="_token" value="{{ csrf_token() }}"></input>


                <div class="row"></div>

                <br>

              <div class="col-md-12"> 

                <div class="col-md-4">
                  <a href="{{asset('admin/propiedad')}}" class="btn btn-danger">Volver </a>
               </div>

                <div class="col-md-4">
               </div>
               <div class="col-md-4 text-right">
               <button class="btn  btn-success text-right" id="botonGuardar">Guardar</button>
               </div>
              </div>


                <!-- /input-group -->
              </div>
            </form>



<?php if(isset($descripcion)){?>

 <div class="box-body">
  <div class="col-md-12">
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">

<link rel="stylesheet" href="//blueimp.github.io/Gallery/css/blueimp-gallery.min.css">
<!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
<link rel="stylesheet" href="{{asset('upload/css/jquery.fileupload.css')}}">
<link rel="stylesheet" href="{{asset('upload/css/jquery.fileupload-ui.css')}}">
<!-- CSS adjustments for browsers with JavaScript disabled -->
<noscript><link rel="stylesheet" href="{{asset('upload/css/jquery.fileupload-noscript.css')}}"></noscript>
<noscript><link rel="stylesheet" href="{{asset('upload/css/jquery.fileupload-ui-noscript.css')}}"></noscript>
</head>
<body>
    <!-- The file upload form used as target for the file upload widget -->
    <form id="fileupload" action="server/php/" method="POST" enctype="multipart/form-data">
        <!-- Redirect browsers with JavaScript disabled to the origin page -->
        <noscript><input type="hidden" name="redirect" value="https://blueimp.github.io/jQuery-File-Upload/"></noscript>
        <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
        <div class="row fileupload-buttonbar">
          <div class="col-md-12">
            <div class="alert alert-info alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-info"></i> Alerta!</h4>
                    Los archivos a cargar deben contener un tamaño de 25.99 megabyte y deben contener la siguiente extensión: gif, jpg o png. 
            </div>
          </div>
            <div class="col-md-7">
                <!-- The fileinput-button span is used to style the file input field as button -->
                <span class="btn btn-success fileinput-button">
                    <i class="glyphicon glyphicon-plus"></i>
                    <span>Agregar Archivo...</span>
                    <input type="file" class="files[]" name="files[]" multiple>
                    <input type="text" class="hidden" name="_token" value="{{ csrf_token() }}"></input>
                </span>
                <button type="submit" class="btn btn-primary start startP hidden" id="botonStart">
                    <i class="glyphicon glyphicon-upload"></i>
                    <span>Subir archivo</span>
                </button>
                <button type="reset" class="btn btn-warning cancel">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span>Cancelar Archivo</span>
                </button>
                <button type="button" class="btn btn-danger delete">
                    <i class="glyphicon glyphicon-trash"></i>
                    <span>Eliminar</span>
                </button>
                <br>
                Eliminar todo<input type="checkbox" class="toggle">
                <!-- The global file processing state -->
                <span class="fileupload-process"></span>


            </div>
            <br>

              <div class="col-md-4 form-group">
                <select name="propiedad" id="propiedad" class="col-md-4 form-control" readonly>
                    <option value=""> Seleccione el proyecto </option>
                @foreach($propiedad as $propiedad):
                    <option value="{{$propiedad->id}}" selected>
                        {{$propiedad->nombre}}
                    </option>
                @endforeach
                </select>
                </div>

            <!-- The global progress state -->
            <div class="col-lg-5 fileupload-progress fade">
                <!-- The global progress bar -->
                <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                    <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                </div>
                <!-- The extended global progress state -->
                <div class="progress-extended">&nbsp;</div>
            </div>
        </div>
        <!-- The table listing the files available for upload/download -->
        <table role="presentation" class="table table-striped"><tbody class="files"></tbody></table>
    </form>
</div>
<!-- The blueimp Gallery widget -->
<div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" data-filter=":even">
    <div class="slides"></div>
    <h3 class="title"></h3>
    <a class="prev">‹</a>
    <a class="next">›</a>
    <a class="close">×</a>
    <a class="play-pause"></a>
    <ol class="indicator"></ol>
</div>
<!-- The template to display files available for upload -->
<script id="template-upload" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-upload fade">
        <td>
            <span class="preview"></span>
        </td>
        <td>
            <p class="name">{%=file.name%}</p>
            <strong class="error text-danger"></strong>
        </td>
        <td>
            <p class="size">Procesar...</p>
            <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
        </td>

        <td>
          {% if (!i && !o.options.autoUpload) { %}
                <button class="btn btn-primary start hidden" disabled>
                    <i class="glyphicon glyphicon-upload"></i>
                    <span>Subir</span>
                </button>
            {% } %}
            {% if (!i) { %}
                <button class="btn btn-warning cancel">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span>Cancelar</span>
                </button>
               <input type="radio" name="destacado" class="destacado" id="destacado" onclick="habilitar()"  value="{%=file.name%}">Principal
                <input type="radio" name="destacadoPromocionl" class="destacadoPromocionl" id="destacadoPromocionl" onclick="habilitar()" value="{%=file.name%}">Promocional
            {% } %}
        </td>
    </tr>
{% } %}
</script>
<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-download fade">
        <td>
        <input type="checkbox" name="delete" value="1" class="toggle">
            <span class="preview">
                {% if (file.thumbnailUrl) { %}
                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}"></a>
                {% } %}
            </span>
        </td>
        <td>
            <p class="name">
                {% if (file.url) { %}
                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
                {% } else { %}
                    <span>{%=file.name%}</span>
                {% } %}
            </p>
            {% if (file.error) { %}
                <div><span class="label label-danger">Error</span> {%=file.error%}</div>
            {% } %}
        </td>
        <td>
            <span class="size">{%=o.formatFileSize(file.size)%}</span>
        </td>
        <td>
            {% if (file.deleteUrl) {  file.deleteUrl.replace('index.php', ''); %}
                <button class="btn btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%= file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                    <i class="glyphicon glyphicon-trash"></i>
                    <span>Eliminar</span>
                </button>
            {% } else { %}
                <button class="btn btn-warning cancel">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span>Cancelar</span>
                </button>

                     <button class="btn btn-warning cancel_all">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span>Cancelar2</span>
                </button>
            {% } %}
        </td>
    </tr>
{% } %}
</script>
</body>
</div>
</div>
<?php }?>

<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.1/jquery-ui.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.css" rel="stylesheet"/>

@stop

@section('scripts')



<script src="http://code.jquery.com/ui/1.10.1/jquery-ui.js"></script>
<script src="{{asset('js/javascripts/jquery.priceformat.js')}}"></script>
<script src="{{asset('js/javascripts/demo.js')}}"></script>
<script type="text/javascript">
  $('#precio_desde_en_dolares').priceFormat({
    prefix: 'USD ',
    centsSeparator: ',',
    thousandsSeparator: '.'
});


$('#precio_en_dolares').priceFormat({
    prefix: 'USD ',
    centsSeparator: ',',
    thousandsSeparator: '.'
});



</script>
<script>
var date = new Date().toJSON().slice(0, 10).split("-").reverse().join("-");
  //var fecha= new Date().toJSON().slice(0,10);
  $(".datepicker").val(date);

 $.datepicker.regional['es'] = {
 closeText: 'Cerrar',
 prevText: '< Ant',
 nextText: 'Sig >',
 currentText: 'Hoy',
 monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
 monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
 dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
 dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
 dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
 weekHeader: 'Sm',
 dateFormat: 'dd-mm-yy',
 firstDay: 1,
 isRTL: false,
 showMonthAfterYear: false,
 yearSuffix: ''
 };
 $.datepicker.setDefaults($.datepicker.regional['es']);
$(function () {
$(".datepicker").datepicker();
});
</script>
<!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
<!-- The Templates plugin is included to render the upload/download listings -->
<script src="//blueimp.github.io/JavaScript-Templates/js/tmpl.min.js"></script>
<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
<script src="//blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js"></script>
<!-- The Canvas to Blob plugin is included for image resizing functionality -->
<script src="//blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>
<!-- Bootstrap JS is not required, but included for the responsive demo navigation -->
<script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<!-- blueimp Gallery script -->
<script src="//blueimp.github.io/Gallery/js/jquery.blueimp-gallery.min.js"></script>
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="{{asset('upload/js/jquery.iframe-transport.js')}}"></script>
<!-- The basic File Upload plugin -->
<script src="{{asset('upload/js/jquery.fileupload.js')}}"></script>
<!-- The File Upload processing plugin -->
<script src="{{asset('upload/js/jquery.fileupload-process.js')}}"></script>
<!-- The File Upload image preview & resize plugin -->
<script src="{{asset('upload/js/jquery.fileupload-image.js')}}"></script>
<!-- The File Upload audio preview plugin -->
<script src="{{asset('upload/js/jquery.fileupload-audio.js')}}"></script>
<!-- The File Upload video preview plugin -->
<script src="{{asset('upload/js/jquery.fileupload-video.js')}}"></script>
<!-- The File Upload validation plugin -->
<script src="{{asset('upload/js/jquery.fileupload-validate.js')}}"></script>
<!-- The File Upload user interface plugin -->
<script src="{{asset('upload/js/jquery.fileupload-ui.js')}}"></script>
<!-- The main application script -->
<script src="{{asset('upload/js/main_propiedad.js')}}"></script>

<!-- Libreri para select Multiple -->
<script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.2/js/bootstrap.min.js"></script>
<script type="text/javascript" src="{{asset('bootstrap/js/bootstrap-multiselect.js')}}"></script>

<!-- Libreri para editores de texto -->
<script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
<script src="{{asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>


<!--Api de google -->
<script type="text/javascript">
      function myMap() {
      var latitud=$('#latitud').val();
      var longitud=$('#longitud').val();

      var myLatLng = {lat:parseFloat(latitud) , lng:parseFloat(longitud)};

  var map = new google.maps.Map(document.getElementById('map_canvas'), {
    zoom: 15,
    center: myLatLng,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  });

  var marker = new google.maps.Marker({
    position: myLatLng,
    map: map,
    title: 'Hello World!',
    draggable:true
  });
}
    </script>
<script type="text/javascript">
$('#search').on("click",function(){
  googleMaps();
})
function googleMaps(){
    ubicacion=$('#address').val();
    if(ubicacion==''){
      ubicacion='Bavaro';
      $('#address').val(ubicacion);
    }
    $.ajax({
    type: "GET",
    url: "https://maps.googleapis.com/maps/api/geocode/json",
    data: {
    address: ubicacion,
    key:'AIzaSyCTTHufNeM1zuZJYKrrtqJ1KUjqNM6_tWA' },
    dataType: "json",
    success: function (resultado) {
      var latitud=resultado['results'][0]['geometry']['location']['lat'];
      var longitud=resultado['results'][0]['geometry']['location']['lng'];
      $('#latitud').val(latitud);
      $('#longitud').val(longitud);
      myMap();
    }
    });
  }
  googleMaps();
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB8kPVK6nmaoLI0EoIm8KvgdoXp3ohWzLQ&callback=myMap"></script>
<!--<script>
</script>
 -->
<!-- Finnnnnnnnnnnnnnn -->

<!-- Script para editor de texto -->
<script>
  $("#tasa").on("blur",function(){
      var tasa=$("#tasa").val();
      var precio_en_dolares=0;
      var precio_desde=0;
      if(tasa!='' && $("#precio_en_dolares").val()!=''){
        precio_en_dolares=$("#precio_en_dolares").val().replace('.','').replace(',','.').replace('USD','');
        precio_desde=parseFloat(precio_en_dolares)*parseFloat(tasa);
        $("#precio").val(precio_desde.toLocaleString('de-DE'));
      }
  });

function habilitar(){
    if($(".destacado").is(':checked') && $(".destacadoPromocionl").is(':checked')) {
        $('.startP').removeClass('hidden');
    }
}
$("#botonStart").click(function(){
$('body, html').animate({ scrollTop: $("#botonStart").offset().top }, 800);
});
  $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('descripcion');
    //bootstrap WYSIHTML5 - text editor
    $(".textarea").wysihtml5();
  });
</script>

<script type="text/javascript">
$(document).ready(function() {
  $('#demo').multiselect({
    buttonWidth: '300px',
    enableHTML: true,
    nonSelectedText: 'Selecione la/s característica/s'
  });
    $('#operaciones').multiselect({
    buttonWidth: '300px',
    enableHTML: true,
    nonSelectedText: 'Selecione la/s operación/es',
  });
  });
</script>

<script>
function validaNumero(e){
    tecla = (document.all) ? e.keyCode : e.which;
    //Tecla de retroceso para borrar, siempre la permite
    if (tecla==8 || tecla==46){
        return true;
    }
    // Patron de entrada, en este caso solo acepta numeros
    patron =/^[+-]?\d+(\.\d+)?$/;
    tecla_final = String.fromCharCode(tecla);
    return patron.test(tecla_final);
}
</script>

<script type="text/javascript">
  
  //Validacion de boton subir en upload
</script>

<script type="text/javascript">
<?php if(isset($descripcion)){?>
$('body, html').animate({ scrollTop: $("#botonGuardar").offset().top }, 800);
<?php }?>
</script>
<script>
$(document).ready(function(){
    $("#pais").change(function(){
        dependencia_estado();});
    $("#ciudad").change(function(){
        dependencia_ciudad();});
    //$("#estado").attr("disabled",true);
    //$("#ciudad").attr("disabled",true);
});


function dependencia_estado()
{
    var code = $("#pais").val();
    var _token = $("#token").val();
$.ajax({
    type: "POST",
    url: "{{asset('scripts/dependenciaEstado')}}",
    data: { code: code,_token:_token },
    dataType: "json",
    success: function (resultado) {
      console.log(resultado);
      //$('#ciudad').append('');
      if(resultado.ciudad == '')
            {
              $('#ciudad').val('');
              $("#ciudad").attr("disabled",true);
            }
            else
            {
                $("#ciudad").attr("disabled",false);
                document.getElementById("ciudad").options.length=1;
                $('#ciudad').append(resultado.ciudad);
            }
          }
})
}


function dependencia_ciudad()
{
    var code = $("#ciudad").val();
    var _token = $("#token").val();
    $.ajax({
    type: "POST",
    url: "{{asset('scripts/dependenciaSector')}}",
    data: { code: code,_token:_token },
    dataType: "json",
    success: function (resultado) {
      console.log(resultado);
      if(resultado.sector == '')
            {
              $('#sector').val('');
              $("#sector").attr("disabled",true);
            }
            else
            {
                $("#sector").attr("disabled",false);
                document.getElementById("sector").options.length=1;
                $('#sector').append(resultado.sector);
            }
          }
})
}

</script>

@stop







