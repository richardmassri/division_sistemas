@extends("panelAdmin")

@section('content')
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">



          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Gestión de Propiedades</h3>
            </div>

            <div class="text-right col-md-12">
            <a href="{{asset('admin/propiedad/create')}}" class="btn btn-success">Registar
            </a>
            </div>
            <br><br>

            <!-- /.box-header -->
            <div class="box-body table-responsive">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Nombre</th>
                  <th>Descripcion Breve</th>
                  <th>Fecha Publicación</th>
                  <th>Precio</th>
                  <th>Destacada</th>
                  <th>Promocional</th>
                  <th>Accion</th>
                </tr>
                </thead>

                <?php $principal=''; $promocional='';?>
                  <tbody>
                  @foreach ($consultarProyecto as $consultarProyecto)
                  <tr>
                    <td><?php echo $consultarProyecto->nombre;?></td>
                    <td><?php echo $consultarProyecto->descripcion_breve;?></td>
                    <td> <?php echo $consultarProyecto->fecha_publicacion;?></td>
                    <td> <?php echo $consultarProyecto->precio;?></td>
                    @foreach($propiedaddesGaleria as $propiedaddes)
                    <?php if($propiedaddes->propiedad_id==$consultarProyecto->id){
                      if($propiedaddes->destacada!=''){
                        $principal=$propiedaddes->destacada;
                      }
                      if($propiedaddes->promocional!=''){
                        $promocional=$propiedaddes->promocional;
                      }
                    }
                    ?>
                    @endforeach
                    <td> <?php echo $principal;?></td>
                    <td> <?php echo $promocional;?></td>
                    <td> 
                    <a href="propiedad/{{$consultarProyecto->id}}" class="fa fa-fw fa-search"> </a>

                    <a href="propiedad/{{$consultarProyecto->id}}/edit" class="fa fa-fw fa-pencil"> </a>
                    </td>
                  </tr>
                @endforeach
               
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
    <!-- /.content -->
 
<!-- ./wrapper -->

<!-- page script -->
@stop
@section('scripts')
  <script src="http://code.jquery.com/ui/1.11.1/jquery-ui.min.js"></script>
<script>

    $(function () {
    $("#example1").DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
        language: {
        "sProcessing":     "Procesando...",
        "sLengthMenu":     "Mostrar _MENU_ registros",
        "sZeroRecords":    "No se encontraron resultados",
        "sEmptyTable":     "Ningún dato disponible en esta tabla",
        "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix":    "",
        "sSearch":         "Buscar:",
        "sUrl":            "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst":    "Primero",
            "sLast":     "Último",
            "sNext":     "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
    }
});
});



</script>
@stop
