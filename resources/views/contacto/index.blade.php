@extends("panelAdmin")
@section('content')
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">


          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Gestión de Solicitudes</h3>
            </div>
          <div id="mensajeDiv" class="alert alert-success hidden">
            <h3 class="box-title" id="contenidoMensaje"></h3>
          </div>

            <!-- /.box-header -->
            <div class="box-body table-responsive">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Nombre</th>
                  <th>email</th>
                  <th>telefono</th>
                  <th>Estatus</th>
                  <th>Accion</th>
                </tr>
                </thead>
                
                  <tbody>
                  @foreach ($consultarContacto as $consultarContacto)
                  <tr>
                    <td><?php echo $consultarContacto->nombre;?></td>
                    <td><?php echo $consultarContacto->correo;?></td>
                    <td><?php echo $consultarContacto->telefono;?></td>                    

                    <td>
                    <?php 
                    //dd($consultarContacto->metodo_estatus->nombre);
                    if($consultarContacto->estatus_id==1){ ?>
                      <span class="block-ribbon block-ribbon-right badge bg-purple">{{$consultarContacto->metodo_estatus->nombre}}</span>
                      <?php
                      }else{ ?>
                        <span class="block-ribbon block-ribbon-right badge bg-yellow">{{$consultarContacto->metodo_estatus->nombre}}</span>
                        <?php 
                      }
                        ?></td>
                    <td> 
                    <a href="{{asset('admin/contacto')}}/{{$consultarContacto->id}}" class="fa fa-fw fa-search"> </a>
                     <?php if($consultarContacto->estatus_id==1){ ?>
                    <a href="#" class="fa fa-fw fa-check-square-o activar" onclick="contactarAgente(<?php echo $consultarContacto->id;?>)"><?php }?>
                    </td>
                  </tr>
                @endforeach
               
                </thead>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
    <!-- /.content -->
 
<!-- ./wrapper -->

<div id="dialog-confirm" class="hidden">
    <div class="sidebar-object">
        <div class="card z-depth-2-top">

       <div class="col-sm-12">
       <br>
        <div class="form-group col-md-12">
        <label class="col-md-12"> Estatus </label>
          <select class="col-md-12 form-control" name="estatus" id="estatus" required="required">

          @foreach($statusSolicitudes as $statusSoli)
            <option  value="{{$statusSoli->id}}">{{$statusSoli->nombre}}</option>
          @endforeach
          </select>
        </div>

        <div class="form-group col-md-12">
        <label class="col-md-12"> Descripción </label>
          <textarea class="col-md-12 form-control" cols="5" rows="10" name="descripcion", id="descripcion"></textarea>
        </div>
        </div>  
    </div>
</div>
</div>

<input type="text" class="hidden" name="_token" id="token" value="{{ csrf_token() }}"></input>
<input type="text" class="hidden" name="id_contacto" id="id_contacto"></input>




<!-- jQuery 2.2.3 -->
<div id="dialog1" title="Dialog Title" hidden="hidden">I'm a dialog</div>
<!-- page script -->




@stop
@section('scripts')
  <script src="http://code.jquery.com/ui/1.11.1/jquery-ui.min.js"></script>
  <link rel="stylesheet" href="https://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css" />
    <script>
    function contactarAgente(id){
        $( "#dialog-confirm" ).removeClass('hidden');
        $( ".ui-state-default" ).addClass('hidden');
        var _token = $("#token").val();
        $("#id_contacto").val(id);

    $( "#dialog-confirm" ).dialog({
            resizable: false,
            modal: true,
            title:"Formulario de atención",
            width:400,
            buttons: [{
                class: 'btn btn-success',
                text: "Cancelar",
                click: function() { 
                  $("#dialog-confirm").dialog("close");
                }
              },
                {
                class: 'btn btn-success',
                text: "Guardar Solicitud",
                click: function() {
                var tipo = "usuario";
                var _token = $("#token").val();
                var estatus = $("#estatus").val();
                var id=$("#id_contacto").val();
                var descripcion=$("#descripcion").val();
                $.ajax({
                    type: "PUT",
                    url: "{{asset('admin/contacto')}}/"+id,
                    data: { usuario_id: id,_token:_token,estatus:estatus,descripcion:descripcion},
                    dataType: "json",
                    success: function (resultado){
                        if(resultado['statusCode']=='Exito'){
                            //$("#estatus").val('');
                            //$("#dialog-confirm").dialog("close");
                            //$('#mensajeDiv').removeClass('hidden');
                            //$('#contenidoMensaje').html(resultado['menaje']);
                            if(resultado['es_agente']=="SI"){
                              window.location.href = "{{asset('admin/contacto/agente')}}/"+resultado['id'];
                            }else{
                              window.location.href = "{{asset('admin/contacto')}}";
                            }
                        }else{
                          console.log("Error al enviar formulario");
                        }
                    }
                });
                }
                    //$( this ).dialog( "close" );
            }]
        });
  }
  </script>

  <script>

    $(function () {
    $("#example1").DataTable({
        dom: 'Bfrtip',
        buttons: [{

                        extend: 'excel',
                        text: 'Exportar a Excel',
                        title: 'Solicitudes'


                }],

      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": false,
      "info": true,
      "autoWidth": false,
        language: {
        "sProcessing":     "Procesando...",
        "sLengthMenu":     "Mostrar _MENU_ registros",
        "sZeroRecords":    "No se encontraron resultados",
        "sEmptyTable":     "Ningún dato disponible en esta tabla",
        "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix":    "",
        "sSearch":         "Buscar:",
        "sUrl":            "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst":    "Primero",
            "sLast":     "Último",
            "sNext":     "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
    }
});
});



</script>

@stop