@extends("panelAdmin")

@section('content')
<div class="box-header with-border">
   <h3 class="box-title">Vista de contacto</h3>
</div>

  <div class="box-body">
              <div class="col-md-4">
                <label class="col-md-12">Nombre</label>
                  <input class="form-control col-md-4"  name="nombre" type="text" maxlength="30" value="{{ $dataForm->nombre }}" readonly="readonly">
                </div>

                <div class="col-md-4">
                <label class="col-md-12">Correo</label>
                  <input class="form-control col-md-4" name="correo" type="text" maxlength="30" value="{{ $dataForm->correo }}" readonly="readonly">
                </div>

              <div class="col-md-4">
                <label class="col-md-12">Teléfono</label>
                  <input class="form-control col-md-4" name="telefono" type="text" maxlength="30" value="{{ $dataForm->telefono }}" readonly="readonly">
                </div>

              </div>

                <div class="row"></div>
                <br>

              <div class="row"></div>
                <br>

              <div class="col-md-12">
                <div class="col-md-12">
                 <label class="col-md-12">Mensaje</label>
                  <textarea class="form-control col-md-4" name="direcccion" cols="40" rows="5" value="{{ $dataForm->mensaje }}" readonly="readonly"> {{ $dataForm->mensaje }}</textarea>
                </div>
              </div>


                <div class="row">
                </div>
                <br>

                <div class="col-md-12">
                 <div class="col-md-4">
                <label class="col-md-12"> Fecha de Actualización </label>
                  <input class="form-control col-md-4" name="contrasena_nuevaForm" type="text" maxlength="50" value="{{ $dataForm->updated_at }}" readonly="readonly">
                </div>
                <div class="col-md-4">
                <label class="col-md-12"> Fecha de Creación </label>
                  <input class="form-control col-md-4" name="contrasenaForm"  type="text" maxlength="50" value="{{ $dataForm->created_at }}" readonly="readonly">
                </div>

                <div class="col-md-4">
                <label class="col-md-12"> Estatus </label>
                  <?php if($dataForm->estatus=='At'){
                    $dataForm->estatus='Atendido';
                  }else if($dataForm->estatus=='A'){
                    $dataForm->estatus="Activo"; }else{
                      $dataForm->estatus="En espera";}?>
                  <input class="form-control col-md-4" name="contrasenaForm"  type="text" maxlength="50" value="{{ $dataForm->estatus }}" readonly="readonly">
                </div>
                </div>
                <div class="row">
                </div>
                <br>
              <div class="col-md-12">
                <div class="col-md-12">
                  <label class="col-md-12"> Descripción </label>
                   <textarea  class="form-control col-md-12" rows="5"> <?php echo $dataForm->descripcion;?> </textarea>
                </div>
              </div>
               <div class="row">
                </div>
                <br>
              <div class="col-md-12">
                @if($dataForm->metodo_propiedad)
                <div class="col-md-4">
                <label class="col-md-12"> Propiedad: </label>
                  <input class="form-control col-md-4" name="contrasenaForm"  type="text" maxlength="50" value="{{ $dataForm->metodo_propiedad->nombre}}" readonly="readonly">
                </div>
                @endif

                @if($dataForm->metodo_proyecto)
                <div class="col-md-4">
                <label class="col-md-12"> Proyecto: </label>
                  <input class="form-control col-md-4" name="contrasenaForm"  type="text" maxlength="50" value="{{ $dataForm->metodo_proyecto->nombre}}" readonly="readonly">
                </div>
                @endif

                @if($dataForm->metodo_contacto)
                <div class="col-md-4">
                <label class="col-md-12"> Contacto: </label>
                  <input class="form-control col-md-4" name="contrasenaForm"  type="text" maxlength="50" value="{{ $dataForm->metodo_contacto->nombre}}" readonly="readonly">
                </div>
                @endif

                @if($dataForm->metodo_usuario_act_id)
                <div class="col-md-4">
                <label class="col-md-12"> Atendido por: </label>
                  <input class="form-control col-md-4" name="contrasenaForm"  type="text" maxlength="50" value="{{ $dataForm->metodo_usuario_act_id->nombre}}" readonly="readonly">
                </div>
                @endif

                

              </div>
              <div class="row"></div>
              <br>
              @if($dataForm->metodo_propiedad)
                <div class="col-md-12 text-center">
                    <a target="_black" href="{{asset('invermaster/propiedad/detalle')}}/{{$dataForm->metodo_propiedad->id}}" class="btn btn-info">Ver informacion </a>
                </div>
              @endif

              @if($dataForm->metodo_proyecto)
                <div class="col-md-12 text-center">
                    <a href="{{asset('invermaster/proyecto/detalle')}}/{{$dataForm->metodo_proyecto->id}}" class="btn btn-info">Ver informacion </a>
                </div>
              @endif

              <div class="row"></div>
              <br>
              <div class="col-md-12">
              @if(Session::get('es_agente')=='NO')
                <div class="col-md-4">
                  <a href="{{asset('admin/contacto')}}" class="btn btn-danger">Volver </a>
               </div>
               @else
                <div class="col-md-4">
                  <a href="{{asset('admin/contacto/agente')}}/{{Session::get('usuario_id')}}" class="btn btn-danger">Volver </a>
               </div>
              @endif

                <div class="col-md-4">
               </div>
              </div>


                <!-- /input-group -->
              </div>
            <form>

            @stop

            {!! Form::close() !!} 
