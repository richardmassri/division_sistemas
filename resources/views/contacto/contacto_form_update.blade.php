@extends("panelAdmin")

@section('content')
<div class="box-header with-border">
   <h3 class="box-title">Atender solicitud</h3>
</div>

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<?php 
  if(isset($descripcion)){?>
    <div class="<?php echo $descripcion['clase'];?>">
      <?php echo $descripcion['mensaje'];?>
    <div>
<?php
  }
?>    
 
{!! Form::open(array('url' => ['admin/empresa',$dataForm->id], 'method' => 'put', 'files'=> true)) !!}﻿

          <input class="hidden" type="file" name="id" value="{{ $dataForm->id }}">

  <div class="box-body">
            <div class="col-md-12">
                  <div class="form-group col-md-3">
                    <label for="exampleInputFile">Seleccione la foto</label>
                    <input id="exampleInputFile col-md-4" type="file" name="logotipo" value="{{ old('logotipo') }}" required="">
                  </div>

                <div class="col-md-3">
                <label class="col-md-12">Nombre</label>
                  <input class="form-control col-md-4"  name="nombre" type="text" maxlength="30" value="{{ $dataForm->nombre }}" required="required">
                </div>

                <div class="col-md-3">
                <label class="col-md-12">Correo</label>
                  <input class="form-control col-md-4" name="correo" type="text" maxlength="30" value="{{ $dataForm->correo }}" required="required">
                </div>

              <div class="col-md-3">
                <label class="col-md-12">Teléfono</label>
                  <input class="form-control col-md-4" name="telefono" type="text" maxlength="30" value="{{ $dataForm->telefono }}" required="required">
                </div>

              </div>

                <div class="row"></div>
                <br>

              <div class="col-md-12">
                <div class="col-md-4">
                 <label class="col-md-12">Facebook</label>
                  <input class="form-control col-md-4" name="facebook" type="text" value="{{ $dataForm->facebook }}" required="required">
                </div>

                <div class="col-md-4">
                 <label class="col-md-12">Twitter</label>
                  <input class="form-control col-md-4" name="twitter" type="text" maxlength="30" value="{{ $dataForm->twitter }}" required="required">
                </div>

                <div class="col-md-4">
                <label class="col-md-12">Instagram</label>
                  <input class="form-control col-md-4" name="instagram" type="text" maxlength="30" value="{{ $dataForm->instagram }}" required="required">
                </div>

              </div>

                <div class="row"></div>
                <br>


              <div class="col-md-12">

                <div class="col-md-6">
                <label class="col-md-12">Quines Somos</label>
                  <textarea class="form-control col-md-4" name="quines_somos" cols="40" rows="5" value="{{ $dataForm->quines_somos }}" required="required"> {{ $dataForm->quines_somos }}</textarea>
                </div>

                <div class="col-md-6">
                 <label class="col-md-12">Vision</label>
                  <textarea class="form-control col-md-4" name="vision" cols="40" rows="5" value="{{ $dataForm->vision }}" required="required"> {{ $dataForm->vision }}</textarea>
                </div>


              </div>

              <div class="row"></div>
                <br>

              <div class="col-md-12">
                <div class="col-md-6">
                 <label class="col-md-12">Direccion</label>
                  <textarea class="form-control col-md-4" name="direcccion" cols="40" rows="5" value="{{ $dataForm->direcccion }}" required="required"> {{ $dataForm->direcccion }}</textarea>
                </div>


              </div>

                <div class="row"></div>
                <br>

              <input type="text" class="hidden" name="_token" value="{{ csrf_token() }}"></input>
              <div class="row"></div>
              <br>
              <div class="col-md-12"> 
                <div class="col-md-4">
                  <a href="{{asset('admin/empresa')}}" class="btn btn-danger">Volver </a>
               </div>

                <div class="col-md-4">
               </div>
               <div class="col-md-4 text-right">
               <button class="btn  btn-success text-right">Actualizar</button>
               </div>
              </div>


                <!-- /input-group -->
              </div>
            <form>

            @stop

            {!! Form::close() !!} 
