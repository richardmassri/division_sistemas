

<!DOCTYPE html>
<html>
<head>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="robots" content="index, follow">
<title>Tribus: Mutipurpose Template</title>

<!-- Page loader -->
<script src="{{asset('tribu/assets/pace/js/pace.min.js')}}"></script>
<link rel="stylesheet" href="{{asset('tribu/assets/pace/css/pace-minimal.css')}}" type="text/css">

<!-- Bootstrap -->
<link rel="stylesheet" href="{{asset('tribu/assets/bootstrap/css/bootstrap.min.css')}}" type="text/css">

<!-- CSS animation library -->
<link rel="stylesheet" href="{{asset('tribu/assets/animate/animate.min.css')}}" type="text/css">

<link href="https://fonts.googleapis.com/css?family=Quicksand:400,500,700" rel="stylesheet">

<!-- Plugins -->
<link rel="stylesheet" href="{{asset('tribu/assets/bootstrap-select/css/bootstrap-select.min.css')}}">
<link rel="stylesheet" href="{{asset('tribu/assets/swiper/css/swiper.min.css')}}">

<!-- Light Gallery -->
<link rel="stylesheet" href="{{asset('tribu/assets/lightgallery/css/lightgallery.min.css')}}">

<!-- Icons -->
<link rel="stylesheet" href="{{asset('tribu/fonts/font-awesome/css/font-awesome.min.css')}}" type="text/css">
<link rel="stylesheet" href="{{asset('tribu/fonts/ionicons/css/ionicons.min.css')}}" type="text/css">
<link rel="stylesheet" href="{{asset('tribu/fonts/line-icons/line-icons.css')}}" type="text/css">
<link rel="stylesheet" href="{{asset('tribu/fonts/line-icons-pro/line-icons-pro.css')}}" type="text/css">

<!-- Global style (main) -->
<link id="stylesheet" type="text/css" href="{{asset('tribu/css/global-style.min.css')}}" rel="stylesheet" media="screen">
<!-- Custom style - Remove if not necessary -->
<link type="text/css" href="{{asset('tribu/css/custom-style.css')}}" rel="stylesheet">

<!-- Favicon -->
<link href="{{asset('tribu/images/favicon.png')}}" rel="icon" type="image/png">


<!-- RS5.0 -->
<link rel="stylesheet" type="text/css" href="{{asset('tribu/assets/revolution/css/settings.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('tribu/assets/revolution/css/layers.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('tribu/assets/revolution/css/navigation.css')}}">

<!-- Cube Portfolio -->
<link rel="stylesheet" href="{{asset('tribu/assets/cubeportfolio/css/cubeportfolio.min.css')}}">

</head>
<body>


<!-- MAIN WRAPPER -->
<div class="body-wrap">
    <div id="st-container" class="st-container">

        <nav class="st-menu st-effect-1" id="menu-1">
    <div class="st-profile">
        <div class="st-profile-user-wrapper">
            <div class="profile-user-image">
                <img src="images/prv/people/person-1.jpg" class="img-circle" />
            </div>
            <div class="profile-user-info">
                <span class="profile-user-name">Bertram Ozzie</span>
                <span class="profile-user-email">username@example.com</span>
            </div>
        </div>
    </div>

    <div class="st-menu-list mt-2">
        <ul>
            <li>
                <a href="#">
                    <i class="ion-ios-bookmarks-outline"></i> Theme documentation
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="ion-ios-cart-outline"></i> Purchase Tribus
                </a>
            </li>
        </ul>
    </div>

    <h3 class="st-menu-title">Account</h3>
    <div class="st-menu-list">
        <ul>
            <li>
                <a href="#">
                    <i class="ion-ios-person-outline"></i> User profile
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="ion-ios-location-outline"></i> My addresses
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="ion-card"></i> My cards
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="ion-ios-unlocked-outline"></i> Password update
                </a>
            </li>
        </ul>
    </div>

    <h3 class="st-menu-title">Support center</h3>
    <div class="st-menu-list">
        <ul>
            <li>
                <a href="#">
                    <i class="ion-ios-information-outline"></i> About Tribus
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="ion-ios-email-outline"></i> Contact &amp; support
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="fa fa-camera"></i> Getting started
                </a>
            </li>
        </ul>
    </div>
</nav>

        <div class="st-pusher">
            <div class="st-content">
                <div class="st-content-inner">
                    <!-- HEADER -->
                    <div class="header-1 header-inner-wrapper" id="header">
	<!-- TOP HEADER -->
	<div class="header-inner header-inner--style-1">
		<div class="container">
			<div class="col-sm-3 col-xs-12">
				<!-- LOGO -->
				<a class="navbar-logo" href="index.html">
					<!-- Logo: Dark layout -->
					<img src="images/logo/logo-1-a.png" class="hide" />

					<!-- Logo: Light layout -->
					<img src="images/logo/logo-1-b.png" />
		      	</a>
			</div>
			<div class="col-sm-9 text-right pr-0">
				<div class="header-col hidden-xs">
					<div class="icon-block--style-3 icon-block--style-3-v1">
                        <i class="icon ion-ios-telephone-outline"></i>
						<span class="icon-block-text heading heading-6 strong-400">
                            445-888.999
                        </span>
						<small class="icon-block-subtext c-gray-light">
							info@example.com
						</small>
                    </div>
				</div>
				<div class="header-col hidden-xs">
					<div class="icon-block--style-3 icon-block--style-3-v1">
                        <i class="icon ion-ios-telephone-outline"></i>
						<span class="icon-block-text heading heading-6 strong-400">
                            National Arena
                        </span>
						<small class="icon-block-subtext c-gray-light">Bucharest, Romania</small>
                    </div>
				</div>
				<div class="header-col hidden-xs hidden-sm hidden-md">
					<ul class="social-media social-media--style-1-v3 social-media-circle">
                        <li>
                            <a href="#" class="facebook" target="_blank" title="Facebook">
                                <i class="fa fa-facebook"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="instagram" target="_blank">
                                <i class="fa fa-instagram"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="dribbble" target="_blank">
                                <i class="fa fa-dribbble"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="dribbble" target="_blank">
                                <i class="fa fa-github"></i>
                            </a>
                        </li>
                    </ul>
				</div>
			</div>
		</div>
	</div>

	<!-- NAVBAR -->
	<nav class="navbar navbar-main navbar--sm navbar--shadow navbar--caps navbar--strong" role="navigation">
		<div class="container relative">
		    <div class="navbar-header">
				<!-- NAVBAR TOGGLE -->
		      	<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
			    </button>

				<!-- LOGO -->
				<a class="navbar-logo" href="index.html">
					<!-- Logo: Dark layout -->
					<img src="images/logo/logo-1-a.png" class="hide" />

					<!-- Logo: Light layout -->
					<img src="images/logo/logo-1-b.png" />
		      	</a>

							    <!-- SLIDEBAR TOGGLE -->
			    <button type="button" class="navbar-icon-btn btn-st-trigger" data-effect="st-effect-1">
					<i class="fa fa-bars"></i>
			    </button>
				
				<!-- NAVBAR SEARCH - for mobile resolutions -->
		      	<button type="button" class="navbar-icon-btn css-animate" data-target="#sctGlobalSearch">
					<i class="fa fa-search"></i>
			    </button>
		   	</div>

		   	<div id="navbar-collapse" class="collapse navbar-collapse">
		      	<ul class="nav navbar-nav" data-hover="dropdown" data-animations="zoomIn zoomIn zoomIn zoomIn">
					<!-- Navbar items/links -->
                   	 <!-- Navbar links -->
<li class="dropdown">
    <a href="#" data-toggle="dropdown">
       <span>Home</span>
    </a>

</li>
<li class="dropdown">
    <a href="#" data-toggle="dropdown">
       <span>Proyecto</span>
    </a>
  
</li>
<li class="dropdown">
    <a href="#" data-toggle="dropdown">
       <span>Propiedades</span>
    </a>
   
</li>
<li class="dropdown mega-dropdown">
    <a href="#" data-toggle="dropdown">
       <span>Quienes Somos</span>
    </a>
    
</li>

<li class="dropdown">
    <a href="#" data-toggle="dropdown">
       <span>Equipo</span>
    </a>
   
</li>

<!-- Shop -->
<li class="dropdown mega-dropdown">
        <a href="#">Contactanos
    </a>
   
</li>
            </ul>   

<li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
       <span>Features</span>
    </a>
    <ul class="dropdown-menu">
        <li class="">
            <a href="one-page-default.html">
               <span>One Page Version</span>
            </a>
        </li>
        <li class="">
            <a tabindex="-1" href="_components.html">
                UI Kit
            </a>
        </li>
        <li class="">
            <a tabindex="-1" href="overview.html">
                Overview
            </a>
        </li>
        <li class="dropdown dropdown-submenu">
            <a href="#">
               <span>Components</span>
            </a>
            <ul class="dropdown-menu">
                <li class="">
                    <a tabindex="-1" href="_documentation.html">
                        Documentation
                    </a>
                </li>
                <li class="dropdown-header text-uppercase">
                    Components
                </li>
                <li class="">
                    <a tabindex="-1" href="_blocks.html">
                        Blocks
                    </a>
                </li>
                <li class="">
                    <a tabindex="-1" href="_icon-blocks.html">
                        Icon blocks
                    </a>
                </li>
                <li class="">
                    <a tabindex="-1" href="_social-buttons.html">
                        Social buttons
                    </a>
                </li>

                <li class="">
                    <a tabindex="-1" href="_forms.html">
                        Forms
                    </a>
                </li>
            </ul>
        </li>
        <li class="dropdown dropdown-submenu">
            <a tabindex="-1" href="#">Boxed layout</a>
            <ul class="dropdown-menu">
                <li><a tabindex="-1" href="layout-boxed-1.html">With margin</a></li>
                <li><a tabindex="-1" href="layout-boxed-2.html">No margin</a></li>
            </ul>
        </li>
        <li class="dropdown dropdown-submenu">
            <a tabindex="-1" href="#">Headers</a>
            <ul class="dropdown-menu">
                <li>
                    <a tabindex="-1" href="_header-default-caps.html">
                        Default header: Light
                    </a>
                </li>
                <li>
                    <a tabindex="-1" href="_header-default-caps-inverse.html">
                        Default header: Dark
                    </a>
                </li>
                <li>
                    <a tabindex="-1" href="_header-default-caps-transparent.html">
                        Transparent header: Light
                    </a>
                </li>
                <li>
                    <a tabindex="-1" href="_header-default-caps-transparent-inverse-bb.html">
                        Transparent header + border: Dark
                    </a>
                </li>
                <li>
                    <a tabindex="-1" href="_header-default-caps-transparent-bb.html">
                        Transparent header + border: Light
                    </a>
                </li>
                <li>
                    <a tabindex="-1" href="_header-default-caps-transparent-inverse.html">
                        Transparent header: Dark
                    </a>
                </li>
                <li>
                    <a tabindex="-1" href="_header-default-caps-opaque.html">
                        Opaque header: Light
                    </a>
                </li>
                <li>
                    <a tabindex="-1" href="_header-default-caps-opaque-inverse.html">
                        Opaque header: Dark
                    </a>
                </li>
                <li class="dropdown-header text-uppercase">
                    And more ...
                </li>
                <li>
                    <a tabindex="-1" href="_header-default-caps-sm.html">
                        Small header: Dark
                    </a>
                </li>
                <li>
                    <a tabindex="-1" href="_header-default-caps-centered-logo.html">
                        Small header + Centered logo : Light
                    </a>
                </li>
                <li>
                    <a tabindex="-1" href="_header-default-caps-strong.html">
                        Extra strong header: Light
                    </a>
                </li>
                <li>
                    <a tabindex="-1" href="_header-default-caps-navbar-right.html">
                        Default header + right nav
                    </a>
                </li>
            </ul>
        </li>
    </ul>
</li>
		      	</ul>

				<ul class="nav navbar-nav navbar-right" data-hover="dropdown" data-animations="zoomIn zoomIn zoomIn zoomIn">
					<!-- Search button -->
					<li id="btnGlobalSearch" class="css-animate dropdown-icon hidden-xs hidden-sm" data-target="#sctGlobalSearch">
						<a href="#" data-toggle="dropdown">
							<span>
								<i class="fa fa-search"></i>
							</span>
						</a>
					</li>

										<li class="dropdown-icon hidden-xs hidden-sm">
						<a href="#" class="btn-st-trigger ttip" data-effect="st-effect-1">
							<span><i class="fa fa-bars"></i></span>
						</a>
					</li>
					
                   	<!-- Login button -->
                   	<li class="navbar-btn-lg">
                    	<a href="#" class="btn text-uppercase">
                    		Schedule a visit
                    	</a>
					</li>
				</ul>
		   	</div>
		</div>
	</nav>

	<!-- GLOBAL SEARCH -->
    <section id="sctGlobalSearch" class="global-search">
    <div class="relative">
        <!-- Backdrop for global search -->
        <div class="global-search-backdrop"></div>

        <!-- Search form -->
        <form class="form-horizontal form-global-search" role="form">
            <div class="container relative">
                <div class="row">
                    <div class="col-xs-12">
                        <input type="text" class="search-input" placeholder="Type and hit enter ...">
                    </div>
                </div>

                <!-- Close button -->
                <button type="button" class="global-search-close-btn css-animate" data-target="#sctGlobalSearch" title="Close search bar">
                    <i class="icon ion-close-round"></i>
                </button>
            </div>
        </form>
    </div>
</section>
</div>

                    <section class="has-bg-cover bg-size-cover flex flex-items-xs-top" style="background-image: url(images/prv/real-estate/page-title-1.jpg); background-position: center bottom;">

                        <span class="mask mask-base-1--style-2"></span>

                        <div class="flex-wrap-item">
                            <div class="container">
                                <div class="page-title page-title--style-2">
                                    <div class="">
                                        <h3 class="heading heading-1 strong-700 text-uppercase c-white">
                                            Properties
                                        </h3>
                                        <h4 class="heading heading-5 text-normal strong-300 c-white">
                                            It's time for your work to be seen! Over 20 layouts to choose from ...
                                        </h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>

                    <!-- Properties listing -->
                    <section class="slice sct-color-2">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-9">
                                    <div class="mb-2">
                                        <h3 class="heading heading-3 strong-600 text-normal">
                                            9975 Emerald Escape, New York
                                        </h3>
                                        <ul class="inline-links inline-links--style-2">
                                            <li>
                                                <a href="#">
                                                    <i class="fa fa-heart"></i> 50
                                                </a>
                                            </li>
                                            <li>
                                                <i class="fa fa-eye"></i> 750
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <i class="fa fa-comment"></i> 9
                                                </a>
                                            </li>
                                            <li>
                                                <div class="dropdown">
                                                    <a class="dropdown-toggle" href="#" id="postShare-1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <i class="fa fa-share-alt"></i>
                                                    </a>
                                                    <ul class="dropdown-menu" aria-labelledby="postShare-1">
                                                        <li><a href="#">Action</a></li>
                                                        <li><a href="#">Another action</a></li>
                                                        <li><a href="#">Something else here</a></li>
                                                        <li role="separator" class="divider"></li>
                                                        <li><a href="#">Separated link</a></li>
                                                    </ul>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>

                                    <!-- Gallery -->
                                    <div class="gallery-top">
                                        <img src="images/prv/real-estate/img-lg-1.jpg">
                                    </div>

                                    <div class="gallery-bottom" id="light_gallery">
                                        <div class="row">
                                            <div class="col-sm-2">
                                                <div class="gallery-thumb">
                                                    <a class="item" href="images/prv/real-estate/img-1.jpg">
                                                        <img src="images/prv/real-estate/img-1.jpg">
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-sm-2">
                                                <div class="gallery-thumb">
                                                    <a class="item" href="images/prv/real-estate/img-2.jpg">
                                                        <img src="images/prv/real-estate/img-2.jpg">
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-sm-2">
                                                <div class="gallery-thumb">
                                                    <a class="item" href="images/prv/real-estate/img-3.jpg">
                                                        <img src="images/prv/real-estate/img-3.jpg">
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-sm-2">
                                                <div class="gallery-thumb">
                                                    <a class="item" href="images/prv/real-estate/img-4.jpg">
                                                        <img src="images/prv/real-estate/img-4.jpg">
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-sm-2">
                                                <div class="gallery-thumb">
                                                    <a class="item" href="images/prv/real-estate/img-5.jpg">
                                                        <img src="images/prv/real-estate/img-5.jpg">
                                                    </a>
                                                </div>
                                            </div>

                                            <div class="col-sm-2">
                                                <div class="gallery-thumb">
                                                    <a class="item" href="images/prv/real-estate/img-6.jpg">
                                                        <img src="images/prv/real-estate/img-6.jpg">
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <span class="space-md-md"></span>

                                    <!-- Description -->
                                    <div class="card z-depth-2-top">
                                        <div class="card-title b-xs-bottom">
                                            <h3 class="heading heading-sm text-uppercase">Property description</h3>
                                        </div>
                                        <div class="card-body">
                                            <p>
                                                Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.
                                            </p>

                                            <p>
                                                Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes in futurum.
                                            </p>

                                        </div>
                                    </div>

                                    <span class="space-md-md"></span>

                                    <!-- Address/Location -->
                                    <div class="card z-depth-2-top">
                                        <div class="card-title b-xs-bottom">
                                            <h3 class="heading heading-sm text-uppercase">Location</h3>
                                            <a href="#" class="btn-aux">
                                                <i class="ion-location"></i>
                                                <span class="aux-text">See location on map</span>
                                            </a>
                                        </div>
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <table class="table table-no-border table-striped table-responsive">
                                                        <tbody>
                                                            <tr>
                                                                <td><strong>Address</strong></td>
                                                                <td>3015 Grand Avenue, CocoWalk</td>
                                                            </tr>
                                                            <tr>
                                                                <td><strong>City</strong></td>
                                                                <td>New York</td>
                                                            </tr>
                                                            <tr>
                                                                <td><strong>Country</strong></td>
                                                                <td>United States</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>

                                                <div class="col-sm-6">
                                                    <table class="table table-no-border table-striped table-responsive">
                                                        <tbody>
                                                            <tr>
                                                                <td><strong>State</strong></td>
                                                                <td>New York</td>
                                                            </tr>
                                                            <tr>
                                                                <td><strong>ZIP code</strong></td>
                                                                <td>588 965</td>
                                                            </tr>
                                                            <tr>
                                                                <td><strong>Neighborhood</strong></td>
                                                                <td>Little Italy</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <span class="space-md-md"></span>

                                    <!-- Property details -->
                                    <div class="card z-depth-2-top">
                                        <div class="card-title b-xs-bottom">
                                            <h3 class="heading heading-sm text-uppercase">Property details</h3>
                                            <span href="#" class="btn-aux">
                                                Updated 1 week ago
                                            </span>
                                        </div>
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <table class="table table-no-border table-striped table-responsive">
                                                        <tbody>
                                                            <tr>
                                                                <td><strong><strong>Property Size:</strong> 2300 Sq Ft</strong></td>
                                                                <td><strong>Lot size:</strong> 5000 Sq Ft</td>
                                                                <td><strong>Price:</strong> $23000</td>
                                                            </tr>
                                                            <tr>
                                                                <td><strong>Rooms:</strong> 5</td>
                                                                <td><strong>Bedrooms:</strong> 4</td>
                                                                <td><strong>Bathrooms:</strong> 2</td>
                                                            </tr>
                                                            <tr>
                                                                <td><strong>Garages:</strong> 2</td>
                                                                <td><strong>Roofing:</strong> New</td>
                                                                <td><strong>Structure Type:</strong> Bricks</td>
                                                            </tr>
                                                            <tr>
                                                                <td><strong>Year built:</strong> 1991</td>
                                                                <td><strong>Available from:</strong> 1 August 2014</td>
                                                                <td></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <span class="space-md-md"></span>

                                    <!-- Video -->
                                    <div class="card z-depth-2-top">
                                        <div class="card-title b-xs-bottom">
                                            <h3 class="heading heading-sm text-uppercase">Property description</h3>
                                        </div>
                                        <div class="card-body">
                                            <!-- 16:9 aspect ratio -->
                                           <div class="embed-responsive embed-responsive-16by9">
                                               <iframe width="560" height="315" src="https://www.youtube.com/embed/OljIlQFjhKs?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
                                           </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="sidebar">
    <div class="sidebar-object">
        <div class="card z-depth-2-top">
            <div class="card-body">
                <a href="#" class="btn btn-block btn-base-1">
                    Call agent
                </a>
                <div class="text-xs-center">
                    <a href="#" class="link link--style-1 strong-500">
                        <small>Get support by calling the 24h helpline.</small>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="sidebar-object">
        <div class="card z-depth-2-top">
            <div class="card-title b-xs-bottom">
                <h3 class="heading heading-sm text-uppercase">Agent details</h3>
            </div>
            <div class="list-group-item">
                <span class="text-uppercase strong-500">Name</span>
                <span class="pull-right">Darby Jewell</span>
            </div>
            <div class="list-group-item">
                <span class="text-uppercase strong-500">Phone</span>
                <span class="pull-right">0755 - 796 456</span>
            </div>

            <div class="card-body">
                <a href="#" class="btn btn-block btn-base-4">
                    Send message
                </a>
                <a href="#" class="btn btn-block btn-base-4 btn-outline mt-1">
                    Save property
                </a>
            </div>
        </div>
    </div>

    <div class="sidebar-object b-xs-bottom pb-2 mb-2">
        <div class="card z-depth-2-top">
            <div class="card-title b-xs-bottom">
                <h3 class="heading heading-sm text-uppercase">Advanced search</h3>
            </div>
            <div class="card-body">
                <form class="form-default" data-toggle="validator" role="form">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group form-group-lg has-feedback">
                                <label for="" class="text-uppercase">Location</label>
                                <input class="form-control" type="text" placeholder="City, Country">
                                <span class="help-block with-errors"></span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group form-group-lg has-feedback">
                                <label for="" class="text-uppercase">Search radius</label>
                                <select class="form-control selectpicker">
                                    <option>Any</option>
                                    <option>Within 1/4 miles</option>
                                    <option>Within 1/2 miles</option>
                                    <option>Within 1 mile</option>
                                    <option>Within 3 miles</option>
                                    <option>Within 5 miles</option>
                                </select>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group form-group-lg has-feedback">
                                <label for="" class="text-uppercase">Property type</label>
                                <select class="form-control selectpicker">
                                    <option>All types</option>
                                    <option>Houses</option>
                                    <option>Flats/Apartments</option>
                                    <option>Bungalows</option>
                                    <option>Lands</option>
                                    <option>Other</option>
                                </select>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-group-lg has-feedback">
                                <label for="" class="text-uppercase">I want</label>
                                <select class="form-control selectpicker">
                                    <option>All</option>
                                    <option>To rent</option>
                                    <option>To buy</option>
                                </select>
                            </div>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group has-feedback">
                                <label class="text-uppercase" for="">Price range</label>
                                <div class="range-slider-wrapper mt-1">
                                    <!-- Range slider container -->
                                    <div id="filterPrinceRange"></div>

                                    <!-- Range slider values -->
                                    <div class="row mt-1">
                                        <div class="col-xs-6">
                                            <small>&#36; </small>
                                            <span class="range-slider-value value-low" id="filterPrinceValueLow"></span>
                                        </div>

                                        <div class="col-xs-6 text-right">
                                            <small>&#36; </small>
                                            <span class="range-slider-value value-hight" id="filterPrinceValueHigh"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <button type="submit" class="btn btn-block btn-lg btn-base-1 mt-2">Search properties</button>
                </form>
            </div>
        </div>
    </div>

    <div class="sidebar-object">
        <div class="section-title section-title--style-1">
            <h4 class="heading heading-xs strong-300 c-gray-light">Listings</h4>
            <h3 class="section-title-inner heading-4 strong-400 mt-0 text-uppercase">
                Recently <strong class="c-base-1">viewd</strong>
            </h3>
        </div>

        <div class="block block--style-3 z-depth-2-top z-depth-3--hover">
            <div class="block-image relative">
                <a href="">
                    <img src="images/prv/real-estate/img-1.jpg" class="img-responsive">
                </a>
                <span class="block-ribbon block-ribbon-right bg-yellow">For sale</span>
            </div>

            <div class="block-body">
                <h3 class="heading heading-sm ">
                    3015 Grand Avenue, CocoWalk
                </h3>
                <span class="star-rating">
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star-o"></i>
                    <i class="fa fa-star-o"></i>
                </span>

                <div class="mt-1">
                    <span class="block-price heading-6">$250.800</span>
                    <span class="block-price-text"></span>
                </div>
            </div>
        </div>

        <div class="block block--style-3 z-depth-2-top z-depth-3--hover mt-3">
            <div class="block-image relative">
                <a href="">
                    <img src="images/prv/real-estate/img-2.jpg" class="img-responsive">
                </a>
                <span class="block-ribbon block-ribbon-right bg-yellow">For sale</span>
            </div>

            <div class="block-body">
                <h3 class="heading heading-sm ">
                    3015 Grand Avenue, CocoWalk
                </h3>
                <span class="star-rating">
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star-o"></i>
                    <i class="fa fa-star-o"></i>
                </span>

                <div class="mt-1">
                    <span class="block-price heading-6">$250.800</span>
                    <span class="block-price-text"></span>
                </div>
            </div>
        </div>

        <div class="block block--style-3 z-depth-2-top z-depth-3--hover mt-3">
            <div class="block-image relative">
                <a href="">
                    <img src="images/prv/real-estate/img-3.jpg" class="img-responsive">
                </a>
            </div>

            <div class="block-body">
                <h3 class="heading heading-sm ">
                    3015 Grand Avenue, CocoWalk
                </h3>
                <span class="star-rating">
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star-o"></i>
                    <i class="fa fa-star-o"></i>
                </span>

                <div class="mt-1">
                    <span class="block-price heading-6">$250.800</span>
                    <span class="block-price-text"></span>
                </div>
            </div>
        </div>
    </div>
</div>
                                </div>
                            </div>
                        </div>
                    </section>

                    <!-- FOOTER -->
                    <footer class="footer">
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-md-2 col-sm-4">
                    <div class="col">
                       <h4 class="heading heading-xs strong-600 text-uppercase mb-1">
                          Main menu
                       </h4>

                       <ul class="footer-links">
                            <li>
                                <a href="index.html" title="Home">
                                    Home
                                </a>
                            </li>
                            <li>
                                <a href="about-creative.html" title="About us">
                                    About us
                                </a>
                            </li>
                            <li>
                                <a href="services-creative.html" title="Services">
                                    Services
                                </a>
                            </li>
                            <li>
                                <a href="blog-cards-sidebar-left.html" title="Blog">
                                    Blog
                                </a>
                            </li>
                            <li>
                                <a href="contact-card.html" title="Contact">
                                    Contact
                                </a>
                            </li>
                        </ul>
                     </div>
                </div>

                <div class="col-md-2 col-sm-4">
                    <div class="col">
                       <h4 class="heading heading-xs strong-600 text-uppercase mb-1">
                           Help and support
                       </h4>

                       <ul class="footer-links">
                            <li>
                                <a href="" title="Help center">
                                    Help center
                                </a>
                            </li>
                            <li>
                                <a href="" title="Discussions">
                                    Discussions
                                </a>
                            </li>
                            <li>
                                <a href="" title="Contact support">
                                    Contact support
                                </a>
                            </li>
                            <li>
                                <a href="" title="Knowledge center">
                                    Knowledge center
                                </a>
                            </li>
                            <li>
                                <a href="" title="Jobs">
                                    FAQ
                                </a>
                            </li>
                        </ul>
                     </div>
                </div>

                <div class="col-md-2 col-sm-4">
                    <div class="col">
                       <h4 class="heading heading-xs strong-600 text-uppercase mb-1">
                           Our services
                       </h4>

                       <ul class="footer-links">
                            <li>
                                <a href="" title="Help center">
                                    Concept
                                </a>
                            </li>
                            <li>
                                <a href="" title="Discussions">
                                    UI/UX design
                                </a>
                            </li>
                            <li>
                                <a href="" title="Contact support">
                                    Web developement
                                </a>
                            </li>
                            <li>
                                <a href="" title="Knowledge center">
                                    Custom solutions
                                </a>
                            </li>
                            <li>
                                <a href="" title="Jobs">
                                    Branding
                                </a>
                            </li>
                        </ul>
                     </div>
                </div>

                <span class="space-sm-only-3"></span>

                <div class="col-md-3 col-sm-6">
                    <div class="col">
                        <h4 class="heading heading-xs strong-600 text-uppercase mb-1">
                            From the blog
                        </h4>

                        <ul class="footer-links">
                            <li>
                                <a href="" title="">Clamtown weekly news</a>
                                <small class="footer-link-date">June 20, 2017</small>
                            </li>
                            <li class="">
                                <a href="" title="">Fall in love with Tribus</a>
                                <small class="footer-link-date">June 18, 2017</small>
                            </li>
                            <li class="">
                                <a href="" title="">Cracking the Tribus code</a>
                                <small class="footer-link-date">March 30, 2017</small>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="col-md-3 col-sm-6">
                    <div class="col">
                        <h4 class="heading heading-xs strong-600 text-uppercase mb-1">
                            Check our feed
                        </h4>

                        <div class="photostream">
                            <span class="photo-wrapper">
                                <a href="images/prv/blog/img-1-200x200.jpg" class="theater">
                                    <img src="images/prv/blog/img-1-200x200.jpg">
                                </a>
                            </span>
                            <span class="photo-wrapper">
                                <a href="images/prv/blog/img-2-200x200.jpg" class="theater">
                                    <img src="images/prv/blog/img-2-200x200.jpg">
                                </a>
                            </span>
                            <span class="photo-wrapper">
                                <a href="images/prv/block-img-3.jpg" class="theater">
                                    <img src="images/prv/blog/img-3-200x200.jpg">
                                </a>
                            </span>
                            <span class="photo-wrapper">
                                <a href="images/prv/block-img-4.jpg" class="theater">
                                    <img src="images/prv/blog/img-4-200x200.jpg">
                                </a>
                            </span>
                            <span class="photo-wrapper">
                                <a href="images/prv/block-img-5.jpg" class="theater">
                                    <img src="images/prv/blog/img-5-200x200.jpg">
                                </a>
                            </span>
                            <span class="photo-wrapper">
                                <a href="images/prv/block-img-6.jpg" class="theater">
                                    <img src="images/prv/blog/img-6-200x200.jpg">
                                </a>
                            </span>
                            <span class="photo-wrapper">
                                <a href="images/prv/block-img-7.jpg" class="theater">
                                    <img src="images/prv/blog/img-7-200x200.jpg">
                                </a>
                            </span>
                            <span class="photo-wrapper">
                                <a href="images/prv/block-img-8.jpg" class="theater">
                                    <img src="images/prv/blog/img-8-200x200.jpg">
                                </a>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="footer-bottom">
        <div class="container">
            <div class="row flex flex-items-xs-middle">
                <div class="col col-sm-7 col-xs-12">
                    <div class="copyright">
                        Copyright &copy; 2017                        <a href="http://webpixels.io" target="_blank" title="Webpixels - Official Website">
                            <strong>Webpixels</strong>
                        </a> -
                        All rights reserved
                    </div>
                </div>
                <div class="col col-sm-5 col-xs-12">
                    <div class="text-xs-right">
                        <ul class="social-media social-media--style-1-v4">
                            <li>
                                <a href="#" class="facebook" target="_blank" data-toggle="tooltip" data-original-title="Facebook">
                                    <i class="fa fa-facebook"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="instagram" target="_blank" data-toggle="tooltip" data-original-title="Instagram">
                                    <i class="fa fa-instagram"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="dribbble" target="_blank" data-toggle="tooltip" data-original-title="Dribbble">
                                    <i class="fa fa-dribbble"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="dribbble" target="_blank" data-toggle="tooltip" data-original-title="Github">
                                    <i class="fa fa-github"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
                </div>
            </div>
        </div><!-- END: st-pusher -->
    </div><!-- END: st-container -->
</div><!-- END: body-wrap -->

<!-- SCRIPTS -->
<a href="#" class="back-to-top"></a>

<!-- Required JSs -->
<script src="{{asset('tribu/js/jquery.js')}}"></script>
<script src="{{asset('tribu/js/jquery-ui.min.js')}}"></script>

<!-- Essential JSs -->
<script src="{{asset('tribu/assets/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('tribu/js/bootstrap-extensions/bootstrap-dropdownhover.js')}}"></script>
<script src="{{asset('tribu/js/bootstrap-extensions/bootstrap-dropdowneffect.js')}}"></script>
<script src="{{asset('tribu/js/bootstrap-extensions/bootstrap-validator.js')}}"></script>
<script src="{{asset('tribu/js/modernizr.custom.js')}}"></script>
<script src="{{asset('tribu/js/jquery.mousewheel-3.0.6.pack.js')}}"></script>
<script src="{{asset('tribu/js/jquery.easing.js')}}"></script>
<script src="{{asset('tribu/js/classie.js')}}"></script>
<script src="{{asset('tribu/js/slidebar/slidebar.js')}}"></script>

<!-- Assets - Required -->
<script src="{{asset('tribu/assets/headroom/headroom.min.js')}}"></script>
<script src="{{asset('tribu/assets/headroom/jquery.headroom.min.js')}}"></script>
<script src="{{asset('tribu/assets/footer-reveal/footer-reveal.min.js')}}"></script>
<script src="{{asset('tribu/assets/bootstrap-select/js/bootstrap-select.min.js')}}"></script>
<script src="{{asset('tribu/assets/swiper/js/swiper.min.js')}}"></script>
<script src="{{asset('tribu/assets/parallax/parallax.min.js')}}"></script>
<script src="{{asset('tribu/assets/viewport-checker/viewportchecker.min.js')}}"></script>
<script src="{{asset('tribu/assets/milestone-counter/jquery.countTo.js')}}"></script>

<!-- Light Gallery -->
<script src="{{asset('tribu/assets/lightgallery/js/lightgallery.min.js')}}"></script>
<script src="{{asset('tribu/assets/lightgallery/js/lg-thumbnail.min.js')}}"></script>
<script src="{{asset('tribu/assets/lightgallery/js/lg-video.js')}}"></script>

<!-- Cross-browser responsivity scripts -->
<!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
<![endif]-->

<!-- Google Analitycs JS -->
<script type="text/javascript" src="{{asset('tribu/js/wpx.ga.js')}}"></script>


<!-- RS5.0 Core JS Files -->
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/jquery.themepunch.tools.min.js?rev=5.0')}}"></script>
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/jquery.themepunch.revolution.min.js?rev=5')}}"></script>
<script type="text/javascript" src="{{asset('tribu/js/revolution/revolution-slider-real-estate-1.js')}}"></script>
<!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/extensions/revolution.extension.actions.min.js')}}"></script>
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/extensions/revolution.extension.carousel.min.js')}}"></script>
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/extensions/revolution.extension.kenburn.min.js')}}"></script>
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/extensions/revolution.extension.layeranimation.min.js')}}"></script>
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/extensions/revolution.extension.migration.min.js')}}"></script>
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/extensions/revolution.extension.navigation.min.js')}}"></script>
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/extensions/revolution.extension.parallax.min.js')}}"></script>
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/extensions/revolution.extension.slideanims.min.js')}}"></script>
<script type="text/javascript" src="{{asset('tribu/assets/revolution/js/extensions/revolution.extension.video.min.js')}}"></script>

<!-- Cube Portfolio -->
<script src="{{asset('tribu/assets/cubeportfolio/js/jquery.cubeportfolio.min.js')}}"></script>
<script src="{{asset('tribu/assets/cubeportfolio/js/main.4-col.space.grid.js')}}"></script>

<!-- No UI slider -->
<script type="text/javascript" src="{{asset('tribu/assets/nouislider/js/nouislider.min.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        var snapSlider = document.getElementById('filterPrinceRange');

        noUiSlider.create(snapSlider, {
            start: [ 100, 900 ],

            connect: true,
            range: {
                'min': 100,
                'max': 900
            }
        });

        var snapValues = [
            document.getElementById('filterPrinceValueLow'),
            document.getElementById('filterPrinceValueHigh')
        ];

        snapSlider.noUiSlider.on('update', function( values, handle ) {
            snapValues[handle].innerHTML = values[handle];
        });
    });
</script>

<!-- App JS -->
<script src="{{asset('tribu/js/wpx.app.js')}}"></script>

<script type="text/javascript">
    
$('#boton').on('click',function(){
    var precio_desde=$('#filterPrinceValueLow').text();
    var precio_hasta=$('#filterPrinceValueHigh').text();
    $('#precio_desde').val(precio_desde);
    $('#precio_hasta').val(precio_hasta);
   /*var precio_desde=$('#filterPrinceValueLow').text();
   var precio_hasta=$('#filterPrinceValueHigh').text();
   var nombre=$('#nombre').val();
   var tipo=$('#tipo').val();
   var operacion=$('#operaciones').val();
   var sector=$('#sector').val();
   var _token=$('#_token').val();
      $.ajax({                        
           type: "POST",                 
           url: '/invermaster/proyecto',                     
           data: {'precio_desde':precio_desde,'precio_hasta':precio_hasta,'nombre':nombre,'tipo':tipo,'operacion':operacion,'sector':sector,'_token':_token},
       success: function (result) {
        window.location.href = '/invermaster/proyecto';
           //do somthing here
      }
       });*/





})
</script>

</body>
</html>
