@extends("panelAdmin")

@section('content')
<div class="box-header with-border">
   <h3 class="box-title">Formulario de tipo de actualizacion</h3>
</div>

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<?php 
  if(isset($descripcion)){?>
    <div class="<?php echo $descripcion['clase'];?>">
      <?php echo $descripcion['mensaje'];?>
    <div>
<?php
  }
?>    
 
{!! Form::open(array('url' => ['admin/tipo',$dataForm->id], 'method' => 'put', 'files'=> true)) !!}﻿

  <div class="box-body">
            <div class="col-md-12">
                <div class="col-md-4">
                <label class="col-md-12">Nombre</label>
                  <input class="form-control col-md-4" placeholder="Nombre" name="nombre" type="text" maxlength="30" value="{{ $dataForm->nombre }}" required="">
                </div>

              </div>

                <div class="row"></div>
                <br>

              <input type="text" class="hidden" name="_token" value="{{ csrf_token() }}"></input>
              <div class="row"></div>
              <br>
              <div class="col-md-12"> 
                <div class="col-md-4">
                  <a href="{{asset('admin/tipo')}}" class="btn btn-danger">Volver </a>
               </div>

                <div class="col-md-4">
               </div>
               <div class="col-md-4 text-right">
               <button class="btn  btn-success text-right">Actualizar</button>
               </div>
              </div>


                <!-- /input-group -->
              </div>
            <form>

            @stop

            {!! Form::close() !!} 
