@extends("panelAdmin")

@section('content')
<div class="box-header with-border">
   <h3 class="box-title">Formulario de Tipo de Registro</h3>
</div>

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>

            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<?php 
  if(isset($descripcion)){?>
    <div class="<?php echo $descripcion['clase'];?>">
      <?php echo $descripcion['mensaje'];?>
    </div>
<?php
  }
?>    


<form action="/admin/tipo" method="post" name="vieja" accept-charset="UTF-8">
  <div class="box-body">
            <div class="col-md-12">
                <div class="col-md-4">
                  <input class="form-control col-md-4" placeholder="Nombre" name="nombre" type="text" maxlength="50" value="{{ old('nombre') }}">
                </div>

              </div>

                <div class="row"></div>
                <br>
                <input type="text" class="hidden" name="_token" value="{{ csrf_token() }}"></input>


              <div class="col-md-12"> 
                <div class="col-md-4">
                  <a href="/admin/parametroSeguridad" class="btn btn-danger">Volver </a>
               </div>

                <div class="col-md-4">
               </div>
               <div class="col-md-4 text-right">
               <button class="btn  btn-success text-right">Guardar></button>
               </div>
              </div>


                <!-- /input-group -->
              </div>
            <form>

            @stop
