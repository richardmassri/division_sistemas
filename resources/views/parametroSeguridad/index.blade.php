@extends("panelAdmin")

@section('content')
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Tabla de datos con funciones completas</h3>
            </div>

            <!-- /.box-header -->
            <div class="box-body table-responsive">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Día de propiedad nueva</th>
                  <th>Estatus</th>
                  <th>Accion</th>
                </tr>
                </thead>
                
                  <tbody>
                  @foreach ($consultarParametroSeguridad as $consultarParametroSeguridad)
                  <tr>
                    <td><?php echo $consultarParametroSeguridad->cantidad;?></td>
                    <td><?php if($consultarParametroSeguridad->estatus=='A'){ 
                      echo 'Activo';}else{ echo 'Inactivo'; }?></td>
                    <td> 
                    <a href="parametroSeguridad/{{$consultarParametroSeguridad->id}}" class="fa fa-fw fa-search"> </a>
                    <a href="parametroSeguridad/{{$consultarParametroSeguridad->id}}/edit" class="fa fa-fw fa-pencil"> </a>
                    </td>


                  </tr>
                @endforeach
               
                </thead>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
    <!-- /.content -->
 
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<div id="dialog1" title="Dialog Title" hidden="hidden">I'm a dialog</div>
<!-- page script -->

<div id="dialog-confirm"> Esta seguro que desea eliminar este registro?
</div>

<div id="activar"> Esta seguro que desea activar este registro?
</div>
<input type="text" class="hidden" name="_token" id="token" value="{{ csrf_token() }}"></input>


@stop
@section('scripts')

      <script>

    $(function () {
    $("#example1").DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
        language: {
        "sProcessing":     "Procesando...",
        "sLengthMenu":     "Mostrar _MENU_ registros",
        "sZeroRecords":    "No se encontraron resultados",
        "sEmptyTable":     "Ningún dato disponible en esta tabla",
        "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix":    "",
        "sSearch":         "Buscar:",
        "sUrl":            "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst":    "Primero",
            "sLast":     "Último",
            "sNext":     "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
    }
});
});

</script>

@stop