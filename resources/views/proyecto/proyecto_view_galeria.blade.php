@extends("panelAdmin")

@section('content')

<style>
 .content-wrapper {
    min-height: 738px;
}
</style>


<?php $paginacion= $dataFormGaleria;?>

<div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Formulario de galeria</h3>
            </div>
<form enctype="multipart/form-data" id="formuploadajax" method="post">
@foreach($dataFormGaleria as $dataFormGaleria)

<div class="col-md-4">
<br>
<?php if($dataFormGaleria->destacada!='SI'){?>
<i class="fa fa-fw fa-trash elim col-md-4" onclick="eliminarImagen({{$dataFormGaleria->id}});"> 
</i>
<?php }else{?> <i class="col-md-1"> 
</i>
<?php } ?>
<label for="FileInput">
    <img src="{{asset($dataFormGaleria->nombre)}}" class="col-md-4" style="cursor:pointer;width:200px;height: 150px " alt="Injaz Msila" style="float:right;margin:7px" onclick="setearCampo('<?php echo $dataFormGaleria->id;?>','<?php echo $dataFormGaleria->nombre;?>');">

    <i class="fa fa-fw fa-pencil elim" onclick="setearCampo('<?php echo $dataFormGaleria->id;?>','<?php echo $dataFormGaleria->nombre;?>');"> 
    </i>

    <input type="file" id="FileInput" name="FileInput[]" style="cursor: pointer;  display: none">
</label>



<br>
     <input type="radio" name="visible" <?php echo ($dataFormGaleria->destacada=="SI")?'checked':''?> value="SI" onclick="cambiarPrincipal({{$dataFormGaleria->proyecto_id}},{{$dataFormGaleria->id}});"><p class="col-md-6 text-right">Principal </p> 
     <br>
     <div class="row"> </div>
          <input type="radio" name="visiblePromo" <?php echo ($dataFormGaleria->promocional=="SI")?'checked':''?> value="SI" onclick="cambiarPromocional({{$dataFormGaleria->proyecto_id}},{{$dataFormGaleria->id}});"><p class="col-md-6 text-right">Promocional </p>
     </div>

  
@endforeach


<input type="text" class="hidden" name="_token" id="token" value="{{ csrf_token() }}"></input>
<input type="text" class="hidden" name="campoId" id="campoId"></input>
<input type="text" class="hidden" name="campoFoto" id="campoFoto"></input>

</form>

</div>
    <div class="col-md-12 text-center">
    <br>
      <a href="{{asset('admin/proyecto')}}/{{$dataFormGaleria->proyecto_id}}/edit" class="btn btn-danger">Volver </a>
   </div>
<div class="row"></div>

<div class="col-md-12 text-right">
<?php echo $paginacion->render(); ?>
</div>
</div>


@stop

@section('scripts')

<script>
    $('#FileInput').on("change",function(){
    var form = $('#formuploadajax')[0];
    var data = new FormData(form);
    data.append('_token', $('input[name="_token"]').val());
$.ajax({
    type: "POST",
    //contentType: false,
    //contentType: "application/json; charset=utf-8",
    url: "{{asset('admin/proyecto/eliminar/foto')}}",
    data: data,
    dataType: "json",
    async: false,
    cache: false,
    contentType: false,
    processData: false,
    success: function (resultado) {
        console.log(resultado);
        if(resultado['statusCode']=='Exitoso'){
            window.location.href = "{{asset('admin/proyecto/galeria')}}/"+resultado['id'];
        }
    }
});
});
    function setearCampo(id,foto)
{
    $("#campoId").val(id);
    $("#campoFoto").val(foto);
}
    function cambiarPrincipal(proyecto_id,id)
{
    var _token = $("#token").val();
$.ajax({
    type: "POST",
    url: "{{asset('admin/proyecto/cambiar/imagen/principla')}}",
    data: { proyecto_id:proyecto_id, id: id,_token:_token },
    dataType: "json",
    success: function (resultado) {
        console.log(resultado);
        if(resultado['statusCode']=='Exitoso'){
            window.location.href = "{{asset('admin/proyecto/galeria')}}/"+resultado['id'];
        }
    }
})
}

    function cambiarPromocional(proyecto_id,id)
{
    var _token = $("#token").val();
$.ajax({
    type: "POST",
    url: "{{asset('admin/proyecto/cambiar/imagen/promocional')}}",
    data: { proyecto_id:proyecto_id,id:id,_token:_token },
    dataType: "json",
    success: function (resultado) {
        console.log(resultado);
        if(resultado['statusCode']=='Exitoso'){
            window.location.href = "{{asset('admin/proyecto/galeria')}}/"+resultado['id'];
        }
    }
})
}

    function eliminarImagen(id)
{
	var _token = $("#token").val();
$.ajax({
    type: "POST",
    url: "{{asset('admin/proyecto/eliminar/imagen')}}",
    data: { id: id,_token:_token },
    dataType: "json",
    success: function (resultado) {
        console.log(resultado);
        if(resultado['statusCode']=='Exitoso'){
            window.location.href = "{{asset('admin/proyecto/galeria')}}/"+resultado['id'];
        }
    }
})
}
</script>

@stop




