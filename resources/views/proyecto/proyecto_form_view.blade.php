@extends("panelAdmin")
@section('css')


<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.0.2/css/bootstrap.min.css">
<link rel="stylesheet" href="{{asset('bootstrap/css/bootstrap-multiselect.css')}}" type="text/css">
@stop
@section('content')

<div id="scrollTop">
<form action="/admin/proyecto" method="post" name="vieja" accept-charset="UTF-8" enctype="multipart/form-data" id="form">
  <div class="box-body">

            <div class="col-md-12">


                <div class="col-md-6">
                <label class="col-md-12"> Nombre </label>
                  <input class="form-control col-md-4" placeholder="Nombre" name="nombre" type="text" maxlength="200" value="{{ $dataForm->nombre }}" readonly="readonly" required="required">
                </div>

                <div class="col-md-6">
                <label class="col-md-12"> Descripción Breve </label>
                <textarea class="form-control col-md-4" name="descripcion_breve" placeholder="Descripcion Breve" cols="40" rows="5" value="{{ $dataForm->descripcion_breve }}" readonly="readonly" required="required" maxlength="107"></textarea>
                </div>


              </div>

                <div class="row"></div>
                <br>


              <div class="col-md-12">

                  <div class="form-group col-md-6">
                  <label class="col-md-12"> Video </label>
                      <input class="form-control col-md-4" name="video" placeholder="Video" type="text" maxlength="200" value="{{ $dataForm->video }}" readonly="readonly" required="required">
                  <!-- /.input group -->
                </div>


                  <div class="form-group col-md-6">
                  <label class="col-md-12"> Fecha de la publicación </label>
                  <?php
                  $convertirFecha=new DateTime($dataForm->fecha_publicacion);?>
                      <input class="form-control col-md-4 datepicker" name="fecha_publicacion" placeholder="Fecha de la Publicación" type="text" maxlength="11" value="{{ $convertirFecha->format('d-m-Y') }}" readonly="readonly" >
                  <!-- /.input group -->
                  </div>

                
                  <!-- /.input group -->



              </div>


                <div class="row">
                </div>
                <br>

                  <div class="col-md-12">
                    <div class="col-md-4">
                    <label class="col-md-12"> Fecha Inicial </label>
                     <?php
                      $convertirFecha=new DateTime($dataForm->fecha_inicial);?>
                      <input class="form-control col-md-4 datepicker" placeholder="Fecha Inicial" name="fecha_inicial" type="text" maxlength="50" value="{{ $convertirFecha->format('d-m-Y') }}" readonly="readonly">
                    </div>
                    <div class="col-md-4">
                    <?php
                      $convertirFecha=new DateTime($dataForm->fecha_final);?>
                    <label class="col-md-12"> Fecha Final </label>
                        <input class="form-control col-md-4 datepicker" placeholder="Fecha Final" name="fecha_final" type="text" maxlength="50" value="{{ $convertirFecha->format('d-m-Y') }}" readonly="readonly">
                      </div>
                        <?php $agenteValor=$dataForm->agente_id;?>
                        <div class="form-group col-md-4">
                        <label class="col-md-12"> Agente </label>
                          <select class="col-md-4 form-control" name="agente_id" required="required" readonly>
                          <option value="">Seleccione el Agente</option>
                           @foreach($agentes as $agente)
                              <option value="{{ $agente->id }}"<?php 
                        if($agenteValor==$agente->id){?> selected="selected"
                        <?php } ?>>{{$agente->nombre}}
                        </option>
                            @endforeach
                          </select>
                        </div>
                  </div>
                  <div class="row">
                  </div>
                <br>

              <div class="col-md-12">

                <div class="col-md-4">
                <label class="col-md-12"> Precio Desde </label>
                  <input class="form-control col-md-4" name="precio_desde"  type="number" placeholder="Precio Desde"  maxlength="50" value="{{ $dataForm->precio_desde }}" readonly="readonly">
                </div>

                <div class="col-md-4">
                <label class="col-md-12"> Precio Hasta </label>
                  <input class="form-control col-md-4" placeholder="Precio Hasta" name="precio_hasta" type="number" maxlength="50" value="{{ $dataForm->precio_hasta }}" readonly="readonly">
                </div>

                <div class="col-md-4">
                <label class="col-md-12"> Tasa </label>
                  <input class="form-control col-md-4" placeholder="Tasa" name="tasa" id="tasa" type="number" maxlength="50" value="{{ $dataForm->tasa }}" required="required" onkeypress="return validaNumero(event)" readonly="readonly">
                </div>
                </div>
                <div class="row"> </div>
                <br>

                <div class="col-md-12"> 
                <div class="col-md-4">
                <label class="col-md-12"> precio desde en $ </label>
                  <input class="form-control col-md-4" placeholder="Precio Desde en Dolares" name="precio_desde_en_dolares" id="precio_desde_en_dolares" type="number" maxlength="50" value="{{ $dataForm->precio_desde_en_dolares }}" readonly="readonly" onkeypress="return validaNumero(event)">
                </div>

                <div class="col-md-4">
                  <label class="col-md-12"> precio hasta en $ </label>
                    <input class="form-control col-md-4" placeholder="Precio Hasta en Dolares" name="precio_hasta_en_dolares" id="precio_hasta_en_dolares" type="number" maxlength="50" value="{{ $dataForm->precio_hasta_en_dolares }}" readonly="readonly" onkeypress="return validaNumero(event)">
                </div>


                <div class="col-md-4">
                    Esta en contruccion? 
                    <input type="radio" name="construccion" value="SI" <?php echo ($dataForm->construccion=="SI")?"checked":'';?>> SI
                    <input type="radio" name="construccion" value="NO" <?php echo ($dataForm->contruccion=="NO")?"checked":'';?>>NO
                </div>
              </div>

                <div class="row"></div>
                <br>
                  <div class="col-md-12">
                   <div class="form-group col-md-4">
                   <label class="col-md-12"> País </label>
                  <select class="col-md-4 form-control" name="pais_id" id="pais" required="required" disabled>

                    <option value="">Pais --Seleccione --</option>
                     @foreach($pais as $pais)
                      <option value="{{ $pais->id }}" <?php echo ($dataForm->pais_id==$pais->id)?"selected":'selected';?>>       {{$pais->nombre}}  
                      </option>
                      @endforeach
                    </select>
                  </div>

                  <div class="form-group col-md-4">
                  <label class="col-md-12"> Ciudad </label>
                  <select class="col-md-4 form-control" name="ciudad_id" id="ciudad" required="required" disabled>
                    @foreach($ciudad as $ciudad)
                      <option value="{{ $ciudad->id }}" <?php echo ($dataForm->ciudad_id==$ciudad->id)?"selected":'';?>> {{$ciudad->nombre}}</option>
                      @endforeach
                  </select>
                </div>                


                  <div class="form-group col-md-4">
                  <label class="col-md-12"> Sector </label>
                  <select class="col-md-4 form-control" name="sector_id" id="sector" required="required" disabled>
                    @foreach($sector as $sector)
                      <option value="{{ $sector->id }}" <?php echo ($dataForm->sector_id==$sector->id)?"selected":'';?>> {{$sector->nombre}}</option>
                      @endforeach
                  </select>
                </div>

                  <input type="text" class="hidden" id="token" name="_token" value="{{ csrf_token() }}"></input>

              </div>
              <br>
              <?php
              $caracteristicasConcat="";?>
                          @foreach($consultaproyectosCaracteristicas as $consultaproyectosCaracteristicas)
                            <?php
                                    $caracteristicasConcat=$caracteristicasConcat.$consultaproyectosCaracteristicas->caracteristica_id.",";
                                  ?>
                          @endforeach
                          <?php
                          $maximoCadena=strlen($caracteristicasConcat);
                          $cadenaMaxima=$caracteristicasConcat[$maximoCadena-1];
                          if($cadenaMaxima==','){
                           $reemplazarCadena=substr($caracteristicasConcat,0,(int)$cadenaMaxima-1);
                          } ?>
                          
                      <input type="text" class="hidden caracteristicaProyecto" value="{{$reemplazarCadena}}"> </input>

                          <?php
                            $caracteristicasConcat="";?>
                          @foreach($consultaproyectosTipos as $consultaproyectosTipos)
                            <?php
                                    $caracteristicasConcat=$caracteristicasConcat.$consultaproyectosTipos->tipo_id.",";
                                  ?>
                          @endforeach
                          <?php
                          $maximoCadena=strlen($caracteristicasConcat);
                          $cadenaMaxima=$caracteristicasConcat[$maximoCadena-1];
                          if($cadenaMaxima==','){
                           $reemplazarCadena=substr($caracteristicasConcat,0,(int)$cadenaMaxima-1);
                          } ?>
                          
                      <input type="text" class="hidden tiposProyecto" value="{{$reemplazarCadena}}"> </input>
                  <div class="col-md-12">
                      <div class="col-md-4 form-group">
                      <label class="col-md-12"> Características </label>
                        <select id="caracteristica" name="caracteristica[]" multiple="multiple" class="col-md-4 form-control caracteristica" readonly="readonly">
                        @foreach($caracteristica as $caracteristica)
                        <option value="{{$caracteristica->id}}"> &lt;img src="/{{$caracteristica->icono}}" width="10%" height="10%" class=&quot;fa fa-user&quot;&gt;&lt;/i&gt;  {{$caracteristica->nombre}} </option>
                        @endforeach
                        </select>
                    </div>
                    <?php $estatus_invermaste=$dataForm->estatus_invermaste;?>
                    <div class="form-group col-md-4">
                    <label class="col-md-12"> Estatus de Invermater </label>
                          <select class="col-md-4 form-control" name="estatus_invermaste" required="required" value="{{ old('estatus_invermaste') }}" readonly="readonly">
                           <option  value="" <?php echo ($estatus_invermaste!="A")?"selected='selected'":'';?>>Estatus de invermaster</option>
                           <option value="A" <?php echo ($estatus_invermaste=="A")?"selected='selected'":'';?>>Activo</option>
                           <option value="I">Inactivo</option>
                          </select>
                    </div>
                    <div class="col-md-4 form-group">
                    <label class="col-md-12"> Tipo </label>
                      <select id="tipo" name="tipo[]" multiple="multiple" class="col-md-4 form-control tipo" readonly>
                      @foreach($tipo as $tipo)
                      <option value="{{$tipo->id}}">{{$tipo->nombre}} </option>
                      @endforeach
                      </select>
                    </div>
                  </div>


              <div class="col-md-12">
                <div class="form-group col-md-12">
                <label class="col-md-12"> Ubicación </label>
                  <input class="col-md-12 form-control" name="ubicacion" placeholder="Ubicacion" type="text" maxlength="500" value="<?php echo $dataForm->ubicacion;?>" required="required" id="address">
                </div>
                <div class="col-md-4">
                  <input class="btn  btn-info text-right" id="search" type="button" value ="Buscar" />
                  </div>
                </div>
                   
                  
                    <div class="clear"></div>
                    <div class="col-md-12 container">
                    <div id="map_canvas" class="col-md-12" style=" width:800px; 
      height:400px;"></div>
                    </div>
              <input type="text" class="hidden" id="latitud" name="latitud">
              <input type="text" class="hidden" id="longitud" name="longitud">
              <br>
                    <input type="text" class="hidden" name="_token" value="{{ csrf_token() }}"></input>  
              <?php $tag=strip_tags($dataForm->descripcion);?>
                <div class="row"> </div>
                  <br> 
                <div class="col-md-12">
                    <textarea id="descripcion" name="descripcion" rows="10" cols="120"><?php echo $tag;?>
                      </textarea>
                </div>


                  <div class="row">
                </div>
                <br>


                <div class="col-md-12">
                 <div class="col-md-6">
                <label class="col-md-12"> Fecha de Actualización </label>
                  <input class="form-control col-md-4" name="contrasena_nuevaForm" type="text" maxlength="50" value="{{ $dataForm->updated_at }}" readonly="readonly">
                </div>
                <div class="col-md-6">
                <label class="col-md-12"> Fecha de Creación </label>
                  <input class="form-control col-md-4" name="contrasenaForm"  type="text" maxlength="50" value="{{ $dataForm->created_at }}" readonly="readonly">
                </div>
                </div>
                <div class="row">
                </div>
                <br>
              <div class="col-md-12">
                @if($dataForm->usuario_ini_id)
                <div class="col-md-6">
                <label class="col-md-12"> Creado por: </label>
                  <input class="form-control col-md-4" name="contrasenaForm"  type="text" maxlength="50" value="{{ $dataForm->metodo_usuario_ini_id->nombre}} {{ $dataForm->metodo_usuario_ini_id->apellido}}" readonly="readonly">
                </div>
                @endif


                @if($dataForm->usuario_act_id)
                <div class="col-md-6">
                <label class="col-md-12"> Actualizado por: </label>
                  <input class="form-control col-md-4" name="contrasenaForm"  type="text" maxlength="50" value="{{ $dataForm->metodo_usuario_act_id->nombre}} {{ $dataForm->metodo_usuario_act_id->apellido}}" readonly="readonly">
                </div>
                @endif


            <div class="row"> </div>
            <br>

              <div class="col-md-12"> 

                <div class="col-md-4">
                  <a href="{{asset('admin/proyecto')}}" class="btn btn-danger">Volver </a>
               </div>
              </div>
              </div>
              </form>
       

</body>
</div>
</div>
</div>
@stop

@section('scripts')

<script type="text/javascript" src="{{asset('bootstrap/js/bootstrap-multiselect.js')}}"></script>


<!--Api de google -->
<script type="text/javascript">
      function myMap() {
      var latitud=$('#latitud').val();
      var longitud=$('#longitud').val();

      var myLatLng = {lat:parseFloat(latitud) , lng:parseFloat(longitud)};

  var map = new google.maps.Map(document.getElementById('map_canvas'), {
    zoom: 6,
    center: myLatLng,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  });

  var marker = new google.maps.Marker({
    position: myLatLng,
    map: map,
    draggable:true,
    title: 'Hello World!'
  });
}
    </script>
<script type="text/javascript">
$('#search').on("click",function(){
  googleMaps();
})
function googleMaps(){
    ubicacion=$('#address').val();
    if(ubicacion==''){
      ubicacion='caracas';
    }
    $.ajax({
    type: "GET",
    url: "https://maps.googleapis.com/maps/api/geocode/json",
    data: {
    address: ubicacion,
    key:'AIzaSyCTTHufNeM1zuZJYKrrtqJ1KUjqNM6_tWA' },
    dataType: "json",
    success: function (resultado) {
      console.log(resultado);
      var latitud=resultado['results'][0]['geometry']['location']['lat'];
      var longitud=resultado['results'][0]['geometry']['location']['lng'];
      $('#latitud').val(latitud);
      $('#longitud').val(longitud);
      myMap();
    }
    });
  }
  googleMaps();
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB8kPVK6nmaoLI0EoIm8KvgdoXp3ohWzLQ&callback=myMap"></script>

<script type="text/javascript">
$(document).ready(function() {

  var values=$('.caracteristicaProyecto').val();
  //var values='1,2,3';
    $.each(values.split(","), function(i,e){
    $(".caracteristica option[value='" + e + "']").prop("selected", true);
});
    var values2=$('.tiposProyecto').val();
  //var values='1,2';
    $.each(values2.split(","), function(i,e){
    $(".tipo option[value='" + e + "']").prop("selected", true);
});
  $('#demo').multiselect({
    buttonWidth: '300px',
    enableHTML: true,
    //nonSelectedText: 'Selecione la operacion'
  })
  $('#caracteristica').multiselect({
    buttonWidth: '300px',
    nonSelectedText: 'Selecione la/s caracteristica',
    enableHTML: true,
  })
  $('#tipo' ).multiselect({
    buttonWidth: '300px',
    nonSelectedText: 'Selecione el/los tipos',
    enableHTML: true,
  });
  });
</script>




@stop




