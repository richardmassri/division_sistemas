@extends("panelAdmin")

@section('content')
<div class="box-header with-border">
   <h3 class="box-title">Formulario de actualización de País</h3>
</div>

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<?php 
  if(isset($descripcion)){?>
    <div class="<?php echo $descripcion['clase'];?>">
      <?php echo $descripcion['mensaje'];?>
    <div>
<?php
  }
?>    
 
{!! Form::open(array('url' => ['admin/sector',$dataForm->id], 'method' => 'put', 'files'=> true)) !!}﻿

  <div class="box-body">
            <div class="col-md-12">
                <div class="col-md-6">
                <label class="col-md-12">Nombre</label>
                  <input class="form-control col-md-4" placeholder="Nombre" name="nombre" type="text" maxlength="30" value="{{ $dataForm->nombre }}" required="">
                </div>

              <div class="col-md-6">
                <label class="col-md-12">Ciudad</label>
                  <select class="col-md-6 form-control" name="ciudade_id" required="required">
                  <option value="">Seleccione</option>
                  @foreach ($ciudad as $ciudad)
                    <option value="{{$ciudad->id}}"<?php echo ($dataForm->ciudade_id==$ciudad->id)?"selected":"";?>>{{$ciudad->nombre}}</option>
                  @endforeach
                  </select>
                </div>

              </div>

                <br>
                <div class="col-md-12">

              <input type="text" class="hidden" name="_token" value="{{ csrf_token() }}"></input>
              <div class="row"></div>
              <br>
              <div class="col-md-12"> 
                <div class="col-md-4">
                  <a href="{{asset('admin/sector')}}" class="btn btn-danger">Volver </a>
               </div>

                <div class="col-md-4">
               </div>
               <div class="col-md-4 text-right">
               <button class="btn  btn-success text-right">Actualizar</button>
               </div>
              </div>


                <!-- /input-group -->
              </div>
            <form>

            @stop

            {!! Form::close() !!} 
