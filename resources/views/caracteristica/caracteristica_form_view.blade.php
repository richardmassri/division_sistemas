@extends("panelAdmin")

@section('content')
<div class="box-header with-border">
   <h3 class="box-title">Formulario de característica Vista</h3>
</div>


  <div class="box-body">
            <div class="col-md-12">
                <div class="col-md-6">
                <label class="col-md-12"> Nombre </label>
                  <input class="form-control col-md-4" name="nombre" type="text" maxlength="50" value="{{ $dataForm->nombre }}" readonly="readonly">
                </div>

               <div class="form-group col-md-6">
               <label class="col-md-12"> Icono </label>
               <img width="120" height="80" src="{{asset($dataForm->icono)}}">
                </div>

              </div>
              <div class="col-md-12">
               <div class="col-md-6">
               <label class="col-md-12"> Aplica Proyecto </label>
                  <input class="form-control col-md-4" name="aplica_proyecto" type="text" maxlength="50" value="{{ $dataForm->aplica_proyecto }}" readonly="readonly">
                </div>

                <div class="col-md-6">
                <label class="col-md-12"> Aplica Propiedad </label>
                  <input class="form-control col-md-4" type="text" maxlength="50" value="{{ $dataForm->aplica_propiedad }}" readonly="readonly">
                </div>
              </div>

                <div class="row"></div>
                <br>
                <div class="col-md-12">
                <div class="col-md-12">
                  <textarea class="form-control col-md-12" cols="40" rows="5" value="" readonly="readonly" required="required">{{ $dataForm->descripcion }}</textarea>
                </div>
                </div>
                <div class="row"></div>
              <br>


            <div class="col-md-12">

                <div class="col-md-6">
                <label class="col-md-12"> Fecha de Actualización </label>
                  <input class="form-control col-md-4" name="contrasena_nuevaForm" type="text" maxlength="50" value="{{ $dataForm->updated_at }}" readonly="readonly">
                </div>
                <div class="col-md-6">
                <label class="col-md-12"> Fecha de Creación </label>
                  <input class="form-control col-md-4" name="contrasenaForm"  type="text" maxlength="50" value="{{ $dataForm->created_at }}" readonly="readonly">
                </div>
              </div>
                <div class="row">
                </div>
                <br>
              <div class="col-md-12">
                @if($dataForm->usuario_ini_id)
                <div class="col-md-6">
                <label class="col-md-12"> Creado por: </label>
                  <input class="form-control col-md-4" name="contrasenaForm"  type="text" maxlength="50" value="{{ $dataForm->metodo_usuario_ini_id->nombre}} {{ $dataForm->metodo_usuario_ini_id->apellido}}" readonly="readonly">
                </div>
                @endif


                @if($dataForm->usuario_act_id)
                <div class="col-md-6">
                <label class="col-md-12"> Actualizado por: </label>
                  <input class="form-control col-md-4" name="contrasenaForm"  type="text" maxlength="50" value="{{ $dataForm->metodo_usuario_act_id->nombre}} {{ $dataForm->metodo_usuario_act_id->apellido}}" readonly="readonly">
                </div>
                @endif

              </div>

                <input type="text" class="hidden" name="_token" value="{{ csrf_token() }}"></input>


              <div class="row"></div>

              <br>

              <div class="col-md-12"> 

                <div class="col-md-4">
                  <a href="{{asset('admin/caracteristica')}}" class="btn btn-danger">Volver </a>
               </div>

                <div class="col-md-4">
               </div>
              </div>


                <!-- /input-group -->
              </div>
            <form>

            @stop

            {!! Form::close() !!} 
