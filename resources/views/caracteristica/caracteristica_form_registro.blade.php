@extends("panelAdmin")

@section('content')
<div class="box-header with-border">
   <h3 class="box-title">Formulario de característica</h3>
</div>

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>

            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<?php 
  if(isset($descripcion)){?>
    <div class="<?php echo $descripcion['clase'];?>">
      <?php echo $descripcion['mensaje'];?>
    </div>
<?php
  }
?>    


<form action="{{asset('admin/caracteristica')}}" method="post" name="vieja" accept-charset="UTF-8" enctype="multipart/form-data">
  <div class="box-body">
            <div class="col-md-12">
                <div class="col-md-6">
                  <input class="form-control col-md-4" placeholder="Nombre" name="nombre" type="text" maxlength="50" value="{{ old('nombre') }}" required>
                </div>

               <div class="form-group col-md-6">
                    <label for="exampleInputFile">Seleccione el icono</label>
                    <input id="exampleInputFile col-md-4" type="file" name="icono" value="{{ old('icono') }}">
                </div>

              </div>
              <div class="col-md-12">
               <div class="col-md-6">
                    Aplica proyecto? 
                    <input type="radio" name="aplica_proyecto" value="SI">SI
                    <input type="radio" name="aplica_proyecto" value="NO">NO
                </div>

                <div class="col-md-6">
                    Aplica propiedad? 
                    <input type="radio" name="aplica_propiedad" value="SI">SI
                    <input type="radio" name="aplica_propiedad" value="NO">NO
                </div>
              </div>

                <div class="row"></div>
                <br>
                <div class="col-md-12">
                <div class="col-md-12">
                  <textarea class="form-control col-md-12" name="descripcion" placeholder="Descripcion" cols="40" rows="5" value="{{ old('descripcion') }}"></textarea>
                </div>
                </div>
                <input type="text" class="hidden" name="_token" value="{{ csrf_token() }}"></input>


              <div class="col-md-12"> 
                              <br>
                <div class="col-md-4">
                  <a href="{{asset('admin/caracteristica')}}" class="btn btn-danger">Volver </a>
               </div>

                <div class="col-md-4">
               </div>
               <div class="col-md-4 text-right">
               <button class="btn  btn-success text-right">Guardar</button>
               </div>
              </div>


                <!-- /input-group -->
              </div>
            <form>

            @stop
