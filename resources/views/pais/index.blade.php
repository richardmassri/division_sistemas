@extends("panelAdmin")

@section('content')
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">

  <link rel="stylesheet" href="{{ asset('dist/css/skins/_all-skins.min.css')}}">



          <div class="box ">
            <div class="box-header">
              <h3 class="box-title"><strong>Gestión de Pais </strong></h3>
            </div>

            <div class="text-right col-md-12">
            <a href="{{asset ('admin/pais/create')}}" class="btn btn-success">Registar
            </a>
            </div>
            <br><br>

            <!-- /.box-header -->
            <div class="box-body table-responsive">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Nombre</th>
                  <th>Estatus</th>
                  <th>Accion</th>
                </tr>
                </thead>
                
                  <tbody>
                  @foreach ($consultarPais as $consultarPais)
                  <tr>
                    <td><?php echo $consultarPais->nombre;?></td>
                    <td><?php if($consultarPais->estatus=='A'){ 
                      echo 'Activo';}else{ echo 'Inactivo'; }?></td>
                    <td>
                    <a href="pais/{{$consultarPais->id}}" class="fa fa-fw fa-search"> </a>
                    <?php if($consultarPais->estatus=='A'){?>
                    <a href="pais/{{$consultarPais->id}}/edit" class="fa fa-fw fa-pencil"> </a>
                    <?php } ?>
                    <?php if($consultarPais->estatus=='A'){?>
                    <a href="#" class="fa fa-fw fa-trash elim" onclick="eliminar(<?php echo $consultarPais->id;?>,'pais')
                    ">
                    <?php }else{?>
                    <a href="#" class="fa fa-fw fa-check-square-o activar" onclick="activar(<?php echo $consultarPais->id;?>,'pais')
                    ">
                    <?php }?> 

                    </a>
                    </td>
                  </tr>
                @endforeach
               
                </thead>
              </table>

              <div style="margin-top: 50px;"> </div>

            <div class="box-header">
              <h3 class="box-title"><strong>Gestión de Ciudad </strong></h3>
            </div>
            <div class="text-right">
              <a href="{{asset ('admin/ciudad/create')}}" class="btn btn-success">Registar
              </a>
            </div>
            <br>
              <table id="example2" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Nombre</th>
                  <th>Pais</th>
                  <th>Estatus</th>
                  <th>Accion</th>
                </tr>
                </thead>
                
                  <tbody>
                  @foreach ($consultarCiudad as $consultarCiudad)
                  <tr>
                    <td><?php echo $consultarCiudad->nombre;?></td>
                    <td><?php echo $consultarCiudad->pais->nombre;?></td>
                    <td><?php if($consultarCiudad->estatus=='A'){ 
                      echo 'Activo';}else{ echo 'Inactivo'; }?></td>
                    <td>
                    <a href="ciudad/{{$consultarCiudad->id}}" class="fa fa-fw fa-search"> </a>
                    <?php if($consultarCiudad->estatus=='A'){?>
                    <a href="ciudad/{{$consultarCiudad->id}}/edit" class="fa fa-fw fa-pencil"> </a>
                    <?php } ?>
                    <?php if($consultarCiudad->estatus=='A'){?>
                    <a href="#" class="fa fa-fw fa-trash elim" onclick="eliminar(<?php echo $consultarCiudad->id;?>,'ciudad')
                    ">
                    <?php }else{?>
                    <a href="#" class="fa fa-fw fa-check-square-o activar" onclick="activar(<?php echo $consultarCiudad->id;?>,'ciudad')
                    ">
                    <?php }?> 

                    </a>
                    </td>
                  </tr>
                @endforeach
               
                </thead>
              </table>

            <div class="box-header">
              <h3 class="box-title"><strong>Gestión de Sector </strong></h3>
            </div>

            <div class="text-right">
              <a href="{{asset ('admin/sector/create')}}" class="btn btn-success">Registar
              </a>
            </div>
            <br>

               <table id="example3" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Nombre</th>
                  <th> Ciudad </th>
                  <th>Estatus</th>
                  <th>Accion</th>
                </tr>
                </thead>
                
                  <tbody>
                  @foreach ($consultarSector as $consultarSector)
                  <tr>
                    <td><?php echo $consultarSector->nombre;?></td>
                    <td><?php echo $consultarSector->ciudad->nombre;?></td>
                    <td><?php if($consultarSector->estatus=='A'){ 
                      echo 'Activo';}else{ echo 'Inactivo'; }?></td>
                    <td>
                    <a href="sector/{{$consultarSector->id}}" class="fa fa-fw fa-search"> </a>
                    <?php if($consultarSector->estatus=='A'){?>
                    <a href="sector/{{$consultarSector->id}}/edit" class="fa fa-fw fa-pencil"> </a>
                    <?php } ?>
                    <?php if($consultarSector->estatus=='A'){?>
                    <a href="#" class="fa fa-fw fa-trash elim" onclick="eliminar(<?php echo $consultarSector->id;?>,'sector')
                    ">
                    <?php }else{?>
                    <a href="#" class="fa fa-fw fa-check-square-o activar" onclick="activar(<?php echo $consultarSector->id;?>,'sector')
                    ">
                    <?php }?> 

                    </a>
                    </td>
                  </tr>
                @endforeach
               
                </thead>
              </table>

            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
    <!-- /.content -->
 
<!-- ./wrapper -->




<!-- jQuery 2.2.3 -->
<div id="dialog1" title="Dialog Title" hidden="hidden">I'm a dialog</div>

<div id="dialog-confirm"> Esta seguro que desea eliminar este registro?
</div>

<div id="activar"> Esta seguro que desea activar este registro?
</div>
<input type="text" class="hidden" name="_token" id="token" value="{{ csrf_token() }}"></input>


@stop
@section('scripts')
  <script src="http://code.jquery.com/ui/1.11.1/jquery-ui.min.js"></script>
  <link rel="stylesheet" href="https://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css" />
    <script>
    function eliminar(id,tipo){
      $('body, div').animate({ scrollTop: $("#dialog-confirm").offset().top }, 800);

      var ruta="";
      if(tipo=="pais"){
        ruta= "{{asset('admin/pais')}}/"+id+'-'+'e';
      }else if(tipo=="ciudad"){
        ruta= "{{asset('admin/ciudad')}}/"+id+'-'+'e';
      }else{
        ruta= "{{asset('admin/sector')}}/"+id+'-'+'e';
      }
      var _token = $("#token").val();
    $( "#dialog-confirm" ).dialog({
            resizable: false,
            scroll: false,
            //position: { my: 'top', at: 'top+50' },
            modal: true,
            //draggable: true,
            buttons: {
                "Eliminar": function() {
                  var _token = $("#token").val();

                  $.ajax({
                  type: "DELETE",
                  url: ruta,
                  data: { id: id,_token:_token},
                  dataType: "json",
                  success: function (resultado) {
                    //$( this ).dialog( "close" );
                    $( "#dialog-confirm" ).dialog("close");
                    window.location.href = "{{asset('admin/pais')}}";
                        }
                  })
                },
                Cancel: function() {
                    $( this ).dialog( "close" );
                }
            }
        });
  }

   function activar(id,tipo){
      $('body, html').animate({ scrollTop: $("#dialog-confirm").offset().top }, 800);
      var _token = $("#token").val();
      var ruta="";
      if(tipo=="pais"){
        ruta= "{{asset('admin/pais')}}/"+id+'-'+'a';
      }else if(tipo=="ciudad"){
        ruta= "{{asset('admin/ciudad')}}/"+id+'-'+'a';
      }else{
        ruta= "{{asset('admin/sector')}}/"+id+'-'+'a';
      }
    $( "#activar" ).dialog({
            resizable: false,
            modal: true,
            //position: { my: 'top', at: 'top+250' },
            buttons: {
                "Activar": function() {
                  var code = $("#ciudad").val();
                  var _token = $("#token").val();
                  var tipo="a";
                  $.ajax({
                  type: "DELETE",
                  url: ruta,
                  data: { id: id,_token:_token},
                  dataType: "json",
                  success: function (resultado) {
                    //$( this ).dialog( "close" );
                    $( "#activar" ).dialog("close");
                    window.location.href = "{{asset('admin/pais')}}";
                        }
                  })
                },
                Cancel: function() {
                    $( this ).dialog( "close" );
                }
            }
        });
  }


</script>


<!-- page script -->
<script>

    $(function () {
    $("#example1").DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
        language: {
        "sProcessing":     "Procesando...",
        "sLengthMenu":     "Mostrar _MENU_ registros",
        "sZeroRecords":    "No se encontraron resultados",
        "sEmptyTable":     "Ningún dato disponible en esta tabla",
        "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix":    "",
        "sSearch":         "Buscar:",
        "sUrl":            "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst":    "Primero",
            "sLast":     "Último",
            "sNext":     "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
    }
});
});


  $(function () {
    $("#example2").DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
        language: {
        "sProcessing":     "Procesando...",
        "sLengthMenu":     "Mostrar _MENU_ registros",
        "sZeroRecords":    "No se encontraron resultados",
        "sEmptyTable":     "Ningún dato disponible en esta tabla",
        "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix":    "",
        "sSearch":         "Buscar:",
        "sUrl":            "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst":    "Primero",
            "sLast":     "Último",
            "sNext":     "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
    }
});
});



  $(function () {
    $("#example3").DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
        language: {
        "sProcessing":     "Procesando...",
        "sLengthMenu":     "Mostrar _MENU_ registros",
        "sZeroRecords":    "No se encontraron resultados",
        "sEmptyTable":     "Ningún dato disponible en esta tabla",
        "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix":    "",
        "sSearch":         "Buscar:",
        "sUrl":            "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst":    "Primero",
            "sLast":     "Último",
            "sNext":     "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
    }
});
});


</script>


@stop