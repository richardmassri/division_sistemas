@extends("panelAdmin")

@section('content')
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">


          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Información de la Empresa</h3>
            </div>

            <!-- /.box-header -->
            <div class="box-body table-responsive">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>logotipo</th>
                  <th>Nombre</th>
                  <th>Telefono</th>
                  <th>Correo</th>
                  <th>Facebook</th>
                  <th>Twitter</th>
                  <th>Instagram</th>
                  <th>Accion</th>
                </tr>
                </thead>
                
                  <tbody>
                  @foreach ($consultarEmpresa as $consultarEmpresa)
                  <tr>

                    <td> <img width="120" height="80" src="{{asset($consultarEmpresa->logotipo)}}"> </td>
                    <td><?php echo $consultarEmpresa->nombre;?></td>
                    <td><?php echo $consultarEmpresa->telefono;?></td>
                    <td><?php echo $consultarEmpresa->correo;?></td>
                    <td><?php echo $consultarEmpresa->facebook;?></td>
                    <td><?php echo $consultarEmpresa->twitter?> </td>
                    <td><?php echo $consultarEmpresa->instagram?></td>
                    <td>
                    <a href="empresa/{{$consultarEmpresa->id}}" class="fa fa-fw fa-search"> </a>
                    <a href="empresa/{{$consultarEmpresa->id}}/edit" class="fa fa-fw fa-pencil"> </a>
                    </td>
                  </tr>
                @endforeach
               
                </thead>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
    <!-- /.content -->
 
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<div id="dialog1" title="Dialog Title" hidden="hidden">I'm a dialog</div>
<!-- page script -->


<div id="dialog-confirm"> Esta seguro que desea eliminar este registro?
</div>

<div id="activar"> Esta seguro que desea activar este registro?
</div>
<input type="text" class="hidden" name="_token" id="token" value="{{ csrf_token() }}"></input>


@stop
@section('scripts')
  <script src="http://code.jquery.com/ui/1.11.1/jquery-ui.min.js"></script>
  <link rel="stylesheet" href="https://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css" />
    <script>
    function eliminar(id){
      var _token = $("#token").val();
    $( "#dialog-confirm" ).dialog({
            resizable: false,
            modal: true,
            buttons: {
                "Eliminar": function() {
                  var code = $("#ciudad").val();
                  var _token = $("#token").val();

                  $.ajax({
                  type: "DELETE",
                  url: "/admin/tipo/"+id+'-'+'e',
                  data: { id: id,_token:_token},
                  dataType: "json",
                  success: function (resultado) {
                    //$( this ).dialog( "close" );
                    $( "#dialog-confirm" ).dialog("close");
                    window.location.href = '/admin/tipo'; 
                        }
                  })
                },
                Cancel: function() {
                    $( this ).dialog( "close" );
                }
            }
        });
  }

   function activar(id){
      var _token = $("#token").val();
    $( "#activar" ).dialog({
            resizable: false,
            modal: true,
            buttons: {
                "Activar": function() {
                  var code = $("#ciudad").val();
                  var _token = $("#token").val();
                  var tipo="a";
                  $.ajax({
                  type: "DELETE",
                  url: "/admin/tipo/"+id+'-'+'a',
                  data: { id: id,_token:_token},
                  dataType: "json",
                  success: function (resultado) {
                    //$( this ).dialog( "close" );
                    $( "#activar" ).dialog("close");
                    window.location.href = '/admin/tipo'; 
                        }
                  })
                },
                Cancel: function() {
                    $( this ).dialog( "close" );
                }
            }
        });
  }


      </script>

    <script>

    $(function () {
    $("#example1").DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
        language: {
        "sProcessing":     "Procesando...",
        "sLengthMenu":     "Mostrar _MENU_ registros",
        "sZeroRecords":    "No se encontraron resultados",
        "sEmptyTable":     "Ningún dato disponible en esta tabla",
        "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix":    "",
        "sSearch":         "Buscar:",
        "sUrl":            "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst":    "Primero",
            "sLast":     "Último",
            "sNext":     "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
    }
});
});
</script>

@stop