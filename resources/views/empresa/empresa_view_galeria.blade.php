@extends("panelAdmin")

@section('content')
<div class="box-header with-border">
   <h3 class="box-title">Formulario de galeria</h3>
</div>
<form enctype="multipart/form-data" id="formuploadajax" method="post">
@foreach($dataFormGaleria as $dataFormGaleria)
<div class="col-md-4">
<?php if($dataFormGaleria->destacada!='SI'){?>
<i class="fa fa-fw fa-trash elim col-md-4" onclick="eliminarImagen({{$dataFormGaleria->id}});"> 
</i>
<?php }else{?> <i class="col-md-1"> 
</i>
<?php } ?>
<label for="FileInput">
    <img src="{{asset($dataFormGaleria->nombre)}}" class="col-md-4" style="cursor:pointer;width:200px;height: 150px " alt="Injaz Msila" style="float:right;margin:7px" onclick="setearCampo('<?php echo $dataFormGaleria->id;?>','<?php echo $dataFormGaleria->nombre;?>');">

    <input type="file" id="FileInput" name="" style="cursor: pointer;  display: none">
</label>

<br>

     <input type="radio" name="visible" <?php echo ($dataFormGaleria->destacada=="SI")?'checked':''?> value="SI" onclick="cambiarPrincipal({{$dataFormGaleria->id}});"><p class="col-md-4 text-right">Principal </p>
     </div>
  
@endforeach

<input type="text" class="hidden" name="_token" id="token" value="{{ csrf_token() }}"></input>
<input type="text" class="hidden" name="campoId" id="campoId"></input>
<input type="text" class="hidden" name="campoFoto" id="campoFoto"></input>

</form>
<div class="col-md-12">
    <div class="col-md-2 text-center">
        <a href="{{ asset('admin/empresa')}}" class="btn btn-danger">Volver </a>
    </div>
</div>


@stop

@section('scripts')

<script type="text/javascript">
    $('#FileInput').on("change",function(){
    //var foto=$('#campoFoto').val();
    //var id=$('#campoId').val();
    //var _token = $("#token").val();
    //var form_upload = $('#formuploadajax').val();
    //var FileInput = $('#FileInput').val();
    //var dataForm = $('#formuploadajax').serialize();
    var form = $('#formuploadajax')[0];
    var data = new FormData(form);
    //console.log(data);
    //var imagen=$('#FileInput');
    //data.append("foto", foto);
    //data.append("id", id);
    //console.log(imagen[0].FileInput);
    //data.append("FileInput", imagen[0].file);
    //data.append("form_upload", form_upload);
    //data.append("_token", _token);
    data.append('_token', $('input[name="_token"]').val());

    //var formData = new FormData($('#formuploadajax'));
    //console.log(formData);
    //formData.append('foto',foto);
    //var id=$('#campoId').val();
$.ajax({
    type: "POST",
    //contentType: false,
    //contentType: "application/json; charset=utf-8",
    url: "{{asset('admin/empresa/eliminar/foto/')}}",
    data: data,
    dataType: "json",
    async: false,
    cache: false,
    contentType: false,
    processData: false,
    success: function (resultado) {
        console.log(resultado);
        if(resultado['statusCode']=='Exitoso'){
            window.location.href = "{{asset('admin/empresa/galeria')}}/"+resultado['id'];
        }
    }
});
});
    function setearCampo(id,foto)
{
    $("#campoId").val(id);
    $("#campoFoto").val(foto);
}
    function cambiarPrincipal(id)
{
    var _token = $("#token").val();
$.ajax({
    type: "POST",
    url: "{{asset('admin/empresa/cambiar/imagen/principla')}}",
    data: { id: id,_token:_token },
    dataType: "json",
    success: function (resultado) {
        console.log(resultado);
        if(resultado['statusCode']=='Exitoso'){
            window.location.href = "{{asset('admin/empresa/galeria')}}/"+resultado['id'];
        }
    }
})
}

    function eliminarImagen(id)
{
	var _token = $("#token").val();
$.ajax({
    type: "POST",
    url: "{{asset('admin/empresa/eliminar/imagen')}}",
    data: { id: id,_token:_token },
    dataType: "json",
    success: function (resultado) {
        console.log(resultado);
        if(resultado['statusCode']=='Exitoso'){
            window.location.href = "{{asset('admin/empresa/galeria')}}/"+resultado['id'];
        }
    }
})
}
</script>

@stop




