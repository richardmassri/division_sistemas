@extends("panelAdmin")

@section('content')
<div class="box-header with-border">
   <h3 class="box-title">Vista de empresa</h3>
</div>

<input type="text" class="hidden" id="latitud" name="latitud" value="{{ $dataForm->latitud }}">
<input type="text" class="hidden" id="longitud" name="longitud" value="{{ $dataForm->longitud}}">

  <div class="box-body">
              <div class="col-md-4">
                <label class="col-md-12">Nombre</label>
                  <input class="form-control col-md-4"  name="nombre" type="text" maxlength="100" value="{{ $dataForm->nombre }}" readonly="readonly">
                </div>

                <div class="col-md-4">
                <label class="col-md-12">Correo</label>
                  <input class="form-control col-md-4" name="correo" type="text" maxlength="30" value="{{ $dataForm->correo }}" readonly="readonly">
                </div>

              <div class="col-md-4">
                <label class="col-md-12">Telefono</label>
                  <input class="form-control col-md-4" name="telefono" type="text" maxlength="30" value="{{ $dataForm->telefono }}" readonly="readonly">
                </div>

              

                <div class="row"></div>
                <br>

              <div class="col-md-12">
                <div class="col-md-4">
                 <label class="col-md-12">Facebook</label>
                  <input class="form-control col-md-4" name="facebook" type="text" value="{{ $dataForm->facebook }}" readonly="readonly">
                </div>

                <div class="col-md-4">
                 <label class="col-md-12">Twitter</label>
                  <input class="form-control col-md-4" name="twitter" type="text" maxlength="30" value="{{ $dataForm->twitter }}" readonly="readonly">
                </div>

                <div class="col-md-4">
                <label class="col-md-12">Instagram</label>
                  <input class="form-control col-md-4" name="instagram" type="text" maxlength="30" value="{{ $dataForm->instagram }}" readonly="readonly">
                </div>

              </div>

                <div class="row"></div>
                <br>


              <div class="col-md-12">

               <div class="col-md-6">
                 <label class="col-md-12">Mision</label>
                  <textarea class="form-control col-md-4" name="vision" cols="40" rows="5" value="{{ $dataForm->vision }}" required="required"> {{ $dataForm->mision }}</textarea>
                </div>

                <div class="col-md-6">
                 <label class="col-md-12">Vision</label>
                  <textarea class="form-control col-md-4" name="vision" cols="40" rows="5" value="{{ $dataForm->vision }}" readonly="readonly"> {{ $dataForm->vision }}</textarea>
                </div>
              </div>

              <div class="row"></div>
                <br>

              <div class="col-md-12">
               <div class="col-md-4">
                <label class="col-md-12">Quines Somos</label>
                  <textarea class="form-control col-md-4" name="quines_somos" cols="40" rows="5" value="{{ $dataForm->quines_somos }}" readonly="readonly"> {{ $dataForm->quines_somos }}</textarea>
                </div>

                <div class="col-md-4">
                 <label class="col-md-12">Direccion</label>
                  <textarea class="form-control col-md-4" name="direcccion" cols="40" rows="5" value="{{ $dataForm->direcccion }}" readonly="readonly"> {{ $dataForm->direcccion }}</textarea>
                </div>

                <div class="col-md-4">
                 <label class="col-md-12">Direccion1</label>
                  <textarea class="form-control col-md-4" name="direccion1" cols="40" rows="5" value="{{ $dataForm->direccion1 }}" readonly="readonly"> {{ $dataForm->direccion1 }}</textarea>
                </div>
              </div>


                <div class="row">
                </div>
                <br>

              <div class="col-md-12">
                <div class="col-md-12">
                    <label class="col-md-12">Ubicacion</label>
                      <input class="form-control col-md-12" readonly="" value="{{ $dataForm->ubicacion }}">
                </div>
              </div>

                <div class="row">
                </div>
                <br>

                <div class="col-md-12">
                    <br>
                  <div id="map_canvas" class="col-md-12" style=" width:800px; 
                      height:400px;"></div>
                </div>

                  <div class="row">
                </div>
                <br>


                <div class="col-md-12">
                 <div class="col-md-6">
                <label class="col-md-12"> Fecha de Actualización </label>
                  <input class="form-control col-md-4" name="contrasena_nuevaForm" type="text" maxlength="50" value="{{ $dataForm->updated_at }}" readonly="readonly">
                </div>
                <div class="col-md-6">
                <label class="col-md-12"> Fecha de Creación </label>
                  <input class="form-control col-md-4" name="contrasenaForm"  type="text" maxlength="50" value="{{ $dataForm->created_at }}" readonly="readonly">
                </div>
                </div>
                <div class="row">
                </div>
                <br>
              <div class="col-md-12">
                @if($dataForm->usuario_ini_id)
                <div class="col-md-6">
                <label class="col-md-12"> Creado por: </label>
                  <input class="form-control col-md-4" name="contrasenaForm"  type="text" maxlength="50" value="{{ $dataForm->metodo_usuario_ini_id->nombre}} {{ $dataForm->metodo_usuario_ini_id->apellido}}" readonly="readonly">
                </div>
                @endif


                @if($dataForm->usuario_act_id)
                <div class="col-md-6">
                <label class="col-md-12"> Actualizado por: </label>
                  <input class="form-control col-md-4" name="contrasenaForm"  type="text" maxlength="50" value="{{ $dataForm->metodo_usuario_act_id->nombre}} {{ $dataForm->metodo_usuario_act_id->apellido}}" readonly="readonly">
                </div>
                @endif

              </div>
              <div class="row"></div>
              <br>

              <div class="col-md-12"> 

                <div class="col-md-4">
                  <a href="{{asset('admin/empresa')}}" class="btn btn-danger">Volver </a>
               </div>

                <div class="col-md-4">
               </div>
              </div>

              </div>


            @stop

            {!! Form::close() !!} 

@section('scripts')

    <script type="text/javascript">
      function myMap() {
      var latitud=$('#latitud').val();
      var longitud=$('#longitud').val();

      var myLatLng = {lat:parseFloat(latitud) , lng:parseFloat(longitud)};

  var map = new google.maps.Map(document.getElementById('map_canvas'), {
    zoom: 15,
    center: myLatLng,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  });

  var marker = new google.maps.Marker({
    position: myLatLng,
    map: map,
    draggable:true,
    title: 'Hello World!'
  });
}
    </script>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB8kPVK6nmaoLI0EoIm8KvgdoXp3ohWzLQ&callback=myMap"></script>

    @stop
