@extends("panelAdmin")
@section('content')
<div class="box-header with-border">
   <h3 class="box-title">Formulario de actualización de empresa</h3>
</div>

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<?php 
  if(isset($descripcion)){?>
    <div class="<?php echo $descripcion['clase'];?>">
      <?php echo $descripcion['mensaje'];?>
    <div>
<?php
  }
?>    


{!! Form::open(array('url' => ['admin/empresa',$dataForm->id], 'method' => 'put', 'files'=> true)) !!}﻿



          <input class="hidden" type="text" name="id" value="{{ $dataForm->id }}">

  <div class="box-body">
            <div class="col-md-12">
            <div class="form-group col-md-3">
              <label for="exampleInputFile">
                <img src="{{asset($dataForm->logotipo)}}" class="col-md-3" style="cursor:pointer;width:200px;height: 150px " alt="Injaz Msila" style="float:right;margin:7px">
                <input id="exampleInputFile" class="col-md-4" type="file" name="logotipo" style="cursor: pointer;  display: none">
            </label>
            <label class="col-md-12 imagen_mensaje hidden"> </label>
            </div>


                <div class="col-md-3">
                <label class="col-md-12">Nombre</label>
                  <input class="form-control col-md-4"  name="nombre" type="text" maxlength="100" value="{{ $dataForm->nombre }}" required="required">
                </div>

                <div class="col-md-3">
                <label class="col-md-12">Correo</label>
                  <input class="form-control col-md-4" name="correo" type="text" maxlength="30" value="{{ $dataForm->correo }}" required="required">
                </div>

              <div class="col-md-3">
                <label class="col-md-12">Teléfono</label>
                  <input class="form-control col-md-4" name="telefono" type="text" maxlength="20" value="{{ $dataForm->telefono }}" required="required">
                </div>

              </div>

                <div class="row"></div>
                <br>

              <div class="col-md-12">
                <div class="col-md-4">
                 <label class="col-md-12">Facebook</label>
                  <input class="form-control col-md-4" name="facebook" type="text" value="{{ $dataForm->facebook }}" required="required">
                </div>

                <div class="col-md-4">
                 <label class="col-md-12">Twitter</label>
                  <input class="form-control col-md-4" name="twitter" type="text" maxlength="30" value="{{ $dataForm->twitter }}" required="required">
                </div>

                <div class="col-md-4">
                <label class="col-md-12">Instagram</label>
                  <input class="form-control col-md-4" name="instagram" type="text" maxlength="30" value="{{ $dataForm->instagram }}" required="required">
                </div>

              </div>

                <div class="row"></div>
                <br>
                <?php $tag_vision=strip_tags($dataForm->vision);?>

                <div class="col-md-12">
                  <section class="content">
                      <div class="row">
                        <div class="col-md-12">
                          <div class="box box-info">
                            <div class="box-header">
                              <h3 class="box-title">Vision
                              </h3>
                              <!-- tools box -->
                              <div class="pull-right box-tools">
                                <button type="button" class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                  <i class="fa fa-minus"></i></button>
                                <button type="button" class="btn btn-info btn-sm" data-widget="remove" data-toggle="tooltip" title="Remove">
                                  <i class="fa fa-times"></i></button>
                              </div>
                              <!-- /. tools -->
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body pad">
                                    <textarea id="vision" name="vision" rows="10" cols="80" value="{{$dataForm->vision}}"><?php echo $dataForm->vision;?>
                                    </textarea> 
                            </div>
                          </div>
                          <!-- /.box -->

                        </div>
                        <!-- /.col-->
                      </div>
                      <!-- ./row -->
                    </section>
                </div>

             <?php $tag_mision=strip_tags($dataForm->mision);?>

                <div class="col-md-12">
                  <section class="content">
                      <div class="row">
                        <div class="col-md-12">
                          <div class="box box-info">
                            <div class="box-header">
                              <h3 class="box-title">Mision
                              </h3>
                              <!-- tools box -->
                              <div class="pull-right box-tools">
                                <button type="button" class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                  <i class="fa fa-minus"></i></button>
                                <button type="button" class="btn btn-info btn-sm" data-widget="remove" data-toggle="tooltip" title="Remove">
                                  <i class="fa fa-times"></i></button>
                              </div>
                              <!-- /. tools -->
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body pad">
                                    <textarea id="mision" name="mision" rows="10" cols="80" value="{{$dataForm->mision}}"><?php echo $dataForm->mision;?>
                                    </textarea> 
                            </div>
                          </div>
                          <!-- /.box -->

                        </div>
                        <!-- /.col-->
                      </div>
                      <!-- ./row -->
                    </section>
                </div>


                <div class="row"></div>
                <br>




              <input type="text" class="hidden" id="latitud" name="latitud" value="{{ $dataForm->latitud }}">
              <input type="text" class="hidden" id="longitud" name="longitud" value="{{ $dataForm->longitud}}">

              <div class="row"></div>
                <br>
                <?php $tag=strip_tags($dataForm->quines_somos);?>

                <div class="col-md-12">
                  <section class="content">
                      <div class="row">
                        <div class="col-md-12">
                          <div class="box box-info">
                            <div class="box-header">
                              <h3 class="box-title">Quienes Somos
                              </h3>
                              <!-- tools box -->
                              <div class="pull-right box-tools">
                                <button type="button" class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                  <i class="fa fa-minus"></i></button>
                                <button type="button" class="btn btn-info btn-sm" data-widget="remove" data-toggle="tooltip" title="Remove">
                                  <i class="fa fa-times"></i></button>
                              </div>
                              <!-- /. tools -->
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body pad">
                              <form>
                                    <textarea id="quines_somos" name="quines_somos" rows="10" cols="80" value="{{$dataForm->quines_somos}}"><?php echo $dataForm->quines_somos;?>
                                    </textarea> 
                              </form>
                            </div>
                          </div>
                          <!-- /.box -->

                        </div>
                        <!-- /.col-->
                      </div>
                      <!-- ./row -->
                    </section>
                </div>

               <div class="col-md-12">
                <div class="col-md-6">
                 <label class="col-md-12">Direccion1</label>
                  <textarea class="form-control col-md-4" name="direccion1" cols="40" rows="5" value="{{ $dataForm->direccion1 }}" required="required"> {{ $dataForm->direccion1 }}</textarea>
                </div>

                <div class="col-md-6">
                 <label class="col-md-12">Direccion2</label>
                  <textarea class="form-control col-md-4" name="direcccion" cols="40" rows="5" value="{{ $dataForm->direcccion }}" required="required"> {{ $dataForm->direcccion }}</textarea>
                </div>

              </div>

              <div class="row"></div>
                <br>

              <div class="col-md-12">
                <div class="col-md-8">
                    <label class="col-md-12">Ubicacion</label>
                      <input class="form-control col-md-8" name="ubicacion" placeholder="Ubicacion" type="text" maxlength="500" id="address" value="{{ $dataForm->ubicacion }}" required="">

                </div>
                  <div class="col-md-4">
                  <label class="col-md-12"><br></label>
                    <input class="btn  btn-info text-right" id="search" type="button" value ="Buscar" />
                  </div>
              </div>

                <div class="col-md-12">
                        <br>
                    <div id="map_canvas" class="col-md-12" style=" width:800px; 
                      height:400px;"></div>
                </div>


              <input type="text" class="hidden" name="_token" value="{{ csrf_token() }}"></input>



            <!--<div class="archivo">
                <input type="file" id="file">
            </div>-->

              <div class="row"></div>
              <br>

              <div class="col-md-4 text-left">
                <a href="{{asset('admin/empresa/galeria')}}/{{$dataForm->id}}" class="btn btn-info">Ver galerias</a>
              </div>
                <div class="col-md-6 text-right">
                  <a href="{{asset('admin/empresa')}}" class="btn btn-danger">Volver </a>
                  </div>

  
               <div class="col-md-2 text-left">
               <button class="btn  btn-success text-right">Actualizar</button>
               </div>
                <!-- /input-group -->
      
</div>
</form>

            <!--Api de google -->

        <!--Cargar imagenes -->

  <link rel="stylesheet" href="//blueimp.github.io/Gallery/css/blueimp-gallery.min.css">
  <!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
  <link rel="stylesheet" href="{{asset('upload/css/jquery.fileupload.css')}}">
  <link rel="stylesheet" href="{{asset('upload/css/jquery.fileupload-ui.css')}}">
  <!-- CSS adjustments for browsers with JavaScript disabled -->
  <noscript><link rel="stylesheet" href="{{asset('upload/css/jquery.fileupload-noscript.css')}}"></noscript>
  <noscript><link rel="stylesheet" href="{{asset('upload/css/jquery.fileupload-ui-noscript.css')}}"></noscript>

    <form id="fileupload" action="server/php/" method="POST" enctype="multipart/form-data">
      <input class="hidden" type="text" name="empresa_id" value="{{ $dataForm->id }}">

        <!-- Redirect browsers with JavaScript disabled to the origin page -->
        <noscript><input type="hidden" name="redirect" value="https://blueimp.github.io/jQuery-File-Upload/"></noscript>
        <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
        <div class="row fileupload-buttonbar">

        <div class="col-md-12">
          <div class="alert alert-info alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <h4><i class="icon fa fa-info"></i> Alerta!</h4>
                  los archivos a cargar deben contener un tamaño de 20.99 megabyte y deben contener la siguiente extensión: gif, jpg o png. 
          </div>
        </div>
            <div class="col-md-12">
                <!-- The fileinput-button span is used to style the file input field as button -->
                <span class="btn btn-success fileinput-button">
                    <i class="glyphicon glyphicon-plus"></i>
                    <span>Agregar Fila...</span>
                    <input type="file" class="files[]" name="files[]" multiple>
                    <input type="text" class="hidden" name="_token" value="{{ csrf_token() }}"></input>
                </span>
                <button type="submit" class="btn btn-primary start" id="botonStart">
                    <i class="glyphicon glyphicon-upload"></i>
                    <span>Subir Archivo</span>
                </button>
                <button type="reset" class="btn btn-warning cancel">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span>Cancelar Archivo</span>
                </button>
                <button type="button" class="btn btn-danger delete">
                    <i class="glyphicon glyphicon-trash"></i>
                    <span>Eliminar</span>
                </button>
                <br>
                Elminar todo<input type="checkbox" class="toggle">
                <!-- The global file processing state -->
                <span class="fileupload-process"></span>


            </div>
            <br>
            <!-- The global progress state -->
            <div class="col-lg-5 fileupload-progress fade">
                <!-- The global progress bar -->
                <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                    <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                </div>
                <!-- The extended global progress state -->
                <div class="progress-extended">&nbsp;</div>
            </div>
        </div>
        <!-- The table listing the files available for upload/download -->
        <table role="presentation" class="table table-striped"><tbody class="files"></tbody></table>

</div>
<!-- The blueimp Gallery widget -->
<div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" data-filter=":even">
    <div class="slides"></div>
    <h3 class="title"></h3>
    <a class="prev">‹</a>
    <a class="next">›</a>
    <a class="close">×</a>
    <a class="play-pause"></a>
    <ol class="indicator"></ol>
</div>
<!-- The template to display files available for upload -->
<script id="template-upload" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-upload fade">
        <td>
            <span class="preview"></span>
        </td>
        <td>
            <p class="name">{%=file.name%}</p>
          <strong class="error text-danger"></strong>
        </td>
        <td>
            <p class="size">Procesar...</p>
            <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
        </td>
        <td>
            {% if (!i && !o.options.autoUpload) { %}
                <button class="btn btn-primary start" disabled>
                    <i class="glyphicon glyphicon-upload"></i>
                    <span>Subir</span>
                </button>
            {% } %}
            {% if (!i) { %}
                <button class="btn btn-warning cancel">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span>Cancel</span>
                </button>
                <input type="radio" name="destacado" class="destacado" id="destacado" value="{%=file.name%}">Principal
            {% } %}
        </td>
    </tr>
{% } %}
</script>
<input type="text" name="nombre_empresa" value="nombre_empresa" class="hidden"> </input>
    </form>
<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-download fade">
        <td>
        <input type="checkbox" name="delete" value="1" class="toggle">
            <span class="preview">
                {% if (file.thumbnailUrl) { %}
                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}"></a>
                {% } %}
            </span>
        </td>
        <td>
            <p class="name">
                {% if (file.url) { %}
                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
                {% } else { %}
                    <span>{%=file.name%}</span>
                {% } %}
            </p>
            {% if (file.error) { %}
                <div><span class="label label-danger">Error</span> {%=file.error%}</div>
            {% } %}
        </td>
        <td>
            <span class="size">{%=o.formatFileSize(file.size)%}</span>
        </td>
        <td>
            {% if (file.deleteUrl) {  file.deleteUrl.replace('index.php', ''); %}
                <button class="btn btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%= file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                    <i class="glyphicon glyphicon-trash"></i>
                    <span>Eliminar</span>
                </button>
            {% } else { %}
                <button class="btn btn-warning cancel">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span>Cancelar</span>
                </button>

            {% } %}
        </td>
    </tr>
{% } %}
</script>
</body>
</div>
</div>
</div>



            @stop

          

  
  @section('scripts')
  <script src="{{asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
  <script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
    <script type="text/javascript">
    $(document).ready(function(){
      $(function () {
        CKEDITOR.replace('quines_somos');
        $(".textarea").wysihtml5();
      });

      $(function () {
        CKEDITOR.replace('mision');
        $(".mision").wysihtml5();
      });
      $(function () {
        CKEDITOR.replace('vision');
        $(".vision").wysihtml5();
      });
    })



    $('#exampleInputFile').on("change",function(){
      $('.imagen_mensaje').removeClass('hidden');
      $('.imagen_mensaje').html($('#exampleInputFile').val());
    })

    function habilitar(){
    if($(".destacado").is(':checked')){  
        $('.start').removeClass('hidden');  
    }
}
      function myMap() {
      var latitud=$('#latitud').val();
      var longitud=$('#longitud').val();

      var myLatLng = {lat:parseFloat(latitud) , lng:parseFloat(longitud)};

  var map = new google.maps.Map(document.getElementById('map_canvas'), {
    zoom: 15,
    center: myLatLng,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  });

  var marker = new google.maps.Marker({
    position: myLatLng,
    map: map,
    draggable:true,
    title: 'Hello World!'
  });
}
    </script>
<script type="text/javascript">
$('#search').on("click",function(){
  googleMaps();
})
function googleMaps(){
    ubicacion=$('#address').val();
    $.ajax({
    type: "GET",
    url: "https://maps.googleapis.com/maps/api/geocode/json",
    data: {
    address: ubicacion,
    key:'AIzaSyCTTHufNeM1zuZJYKrrtqJ1KUjqNM6_tWA' },
    dataType: "json",
    success: function (resultado) {
      var latitud=resultado['results'][0]['geometry']['location']['lat'];
      var longitud=resultado['results'][0]['geometry']['location']['lng'];
      $('#latitud').val(latitud);
      $('#longitud').val(longitud);
      myMap();
    }
    });
  }
  googleMaps();
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB8kPVK6nmaoLI0EoIm8KvgdoXp3ohWzLQ&callback=myMap"></script>


<!--Foliupload -->

<script src="http://code.jquery.com/ui/1.10.1/jquery-ui.js"></script>

<!-- The Templates plugin is included to render the upload/download listings -->
<script src="//blueimp.github.io/JavaScript-Templates/js/tmpl.min.js"></script>
<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
<script src="//blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js"></script>
<!-- The Canvas to Blob plugin is included for image resizing functionality -->
<script src="//blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>
<!-- Bootstrap JS is not required, but included for the responsive demo navigation -->
<script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<!-- blueimp Gallery script -->
<script src="//blueimp.github.io/Gallery/js/jquery.blueimp-gallery.min.js"></script>
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="{{asset('upload/js/jquery.iframe-transport.js')}}"></script>
<!-- The basic File Upload plugin -->
<script src="{{asset('upload/js/jquery.fileupload.js')}}"></script>
<!-- The File Upload processing plugin -->
<script src="{{asset('upload/js/jquery.fileupload-process.js')}}"></script>
<!-- The File Upload image preview & resize plugin -->
<script src="{{asset('upload/js/jquery.fileupload-image.js')}}"></script>
<!-- The File Upload audio preview plugin -->
<script src="{{asset('upload/js/jquery.fileupload-audio.js')}}"></script>
<!-- The File Upload video preview plugin -->
<script src="{{asset('upload/js/jquery.fileupload-video.js')}}"></script>
<!-- The File Upload validation plugin -->
<script src="{{asset('upload/js/jquery.fileupload-validate.js')}}"></script>
<!-- The File Upload user interface plugin -->
<script src="{{asset('upload/js/jquery.fileupload-ui.js')}}"></script>
<!-- The main application script -->
<script src="{{asset('upload/js/main_empresa.js')}}"></script>


<!--Foliupload -->
<!-- Include the plugin's CSS and JS: -->


             @stop



