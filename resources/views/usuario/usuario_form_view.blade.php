@extends("panelAdmin")

@section('content')
<div class="box-header with-border">
   <h3 class="box-title">Formulario de Usuario de Vista</h3>
</div>


  <div class="box-body">
            <div class="col-md-12">
                <div class="col-md-4">
                <label class="col-md-12"> Nombre </label>
                  <input class="form-control col-md-4" placeholder="Nombre" name="nombreForm" type="text" maxlength="30" value="{{ $dataForm->nombre }}" readonly="readonly">
                </div>
                <div class="col-md-4">
                <label class="col-md-12"> Apellido </label>
                  <input class="form-control col-md-4" type="text" placeholder="Apellido" name="ApellidoForm" maxlength="30" value="{{ $dataForm->apellido }}" readonly="readonly">
                </div>
                <div class="col-md-4 input-group">
                    <label class="col-md-12"> Correo </label>
                  <input class="form-control col-md-4" placeholder="Correo" type="email" name="CorreoFrom" maxlength="50" value="{{ $dataForm->correo }}" readonly="readonly">
                </div>
              </div>

                <div class="row"></div>
                <br>

              <div class="col-md-12">

                  <div class="form-group col-md-4">
                    <div class="input-group">
                    <label class="col-md-12"> Telefono Celular </label>
                      <input class="form-control col-md-4" name="telefono_celularForm" placeholder="Teléfono Celular" type="text" maxlength="15" value="{{ $dataForm->telefono_celular }}" readonly="readonly">
                    </div>
                  <!-- /.input group -->
                </div>

                  <div class="form-group col-md-4">
                    <div class="input-group">
                    <label class="col-md-12"> Telefono Fijo </label>
                      <input class="form-control col-md-4" name="telefono_fijoForm" placeholder="Teléfono Fijo" type="text" maxlength="15" value="{{ $dataForm->telefono_local }}" readonly="readonly">
                    </div>
                  <!-- /.input group -->
                </div>
                  <!-- /.input group -->

                  <div class="form-group col-md-4">
                  <label class="col-md-12"> Foto </label>
                  <img width="120" height="80" src="{{asset($dataForm->foto)}}">

                  </div>


              </div>


                <div class="row">
                </div>
                <br>



              <div class="col-md-12">
                <div class="col-md-4">
                <label class="col-md-12"> Fecha de Creación </label>
                  <input class="form-control col-md-4" placeholder="Rpetir Contraseña" name="contrasena_nuevaForm" type="text" maxlength="50" value="{{ $dataForm->updated_at }}" readonly="readonly">
                </div>
                <div class="col-md-4">
                <label class="col-md-12"> Fecha de Actualización </label>
                  <input class="form-control col-md-4" name="contrasenaForm"  type="text" placeholder="Contraseña" maxlength="50" value="{{ $dataForm->created_at }}" readonly="readonly">
                </div>

              </div>

                <input type="text" class="hidden" name="_token" value="{{ csrf_token() }}"></input>


              <div class="row"></div>

              <br>

              <div class="col-md-12"> 

                <div class="col-md-4">
                  <a href="{{asset('admin/usuario')}}" class="btn btn-danger">Volver </a>
               </div>

                <div class="col-md-4">
               </div>
              </div>


                <!-- /input-group -->
              </div>
            <form>

            @stop

            {!! Form::close() !!} 
