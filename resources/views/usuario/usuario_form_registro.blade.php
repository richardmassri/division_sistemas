@extends("panelAdmin")

@section('content')
<div class="box-header with-border">
   <h3 class="box-title">Crear Nuevo Usuario</h3>
</div>

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>

            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<?php 
  if(isset($descripcion)){?>
    <div class="<?php echo $descripcion['clase'];?>">
      <?php echo $descripcion['mensaje'];?>
    </div>
<?php
  }
?>    






<form action="{{asset('admin/usuario')}}" method="post" name="vieja" accept-charset="UTF-8" enctype="multipart/form-data">
  <div class="box-body">
            <div class="col-md-12">
                <div class="col-md-4">
                  <label class="col-md-12"> Nombre </label>
                  <input class="form-control col-md-4" placeholder="Nombre" name="nombreForm" type="text" maxlength="30" value="{{ old('nombreForm') }}" required="required">
                </div>

                <div class="col-md-4">
                  <label class="col-md-12"> Apellido </label>
                  <input class="form-control col-md-4" type="text" placeholder="Apellido" name="ApellidoForm" maxlength="30" value="{{ old('ApellidoForm') }}" required="required">
                </div>

                    <div class="form-group col-md-4">
                                          <label class="col-md-12"> Correo </label>

                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-envelope"></i>
                      </div>
                  <input class="form-control col-md-4" placeholder="Correo" type="email" name="CorreoFrom" maxlength="50" value="{{ old('CorreoFrom') }}" required="required">
                </div>
              </div>
              </div>



                <div class="row"></div>
                <br>
              <div class="col-md-12">
                  <div class="form-group col-md-4">
                    <label class="col-md-12"> Teléfono Celular </label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-phone"></i>
                      </div>
                      <input class="form-control col-md-4" name="telefono_celularForm" placeholder="Teléfono Celular" type="text" maxlength="15" value="{{ old('telefono_celularForm') }}" required="required">
                    </div>
                  <!-- /.input group -->
                </div>

                  <div class="form-group col-md-4">
                      <label class="col-md-12"> Teléfono Fijo </label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-phone"></i>
                      </div>
 
                      <input class="form-control col-md-4" name="telefono_fijoForm" placeholder="Teléfono Fijo" type="text" maxlength="15" value="{{ old('telefono_fijoForm') }}" required="required">
                    </div>
                  <!-- /.input group -->
                </div>
                  <!-- /.input group -->

                  <div class="form-group col-md-4">
                    <label for="exampleInputFile col-md-12">Seleccione la foto</label>
                    <input id="exampleInputFile col-md-4" type="file" name="fotoForm" value="{{ old('fotoForm') }}">
                  </div>

              </div>


                <div class="row">
                </div>
                <br>



              <div class="col-md-12">
                <div class="col-md-4">
                <label class="col-md-12"> Contraseña </label>
                  <input class="form-control col-md-4" name="contrasenaForm"  type="password" placeholder="Contraseña" maxlength="50" value="{{ old('contrasenaForm') }}" required="required">
                </div>

                <div class="col-md-4">
                  <label class="col-md-12"> Repetir Contraseña </label>
                  <input class="form-control col-md-4" placeholder="Repetir Contraseña" name="contrasena_nuevaForm" type="password" maxlength="50" value="{{ old('contrasena_nuevaForm') }}" required="required">
                </div>
              </div>

                <input type="text" class="hidden" name="_token" value="{{ csrf_token() }}"></input>


              <div class="row"></div>

              <br>

              <div class="col-md-12"> 

                <div class="col-md-4">
                  <a href="{{asset('admin/usuario')}}" class="btn btn-danger">Volver </a>
               </div>

                <div class="col-md-4">
               </div>
               <div class="col-md-4 text-right">
               <button class="btn  btn-success text-right">Guardar</button>
               </div>
              </div>


                <!-- /input-group -->
              </div>
            <form>

            @stop