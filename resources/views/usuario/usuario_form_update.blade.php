@extends("panelAdmin")

@section('content')
<div class="box-header with-border">
   <h3 class="box-title">Formulario de Usuario de Actualizacion</h3>
</div>

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<?php 
  if(isset($descripcion)){?>
    <div class="<?php echo $descripcion['clase'];?>">
      <?php echo $descripcion['mensaje'];?>
    <div>
<?php
  }
?>    
 
{!! Form::open(array('url' => ['admin/usuario',$dataForm->id], 'method' => 'put', 'files'=> true)) !!}﻿
            <input class="form-control col-md-4 hidden" name="id" type="text" maxlength="30" value="{{ $dataForm->id }}">
  <div class="box-body">
            <div class="col-md-12">
                <div class="col-md-4">
                <label class="col-md-12"> Nombre </label>

                  <input class="form-control col-md-4" placeholder="Nombre" name="nombreForm" type="text" maxlength="30" value="{{ $dataForm->nombre }}" required="required">
                </div>

                <div class="col-md-4">
                  <label class="col-md-12"> Apellido </label>

                  <input class="form-control col-md-4" type="text" placeholder="Apellido" name="ApellidoForm" maxlength="30" value="{{ $dataForm->apellido }}" required="required">
                </div>


                  <div class="form-group col-md-4">
                    <label class="col-md-12"> Correo </label>

                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-envelope"></i>
                      </div>
                  <input class="form-control col-md-4" placeholder="Correo" type="email" name="CorreoFrom" value="{{ $dataForm->correo }}" required="required">
                </div>
              </div>

              </div>

                <div class="row"></div>
                <br>

              <div class="col-md-12">

                  <div class="form-group col-md-4">
                      <label class="col-md-12"> Teléfono Celular </label>

                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-phone"></i>
                      </div>
                      <input class="form-control col-md-4" name="telefono_celularForm" placeholder="Teléfono Celular" type="text" maxlength="15" value="{{ $dataForm->telefono_celular }}" required="required">
                    </div>
                  <!-- /.input group -->
                </div>

                  <div class="form-group col-md-4">
                    <label class="col-md-12"> Teléfono Fijo </label>

                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-phone"></i>
                      </div>
                      <input class="form-control col-md-4" name="telefono_fijoForm" placeholder="Teléfono Fijo" type="text" maxlength="15" value="{{ $dataForm->telefono_local }}" required="required">
                    </div>
                  <!-- /.input group -->
                </div>
                  <!-- /.input group -->

                  <div class="form-group col-md-4">
                    <label for="exampleInputFile">Seleccione la foto</label>
                    <input id="exampleInputFile col-md-4" type="file" name="fotoForm" value="{{ $dataForm->foto }}">

                  </div>


              </div>


                <div class="row">
                </div>
                <br>



              <div class="col-md-12">

                <div class="col-md-4">
              <label class="col-md-12"> Contraseña </label>
                  <input class="form-control col-md-4" name="contrasenaForm"  type="password" placeholder="Contraseña" maxlength="50" value="{{ $dataForm->contrasena }}">
                </div>

                <div class="col-md-4">
              <label class="col-md-12"> Repetir Contraseña </label>

                  <input class="form-control col-md-4" placeholder="Repetir Contraseña" name="contrasena_nuevaForm" type="password" maxlength="50" value="{{ $dataForm->contrasena_nueva }}">
                </div>

              

                <input type="text" class="hidden" name="_token" value="{{ csrf_token() }}"></input>

              </div>

              <div class="col-md-12">
               <br>
                <div class="col-md-4">
               </div>
               <div class="col-md-4 text-right">
               <button class="btn  btn-success text-right">Actualizar</button>
               </div>
              </div>


                <!-- /input-group -->
              </div>
            <form>

            @stop

            {!! Form::close() !!} 
