<?php 
$validacion_sesion=Session::get('usuario_id');
if(isset($validacion_sesion)){
  if($validacion_sesion!='' or $validacion_sesion!=null){
  ?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Invermaster</title>

  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="{{asset('bootstrap/css/bootstrap.min.css')}}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('plugins/datatables/dataTables.bootstrap.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('dist/css/AdminLTE.min.css')}}">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="{{ asset('dist/css/skins/_all-skins.min.css')}}">

  <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('css/smartadmin-production-plugins.min.css')}}">

@yield('css')


<!-- Bootstrap 3.3.6 -->


  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">

<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="{{ asset('index2.html')}}" class="logo" style="height:55px; background-color: #0066FF">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>I</b>nvermaster</span>
      <!-- logo for regular state and mobile devices -->
    <span class="logo-lg">Invermaster</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" style="height:55px;background-color: #0066FF">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->

          <!-- Notifications: style can be found in dropdown.less -->

          <!-- Tasks: style can be found in dropdown.less -->

          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
          <?php $foto=Session::get('foto');?>
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="{{asset($foto)}}" class="user-image" alt="User Image">
              <span class="hidden-xs">{{Session::get('nombre')}} {{Session::get('apellido')}}</span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="{{asset(Session::get('foto'))}}" class="img-circle" alt="User Image">

                <p>
                  {{Session::get('nombre')}} {{Session::get('apellido')}}
                </p>
              </li>

              <!-- Menu Footer-->
              <?php $id_usuario=Session::get('usuario_id');?>


      
              <li class="user-footer">
                <div class="pull-left">
                  <a href="{{asset('admin/usuario/'.$id_usuario.'/edit')}}" class="btn btn-default btn-flat">Mi perfil</a>
                </div>
                <div class="pull-right">
                  <a href="{{asset('logout')}}" class="btn btn-default btn-flat">Salir</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{asset(Session::get('foto'))}}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>{{Session::get('nombre')}} {{Session::get('apellido')}}</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->

      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->


      <ul class="sidebar-menu">
          @if(Session::get('es_agente')=='NO')
            <li class="treeview">
              <a href="{{asset('admin/contacto')}}">
                <i class="fa fa-files-o"></i>
                <span>Solicitudes</span>
                <span class="pull-right-container">
                  <span class="label label-primary pull-right">{{Session::get('solicitud_admin')}}</span>
                </span>
              </a>
            </li>
          @else
            <li class="treeview">
              <a href="{{asset('admin/contacto/agente')}}/{{Session::get('usuario_id')}}">
                <i class="fa fa-files-o"></i>
                <span>Solicitudes</span>
                <span class="pull-right-container">
                  <span class="label label-primary pull-right">{{Session::get('solicitud_agente')}}</span>
                </span>
              </a>
            </li>
          @endif
                  <li>
                  <a href="#" class="usr" 
                      data-chat-id="cha4" 
                      data-chat-fname="Barley" 
                      data-chat-lname="Krazurkth" 
                      data-chat-status="away" 
                      data-rel="popover-hover" 
                      data-placement="right" 
                      data-html="true" 
                      data-content="
                      <div class='usr-card'>
                        <img src='img/avatars/4.png' alt='Barley Krazurkth'>
                        <div class='usr-card-content'>
                          <h3>Barley Krazurkth</h3>
                          <p>Sales Director</p>
                        </div>
                      </div>
                    "> 
                      <i></i>Barley Krazurkth
                  </a>
              </li>


        <li class="treeview">
          <a href="{{asset('admin/proyecto')}}">
            <i class="fa fa-edit"></i> 
            <span>Proyectos</span>
                <span class="pull-right-container">
                </span>
          </a>
        </li>

        <li class="treeview">
          <a href="{{asset('admin/propiedad')}}">
            <i class="fa fa-edit"></i> <span>Propiedades</span>
                <span class="pull-right-container">
                </span>
          </a>
        </li>
        <li class="treeview active">
          <a href="#">
            <i class="fa fa-table"></i> <span>Administración</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
          </a>
          <ul class="treeview-menu">
             <li><a href="{{asset('admin/estatus')}}"><i class="fa fa-circle-o"></i> Estatus de Solicitudes</a></li>
          @if(Session::get('es_agente')=='NO')
            <li><a href="{{asset('admin/usuario')}}"><i class="fa fa-circle-o"></i> Usuarios</a></li>
          @endif
            <li><a href="{{asset('admin/tipo')}}"><i class="fa fa-circle-o"></i> Tipos</a></li>
            <li><a href="{{asset('admin/caracteristica')}}"><i class="fa fa-circle-o"></i>Características</a></li>
            <li><a href="{{asset('admin/operacion')}}"><i class="fa fa-circle-o"></i>Operaciones </a> </li>
            
            <li><a href="{{asset('admin/empresa/1/edit')}}"><i class="fa fa-circle-o"></i>La Empresa </a> </li>
            <li><a href="{{asset('admin/parametroSeguridad')}}"><i class="fa fa-circle-o"></i>Parámetros  </a> </li>
            <li><a href="{{asset('admin/pais')}}"><i class="fa fa-circle-o"></i>País Ciudad Estado  </a> </li>


          </ul> 
        </li>

      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Content Header (Page header) -->
    <section class="content-header">

     @yield('content')

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
      <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane" id="control-sidebar-home-tab">
        <h3 class="control-sidebar-heading">Recent Activity</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-birthday-cake bg-red"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                <p>Will be 23 on April 24th</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-user bg-yellow"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Frodo Updated His Profile</h4>

                <p>New phone +1(800)555-1234</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-envelope-o bg-light-blue"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Nora Joined Mailing List</h4>

                <p>nora@example.com</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-file-code-o bg-green"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Cron Job 254 Executed</h4>

                <p>Execution time 5 seconds</p>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

        <h3 class="control-sidebar-heading">Tasks Progress</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Custom Template Design
                <span class="label label-danger pull-right">70%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Update Resume
                <span class="label label-success pull-right">95%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-success" style="width: 95%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Laravel Integration
                <span class="label label-warning pull-right">50%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-warning" style="width: 50%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Back End Framework
                <span class="label label-primary pull-right">68%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-primary" style="width: 68%"></div>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

      </div>
      <!-- /.tab-pane -->
      <!-- Stats tab content -->
      <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
      <!-- /.tab-pane -->
      <!-- Settings tab content -->
      <div class="tab-pane" id="control-sidebar-settings-tab">
        <form method="post">
          <h3 class="control-sidebar-heading">General Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Report panel usage
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Some information about this general settings option
            </p>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Allow mail redirect
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Other sets of options are available
            </p>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Expose author name in posts
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Allow the user to show his name in blog posts
            </p>
          </div>
          <!-- /.form-group -->

          <h3 class="control-sidebar-heading">Chat Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Show me as online
              <input type="checkbox" class="pull-right" checked>
            </label>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Turn off notifications
              <input type="checkbox" class="pull-right">
            </label>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Delete chat history
              <a href="javascript:void(0)" class="text-red pull-right"><i class="fa fa-trash-o"></i></a>
            </label>
          </div>
          <!-- /.form-group -->
        </form>
      </div>
      <!-- /.tab-pane -->
    </div>
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
<!-- ./wrapper -->
<!-- jQuery 2.2.3 -->
<!--<script
        src="http://code.jquery.com/jquery-2.2.4.min.js"
        integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
        crossorigin="anonymous"></script>-->

  <script src="{{asset('plugins/jQuery/jquery-2.2.3.min.js')}}"></script>


 <!-- <script
        src="https://code.jquery.com/ui/1.10.3/jquery-ui.min.js"
        integrity="sha256-lnH4vnCtlKU2LmD0ZW1dU7ohTTKrcKP50WA9fa350cE="
        crossorigin="anonymous"></script>-->
<script src="{{asset('bootstrap/js/bootstrap.min.js')}}"></script>


<!-- DataTables -->
<!-- AdminLTE App -->
<!-- AdminLTE for demo purposes -->
   <!-- <script src="{{asset('js/smart-chat-ui/smart.chat.ui.min.js')}}"></script>
    <script src="{{asset('js/smart-chat-ui/smart.chat.manager.min.js')}}"></script>-->

    <!--<script src="{{asset('js/smart-chat-ui/app.config.js')}}"></script>-->
    <!--<script src="{{asset('js/smart-chat-ui/app.min.js')}}"></script>-->

@yield('scripts')



<!-- page script -->
</body>
</html>

<?php
  }else{?>
    <script type="text/javascript">
    window.location = "{{asset('logout')}}";//here double curly bracket
    </script>
  <?php }
  }else{?>
    <script type="text/javascript">
    window.location = "{{asset('logout')}}";//here double curly bracket
    </script>  
    <?php
    }?>


