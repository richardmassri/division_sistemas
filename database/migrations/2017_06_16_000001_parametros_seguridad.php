<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ParametrosSeguridad extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parametros_seguridades', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre',200)->required();
            $table->integer('cantidad')->required();
            $table->string('estatus',1);
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->unsignedInteger('usuario_ini_id');
            $table->unsignedInteger('usuario_act_id');
            $table->foreign('usuario_ini_id')->references('id')->on('usuarios')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('usuario_act_id')->references('id')->on('usuarios')->onDelete('cascade')->onUpdate('cascade');
        });

        //
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parametros_seguridades');

        //
    }
}
