<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuarios', function (Blueprint $table) {
            $table->increments('id');
            $table->string('correo',50)->required();
            $table->string('nombre',50)->unique();
            $table->string('apellido',60);
            $table->string('telefono_local',20);
            $table->string('foto',200);
            $table->string('telefono_celular',20);
            $table->string('contrasenia',100);
            $table->string('contrasenia_nueva',100);
            $table->string('estatus',1);
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->rememberToken();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuarios');
    }
}
