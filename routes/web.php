<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('login');
});

Route::get('logout', function () {
    return view('login');
});

Route::any('logout', [
    'as'=>'usuario',
    'uses' => 'usuarioController@logout'
]);

Route::get('/panelAdmin', function () {
    return view('panelAdmin');
});


Route::get('/upload', function () {
    return view('panelAdmin');
});


Route::any('invermaster/proyecto', [
    'as'=>'proyectoCliente.plantilla',
    'uses' => 'proyectoController@viewProyectoCliente'
]);

Route::any('propiedad/meGusta', [
    'as'=>'propiedadCliente.plantilla',
    'uses' => 'propiedadController@meGusta'
]);

Route::any('scripts/dependenciaEstado', [
    'as'=>'propiedadCliente.plantilla',
    'uses' => 'propiedadController@dependenciaEstado'
]);

Route::any('scripts/dependenciaSector', [
    'as'=>'propiedadCliente.plantilla',
    'uses' => 'propiedadController@dependenciaSector'
]);

Route::any('admin/empresa/galeria/{id}', [
    'as'=>'empresa.plantilla',
    'uses' => 'empresaController@showGaleria'
]);

Route::any('admin/proyecto/galeria/{id}', [
    'as'=>'proyecto.plantilla',
    'uses' => 'proyectoController@showGaleriaProyecto'
]);


Route::any('admin/propiedad/galeria/{id}', [
    'as'=>'propiedad.plantilla',
    'uses' => 'propiedadController@showGaleriaPropiedad'
]);

Route::any('chat/chatfrontend', [
    'as'=>'plantilla.chatfrontend',
    'uses' => 'propiedadController@chatfrontend'
]);
Route::any('chat/chatbackend', [
    'as'=>'plantilla.chatbackend',
    'uses' => 'propiedadController@chatbackend'
]);



Route::any('admin/empresa/cambiar/imagen/principla', [
    'as'=>'empresa.plantilla',
    'uses' => 'empresaController@cambiarImagenPrincipalEmpresa'
]);

Route::any('admin/proyecto/cambiar/imagen/principla', [
    'as'=>'proyecto.plantilla',
    'uses' => 'proyectoController@cambiarImagenPrincipalProyecto'
]);

Route::any('admin/propiedad/cambiar/imagen/principla', [
    'as'=>'propiedad.plantilla',
    'uses' => 'propiedadController@cambiarImagenPrincipalpropiedad'
]);

Route::any('admin/proyecto/cambiar/imagen/promocional', [
    'as'=>'proyecto.plantilla',
    'uses' => 'proyectoController@cambiarImagenPromocionalProyecto'
]);

Route::any('admin/propiedad/cambiar/imagen/promocional', [
    'as'=>'proyecto.plantilla',
    'uses' => 'propiedadController@cambiarImagenPromocionalpropiedad'
]);

Route::any('admin/empresa/eliminar/foto', [
    'as'=>'empresa.plantilla',
    'uses' => 'empresaController@eliminarFoto'
]);
Route::any('admin/proyecto/eliminar/foto', [
    'as'=>'proyecto.plantilla',
    'uses' => 'proyectoController@eliminarFoto'
]);

Route::any('admin/propiedad/eliminar/foto', [
    'as'=>'propiedad.plantilla',
    'uses' => 'propiedadController@eliminarFoto'
]);

Route::any('admin/empresa/eliminar/imagen', [
    'as'=>'empresa.plantilla',
    'uses' => 'empresaController@eliminarImagen'
]);

Route::any('admin/proyecto/eliminar/imagen', [
    'as'=>'proyecto.plantilla',
    'uses' => 'proyectoController@eliminarImagen'
]);

Route::any('admin/propiedad/eliminar/imagen', [
    'as'=>'propiedad.plantilla',
    'uses' => 'propiedadController@eliminarImagen'
]);



Route::group(['prefix' => 'admin'],function(){
    Route::resource('categoria', 'categoriaController');
});
Route::group(['prefix' => 'admin'],function(){
    Route::resource('estatus', 'estatusController');
});

Route::group(['prefix' => 'plantilla'],function(){
    Route::resource('block', 'blockController');
});

Route::group(['prefix' => 'admin'],function(){
    Route::resource('pais', 'paisController');
});

Route::group(['prefix' => 'admin'],function(){
    Route::resource('ciudad', 'ciudadController');
});

Route::group(['prefix' => 'admin'],function(){
    Route::resource('pais', 'paisController');
});

Route::group(['prefix' => 'admin'],function(){
    Route::resource('sector', 'sectorController');
});

Route::group(['prefix' => 'admin'],function(){
    Route::resource('usuario', 'usuarioController');
});

Route::group(['prefix' => 'admin'],function(){
	Route::resource('proyecto', 'proyectoController');
});

Route::group(['prefix' => 'admin'],function(){
    Route::resource('propiedad', 'propiedadController');
});

//resource de tipo
Route::group(['prefix' => 'admin'],function(){
    Route::resource('tipo', 'tipoController');
});

//resource de operaciones
Route::group(['prefix' => 'admin'],function(){
    Route::resource('operacion', 'operacionController');
});

//resource de caracteristicas
Route::group(['prefix' => 'admin'],function(){
    Route::resource('caracteristica', 'caracteristicaController');
});

//resource de parametros e seguridad
Route::group(['prefix' => 'admin'],function(){
    Route::resource('parametroSeguridad', 'parametroSeguridadController');
});

//resource de empresa
Route::group(['prefix' => 'admin'],function(){
    Route::resource('empresa', 'empresaController');
});

//resource de empresa
Route::group(['prefix' => 'admin'],function(){
    Route::resource('contacto', 'contactoController');
});

Route::group(['prefix' => 'admin'],function(){
    Route::resource('tags', 'tagsController');
});

Route::group(['prefix' => 'admin'],function(){
    Route::resource('post', 'postController');
});





 Route::group(['prefix' => 'admin'],function(){
    Route::resource('galeria', 'galeriaController');
});


Route::any('admin/upload/photos/upload/', [
    'as'=>'galeria.index',
    'uses' => 'proyectoController@postGaleriaPro'
]);

Route::any('embeddable/config/', [
    'as'=>'galeria.index',
    'uses' => 'proyectoController@embeddable'
]);



Route::any('empresa/galeria/imagenes', [
    'as'=>'empresa.index',
    'uses' => 'empresaController@postGaleriaEmpresa'
]);

Route::any('admin/empresa/{id}/empresa/galeria/imagenes', [
    'as'=>'empresa.index',
    'uses' => 'empresaController@postGaleriaEmpresa'
]);


Route::any('admin/upload/photos/propiedad/', [
    'as'=>'galeria.index',
    'uses' => 'propiedadController@postGaleria'
]);

Route::any('admin/propiedad/{id}/upload/photos/upload/', [
    'as'=>'galeria.index',
    'uses' => 'propiedadController@postGaleria'
]);

Route::get('invermaster/proyecto/detalle/{id}', [
    'as'=>'proyecto.index',
    'uses' => 'proyectoController@detaleProyecto'
]);

Route::any('admin/proyecto/{id}/upload/photos/upload/', [
    'as'=>'proyecto.index',
    'uses' => 'proyectoController@postGaleriaPro'
]);

Route::get('admin/contacto/agente/{id}', [
    'as'=>'contacto.index',
    'uses' => 'contactoController@solicitudAgente'
]);


Route::get('invermaster/propiedad/detalle/{id}', [
    'as'=>'propiedad.index',
    'uses' => 'proyectoController@detallePropiedad'
    
]);

//consultar propiedades generales
Route::any('invermaster/propiedad/general/', [
    'as'=>'propiedad.index',
    'uses' => 'proyectoController@propiedadGenerale'
]);
//consultar proyectos generales
Route::any('invermaster/proyecto/general/', [
    'as'=>'proyecto.index',
    'uses' => 'proyectoController@proyectoGeneral'
]);

//consultar propiedades generales
Route::any('invermaster/equipo', [
    'as'=>'equipo.index',
    'uses' => 'proyectoController@equipoGenerale'
]);

//Guardar contacto
Route::any('invermaster/contacto', [
    'as'=>'contacto.index',
    'uses' => 'proyectoController@contacto'
]);

//Guardar contacto
Route::any('invermaster/contacto/empresa', [
    'as'=>'contacto.index',
    'uses' => 'proyectoController@contactoEmpresa'
]);

//Quines Somos
Route::any('invermaster/quinesomos/empresa', [
    'as'=>'contacto.index',
    'uses' => 'proyectoController@quienesSomos'
]);





  Route::post('principal', 'UsuarioController@postConsulta')->name('consultaLogin');
    //Route::post('upload', 'GaleriaController@postGaleria')->name('consultaGaleria');

Route::post('sendmessage', 'UsuarioController@sendMessage');

Route::post('sendmessageForm', 'UsuarioController@sendmessageForm');
