<?php

namespace laravelPrueba;

use Illuminate\Database\Eloquent\Model;

class Agente extends Model
{
	protected $table = 'agentes';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
         'nombre', 'apellido', 'correo', 'telefono_local', 'telefono_celular', 'estatus', 'updated_at', 'created_at','usuario_ini_id', 'usuario_act_id'
    ];
    //
}
