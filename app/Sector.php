<?php

namespace laravelPrueba;

use Illuminate\Database\Eloquent\Model;

class Sector extends Model
{

	protected $table = 'sectores';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'nombre','updated_at', 'created_at','ciudade_id'
    ];

public function ciudad(){
    return $this->belongsTo('laravelPrueba\Ciudad','ciudade_id','id');
}

public function metodo_usuario_act_id() {
    return $this->belongsTo('laravelPrueba\User','usuario_act_id','id');
    
}
public function metodo_usuario_ini_id() {
    return $this->belongsTo('laravelPrueba\User','usuario_ini_id','id');
}
    //
}
