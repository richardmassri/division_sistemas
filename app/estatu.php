<?php

namespace laravelPrueba;

use Illuminate\Database\Eloquent\Model;

class estatu extends Model
{
    protected $table = 'estatus';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'nombre','estatus','updated_at', 'created_at','usuario_ini_id','usuario_act_id'
    ];





public function metodo_usuario_act_id() {
    return $this->belongsTo('laravelPrueba\User','usuario_act_id','id');
    
}
public function metodo_usuario_ini_id() {
    return $this->belongsTo('laravelPrueba\User','usuario_ini_id','id');
    
}
}
