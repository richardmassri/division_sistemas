<?php

namespace laravelPrueba;

use Illuminate\Database\Eloquent\Model;

class GaleriasPropiedades extends Model
{

		protected $table = 'galerias_propiedades';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'nombre', 'estatus', 'updated_at', 'created_at','propiedad_id','destacada','promocional'
    ];

public function empresa() {
    return $this->belongsTo('laravelPrueba\empresa');
}
    //
}
