<?php

namespace laravelPrueba;

use Illuminate\Database\Eloquent\Model;

class vistaProyecto extends Model
{
	protected $table = 'vistas_proyectos';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
          'estatus', 'updated_at', 'created_at','proyecto_id'
    ];

public function propiedad() {
    return $this->belongsTo('laravelPrueba\proyecto','proyecto_id','id');
}
    //
}
