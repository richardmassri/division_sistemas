<?php

namespace laravelPrueba;

use Illuminate\Database\Eloquent\Model;

class Propiedad extends Model
{

protected $table = 'propiedades';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'nombre', 'descripcion_breve', 'descripcion', 'ubicacion', 'video', 'fecha_publicacion', 'precio','fecha_inicial','fecha_final','estatus_invermaste','metros_cuadrado','antiguedad','banios','dormitorio','estacionamiento','construccion','latitud','longitud','estatus','updated_at','created_at','usuario_ini_id','usuario_act_id','tipo_id','agente_id', 'caracteristica_id', 'sector_id','ciudad_id','pais_id','destacada','promocional','mantenimiento','tasa','precio_en_dolares'
    ];

       public function metodo_usuario_act_id() {
    return $this->belongsTo('laravelPrueba\User','usuario_act_id','id');
    
}
public function metodo_usuario_ini_id() {
    return $this->belongsTo('laravelPrueba\User','usuario_ini_id','id');
    
}
    //
}
