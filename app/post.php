<?php

namespace laravelPrueba;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{

	   protected $table = 'post';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'nombre','descripcion','descripcion_breve','foto','estatus','updated_at', 'created_at','usuario_ini_id','usuario_act_id','categoria_id'
    ];

public function metodo_usuario_act_id() {
    return $this->belongsTo('laravelPrueba\User','usuario_act_id','id');
    
}
public function metodo_usuario_ini_id() {
    return $this->belongsTo('laravelPrueba\User','usuario_ini_id','id');
    
}

public function categoria() {
    return $this->belongsTo('laravelPrueba\Categoria','categoria_id','id');
    
}




    //
}
