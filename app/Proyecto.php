<?php

namespace laravelPrueba;

use Illuminate\Database\Eloquent\Model;

class Proyecto extends Model
{
	    protected $table = 'proyectos';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'logotipo', 'nombre', 'descripcion_breve', 'descripcion', 'ubicacion', 'video', 'fecha_publicacion','precio_desde', 'precio_hasta','fecha_inicial','fecha_final','estatus_invermaste','latitud','longitud','construccion', 'destacada','promocional','estatus','updated_at','created_at','usuario_ini_id','usuario_act_id','agente_id', 'sector_id','ciudad_id','pais_id','latitud','longitud','tasa','precio_desde_en_dolares','precio_hasta_en_dolares'
    ];


public function galeria() {
    return $this->hasOne('laravelPrueba\Galeria','id','proyecto_id');
}

public function tiposProyecto() {
    return $this->belongsTo('laravelPrueba\TiposProyectos','id','proyecto_id');
}
//public function tiposProyectoMuchos() {
//return $this->belongsToMany('laravelPrueba\TiposProyectos')->withPivot('proyecto_id', 'tipo_id');
//}

public function tipo() {
    return $this->belongsTo('laravelPrueba\Tipo','id','tipo_id');
}

public function caracteristicaPryecto() {
    return $this->belongsTo('laravelPrueba\CaracteristicasProyecto','id','proyecto_id');
}

   public function metodo_usuario_act_id() {
    return $this->belongsTo('laravelPrueba\User','usuario_act_id','id');
    
}
public function metodo_usuario_ini_id() {
    return $this->belongsTo('laravelPrueba\User','usuario_ini_id','id');
    
}






    //
}
