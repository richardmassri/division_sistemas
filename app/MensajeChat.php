<?php

namespace laravelPrueba;

use Illuminate\Database\Eloquent\Model;

class MensajeChat extends Model
{

	   protected $table = 'mensaje_chat';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'nombre','persona_mensaje_id','updated_at','created_at'
    ];



    //
}
