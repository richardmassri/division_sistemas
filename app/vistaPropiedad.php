<?php

namespace laravelPrueba;

use Illuminate\Database\Eloquent\Model;

class vistaPropiedad extends Model
{
protected $table = 'vistas_propiedades';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
          'estatus', 'updated_at', 'created_at','propiedad_id'
    ];

public function propiedad() {
    return $this->belongsTo('laravelPrueba\Propiedad','propiedad_id','id');
}
}
