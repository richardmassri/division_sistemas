<?php

namespace laravelPrueba;

use Illuminate\Database\Eloquent\Model;

class Caracteristica extends Model
{

	protected $table = 'caracteristicas';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'icono','nombre', 'descripcion', 'aplica_proyecto', 'aplica_propiedad', 'estatus', 'updated_at', 'created_at'
    ];

public function metodo_usuario_act_id() {
    return $this->belongsTo('laravelPrueba\User','usuario_act_id','id');
    
}
public function metodo_usuario_ini_id() {
    return $this->belongsTo('laravelPrueba\User','usuario_ini_id','id');
    
}
    //
}
