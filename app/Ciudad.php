<?php

namespace laravelPrueba;

use Illuminate\Database\Eloquent\Model;

class Ciudad extends Model
{


	   protected $table = 'ciudades';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'nombre','updated_at', 'created_at','paise_id'
    ];

public function pais(){
    return $this->belongsTo('laravelPrueba\Paise','paise_id','id');
}

public function metodo_usuario_act_id() {
    return $this->belongsTo('laravelPrueba\User','usuario_act_id','id');
    
}
public function metodo_usuario_ini_id() {
    return $this->belongsTo('laravelPrueba\User','usuario_ini_id','id');
}
    //
}
