<?php

namespace laravelPrueba\Http\Controllers;

use Illuminate\Http\Request;

use laravelPrueba\Operaciones;
use laravelPrueba\User;

use Validator;
use Session;

class operacionController extends Controller
{

    protected $validationRules=[
          'nombre' => 'required|unique:operaciones',
    ];

      protected $validationMessages = [

        'nombre.required' => 'Debe ingresar el nombre',
        'nombre.unique' => 'El nombre de la operación es unico'
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $consultarOperacion=Operaciones::all();
      return view('operacion.index')->with('consultarOperacion', $consultarOperacion);
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('operacion.operacion_form_registro');
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $operacion=new Operaciones($request->all());
        $descripcion=[];
      $v = Validator::make($request->all(), $this->validationRules,$this->validationMessages);
      if ($v->fails())
      {
        return redirect('admin/operacion/create')->withInput()->withErrors($v);
      // The given data did not pass validation
      }else{
        $operacion->estatus= 'A';
        $operacion->updated_at=date('Y-m-d H:i:s');
        $operacion->created_at=date('Y-m-d H:i:s');
        $operacion->usuario_ini_id=Session::get('usuario_id');
        $operacion->usuario_act_id=Session::get('usuario_id');
        if($operacion->save()){
            $mensaje="Se ha agregado con éxito la operación ".$operacion->nombre;
            $clase="callout callout-success";
            $descripcion=['mensaje'=>$mensaje,'clase'=>$clase];
        }
          #return redirect()->withInput('error', 'Something went wrong.');
        return view('operacion.operacion_form_registro')->with('descripcion',$descripcion);
    }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $dataForm=Operaciones::find($id);
      //dd($dataForm);
      return view('operacion.operacion_form_view')->with('dataForm', $dataForm);
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dataForm=Operaciones::find($id);
      //dd($dataForm);
      return view('operacion.operacion_form_update')->with('dataForm', $dataForm);
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    $validationRules=[
          'nombre' => 'required',
    ];

    $validationMessages = [
        'nombre.required' => 'Debe ingresar el nombre',
        'nombre.unique' => 'El nombre de la operación es unico'
    ];

      $descripcion=[];
      $v = Validator::make($request->all(), $validationRules,$validationMessages);
      if ($v->fails())
      {
        return redirect('admin/operacion/'.$id.'/edit')->withInput()->withErrors($v);
      // The given data did not pass validation
      }else{
        $dataForm=Operaciones::find($id);
        $dataForm->updated_at=date('Y-m-d H:i:s');
        $dataForm->usuario_act_id=Session::get('usuario_id');
        $dataForm->nombre=$request['nombre'];
        if($dataForm->save()){
            $mensaje="El registro se ha guardado de forma exitosa";
            $clase="callout callout-success";
            $descripcion=['mensaje'=>$mensaje,'clase'=>$clase];
            return redirect('admin/operacion/');
        }
    }
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $idRequest=explode('-',$id);
        $dataForm=Operaciones::find($idRequest[0]);
        $dataForm->updated_at=date('Y-m-d H:i:s');
        if($idRequest[1]=="e"){
            $dataForm->estatus='E';    
        }else{
            $dataForm->estatus='A';   
        }
        $dataForm->usuario_act_id=Session::get('usuario_id');
        if($dataForm->save()){
            $mensaje="El registro se ha guardado de forma exitosa";
            $clase="callout callout-success";
            $descripcion=['mensaje'=>$mensaje,'clase'=>$clase];
            echo json_encode(['statusCode'=>'Exito']);
            //return redirect('/admin/tipo/');
        }
    }
}
