<?php

namespace laravelPrueba\Http\Controllers;

use Illuminate\Http\Request;
use laravelPrueba\post;
use laravelPrueba\postTags;
use laravelPrueba\Categoria;

use Validator;
use Session;
use Storage;
use laravelPrueba\tag;
use DB;
class postController extends Controller
{

      protected $validationRules=[
          'nombre' => 'required|unique:post',
          'foto' => 'required|image',
          'descripcion_breve'=>'required',
          'descripcion'=>'required',
          'categoria_id'=>'required'
    ];

      protected $validationMessages = [
        'nombre.required' => 'Debe ingresar el post',
        'nombre.unique' => 'El nombre es unico',
        'foto.image' => 'Debe ingresar el formato correcto de la imagen',
        'descripcion_breve.required'=>'Debe ingresar la descripción breve',
        'descripcion.required'=>'Debe ingresar la descripción',
        'categoria_id.required'=>'Debe ingresar la categoria'
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $consultarPost=Post::orderBy('id', 'desc')->get();
      return view('post.index')->with('consultarPost', $consultarPost);
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tags=Tag::where('estatus','=','A')->get();
        $categorias=Categoria::where('estatus','=','A')->get();
        return view('post.post_form_registro')->with(['tags'=>$tags,'categorias'=>$categorias]);
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tags=Tag::where('estatus','=','A')->get();
        $categorias=Categoria::where('estatus','=','A')->get();
        $post=new Post($request->all());
        $descripcion=[];
        $fileContenido=$request['nombre'];
        \Storage::disk('galeriaPost')->put($fileContenido,  file_get_contents($request['foto']->getRealPath()));
        $post->foto="galeriaPost/".$fileContenido;
      $v = Validator::make($request->all(), $this->validationRules,$this->validationMessages);
      if ($v->fails())
      {
        return redirect('admin/post/create')->withInput()->withErrors($v);
      // The given data did not pass validation
      }else{
        $post->estatus= 'A';
        $post->updated_at=date('Y-m-d H:i:s');
        $post->created_at=date('Y-m-d H:i:s');
        $post->usuario_ini_id=Session::get('usuario_id');
        $post->usuario_act_id=Session::get('usuario_id');
        if($post->save()){
            foreach ($request['tags'] as $key => $value){
                $post_tags = new postTags();
                $post_tags->post_id = $post->id;
                $post_tags->tags_id=$value;
                $post_tags->updated_at=date('Y-m-d H:i:s');
                $post_tags->created_at=date('Y-m-d H:i:s');
                $post_tags->estatus='A';
                $post_tags->save();
            }
            $mensaje="Se ha agregado con éxito el ".$post->nombre;
            $clase="callout callout-success";
            $descripcion=['mensaje'=>$mensaje,'clase'=>$clase];
        }
          #return redirect()->withInput('error', 'Something went wrong.');
        return view('post.post_form_registro')->with(['descripcion'=>$descripcion,'tags'=>$tags,'categorias'=>$categorias]);
    }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $dataForm=Post::find($id);
        $tags=Tag::where('estatus','=','A')->get();
        $posTags=DB::table('tags')->select('tags.*','post_tag.*')->join('post_tag','tags.id','=','post_tag.tags_id')->where([
            ['tags.estatus','=','A'],
            ['post_tag.post_id','=',$id],
            ])->orderBy('id', 'desc')->get();
      return view('post.post_form_view')->with(['dataForm'=> $dataForm,'posTags'=>$posTags,'tags'=>$tags]);
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categorias=Categoria::where('estatus','=','A')->get();
        $dataForm=Post::find($id);
        $tags=Tag::where('estatus','=','A')->get();

        $posTags=DB::table('tags')->select('tags.*','post_tag.*')->join('post_tag','tags.id','=','post_tag.tags_id')->where([
            ['tags.estatus','=','A'],
            ['post_tag.post_id','=',$id],
            ])->get();
      return view('post.post_form_update')->with(['dataForm'=> $dataForm,'posTags'=>$posTags,'tags'=>$tags,'categorias'=>$categorias]);
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    $validationRules=[
          'nombre' => 'required',
          'foto' => 'required|image',
          'descripcion_breve'=>'required',
          'descripcion'=>'required'
        ];

      $validationMessages = [
        'nombre.required' => 'Debe ingresar el post',
        'nombre.unique' => 'El nombre es unico',
        'foto.image' => 'Debe ingresar el formato correcto de la imagen',
        'descripcion_breve.required'=>'Debe ingresar la descripción breve',
        'descripcion.required'=>'Debe ingresar la descripción',
    ];
        
      $descripcion=[];
      if($request['foto']==null){
        $validationRules=[
              'nombre' => 'required',
              'descripcion_breve'=>'required',
              'descripcion'=>'required'
            ];

          $validationMessages = [
            'nombre.required' => 'Debe ingresar el post',
            'nombre.unique' => 'El nombre es unico',
            'descripcion_breve.required'=>'Debe ingresar la descripción breve',
            'descripcion.required'=>'Debe ingresar la descripción',
        ];
      }
      $v = Validator::make($request->all(), $validationRules,$validationMessages);
      if ($v->fails())
      {
        return redirect('admin/post/'.$id.'/edit')->withInput()->withErrors($v);
      // The given data did not pass validation
      }else{
        $dataForm=Post::find($id);
        $fileContenido=$request['nombre'];
         if($request['foto']!=null){
            \Storage::disk('galeriaPost')->put($fileContenido,  file_get_contents($request['foto']->getRealPath()));

            $dataForm->foto="galeriaPost/".$fileContenido;
        }
        $dataForm->updated_at=date('Y-m-d H:i:s');
        $dataForm->usuario_act_id=Session::get('usuario_id');
        $dataForm->nombre=$request['nombre'];
        $dataForm->descripcion=$request['descripcion'];
        $dataForm->descripcion_breve=$request['descripcion_breve'];
        if($request['categoria_id']!=''){
          $dataForm->categoria_id=$request['categoria_id'];
        }
        if($dataForm->save()){
            if($request['tags']!=''){
                foreach ($request['tags'] as $key => $value){
                    $caracteriticas=postTags::where([
                      ['tags_id','=',$value],
                      ['post_id','=',$id]]
                      )->get();
                    if(count($caracteriticas)<=0){
                        $post_tags = new postTags();
                        $post_tags->post_id = $id;
                        $post_tags->tags_id=$value;
                        $post_tags->updated_at=date('Y-m-d H:i:s');
                        $post_tags->created_at=date('Y-m-d H:i:s');
                        $post_tags->estatus='A';
                        $post_tags->save();
                    }
                }
            }
            $mensaje="El registro se ha guardado de forma exitosa";
            $clase="callout callout-success";
            $descripcion=['mensaje'=>$mensaje,'clase'=>$clase];
            return redirect('admin/post');
        }

    }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $idRequest=explode('-',$id);
        $dataForm=Post::find($idRequest[0]);
        $dataForm->updated_at=date('Y-m-d H:i:s');
        if($idRequest[1]=="e"){
            $dataForm->estatus='E';    
        }else{
            $dataForm->estatus='A';   
        }
        
        $dataForm->usuario_act_id=Session::get('usuario_id');
        if($dataForm->save()){
            $mensaje="El registro se ha guardado de forma exitosa";
            $clase="callout callout-success";
            $descripcion=['mensaje'=>$mensaje,'clase'=>$clase];
            echo json_encode(['statusCode'=>'Exito']);
            //return redirect('/admin/tipo/');
        }
    }
}
