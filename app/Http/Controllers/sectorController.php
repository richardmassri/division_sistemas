<?php

namespace laravelPrueba\Http\Controllers;

use Illuminate\Http\Request;
use laravelPrueba\Paise;
use laravelPrueba\Ciudad;
use laravelPrueba\Sector;

use Validator;
use Session;
use DB;

class sectorController extends Controller
{

    protected $validationRules=[
          'nombre' => 'required',
          'ciudade_id' => 'required',
    ];

      protected $validationMessages = [

        'nombre.required' => 'Debe ingresar el sector',
        'ciudade_id.required' => 'Debe ingresar la ciudad'
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $consultarPais=Paise::all();
        $consultarCiudad=Ciudad::all();
        $consultarSector=Sector::all();
      return view('pais.index')->with(['consultarPais'=> $consultarPais,'consultarCiudad'=>$consultarCiudad,'consultarSector'=>$consultarSector]);
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    $ciudad=Ciudad::where('estatus','=','A')->get();
    return view('sector.sector_form_registro')->with('ciudad', $ciudad);

        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request)
    {
        $sector=new Sector($request->all());
        $descripcion=[];        
      $v = Validator::make($request->all(), $this->validationRules,$this->validationMessages);
      if ($v->fails())
      {
        return redirect('admin/sector/create')->withInput()->withErrors($v);
      // The given data did not pass validation
      }else{
        $sector->estatus= 'A';
        $ciudad=Paise::where('estatus','=','A')->get();
        $sector->updated_at=date('Y-m-d H:i:s');
        $sector->created_at=date('Y-m-d H:i:s');
        $sector->usuario_ini_id=Session::get('usuario_id');
        $sector->usuario_act_id=Session::get('usuario_id');
        if($sector->save()){
            $mensaje="Se ha agregado con éxito el sector ".$sector->nombre;
            $clase="callout callout-success";
            $descripcion=['mensaje'=>$mensaje,'clase'=>$clase];
        }
          #return redirect()->withInput('error', 'Something went wrong.');
        return view('sector.sector_form_registro')->with(['descripcion'=>$descripcion,'ciudad'=>$ciudad]);
    }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $dataForm=Sector::find($id);
      //dd($dataForm);
      return view('sector.sector_form_view')->with('dataForm', $dataForm);
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   public function edit($id)
    {
      $ciudad=Ciudad::where('estatus','=','A')->get();
      $dataForm=Sector::find($id);
      //dd($dataForm);
      return view('sector.sector_form_update')->with(['dataForm'=> $dataForm,'ciudad'=>$ciudad]);
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

      $descripcion=[];
      $v = Validator::make($request->all(), $this->validationRules,$this->validationMessages);
      if ($v->fails())
      {
        return redirect('admin/sector/'.$id.'/edit')->withInput()->withErrors($v);
      // The given data did not pass validation
      }else{
        $dataForm=Sector::find($id);
        $dataForm->updated_at=date('Y-m-d H:i:s');
        $dataForm->usuario_act_id=Session::get('usuario_id');
        $dataForm->nombre=$request['nombre'];
        $dataForm->ciudade_id=$request['ciudade_id'];
        if($dataForm->save()){
            $mensaje="Se ha actualizado con éxito el sector " .$dataForm->nombre;
            $clase="callout callout-success";
            $descripcion=['mensaje'=>$mensaje,'clase'=>$clase];
            return redirect('admin/sector/');
        }
    }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $idRequest=explode('-',$id);
        $dataForm=Sector::find($idRequest[0]);
        $dataForm->updated_at=date('Y-m-d H:i:s');
        if($idRequest[1]=="e"){
            $dataForm->estatus='E';    
        }else{
            $dataForm->estatus='A';   
        }
        
        $dataForm->usuario_act_id=Session::get('usuario_id');
        if($dataForm->save()){
            $mensaje="El registro se ha guardado de forma exitosa";
            $clase="callout callout-success";
            $descripcion=['mensaje'=>$mensaje,'clase'=>$clase];
            echo json_encode(['statusCode'=>'Exito']);
            //return redirect('/admin/tipo/');
        }
    }
}
