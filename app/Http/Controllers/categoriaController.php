<?php

namespace laravelPrueba\Http\Controllers;

use Illuminate\Http\Request;

use laravelPrueba\Categoria;

use Validator;
use Session;
use DB;

class categoriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    protected $validationRules=[
          'nombre' => 'required|unique:categoria',
    ];

      protected $validationMessages = [

        'nombre.required' => 'Debe ingresar la categoria',
        'nombre.unique' => 'La categoria es unica',
    ];

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $consultarCategoria=Categoria::orderBy('id', 'desc')->get();
      return view('categoria.index')->with('consultarCategoria', $consultarCategoria);
        //
    }

    public function create()
    {
        return view('categoria.categoria_form_registro');
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $categoria=new Categoria($request->all());
        $descripcion=[];
      $v = Validator::make($request->all(), $this->validationRules,$this->validationMessages);
      if ($v->fails())
      {
        return redirect('admin/categoria/create')->withInput()->withErrors($v);
      // The given data did not pass validation
      }else{
        $categoria->estatus= 'A';
        $categoria->updated_at=date('Y-m-d H:i:s');
        $categoria->created_at=date('Y-m-d H:i:s');
        $categoria->usuario_ini_id=Session::get('usuario_id');
        $categoria->usuario_act_id=Session::get('usuario_id');
        if($categoria->save()){
            $mensaje="Se ha agregado con éxito el ".$categoria->nombre;
            $clase="callout callout-success";
            $descripcion=['mensaje'=>$mensaje,'clase'=>$clase];
        }
          #return redirect()->withInput('error', 'Something went wrong.');
        return view('categoria.categoria_form_registro')->with('descripcion',$descripcion);
    }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $dataForm=Categoria::find($id);
      return view('categoria.categoria_form_view')->with('dataForm', $dataForm);
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

      $dataForm=Categoria::find($id);
      //dd($dataForm);
      return view('categoria.categoria_form_update')->with('dataForm', $dataForm);
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

     $validationRules=[
          'nombre' => 'required',
            'descripcion' => 'required',
            'aplica_proyecto' => 'required',
            'aplica_propiedad' => 'required',
            'icono' => 'required|image'
    ];

       $validationMessages = [

        'nombre.required' => 'Debe ingresar el nombre',
        'descripcion.required' => 'Debe ingresar una descripcion',
        'icono.required' => 'Debe ingresar la foto',
        'icono.image' => 'Debe ingresar el formato correcto de la imagen',
        'nombre.unique' => 'El nombre es de la caracteristica es unico'
    ];

      $descripcion=[];
      $v = Validator::make($request->all(), $validationRules,$validationMessages);
      if ($v->fails())
      {
        return redirect('admin/categoria/'.$id.'/edit')->withInput()->withErrors($v);
      // The given data did not pass validation
      }else{
        $dataForm=Categoria::find($id);
        $dataForm->updated_at=date('Y-m-d H:i:s');
        $dataForm->usuario_act_id=Session::get('usuario_id');
        $dataForm->nombre=$request['nombre'];
        if($dataForm->save()){
            $mensaje="El registro se ha guardado de forma exitosa";
            $clase="callout callout-success";
            $descripcion=['mensaje'=>$mensaje,'clase'=>$clase];
            return redirect('admin/categoria/');
        }
    }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $idRequest=explode('-',$id);
        $dataForm=Categoria::find($idRequest[0]);
        $dataForm->updated_at=date('Y-m-d H:i:s');
        if($idRequest[1]=="e"){
            $dataForm->estatus='E';    
        }else{
            $dataForm->estatus='A';   
        }
        $dataForm->usuario_act_id=Session::get('usuario_id');
        if($dataForm->save()){
            $mensaje="El registro se ha guardado de forma exitosa";
            $clase="callout callout-success";
            $descripcion=['mensaje'=>$mensaje,'clase'=>$clase];
            echo json_encode(['statusCode'=>'Exito']);
            //return redirect('/admin/tipo/');
        }
    }
}
