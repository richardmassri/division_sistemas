<?php

namespace laravelPrueba\Http\Controllers;

use Illuminate\Http\Request;

use laravelPrueba\Paise;
use laravelPrueba\Ciudad;
use laravelPrueba\Sector;

use Validator;
use Session;
use DB;

class paisController extends Controller
{


    protected $validationRules=[
          'nombre' => 'required|unique:paises',
    ];

      protected $validationMessages = [

        'nombre.required' => 'Debe ingresar el pais',
        'nombre.unique' => 'El nombre es unico',
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $consultarPais=Paise::all();
        $consultarCiudad=Ciudad::all();
        $consultarSector=Sector::all();
      return view('pais.index')->with(['consultarPais'=> $consultarPais,'consultarCiudad'=>$consultarCiudad,'consultarSector'=>$consultarSector]);
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    return view('pais.pais_form_registro');

        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $pais=new Paise($request->all());
        $descripcion=[];
      $v = Validator::make($request->all(), $this->validationRules,$this->validationMessages);
      if ($v->fails())
      {
        return redirect('admin/pais/create')->withInput()->withErrors($v);
      // The given data did not pass validation
      }else{
        $pais->estatus= 'A';
        $pais->updated_at=date('Y-m-d H:i:s');
        $pais->created_at=date('Y-m-d H:i:s');
        $pais->usuario_ini_id=Session::get('usuario_id');
        $pais->usuario_act_id=Session::get('usuario_id');
        if($pais->save()){
            $mensaje="Se ha agregado con éxito el ".$pais->nombre;
            $clase="callout callout-success";
            $descripcion=['mensaje'=>$mensaje,'clase'=>$clase];
        }
          #return redirect()->withInput('error', 'Something went wrong.');
        return view('pais.pais_form_registro')->with('descripcion',$descripcion);
    }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $dataForm=Paise::find($id);
      //dd($dataForm);
      return view('pais.pais_form_view')->with('dataForm', $dataForm);
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   public function edit($id)
    {

      $dataForm=Paise::find($id);
      //dd($dataForm);
      return view('pais.pais_form_update')->with('dataForm', $dataForm);
        //
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    $validationRules=[
          'nombre' => 'required',
    ];

    $validationMessages = [
        'nombre.required' => 'Debe ingresar el pais',
        'nombre.unique' => 'El nombre es unico',
    ];

      $descripcion=[];
      $v = Validator::make($request->all(), $validationRules,$validationMessages);
      if ($v->fails())
      {
        return redirect('admin/pais/'.$id.'/edit')->withInput()->withErrors($v);
      // The given data did not pass validation
      }else{
        $dataForm=Paise::find($id);
        $dataForm->updated_at=date('Y-m-d H:i:s');
        $dataForm->usuario_act_id=Session::get('usuario_id');
        $dataForm->nombre=$request['nombre'];
        if($dataForm->save()){
            $mensaje="Se ha actualizado con éxito el pais ".$dataForm->nombre;
            $clase="callout callout-success";
            $descripcion=['mensaje'=>$mensaje,'clase'=>$clase];
            return redirect('admin/pais/');
        }
    }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $idRequest=explode('-',$id);
        $dataForm=Paise::find($idRequest[0]);
        $dataForm->updated_at=date('Y-m-d H:i:s');
        if($idRequest[1]=="e"){
            $dataForm->estatus='E';    
        }else{
            $dataForm->estatus='A';   
        }
        
        $dataForm->usuario_act_id=Session::get('usuario_id');
        if($dataForm->save()){
            $mensaje="El registro se ha guardado de forma exitosa";
            $clase="callout callout-success";
            $descripcion=['mensaje'=>$mensaje,'clase'=>$clase];
            echo json_encode(['statusCode'=>'Exito']);
            //return redirect('/admin/tipo/');
        }
    }
}
