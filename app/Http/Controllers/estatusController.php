<?php

namespace laravelPrueba\Http\Controllers;

use Illuminate\Http\Request;
use laravelPrueba\estatu;
use laravelPrueba\User;

use Validator;
use Session;

class estatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
        protected $validationRules=[
          'nombre' => 'required|unique:tipos',
    ];

      protected $validationMessages = [

        'nombre.required' => 'Debe ingresar el nombre',
        'nombre.unique' => 'El nombre es unico',
    ];
    public function index()
    {
        $consultarEstatus=estatu::orderBy('id', 'desc')->get();
      return view('estatus.index')->with('consultarEstatus', $consultarEstatus);
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('estatus.estatus_form_registro');
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tipo=new estatu($request->all());
        $descripcion=[];
      $v = Validator::make($request->all(), $this->validationRules,$this->validationMessages);
      if ($v->fails())
      {
        return redirect('admin/estatus/create')->withInput()->withErrors($v);
      // The given data did not pass validation
      }else{
        $tipo->estatus= 'A';
        $tipo->updated_at=date('Y-m-d H:i:s');
        $tipo->created_at=date('Y-m-d H:i:s');
        $tipo->usuario_ini_id=Session::get('usuario_id');
        $tipo->usuario_act_id=Session::get('usuario_id');
        if($tipo->save()){
            $mensaje="El registro se ha guardado de forma exitosa";
            $clase="callout callout-success";
            $descripcion=['mensaje'=>$mensaje,'clase'=>$clase];
        }
          #return redirect()->withInput('error', 'Something went wrong.');
        return view('estatus.estatus_form_registro')->with('descripcion',$descripcion);
    }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $dataForm=estatu::find($id);
      //dd($dataForm);
      return view('estatus.estatus_form_view')->with('dataForm', $dataForm);
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

      $dataForm=estatu::find($id);
      //dd($dataForm);
      return view('estatus.estatus_form_update')->with('dataForm', $dataForm);
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

      $descripcion=[];
      $v = Validator::make($request->all(), $this->validationRules,$this->validationMessages);
      if ($v->fails())
      {
        return redirect('admin/estatus/'.$id.'/edit')->withInput()->withErrors($v);
      // The given data did not pass validation
      }else{
        $dataForm=estatu::find($id);
        $dataForm->updated_at=date('Y-m-d H:i:s');
        $dataForm->usuario_act_id=Session::get('usuario_id');
        $dataForm->nombre=$request['nombre'];
        if($dataForm->save()){
            $mensaje="El registro se ha guardado de forma exitosa";
            $clase="callout callout-success";
            $descripcion=['mensaje'=>$mensaje,'clase'=>$clase];
            return redirect('admin/estatus/');
        }
    }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $idRequest=explode('-',$id);
        $dataForm=estatu::find($idRequest[0]);
        $dataForm->updated_at=date('Y-m-d H:i:s');
        if($idRequest[1]=="e"){
            $dataForm->estatus='E';    
        }else{
            $dataForm->estatus='A';   
        }
        
        $dataForm->usuario_act_id=Session::get('usuario_id');
        if($dataForm->save()){
            $mensaje="El registro se ha guardado de forma exitosa";
            $clase="callout callout-success";
            $descripcion=['mensaje'=>$mensaje,'clase'=>$clase];
            echo json_encode(['statusCode'=>'Exito']);
            //return redirect('/admin/tipo/');
        }
    }
}
