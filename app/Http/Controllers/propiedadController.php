<?php

namespace laravelPrueba\Http\Controllers;

use Illuminate\Http\Request;

use laravelPrueba\Tipo;
use laravelPrueba\User;
use laravelPrueba\Caracteristica;
use laravelPrueba\Sector;
use laravelPrueba\Galeria;
use laravelPrueba\Propiedad;
use laravelPrueba\Paise;
use laravelPrueba\vistaPropiedad;
use laravelPrueba\Ciudad;
use laravelPrueba\Proyecto;
use laravelPrueba\GaleriasPropiedades;
use laravelPrueba\Operaciones;
use laravelPrueba\OperacionesPropiedades;
use laravelPrueba\CaracteristicasPropiedades;
use Validator;
use Session;
use laravelPrueba\uploadHandler;
use DB;

class propiedadController extends Controller
{

         protected $validationRules=[
            'nombre' => 'required',
            'descripcion_breve' => 'required',
            'descripcion' => 'required',
            'ubicacion' => 'required',
            'precio' => 'required',
            'fecha_inicial' => 'required',
            'fecha_final' => 'required',
            'estatus_invermaste' => 'required',
            'agente_id' => 'required',
            'sector_id' => 'required',
            'tipo_id' => 'required'
    ];


      protected $validationMessages = [

        'nombre.required' => 'Debe ingresar el nombre',
        'descripcion.required' => 'Debe ingresar la Descripción'
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $propiedaddesGaleria=DB::table('propiedades')->select('propiedades.*','galerias_propiedades.*')->join('galerias_propiedades','propiedades.id','=','galerias_propiedades.propiedad_id')->where([
            ['propiedades.estatus','=','A']
            ])->get();
      $consultarProyecto=propiedad::where('estatus', 'A')->get();
      $consultarGaleria=GaleriasPropiedades::where('estatus', 'A')->get();
      return view('propiedades.index')->with(['consultarProyecto'=> $consultarProyecto,'propiedaddesGaleria'=>$propiedaddesGaleria]);
          $consultarPropiedad=Propiedad::where('estatus', 'A')->get();
    }

    public function meGusta(){
      $id=$_REQUEST['id'];
      \Cache::put($id,$id, 60);
      echo json_encode(['propiedad_id'=>$id]);
    }

    public function chatbackend(){
      $mensaje=$_REQUEST['mensaje'];
      $mensaje_concat="";
      if($mensaje!=null){
        $mensaje_concat=\Cache::get('mensaje_bakend').$mensaje;
        //$mensaje_concat=$mensaje_concat+
      }
      \Cache::put('mensaje_bakend',$mensaje_concat, 60);
      echo json_encode(['mensaje'=>$mensaje_concat]);
    }

      public function chatFrontend(){
        $mensaje_concat2="";
        $mensaje=$_REQUEST['mensaje'];
      if($mensaje!=null){
        $mensaje_concat2=\Cache::get('mensaje_bakend').$mensaje;
      }
      \Cache::put('mensaje_frontend',$mensaje_concat2, 60);
      echo json_encode(['mensaje'=>$mensaje_concat2]);
    }

    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $es_agente=Session::get('es_agente');
        $usuario_id=Session::get('usuario_id');
        $usuario=Session::get('nombre');
        $tipo=Tipo::where('estatus', 'A')->get();
        $agente=User::where([['estatus','=','A'],['es_agente','=','SI']])->get();
        $caracteristica=Caracteristica::where('estatus', 'A')->get();
        $sector=Sector::where('estatus', 'A')->get();
        $pais=Paise::where('estatus', 'A')->get();
        $ciudad=Ciudad::where('estatus', 'A')->get();
        $propiedad=Propiedad::where('estatus', 'A')->get();
        $caracteristica=Caracteristica::where([['estatus','=', 'A'],['aplica_propiedad','=','SI']])->get();
        $operaciones=Operaciones::where('estatus', 'A')->get();
        return view('propiedades.propiedad_form_registro')->with(array('tipos'=>$tipo,'agentes'=>$agente,'caracteristicas'=> $caracteristica,'sectores' => $sector,'pais'=>$pais,'ciudad'=>$ciudad,'propiedad'=>$propiedad,'caracteristica'=>$caracteristica,'operaciones'=>$operaciones,'es_agente'=>$es_agente,'usuario_id'=>$usuario_id,'usuario'=>$usuario));
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $es_agente=Session::get('es_agente');
        $usuario_id=Session::get('usuario_id');
        $usuario=Session::get('nombre');
        $v = Validator::make($request->all(), $this->validationRules,$this->validationMessages);
      if ($v->fails())
      {
        return redirect('admin/propiedad/create')->withInput()->withErrors($v);
      // The given data did not pass validation
      }else{
        $precio_desde=str_replace(".", "", $request['precio_en_dolares']);
        $precio_desde=str_replace(",", ".", $precio_desde);
        $precio_desde=str_replace("USD ", "", $precio_desde);
        $request['precio_en_dolares']=$precio_desde;
        $propiedad = new Propiedad($request->all());
        $propiedad->fecha_publicacion = date("Y-m-d", strtotime($request['fecha_publicacion']));
        $propiedad->fecha_inicial = date("Y-m-d", strtotime($request['fecha_inicial']));
        $propiedad->fecha_final = date("Y-m-d", strtotime($request['fecha_final']));
        $propiedad->estatus='A';
        $propiedad->usuario_ini_id=Session::get('usuario_id');
        $propiedad->usuario_act_id=Session::get('usuario_id');
        $propiedad->updated_at=date('Y-m-d H:i:s');
        $propiedad->created_at=date('Y-m-d H:i:s');
        $propiedad->precio=$request['precio'];
        $propiedad->construccion='SI';
        //dd($request['logotipo']);
        $propiedad->save();
        foreach ($request['caracteristica'] as $key => $value){
            $caracteristicae_propiedades = new CaracteristicasPropiedades();
            $caracteristicae_propiedades->propiedad_id = $propiedad->id;
            $caracteristicae_propiedades->caracteristica_id=$value;
            $caracteristicae_propiedades->updated_at=date('Y-m-d H:i:s');
            $caracteristicae_propiedades->created_at=date('Y-m-d H:i:s');
            $caracteristicae_propiedades->estatus='A';
            $caracteristicae_propiedades->nombre='';
            $caracteristicae_propiedades->save();
        }

        foreach ($request['operaciones'] as $key => $value){
            $OperacionesPropiedades = new OperacionesPropiedades();
            $OperacionesPropiedades->propiedad_id = $propiedad->id;
            $OperacionesPropiedades->operacion_id=$value;
            $OperacionesPropiedades->updated_at=date('Y-m-d H:i:s');
            $OperacionesPropiedades->created_at=date('Y-m-d H:i:s');
            $OperacionesPropiedades->estatus='A';
            $OperacionesPropiedades->nombre='';
            $OperacionesPropiedades->save();
        }
        //dd($request['logotipo']);
        //$request['logotipo']->getClientOriginalName());
        $mensaje="Se ha agregado con éxito la propiedad ".$propiedad->nombre;
        $clase="callout callout-success";
        $descripcion=['mensaje'=>$mensaje,'clase'=>$clase];
        $tipo=Tipo::where('estatus', 'A')->get();
        $agente=User::where('estatus', 'A')->get();
        $sector=Sector::where('estatus', 'A')->get();
        $pais=Paise::where('estatus', 'A')->get();
        $ciudad=Ciudad::where('estatus', 'A')->get();
        $caracteristica=Caracteristica::where('estatus', 'A')->get();
        $operaciones=Operaciones::where('estatus', 'A')->get();
        //$proyectos=Propiedades::where('estatus', 'A')->get();
        $propiedad=Propiedad::where('estatus', 'A')->get();
        return view('propiedades.propiedad_form_registro')->with(array('descripcion'=>$descripcion,'tipos'=>$tipo,'agentes'=>$agente,'caracteristica'=> $caracteristica,'sectores' => $sector,'pais'=>$pais,'propiedad'=>$propiedad,'ciudad'=>$ciudad,'operaciones'=>$operaciones,'propiedad'=>$propiedad,'es_agente'=>$es_agente,'usuario'=>$usuario,'usuario_id'=>$usuario_id));

        //$foto_cortar=explode('/',$request['logotipo']->store('public'));
        //
    }
        //
    }



     public function postGaleria(Request $request){
        //dd($request);
        
        $upload_handler='';
        $contadorPrincipal=0;
        $contadorPromocional=0;
        $destacado='';
        $promocional='';
        $filePost="";
        $filePostPro="";
        if($request->isMethod('post')){
            $upload_handler = new UploadHandler();
            if(isset($_REQUEST['destacado'])){
                $filePost=$_REQUEST['destacado'];
              $filaName=explode('(',$upload_handler['response']['files'][0]->name);
              if(isset($filaName[1])){
                $filaName=$filaName[1];
                $filePostExplode=explode('.',$_REQUEST['destacado']);
                $filePost=$filePostExplode[0].' ('.$filaName[0].')'.'.'.$filePostExplode[1];
              }
                  if($filePost==$upload_handler['response']['files'][0]->name){
                    $destacado="SI";
                    if(isset($_REQUEST['edit'])){
                      $dataImagenPrincipal=GaleriasPropiedades::where([
                        ['destacada','=','SI'],
                        ['propiedad_id','=',$_REQUEST['propiedad']]])->get();
                      if(count($dataImagenPrincipal)>0){
                        $dataImagenPrincipal=GaleriasPropiedades::find($dataImagenPrincipal[0]->id);
                        $dataImagenPrincipal->destacada='';
                        $dataImagenPrincipal->save();
                      }
                    }
                  }
            }
            if(isset($_REQUEST['destacadoPromocionl'])){
                  $filePostPro=$_REQUEST['destacadoPromocionl'];
                  $filaNamePro=explode('(',$upload_handler['response']['files'][0]->name);
                  if(isset($filaNamePro[1])){
                    $filaNamePro=$filaNamePro[1];
                    $filePostExplode=explode('.',$_REQUEST['destacadoPromocionl']);
                    $filePostPro=$filePostExplode[0].' ('.$filaNamePro[0].')'.'.'.$filePostExplode[1];
                   }
                       if($filePostPro==$upload_handler['response']['files'][0]->name){
                            $promocional="SI";
                            if(isset($_REQUEST['edit'])){
                            $dataImagenPrincipal=GaleriasPropiedades::where([
                        ['promocional','=','SI'],
                        ['propiedad_id','=',$_REQUEST['propiedad']]])->get();
                            if(count($dataImagenPrincipal)>0){
                              $dataImagenPrincipal=GaleriasPropiedades::find($dataImagenPrincipal[0]->id);
                              $dataImagenPrincipal->promocional='';
                              $dataImagenPrincipal->save();
                            }
                          }
                        }
            }
            $galeria = new GaleriasPropiedades();
            $galeria->updated_at=date('Y-m-d H:i:s');
            $galeria->created_at=date('Y-m-d H:i:s');
            $galeria->usuario_ini_id=1;
            $galeria->usuario_act_id=1;
            $galeria->estatus='A';
            $galeria->propiedad_id=$_REQUEST['propiedad'];
            $galeria->destacada=$destacado;
            $galeria->promocional=$promocional;
            $galeria->nombre='upload/server/php/files/'.$upload_handler['response']['files'][0]->name;
            $galeria->save();

        }else{
            error_reporting(E_ALL | E_STRICT);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $dataForm=Propiedad::find($id);
        $tipo=Tipo::where('estatus', 'A')->get();
        $operaciones=Operaciones::where('estatus', 'A')->get();
        $agente=User::where('estatus', 'A')->get();
        $caracteristica=Caracteristica::where('estatus', 'A')->get();
        $sector=Sector::where('estatus', 'A')->get();
        $pais=Paise::where('estatus', 'A')->get();
        $ciudad=Ciudad::where('estatus', 'A')->get();
        $consultaproyectosCaracteristicas=DB::table('propiedades')->select('propiedades.*','caracteristicas_propiedades.*')->join('caracteristicas_propiedades','propiedades.id','=','caracteristicas_propiedades.propiedad_id')->where([
            ['propiedades.estatus','=','A'],
            ['propiedades.id','=',$id],
            ])->get();
        $consultaproyectosOperacion=DB::table('propiedades')->select('propiedades.*','operaciones_propiedades.*')->join('operaciones_propiedades','propiedades.id','=','operaciones_propiedades.propiedad_id')->where([
            ['propiedades.estatus','=','A'],
            ['propiedades.id','=',$id],
            ])->get();
        //dd($consultaproyectosCaracteristicas);
      //dd($dataForm);
      return view('propiedades.propiedad_form_view')->with(['dataForm'=> $dataForm,'tipo'=>$tipo,'agentes'=>$agente,'caracteristica'=>$caracteristica,'sector'=>$sector,'pais'=>$pais,'ciudad'=>$ciudad,'consultaproyectosCaracteristicas'=>$consultaproyectosCaracteristicas,'consultaproyectosOperacion'=>$consultaproyectosOperacion,'operaciones'=>$operaciones]);
    }

        public function showGaleriaPropiedad($id)
    {
      $dataForm=Propiedad::find($id);
      $dataFormGaleria=GaleriasPropiedades::where([
        ['estatus','=','A'],
        ['propiedad_id','=',$id]
        ])->orderBy('destacada','promocional','desc')->get();
        return view('propiedades.propiedad_view_galeria')->with(['dataForm'=>$dataForm,'dataFormGaleria'=>$dataFormGaleria]);
        //
    }

        public function eliminarFoto(Request $request)
    {
      $dataImagenPrincipal=GaleriasPropiedades::find($request['campoId']);
      $fileContenido=$dataImagenPrincipal->id.'-'.$request['FileInput'][0]->getClientOriginalName();
        $archivoEliminado=unlink($request['campoFoto']);
        //$request['FileInput'][0]->store('public');
        if($archivoEliminado){
        \Storage::disk('galeria')->put($fileContenido,  file_get_contents($request['FileInput'][0]->getRealPath()));
        $dataImagenPrincipal->nombre="upload/server/php/files/".$fileContenido;
        $dataImagenPrincipal->save();
        }
          echo json_encode(['statusCode'=>'Exitoso','id'=>$dataImagenPrincipal->propiedad_id]);
    }

    public function cambiarImagenPrincipalPropiedad()
    {
      $id=$_REQUEST['id'];
      $propiedad_id=$_REQUEST['propiedad_id'];
      $dataImagenPrincipal=GaleriasPropiedades::where([
        ['destacada','=','SI'],
        ['propiedad_id','=',$propiedad_id]])->get();
      $dataImagenPrincipal=GaleriasPropiedades::find($dataImagenPrincipal[0]->id);
      $dataImagenPrincipal->destacada='';
      if($dataImagenPrincipal->save()){
        $dataImagenPrincipal=GaleriasPropiedades::find($id);
        $dataImagenPrincipal->destacada="SI";
        $dataImagenPrincipal->save();
        echo json_encode(['statusCode'=>'Exitoso','id'=>$propiedad_id]);
      }
    }

    public function cambiarImagenPromocionalPropiedad()
    {
      $id=$_REQUEST['id'];
      $propiedad_id=$_REQUEST['propiedad_id'];
      $dataImagenPrincipal=GaleriasPropiedades::where([
        ['promocional','=','SI'],
        ['propiedad_id','=',$propiedad_id]])->get();
      $dataImagenPrincipal=GaleriasPropiedades::find($dataImagenPrincipal[0]->id);
      $dataImagenPrincipal->promocional='';
      if($dataImagenPrincipal->save()){
        $dataImagenPrincipal=GaleriasPropiedades::find($id);
        $dataImagenPrincipal->promocional="SI";
        $dataImagenPrincipal->save();
        echo json_encode(['statusCode'=>'Exitoso','id'=>$propiedad_id]);
      }
    }

            public function eliminarImagen(Request $request)
    {
      $eliminarImagen=GaleriasPropiedades::find($request['id']);
      $archivoEliminado=unlink($eliminarImagen->nombre);
      $eliminarImagen->delete();
      echo json_encode(['statusCode'=>'Exitoso','id'=>$eliminarImagen->propiedad_id]);
        //dd($request['FileInput'][0]->getClientOriginalName());

      }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

       public function edit($id)
    {
        $dataForm=Propiedad::find($id);
        $tipo=Tipo::where('estatus', 'A')->get();
        $operaciones=Operaciones::where('estatus', 'A')->get();
        $agente=User::where('estatus', 'A')->get();
        $caracteristica=Caracteristica::where('estatus', 'A')->get();
        $sector=Sector::where('estatus', 'A')->get();
        $pais=Paise::where('estatus', 'A')->get();
        $ciudad=Ciudad::where('estatus', 'A')->get();
        $consultaproyectosCaracteristicas=DB::table('propiedades')->select('propiedades.*','caracteristicas_propiedades.*')->join('caracteristicas_propiedades','propiedades.id','=','caracteristicas_propiedades.propiedad_id')->where([
            ['propiedades.estatus','=','A'],
            ['propiedades.id','=',$id],
            ])->get();
        //dd($consultaproyectosCaracteristicas);
        $consultaproyectosOperacion=DB::table('propiedades')->select('propiedades.*','operaciones_propiedades.*')->join('operaciones_propiedades','propiedades.id','=','operaciones_propiedades.propiedad_id')->where([
            ['propiedades.estatus','=','A'],
            ['propiedades.id','=',$id],
            ])->get();
        //dd($consultaproyectosCaracteristicas);
      //dd($dataForm);
      
      return view('propiedades.propiedad_form_update')->with(['dataForm'=> $dataForm,'tipo'=>$tipo,'agentes'=>$agente,'caracteristica'=>$caracteristica,'sector'=>$sector,'pais'=>$pais,'ciudad'=>$ciudad,'consultaproyectosCaracteristicas'=>$consultaproyectosCaracteristicas,'consultaproyectosOperacion'=>$consultaproyectosOperacion,'operaciones'=>$operaciones]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $operaciones_elimi=DB::table('operaciones_propiedades')->where('propiedad_id',$id)->delete();
      if($request['operaciones']!=''){
        foreach ($request['operaciones'] as $key => $value){
            $operaciones_propiedades = new OperacionesPropiedades();
            $operaciones=OperacionesPropiedades::where([
              ['operacion_id','=',$value],
              ['propiedad_id','=',$id]])->get();
            if(count($operaciones)<=0){
              $operaciones_propiedades->propiedad_id = $id;
              $operaciones_propiedades->operacion_id=$value;
              $operaciones_propiedades->updated_at=date('Y-m-d H:i:s');
              $operaciones_propiedades->created_at=date('Y-m-d H:i:s');
              $operaciones_propiedades->estatus='A';
              $operaciones_propiedades->nombre='';
              $operaciones_propiedades->save();
            }
        }
      }

      $caracteristicas_elimi=DB::table('caracteristicas_propiedades')->where('propiedad_id',$id)->delete();

      if($request['caracteristica']!=''){
        foreach ($request['caracteristica'] as $key => $value){
            $caracteristicae_propiedad = new CaracteristicasPropiedades();
            $caracteriticas=CaracteristicasPropiedades::where([
              ['caracteristica_id','=',$value],
              ['propiedad_id','=',$id]]
              )->get();
            if(count($caracteriticas)<=0){
              $caracteristicae_propiedad->propiedad_id = $id;
              $caracteristicae_propiedad->caracteristica_id=$value;
              $caracteristicae_propiedad->updated_at=date('Y-m-d H:i:s');
              $caracteristicae_propiedad->created_at=date('Y-m-d H:i:s');
              $caracteristicae_propiedad->estatus='A';
              $caracteristicae_propiedad->nombre='';
              $caracteristicae_propiedad->save();
            }
        }
      }
        $dataForm=Propiedad::find($id);
        $dataForm->updated_at=date('Y-m-d H:i:s');
        $dataForm->usuario_act_id=Session::get('usuario_id');
        $request['fecha_publicacion'] = date("Y-m-d", strtotime($request['fecha_publicacion']));
        $request['fecha_inicial'] = date("Y-m-d", strtotime($request['fecha_inicial']));
        $request['fecha_final'] = date("Y-m-d", strtotime($request['fecha_final']));

        $precio_en_dolares=str_replace(".", "", $request['precio_en_dolares']);
        $precio_en_dolares=str_replace(",", ".", $precio_en_dolares);

        $precio_en_dolares=str_replace("USD ", "", $precio_en_dolares);

        $precio=str_replace(".", "", $request['precio']);
        $precio=str_replace(",", ".", $precio);
        $precio=str_replace("USD ", "", $precio);

        $request['precio_en_dolares']=$precio_en_dolares;
        $request['precio']=$precio;
        if($dataForm->update($request->all())){
            return redirect('admin/propiedad/');
        }
        //
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function dependenciaEstado(){
      $pais = $_REQUEST["code"];
      if($pais!=''){
        $ciudades='';
          $consultaCiudad=DB::table('ciudades')->select('ciudades.nombre as nombre_proyecto','ciudades.id as id')->
          where('ciudades.paise_id',$pais)
          ->get();
        foreach($consultaCiudad as $value)
        {
            $ciudades.="<option value=\"$value->id\">$value->nombre_proyecto</option>";
        }
        echo json_encode(['ciudad'=>$ciudades]);
      }else{
        echo json_encode(['ciudad'=>'']);
      }
    }



    public function dependenciaSector(){
      $ciudad = $_REQUEST["code"];
      if($ciudad!=''){
        $sector='';
          $consultaSector=DB::table('sectores')->select('sectores.nombre as nombre_sector','sectores.id as id')->
          where('sectores.ciudade_id',$ciudad)
          ->get();
        foreach($consultaSector as $value)
        {
            $sector.="<option value=\"$value->id\">$value->nombre_sector</option>";
        }
        echo json_encode(['sector'=>$sector]);
      }else{
        echo json_encode(['sector'=>'']);
      }
    }
  }
