<?php

namespace laravelPrueba\Http\Controllers;

use Illuminate\Http\Request;

use laravelPrueba\Paise;
use laravelPrueba\Ciudad;
use laravelPrueba\Sector;

use Validator;
use Session;
use DB;

class ciudadController extends Controller
{

        protected $validationRules=[
          'nombre' => 'required',
          'paise_id' => 'required',
    ];

      protected $validationMessages = [

        'nombre.required' => 'Debe ingresar la ciudad',
        'paise_id.required' => 'Debe ingresar el pais',
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $consultarPais=Paise::all();
        $consultarCiudad=Ciudad::all();
        $consultarSector=Sector::all();
      return view('pais.index')->with(['consultarPais'=> $consultarPais,'consultarCiudad'=>$consultarCiudad,'consultarSector'=>$consultarSector]);
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    $pais=Paise::where('estatus','=','A')->get();
    return view('ciudad.ciudad_form_registro')->with('pais', $pais);

        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $ciudad=new Ciudad($request->all());
        $descripcion=[];        
      $v = Validator::make($request->all(), $this->validationRules,$this->validationMessages);
      if ($v->fails())
      {
        return redirect('admin/ciudad/create')->withInput()->withErrors($v);
      // The given data did not pass validation
      }else{
        $ciudad->estatus= 'A';
        $pais=Paise::where('estatus','=','A')->get();
        $ciudad->updated_at=date('Y-m-d H:i:s');
        $ciudad->created_at=date('Y-m-d H:i:s');
        $ciudad->usuario_ini_id=Session::get('usuario_id');
        $ciudad->usuario_act_id=Session::get('usuario_id');
        if($ciudad->save()){
            $mensaje="Se ha agregado con éxito la ciudad ".$ciudad->nombre;
            $clase="callout callout-success";
            $descripcion=['mensaje'=>$mensaje,'clase'=>$clase];
        }
          #return redirect()->withInput('error', 'Something went wrong.');
        return view('ciudad.ciudad_form_registro')->with(['descripcion'=>$descripcion,'pais'=>$pais]);
    }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pais=Paise::where('estatus','=','A')->get();
        $dataForm=Ciudad::find($id);
      //dd($dataForm);
      return view('ciudad.ciudad_form_view')->with(['dataForm'=>$dataForm,'pais'=>$pais]);
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   public function edit($id)
    {

      $dataForm=Ciudad::find($id);
      $pais=Paise::where('estatus','=','A')->get();
      //dd($dataForm);
      return view('ciudad.ciudad_form_update')->with(['dataForm'=> $dataForm,'pais'=>$pais]);
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

      $descripcion=[];
      $v = Validator::make($request->all(), $this->validationRules,$this->validationMessages);
      if ($v->fails())
      {
        return redirect('admin/ciudad/'.$id.'/edit')->withInput()->withErrors($v);
      // The given data did not pass validation
      }else{
        $dataForm=Ciudad::find($id);
        $dataForm->updated_at=date('Y-m-d H:i:s');
        $dataForm->usuario_act_id=Session::get('usuario_id');
        $dataForm->nombre=$request['nombre'];
        $dataForm->paise_id=$request['paise_id'];
        if($dataForm->save()){
            $mensaje="Se ha actualizado con éxito la ciudad ".$dataForm->nombre;
            $mensaje="El registro se ha guardado de forma exitosa";
            $clase="callout callout-success";
            $descripcion=['mensaje'=>$mensaje,'clase'=>$clase];
            return redirect('admin/pais/');
        }
    }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $idRequest=explode('-',$id);
        $dataForm=Ciudad::find($idRequest[0]);
        $dataForm->updated_at=date('Y-m-d H:i:s');
        if($idRequest[1]=="e"){
            $dataForm->estatus='E';    
        }else{
            $dataForm->estatus='A';   
        }
        
        $dataForm->usuario_act_id=Session::get('usuario_id');
        if($dataForm->save()){
            $mensaje="El registro se ha guardado de forma exitosa";
            $clase="callout callout-success";
            $descripcion=['mensaje'=>$mensaje,'clase'=>$clase];
            echo json_encode(['statusCode'=>'Exito']);
            //return redirect('/admin/tipo/');
        }
    }
}
