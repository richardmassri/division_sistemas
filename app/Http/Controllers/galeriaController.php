<?php

namespace laravelPrueba\Http\Controllers;

use Illuminate\Http\Request;
use laravelPrueba\uploadHandler;

use laravelPrueba\Galeria;
use laravelPrueba\Proyecto;
use Validator;

class galeriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$proyectos=Proyecto::all();
   return view('galeria.index')->with('proyectos',$proyectos);

    }
    public function postGaleriaDelete()
    {

    }

public function postGaleria(Request $request){
    	//dd($request);
		if($request->isMethod('post')){
            $upload_handler = new UploadHandler();
			$galeria = new Galeria();
			$galeria->updated_at=date('Y-m-d H:i:s');
    		$galeria->created_at=date('Y-m-d H:i:s');
    		$galeria->usuario_ini_id=1;
    		$galeria->usuario_act_id=1;
    		$galeria->estatus='A';
    		$galeria->proyecto_id=$_REQUEST['proyecto'];
            if(isset($_REQUEST['destacada'])){
                $galeria->destacada=$_REQUEST['destacada'];
            }
    		$galeria->nombre='upload/server/php/files/'.$upload_handler['response']['files'][0]->name;
    		$galeria->save();
			//$upload_handler = new UploadHandler();
			//var_dump("Finitooooooooo");
			//var_dump($upload_handler['response']['files'][0]->name);
		}else{
            error_reporting(E_ALL | E_STRICT);
        }
		//var_dump($upload_handler);
		//return response()->json(['mensaje'=>'si','upload_handler'=>$upload_handler]);
    	//dd("aquiiii");
   //return view('galeria.index');
        //
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    error_reporting(E_ALL | E_STRICT);
	$upload_handler = new UploadHandler();
	dd($upload_handler);
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
