<?php

namespace laravelPrueba\Http\Controllers;

use Illuminate\Http\Request;

use laravelPrueba\contactarAgente;
use laravelPrueba\User;
use laravelPrueba\estatu;

use Validator;
use Session;
use Mail;

class contactoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $consultarContacto=contactarAgente::orderBy('id','desc','estatus_id','asc')->get();

        $statusSolicitudes=estatu::where('id','!=',1)->get();
      return view('contacto.index')->with(['consultarContacto'=> $consultarContacto,'statusSolicitudes'=>$statusSolicitudes]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    public function solicitudAgente($id)
    {
      $consultarContacto=contactarAgente::where('equipo_id','=',$id)->orWhere('contacto_id','=',1)->orderBy('id', 'DESC')->get();
      $statusSolicitudes=estatu::where('estatus','=','A')->get();

      return view('contacto.index')->with(['consultarContacto'=> $consultarContacto,'statusSolicitudes'=>$statusSolicitudes]);  
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $dataForm=contactarAgente::find($id);
      //dd($dataForm);
      return view('contacto.contacto_form_view')->with('dataForm', $dataForm);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data=[];
        $dataForm=contactarAgente::find($id);
        $es_agente=Session::get('es_agente');
        $dataForm->updated_at=date('Y-m-d H:i:s');
        $dataForm->usuario_act_id=Session::get('usuario_id');
        $dataForm->estatus_id=$request['estatus'];
        $dataForm->descripcion=$request['descripcion'];
        $dataForm->estatus='A';
        if($dataForm->save()){
            //dd($data);
            $dato['administracion']="backend";
            $dato['nombre']=Session::get('nombre');
            $dato['email']=Session::get('correo');
            \Mail::send('contacto.correo_envio',['data'=>$dato],function($msj){
                $msj->subject('Solicitud Invermaster');
                $email=Session::get('correo');
                $msj->to($email);
            });
            if($es_agente=="SI"){
                $cantidad=Session::get('solicitud_agente')-1;
                Session::put('solicitud_agente',$cantidad);
            }else{
                if(Session::get('solicitud_admin')>0){
                    $cantidad=Session::get('solicitud_admin')-1;
                    Session::put('solicitud_admin',$cantidad);
                }
            }
            $mensaje="El registro se ha actuaizado de forma exitosa";
            $clase="callout callout-success";
            $descripcion=['mensaje'=>$mensaje,'clase'=>$clase];
            echo json_encode(['statusCode'=>'Exito','id'=>Session::get('usuario_id'),'es_agente'=>$es_agente]);
        }
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
