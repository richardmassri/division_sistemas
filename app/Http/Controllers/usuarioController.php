<?php

namespace laravelPrueba\Http\Controllers;

use Illuminate\Http\Request;

use laravelPrueba\User;

use Validator;
use Session;
use DB;
use LRedis;

class UsuarioController extends Controller
{

    public function postConsulta(Request $request){
      //$usuario_set="richard";
      //$clave_set="1234";
      $correo = $request->get('correo');
      $password = $request->get('contrasenia');
      $consultarUser=User::where(['correo'=>$correo,'contrasenia'=>$password])->get();
      if(count($consultarUser)>0){
        Session::put('usuario_id', $consultarUser[0]['id']);
        Session::put('nombre', $consultarUser[0]['nombre']);
        Session::put('apellido', $consultarUser[0]['apellido']);
        Session::put('correo', $consultarUser[0]['correo']);
        Session::put('foto', $consultarUser[0]['foto']);
        Session::put('es_agente', $consultarUser[0]['es_agente']);
        //return view('panelAdmin')->with('mensaje', '');
         //return view('panelAdmin');
         return Redirect('/panelAdmin');
      }else{
        $mensaje="Usuario o clave incorrecta";
         return view('login')->with('mensaje', $mensaje);
        
          // Session::put('usuario_id', $consltarUser[0]['id']);
        
          //    return response()->json(["mensaje" => "El usuario se ha logeado de forma correcta",
          //     "resultado" => false,
          //     "statusCode" => "LOGEO_EXITOSO"]);
          // $response = [];
   //return redirect('login/main');

          //return Response::json($response);
      }

  }

  public function sendMessage(){
    $redis = LRedis::connection();
    $data = ['message' => $_REQUEST['message'], 'user' => $_REQUEST['user']];
    $redis->publish('message', json_encode($data));
    return response()->json([]);
  }

  public function sendmessageForm(){
    $nombre= $_REQUEST['nombre'];
    $apellido= $_REQUEST['apellido'];
    $correo= $_REQUEST['correo'];
    $nombreApellido=$nombre." ".$apellido;
    \Cache::put('nombreApellido',$nombreApellido, 60);
    //$redis->publish('message', json_encode($data));
    return response()->json(['nombreApellido'=>\Cache::get('nombreApellido')]);
  }

    public function logout(){
      Session::flush();
      return redirect('/');
    }


    public function index()
    {
      $consultarUser=User::all();
      return view('usuario.index')->with('consultarUser', $consultarUser);
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
      return view('usuario.usuario_form_registro');

        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    
     protected $validationRules=[
          'nombreForm' => 'required',
            'contrasenaForm' => 'required',
            'CorreoFrom' => 'required|email',
            'correo' => 'unique:usuarios',
            'fotoForm' => 'required|image'
    ];

      protected $validationMessages = [

        'nombreForm.required' => 'Debe ingresar el nombre',
        'contrasenaForm.required' => 'Debe ingresar el password',
        'contrasenaForm.min' => 'Debe ingresar el minimos del password',
        'CorreoFrom.required' => 'Debe ingresar el correo',
        'fotoForm.required' => 'Debe ingresar la foto',
        'fotoForm.image' => 'Debe ingresar el formato correcto de la imagen',
        'correo.unique' => 'El correo ya existe',
        'CorreoFrom.email' => 'Debe ingresar un correo valido'
    ];


    public function store(Request $request){
      $usuario=new User();
      $v = Validator::make($request->all(), $this->validationRules,$this->validationMessages);
      if ($v->fails())
      {
        return redirect('/admin/usuario/create')->withInput()->withErrors($v);
      // The given data did not pass validation
      }else{
    $usuario->correo=$request['CorreoFrom'];
    $usuario->nombre=$request['nombreForm'];
    $usuario->apellido=$request['ApellidoForm'];
    $usuario->telefono_local=$request['telefono_fijoForm'];
    $usuario->telefono_celular=$request['telefono_celularForm'];
    $usuario->contrasenia=$request['contrasenaForm'];
    $usuario->contrasenia_nueva=$request['contrasena_nuevaForm'];
    $usuario->remember_token= $request['_token'];
    $usuario->estatus= 'A';
    $usuario->updated_at=date('Y-m-d H:i:s');
    $usuario->created_at=date('Y-m-d H:i:s');
    $a = array("jpeg", "png","jpg","GIF","gif","tif");
    $clase="callout callout-success";
    $mensaje="Se ha agregado con éxito el usuario ".$usuario->nombre;
    $descripcion=['mensaje'=>$mensaje,'clase'=>$clase];
    $request['fotoForm']->store('public');
    $foto_cortar=explode('/',$request['fotoForm']->store('public'));
    $usuario->foto='storage/'.$foto_cortar[1];
    $usuario->save();
      #return redirect()->withInput('error', 'Something went wrong.');
      return view('usuario.usuario_form_registro')->with('descripcion',$descripcion);
    }
  }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $dataForm=user::find($id);
      //dd($dataForm);
      return view('usuario.usuario_form_view')->with('dataForm', $dataForm);
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $dataForm=user::find($id);
      //dd($dataForm);
      return view('usuario.usuario_form_update')->with('dataForm', $dataForm);

  }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

           $validationRules=[
          'nombreForm' => 'required',
            'CorreoFrom' => 'required|email',
            'correo' => 'unique:usuarios',
    ];

       $validationMessages = [

        'nombreForm.required' => 'Debe ingresar el nombre',
        'contrasenaForm.min' => 'Debe ingresar el minimos del password',
        'CorreoFrom.required' => 'Debe ingresar el correo',
        'correo.unique' => 'El correo ya existe',
        'CorreoFrom.email' => 'Debe ingresar un correo valido'
    ];

      if($request['fotoForm']!=null or $request['fotoForm']!=''){
        $validationRules=[
          'nombreForm' => 'required',
          'CorreoFrom' => 'required|email',
          'correo' => 'unique:usuarios',
          'fotoForm' => 'required|image',

        ];
         $validationMessages = [
          'nombreForm.required' => 'Debe ingresar el nombre',
          'contrasenaForm.required' => 'Debe ingresar el password',
          'CorreoFrom.required' => 'Debe ingresar el correo',
          'fotoForm.required' => 'Debe ingresar la foto',
          'fotoForm.image' => 'Debe ingresar el formato correcto de la imagen',
          'correo.unique' => 'El correo ya existe',
          'CorreoFrom.email' => 'Debe ingresar un correo valido'
        ];
      }else if($request['contrasenaForm']!=null or $request['contrasenaForm']!=''){
        $validationRules=[
          'nombreForm' => 'required',
          'CorreoFrom' => 'required|email',
          'correo' => 'unique:usuarios',
          'contrasenaForm' => 'required|min:8',
        ];
         $validationMessages = [
          'nombreForm.required' => 'Debe ingresar el nombre',
          'CorreoFrom.required' => 'Debe ingresar el correo',
          'correo.unique' => 'El correo ya existe',
          'CorreoFrom.email' => 'Debe ingresar un correo valido',
          'contrasenaForm.min' => 'Debe ingresar el minimos del password',
        ];
      }
      $v = Validator::make($request->all(), $validationRules,$validationMessages);
      if ($v->fails())
      {
        return redirect('/admin/usuario/'.$id.'/edit')->withInput()->withErrors($v);
      }else{
        $dataForm=user::find($id);
        $dataForm->correo=$request['CorreoFrom'];
        $dataForm->nombre=$request['nombreForm'];
        $dataForm->apellido=$request['ApellidoForm'];
        $dataForm->telefono_local=$request['telefono_fijoForm'];
        $dataForm->telefono_celular=$request['telefono_celularForm'];
      if($request['contrasenaForm']!=null or $request['contrasenaForm']!=''){
        $dataForm->contrasenia=$request['contrasenaForm'];
        $dataForm->contrasenia_nueva=$request['contrasena_nuevaForm'];
      }
      $dataForm->remember_token= $request['_token'];
      $dataForm->updated_at=date('Y-m-d H:i:s');
      $dataForm->created_at=date('Y-m-d H:i:s');
      $a = array("jpeg", "png","jpg","GIF","gif","tif");
      $mensaje="El registro se ha guardado de forma exitosa";
      $clase="callout callout-success";
      $descripcion=['mensaje'=>$mensaje,'clase'=>$clase];
      if($request['fotoForm']!=null or $request['fotoForm']!=''){
        $request['fotoForm']->store('public');
        $foto_cortar=explode('/',$request['fotoForm']->store('public'));
        $dataForm->foto='storage/'.$foto_cortar[1];
      }
      if($dataForm->save()){
        Session::put('usuario_id', $dataForm->id);
        Session::put('nombre', $dataForm->nombre);
        Session::put('correo', $dataForm->correo);
        Session::put('foto', $dataForm->foto);
        Session::put('es_agente', $dataForm->es_agente);
      }
      $mensaje="El registro se ha guardado de forma exitosa";
      $clase="callout callout-success";
    //return view('usuario.usuario_form_update')->with('descripcion',$descripcion);

    return redirect('/admin/usuario/');
        //
    }
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $idRequest=explode('-',$id);
        $dataForm=User::find($idRequest[0]);
        $dataForm->updated_at=date('Y-m-d H:i:s');
        if($idRequest[1]=="e"){
            $dataForm->estatus='E';    
        }else{
            $dataForm->estatus='A';   
        }
        
        //$dataForm->usuario_act_id=Session::get('usuario_id');
        if($dataForm->save()){
            $mensaje="El registro se ha guardado de forma exitosa";
            $clase="callout callout-success";
            $descripcion=['mensaje'=>$mensaje,'clase'=>$clase];
            echo json_encode(['statusCode'=>'Exito']);
            //return redirect('/admin/tipo/');
        }
    }
  }

