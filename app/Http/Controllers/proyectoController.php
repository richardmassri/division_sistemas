<?php

namespace laravelPrueba\Http\Controllers;

use Illuminate\Http\Request;
use laravelPrueba\Proyecto;
use laravelPrueba\Tipo;
use laravelPrueba\Agente;
use laravelPrueba\Caracteristica;
use laravelPrueba\Sector;
use laravelPrueba\Galeria;
use laravelPrueba\User;
use laravelPrueba\CaracteristicasProyecto;
use laravelPrueba\TiposProyectos;
use laravelPrueba\Operaciones;
use laravelPrueba\vistaPropiedad;
use laravelPrueba\vistaProyecto;
use laravelPrueba\Propiedad;
use laravelPrueba\Ciudad;
use laravelPrueba\Paise;
use laravelPrueba\ParametroSeguridades;
use Mail;
use Validator;
use laravelPrueba\uploadHandler;
use laravelPrueba\contactarAgente;
use DB;
use Session;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;


class ProyectoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $propiedaddesGaleria=DB::table('proyectos')->select('proyectos.*','galerias.*')->join('galerias','proyectos.id','=','galerias.proyecto_id')->where([
            ['proyectos.estatus','=','A']
            ])->get();
      $consultarProyecto=Proyecto::where('estatus', 'A')->get();
      $consultarGaleria=Galeria::where('estatus', 'A')->get();
      return view('proyecto.index')->with(['consultarProyecto'=> $consultarProyecto,'propiedaddesGaleria'=>$propiedaddesGaleria]);
        //
    }


    protected $validationRules=[
            'nombre' => 'required',
            //'logotipo' => 'required|image',
            'descripcion_breve' => 'required',
            'descripcion' => 'required',
            'ubicacion' => 'required',
            //'video' => 'required',
            'precio_desde' => 'required',
            'precio_hasta' => 'required',
            'fecha_inicial' => 'required',
            'fecha_final' => 'required',
            'estatus_invermaste' => 'required',
            'agente_id' => 'required',
            'sector_id' => 'required'
    ];

      protected $validationMessages = [

        'nombre.required' => 'Debe ingresar el nombre',
        //'logotipo.required' => 'Debe ingresar el nombre',
        'descripcion.required' => 'Debe ingresar la Descripción'

    ];

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $es_agente=Session::get('es_agente');
        $usuario_id=Session::get('usuario_id');
        $usuario=Session::get('nombre');
        $tipo=Tipo::where('estatus', 'A')->get();
        $agente=User::where([['estatus','=','A'],['es_agente','=','SI']])->get();
        $caracteristica=Caracteristica::where([['estatus','=', 'A'],['aplica_proyecto','=', 'SI']])->get();
        $sector=Sector::where('estatus', 'A')->get();
        $proyectos=Proyecto::where('estatus', 'A')->get();
        $sector=Sector::where('estatus', 'A')->get();
        $pais=Paise::where('estatus', 'A')->get();
        $ciudad=Ciudad::where('estatus', 'A')->get();
        return view('proyecto.proyecto_form_registro')->with(array('tipo'=>$tipo,'agentes'=>$agente,'caracteristica'=> $caracteristica,'sectores' => $sector,'proyecto'=>$proyectos,'pais'=>$pais,'ciudad'=>$ciudad,'es_agente'=>$es_agente,'usuario'=>$usuario,'usuario_id'=>$usuario_id));
        //
    }

        public function viewProyectoCliente(){
            //$id=$_REQUEST['id'];
            \Cache::put('20413975','20413975', 60);
            $where='';
            $tipoConsulta=Tipo::where('estatus', 'A')->get();
            $agenteConsulta=User::where('estatus', 'A')->get();
            $caracteristicaConsulta=Caracteristica::where('estatus', 'A')->get();
            $sectorConsulta=Sector::where('estatus', 'A')->get();
            $operaciones=Operaciones::where('estatus', 'A')->get();
            $ciudad=Ciudad::where('estatus', 'A')->get();
            $propiedad=Propiedad::where('estatus', 'A')->get();
            $parametro_seguridad=ParametroSeguridades::where('estatus', 'A')->get();
            $consultaMaximoPrecio=DB::table('propiedades')->select(DB::raw('MAX(propiedades.precio_en_dolares) AS precio_maximo'))->get();

            $consultaMinimoPrecio=DB::table('propiedades')->select(DB::raw('MIN(propiedades.precio_en_dolares) AS precio_minimo'))->get();

            $consultaEmpresa=DB::table('empresas')->select('empresas.*')->get();
            $galeriaEmpresa=DB::table('galerias_empresas')->select('galerias_empresas.*')->orderBy('destacada','DESC')->get();
            $operacionesPropiedad=DB::table('propiedades')->select('propiedades.id','operaciones_propiedades.propiedad_id as propiedad_id','operaciones.id','operaciones.nombre as nombre_operacion')->join('operaciones_propiedades','propiedades.id','=','operaciones_propiedades.propiedad_id')->join('operaciones','operaciones.id','=','operaciones_propiedades.operacion_id')->paginate(4);



            if($_POST){
                $consultaCantidadPropiedadesVistas=DB::table('vistas_propiedades')->select('vistas_propiedades.propiedad_id as propiedad_id')->where('estatus','=','A')->paginate(4);
                $tipo=$_REQUEST['tipo'];
                $estacionamoento=$_REQUEST['estacionamiento'];
                //$precio_desde=$_REQUEST['precio_desde'];
                $precio_desde=str_replace(",", "", $_REQUEST['precio_desde']);

                $precio_hasta=str_replace(",", "", $_REQUEST['precio_hasta']);
                $sector_ciudad_nombre=$_REQUEST['sector_ciudad_nombre'];
                $banio=$_REQUEST['banios'];
                $dormitorio=$_REQUEST['dormitorios'];
                $consultaPropiedades=[];
                $consultaProyecto=[];
                if($tipo==""){
                    $tipo=0;
                }
                if($banio==""){
                    $banio=0;
                }
                if($dormitorio==""){
                    $dormitorio=0;
                }
                if($estacionamoento==""){
                    $estacionamoento=0;
                }
                if($sector_ciudad_nombre==""){
                    $sector_ciudad_nombre='""';
                }
                $sqlPropiedades = \DB::select("CALL busqueda_avanzada($sector_ciudad_nombre,$dormitorio,$estacionamoento,$banio,$tipo,$precio_desde,$precio_hasta)");
                $currentPage = LengthAwarePaginator::resolveCurrentPage();
                $col = new Collection($sqlPropiedades);
                $perPage = 4;
                $currentPageSearchResults = $col->slice(($currentPage - 1) * $perPage, $perPage)->all();
                $consultaPropiedades = new LengthAwarePaginator($currentPageSearchResults, count($col), $perPage);
                //$consultaPropiedades['LengthAwarePaginator']="www";
                $consultaPropiedades->setPath('/public/invermaster/propiedad/general');
                
                
                if(count($consultaPropiedades)>0){
                   return view('plantillaCliente.plantillaPropiedadNueva')->with(['consultaProyecto'=>$consultaProyecto,'tipo'=>$tipoConsulta,'consultaPropiedades'=>$consultaPropiedades,'agente'=>$agenteConsulta,'sector'=>$sectorConsulta,'caracteristica'=>$caracteristicaConsulta,'operaciones'=>$operaciones,'ciudad'=>$ciudad,'propiedad'=>$propiedad,'consultaEmpresa'=>$consultaEmpresa,'consultaMaximoPrecio'=>$consultaMaximoPrecio,'consultaMinimoPrecio'=>$consultaMinimoPrecio,'operacionesPropiedad'=>$operacionesPropiedad,'consultaCantidadPropiedadesVistas'=>$consultaCantidadPropiedadesVistas]);
                }

     
            }else{
                $consultaProyecto=DB::table('proyectos')->select('proyectos.nombre as nombre_proyecto' ,'galerias.nombre as nombre_galeria', 'proyectos.id','proyectos.precio_desde_en_dolares as precio_desde','proyectos.precio_hasta_en_dolares as precio_hasta','proyectos.descripcion_breve', 'proyectos.logotipo')->join('galerias','proyectos.id','=','galerias.proyecto_id')->join('vistas_proyectos','vistas_proyectos.proyecto_id','=','proyectos.id')->groupBy('nombre_proyecto','galerias.nombre','proyectos.precio_desde_en_dolares','proyectos.precio_hasta_en_dolares','proyectos.precio_hasta','proyectos.descripcion_breve','proyectos.logotipo','proyectos.id')->where('promocional','SI')->orderBy(DB::raw('COUNT(proyectos.id)'),'DESC')->where('proyectos.estatus_invermaste','=','A')->limit(4)->get();


                $consultaPropiedades=DB::table('propiedades')->select('propiedades.nombre as nombre_propiedad' ,'galerias_propiedades.nombre as nombre_galeria', 'propiedades.id', 'propiedades.precio_en_dolares as precio_desde','propiedades.descripcion_breve','propiedades.metros_cuadrado', 'propiedades.dormitorio', 'propiedades.banios', 'propiedades.estacionamiento', 'propiedades.fecha_publicacion', 'propiedades.ubicacion')->join('vistas_propiedades','vistas_propiedades.propiedad_id','=','propiedades.id')->join('galerias_propiedades','propiedades.id','=','galerias_propiedades.propiedad_id')->groupBy('propiedades.nombre','galerias_propiedades.nombre','propiedades.precio_en_dolares','propiedades.id','propiedades.descripcion_breve','propiedades.metros_cuadrado','propiedades.dormitorio','propiedades.banios','propiedades.estacionamiento','propiedades.fecha_publicacion','propiedades.ubicacion')->where([
                    ['promocional','SI'],
                    ['propiedades.estatus_invermaste','=','A']])->orderBy(DB::raw('COUNT(propiedades.id)'),'DESC')->limit(4)->get();


                //dd($consultaPropiedades);
            }
            //dd($consultaPropiedades);
            //$proyecto=Proyecto::find();
            //$galeria=Galeria::all();
            // dd($proyecto->galeriaaaa->nombre);
            //dd($proyecto->galeria);


            return view('plantillaCliente.plantilla')->with(['consultaProyecto'=>$consultaProyecto,'tipo'=>$tipoConsulta,'consultaPropiedades'=>$consultaPropiedades,'agente'=>$agenteConsulta,'sector'=>$sectorConsulta,'caracteristica'=>$caracteristicaConsulta,'operaciones'=>$operaciones,'ciudad'=>$ciudad,'propiedad'=>$propiedad,'consultaMaximoPrecio'=>$consultaMaximoPrecio,'parametro_seguridad'=>$parametro_seguridad,'consultaEmpresa'=>$consultaEmpresa,'galeriaEmpresa'=>$galeriaEmpresa,'operacionesPropiedad'=>$operacionesPropiedad,'consultaMinimoPrecio'=>$consultaMinimoPrecio]);
        }

        public function salidar(){
            $a="Hola Mundo";
            return $a;
        }

        public function busqueda($tipo, $sector_ciudad_nombre, $dormitorio, $estacionamoento, $banio,$precio_desde,$precio_hasta){
            $consultaPropiedades="";
            if($tipo!='' and $sector_ciudad_nombre!='' and $dormitorio!='' and $estacionamoento!='' and $banio!=''){
                    $consultaPropiedades=DB::table('propiedades')->select('propiedades.nombre as nombre_propiedad' ,'galerias_propiedades.nombre as nombre_galeria', 'propiedades.fecha_publicacion','sectores.nombre as nombre_sector', 'propiedades.id', 'propiedades.precio as precio_desde','propiedades.descripcion_breve','propiedades.metros_cuadrado', 'propiedades.dormitorio', 'propiedades.banios', 'propiedades.estacionamiento', 'propiedades.ubicacion')->join('galerias_propiedades','propiedades.id','=','galerias_propiedades.propiedad_id')->join('sectores','propiedades.sector_id','=','sectores.id')->where([
                        //['propiedades.nombre','=',$nombre],
                        //['propiedades.tipo_id','=',$tipo],
                        ['propiedades.tipo_id','=',$tipo],
                        ['propiedades.dormitorio','=',$dormitorio],
                        ['propiedades.banios','=',$banio],
                        ['propiedades.estacionamiento','=',$estacionamoento],
                        ['propiedades.precio','>=',$precio_desde],
                        ['propiedades.precio','<=',$precio_hasta],
                        ['promocional','=','SI'],
                        ])->orWhere([
                        ['propiedades.nombre','like','%'.$sector_ciudad_nombre.'%'],
                        ['propiedades.precio','>=',$precio_desde],
                        ['propiedades.precio','<=',$precio_hasta],
                        ['propiedades.tipo_id','=',$tipo],
                        ['propiedades.dormitorio','=',$dormitorio],
                        ['propiedades.banios','=',$banio],
                        ['propiedades.estacionamiento','=',$estacionamoento],
                        ['promocional','=','SI']])->orWhere([

                        ['propiedades.sector_id','=',$sector_ciudad_nombre],
                        ['propiedades.precio','>=',$precio_desde],
                        ['propiedades.precio','<=',$precio_hasta],
                        ['propiedades.tipo_id','=',$tipo],
                        ['propiedades.dormitorio','=',$dormitorio],
                        ['propiedades.banios','=',$banio],
                        ['propiedades.estacionamiento','=',$estacionamoento],

                        ['promocional','=','SI']])->orWhere([
                        ['propiedades.ciudad_id','=',$sector_ciudad_nombre],
                        ['propiedades.precio','>=',$precio_desde],
                        ['propiedades.precio','<=',$precio_hasta],
                        ['propiedades.tipo_id','=',$tipo],
                        ['propiedades.dormitorio','=',$dormitorio],
                        ['propiedades.banios','=',$banio],
                        ['propiedades.estacionamiento','=',$estacionamoento],
                        ['promocional','=','SI']])->distinct()->paginate(4);
                        $consultaProyecto=[];
                }else if($tipo!='' and $sector_ciudad_nombre!='' and $dormitorio=='' and $estacionamoento=='' and $banio==''){
                    $consultaPropiedades=DB::table('propiedades')->select('propiedades.nombre as nombre_propiedad' ,'galerias_propiedades.nombre as nombre_galeria', 'propiedades.fecha_publicacion','sectores.nombre as nombre_sector', 'propiedades.id', 'propiedades.precio as precio_desde','propiedades.descripcion_breve','propiedades.metros_cuadrado', 'propiedades.dormitorio', 'propiedades.banios', 'propiedades.estacionamiento', 'propiedades.ubicacion')->join('galerias_propiedades','propiedades.id','=','galerias_propiedades.propiedad_id')->join('sectores','propiedades.sector_id','=','sectores.id')->where([
                        //['propiedades.nombre','=',$nombre],
                        //['propiedades.tipo_id','=',$tipo],
                        ['propiedades.tipo_id','=',$tipo],
                        ['propiedades.precio','>=',$precio_desde],
                        ['propiedades.precio','<=',$precio_hasta],
                        ['promocional','=','SI'],
                         ])->orWhere([
                        ['propiedades.nombre','like','%'.$sector_ciudad_nombre.'%'],
                        ['propiedades.precio','>=',$precio_desde],
                        ['propiedades.precio','<=',$precio_hasta],
                        ['propiedades.tipo_id','=',$tipo],
                        ['promocional','=','SI']])->orWhere([

                        ['propiedades.sector_id','=',$sector_ciudad_nombre],
                        ['propiedades.precio','>=',$precio_desde],
                        ['propiedades.precio','<=',$precio_hasta],
                        ['propiedades.tipo_id','=',$tipo],

                        ['promocional','=','SI']])->orWhere([
                        ['propiedades.ciudad_id','=',$sector_ciudad_nombre],
                        ['propiedades.precio','>=',$precio_desde],
                        ['propiedades.precio','<=',$precio_hasta],
                        ['propiedades.tipo_id','=',$tipo],
                        ['promocional','=','SI']])->distinct()->paginate(4);
                        $consultaProyecto=[];
                }else if($tipo!='' and $sector_ciudad_nombre!='' and $dormitorio!='' and $estacionamoento=='' and $banio==''){
                    $consultaPropiedades=DB::table('propiedades')->select('propiedades.nombre as nombre_propiedad' ,'galerias_propiedades.nombre as nombre_galeria', 'propiedades.fecha_publicacion','sectores.nombre as nombre_sector', 'propiedades.id', 'propiedades.precio as precio_desde','propiedades.descripcion_breve','propiedades.metros_cuadrado', 'propiedades.dormitorio', 'propiedades.banios', 'propiedades.estacionamiento', 'propiedades.ubicacion')->join('galerias_propiedades','propiedades.id','=','galerias_propiedades.propiedad_id')->join('sectores','propiedades.sector_id','=','sectores.id')->where([
                        //['propiedades.nombre','=',$nombre],
                        //['propiedades.tipo_id','=',$tipo],
                        ['propiedades.tipo_id','=',$tipo],
                        ['propiedades.dormitorio','=',$dormitorio],
                        ['propiedades.precio','>=',$precio_desde],
                        ['propiedades.precio','<=',$precio_hasta],
                        ['promocional','=','SI']])->distinct()->paginate(4);
                        $consultaProyecto=[];

                }else if($tipo!='' and $sector_ciudad_nombre!='' and $dormitorio!='' and $estacionamoento=='' and $banio!=''){
                    $consultaPropiedades=DB::table('propiedades')->select('propiedades.nombre as nombre_propiedad' ,'galerias_propiedades.nombre as nombre_galeria', 'propiedades.fecha_publicacion','sectores.nombre as nombre_sector', 'propiedades.id', 'propiedades.precio as precio_desde','propiedades.descripcion_breve','propiedades.metros_cuadrado', 'propiedades.dormitorio', 'propiedades.banios', 'propiedades.estacionamiento', 'propiedades.ubicacion')->join('galerias_propiedades','propiedades.id','=','galerias_propiedades.propiedad_id')->join('sectores','propiedades.sector_id','=','sectores.id')->where([
                        //['propiedades.nombre','=',$nombre],
                        //['propiedades.tipo_id','=',$tipo],
                        ['propiedades.tipo_id','=',$tipo],
                        ['propiedades.dormitorio','=',$dormitorio],
                        ['propiedades.banios','=',$banio],
                        ['propiedades.precio','>=',$precio_desde],
                        ['propiedades.precio','<=',$precio_hasta],
                        ['promocional','=','SI']])->distinct()->paginate(4);
                        $consultaProyecto=[];
                }else if($tipo!='' and $sector_ciudad_nombre!='' and $dormitorio!='' and $estacionamoento!='' and $banio==''){
                    $consultaPropiedades=DB::table('propiedades')->select('propiedades.nombre as nombre_propiedad' ,'galerias_propiedades.nombre as nombre_galeria', 'propiedades.fecha_publicacion','sectores.nombre as nombre_sector', 'propiedades.id', 'propiedades.precio as precio_desde','propiedades.descripcion_breve','propiedades.metros_cuadrado', 'propiedades.dormitorio', 'propiedades.banios', 'propiedades.estacionamiento', 'propiedades.ubicacion')->join('galerias_propiedades','propiedades.id','=','galerias_propiedades.propiedad_id')->join('sectores','propiedades.sector_id','=','sectores.id')->where([
                        //['propiedades.nombre','=',$nombre],
                        //['propiedades.tipo_id','=',$tipo],
                        ['propiedades.tipo_id','=',$tipo],
                        ['propiedades.dormitorio','=',$dormitorio],
                        ['propiedades.estacionamiento','=',$estacionamoento],
                        ['propiedades.precio','>=',$precio_desde],
                        ['propiedades.precio','<=',$precio_hasta],
                        ['promocional','=','SI']])->distinct()->paginate(4);
                        $consultaProyecto=[];
                //dd($_REQUEST);
                }else if($tipo=='' and $estacionamoento=='' and $sector_ciudad_nombre=='' and $banio=='' and $dormitorio==''){
                    $consultaPropiedades=DB::table('propiedades')->select('propiedades.nombre as nombre_propiedad' ,'galerias_propiedades.nombre as nombre_galeria', 'propiedades.fecha_publicacion','sectores.nombre as nombre_sector', 'propiedades.id', 'propiedades.precio as precio_desde','propiedades.descripcion_breve','propiedades.metros_cuadrado', 'propiedades.dormitorio', 'propiedades.banios', 'propiedades.estacionamiento', 'propiedades.ubicacion')->join('galerias_propiedades','propiedades.id','=','galerias_propiedades.propiedad_id')->join('sectores','propiedades.sector_id','=','sectores.id')->join('tipos','tipos.id','=','propiedades.tipo_id')->where([
                        ['propiedades.precio','>=',$precio_desde],
                        ['promocional','=','SI']])->paginate(4);

                   /* $consultaProyecto=DB::table('proyectos')->select('proyectos.nombre as nombre_proyecto' ,'galerias.nombre as nombre_galeria', 'galerias.id','proyectos.precio_desde','proyectos.precio_hasta','proyectos.descripcion_breve','proyectos.logotipo')->join('galerias','proyectos.id','=','galerias.proyecto_id')->where([
                        ['proyectos.precio_desde','>=',$precio_desde],
                        ['proyectos.precio_hasta','>=',$precio_hasta]])->get();*/
                        $consultaProyecto=[];

                        //dd($consultaProyecto);

                }else if($tipo!='' and $estacionamoento=='' and $sector_ciudad_nombre=='' and $banio=='' and $dormitorio==''){
                    $consultaPropiedades=DB::table('propiedades')->select('propiedades.nombre as nombre_propiedad' ,'galerias_propiedades.nombre as nombre_galeria', 'propiedades.fecha_publicacion','sectores.nombre as nombre_sector', 'propiedades.id', 'propiedades.precio as precio_desde','propiedades.precio_en_dolares','propiedades.descripcion_breve','propiedades.metros_cuadrado', 'propiedades.dormitorio', 'propiedades.banios', 'propiedades.estacionamiento', 'propiedades.ubicacion')->join('galerias_propiedades','propiedades.id','=','galerias_propiedades.propiedad_id')->join('sectores','propiedades.sector_id','=','sectores.id')->leftJoin('tipos','tipos.id','=','propiedades.tipo_id')->where([
                        //['propiedades.nombre','=',$nombre],
                        ['propiedades.tipo_id','=',$tipo],
                        ['propiedades.precio','>=',$precio_desde],
                        ['propiedades.precio','<=',$precio_hasta],
                        ['promocional','=','SI']
                        ])->paginate(4);
                        /*$consultaProyecto=DB::table('proyectos')->select('proyectos.nombre as nombre_proyecto' ,'galerias.nombre as nombre_galeria', 'galerias.id','proyectos.precio_desde','proyectos.precio_hasta','proyectos.descripcion_breve','proyectos.logotipo')->join('galerias','proyectos.id','=','galerias.proyecto_id')->where([
                        ['proyectos.nombre','=',$nombre],
                        ['proyectos.tipo_id','=',$tipo],
                        ['proyectos.precio_desde','>=',$precio_desde],
                        ['proyectos.precio_hasta','>=',$precio_hasta]])->get();*/
                        $consultaProyecto=[];

                }else if($tipo=='' and $sector_ciudad_nombre=='' and $banio=='' and $dormitorio=='' and $estacionamoento!=''){
                    $consultaPropiedades=DB::table('propiedades')->select('propiedades.nombre as nombre_propiedad' ,'galerias_propiedades.nombre as nombre_galeria', 'propiedades.fecha_publicacion','sectores.nombre as nombre_sector', 'propiedades.id', 'propiedades.precio as precio_desde','propiedades.descripcion_breve','propiedades.metros_cuadrado', 'propiedades.dormitorio', 'propiedades.banios', 'propiedades.estacionamiento', 'propiedades.ubicacion')->join('galerias_propiedades','propiedades.id','=','galerias_propiedades.propiedad_id')->join('sectores','propiedades.sector_id','=','sectores.id')->where([
                        //['propiedades.nombre','=',$nombre],
                        //['propiedades.tipo_id','=',$tipo],
                        ['propiedades.estacionamiento','=',$estacionamoento],
                        ['propiedades.precio','>=',$precio_desde],
                        ['propiedades.precio','<=',$precio_hasta],
                        ['promocional','=','SI']
                        ])->paginate(4);

                        $consultaProyecto=[];

                }else if($tipo=='' and $estacionamoento=='' and  $banio=='' and $dormitorio=='' and $sector_ciudad_nombre!=''){
                    $consultaPropiedades=DB::table('propiedades')->select('propiedades.nombre as nombre_propiedad' ,'galerias_propiedades.nombre as nombre_galeria', 'propiedades.fecha_publicacion','sectores.nombre as nombre_sector', 'propiedades.id', 'propiedades.precio as precio_desde','propiedades.descripcion_breve','propiedades.metros_cuadrado', 'propiedades.dormitorio', 'propiedades.banios', 'propiedades.estacionamiento', 'propiedades.ubicacion')->join('galerias_propiedades','propiedades.id','=','galerias_propiedades.propiedad_id')->join('sectores','propiedades.sector_id','=','sectores.id')->where([
                        //['propiedades.nombre','=',$nombre],
                        //['propiedades.tipo_id','=',$tipo],
                        ['propiedades.precio','>=',$precio_desde],
                        ['propiedades.precio','<=',$precio_hasta],
                        ['promocional','=','SI'],
                        ])->orWhere([
                        ['propiedades.nombre','like','%'.$sector_ciudad_nombre.'%'],
                        ['propiedades.precio','>=',$precio_desde],
                        ['propiedades.precio','<=',$precio_hasta],
                        ['promocional','=','SI']])->orWhere([
                        ['propiedades.sector_id','=',$sector_ciudad_nombre],
                        ['propiedades.precio','>=',$precio_desde],
                        ['propiedades.precio','<=',$precio_hasta],
                        ['promocional','=','SI']])->orWhere([
                        ['propiedades.ciudad_id','=',$sector_ciudad_nombre],
                        ['propiedades.precio','>=',$precio_desde],
                        ['propiedades.precio','<=',$precio_hasta],
                        ['promocional','=','SI']])->paginate(4);
                        $consultaProyecto=[];
                }else if($tipo=='' and $sector_ciudad_nombre=='' and $banio=='' and $estacionamoento=='' and $dormitorio!=''){
                    $consultaPropiedades=DB::table('propiedades')->select('propiedades.nombre as nombre_propiedad' ,'galerias_propiedades.nombre as nombre_galeria', 'propiedades.fecha_publicacion','sectores.nombre as nombre_sector', 'propiedades.id', 'propiedades.precio as precio_desde','propiedades.descripcion_breve','propiedades.metros_cuadrado', 'propiedades.dormitorio', 'propiedades.banios', 'propiedades.estacionamiento', 'propiedades.ubicacion')->join('galerias_propiedades','propiedades.id','=','galerias_propiedades.propiedad_id')->join('sectores','propiedades.sector_id','=','sectores.id')->where([
                        //['propiedades.nombre','=',$nombre],
                        //['propiedades.tipo_id','=',$tipo],
                        ['propiedades.dormitorio','=',$dormitorio],
                        ['propiedades.precio','>=',$precio_desde],
                        ['propiedades.precio','<=',$precio_hasta],
                        ['promocional','=','SI']
                        ])->paginate(4);
                        $consultaProyecto=[];
                }else if($tipo=='' and $sector_ciudad_nombre=='' and $dormitorio=='' and $estacionamoento==''  and $banio!=''){
                    $consultaPropiedades=DB::table('propiedades')->select('propiedades.nombre as nombre_propiedad' ,'galerias_propiedades.nombre as nombre_galeria', 'propiedades.fecha_publicacion','sectores.nombre as nombre_sector', 'propiedades.id', 'propiedades.precio as precio_desde','propiedades.descripcion_breve','propiedades.metros_cuadrado', 'propiedades.dormitorio', 'propiedades.banios', 'propiedades.estacionamiento', 'propiedades.ubicacion')->join('galerias_propiedades','propiedades.id','=','galerias_propiedades.propiedad_id')->join('sectores','propiedades.sector_id','=','sectores.id')->where([
                        //['propiedades.nombre','=',$nombre],
                        //['propiedades.tipo_id','=',$tipo],
                        ['propiedades.banios','=',$banio],
                        ['propiedades.precio','>=',$precio_desde],
                        ['propiedades.precio','<=',$precio_hasta],
                        ['promocional','=','SI']
                        ])->paginate(4);
                        $consultaProyecto=[];

                }else if($sector_ciudad_nombre!='' and $tipo!='' and $estacionamoento!=''){
                    $consultaPropiedades=DB::table('propiedades')->select('propiedades.nombre as nombre_propiedad' ,'galerias_propiedades.nombre as nombre_galeria', 'propiedades.fecha_publicacion','sectores.nombre as nombre_sector', 'propiedades.id', 'propiedades.precio as precio_desde','propiedades.descripcion_breve','propiedades.metros_cuadrado', 'propiedades.dormitorio', 'propiedades.banios', 'propiedades.estacionamiento', 'propiedades.ubicacion')->join('galerias_propiedades','propiedades.id','=','galerias_propiedades.propiedad_id')->join('sectores','propiedades.sector_id','=','sectores.id')->where([
                        ['propiedades.nombre','=',$nombre],
                        ['propiedades.tipo_id','=',$tipo],
                        ['propiedades.estacionamiento','=',$estacionamoento],
                        ['propiedades.precio','>=',$precio_desde],
                        ['propiedades.precio','<=',$precio_hasta],
                        ['promocional','=','SI']
                        ])->paginate(4);

                        $consultaProyecto=[];

                }
                return $consultaPropiedades;
        }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $es_agente=Session::get('es_agente');
        $usuario_id=Session::get('usuario_id');
        $usuario=Session::get('nombre');

      $v = Validator::make($request->all(), $this->validationRules,$this->validationMessages);
      if ($v->fails())
      {
        return redirect('admin/proyecto/create')->withInput()->withErrors($v);
      // The given data did not pass validation
      }else{
        $precio_desde=str_replace(".", "", $request['precio_desde']);
        $precio_desde=str_replace(",", ".", $precio_desde);
        $precio_hasta=str_replace(".", "", $request['precio_hasta']);
        $precio_hasta=str_replace(",", ".", $precio_hasta);

        $precio_desde=str_replace("USD ", "", $precio_desde);
        $precio_hasta=str_replace("USD ", "", $precio_hasta);


        $precio_desde_dolares=str_replace(".", "", $request['precio_desde_en_dolares']);
        $precio_desde_dolares=str_replace(",", ".", $precio_desde_dolares);
        $precio_desde_dolares=str_replace("USD ", "", $precio_desde_dolares);
        $precio_hasta_dolares=str_replace(".", "", $request['precio_hasta_en_dolares']);
        $precio_hasta_dolares=str_replace(",", ".", $precio_hasta_dolares);

        $precio_hasta_dolares=str_replace("USD ", "", $precio_hasta_dolares);

        $request['precio_desde']=$precio_desde;
        $request['precio_hasta']=$precio_hasta;

        $request['precio_desde_en_dolares']=$precio_desde_dolares;
        $request['precio_hasta_en_dolares']=$precio_hasta_dolares;
        $proyecto = new Proyecto($request->all());
        //$proyecto->logotipo='storage/'.$request['logotipo']->getClientOriginalName();
        //dd($request['logotipo']);
        if($request['logotipo']!=null){
            $request['logotipo']->store('public');
            $foto_cortar=explode('/',$request['logotipo']->store('public'));
            $proyecto->logotipo='storage/'.$foto_cortar[1];
        }else{
            $proyecto->logotipo='';
        }
        $proyecto->fecha_publicacion = date("Y-m-d", strtotime($request['fecha_publicacion']));
        $proyecto->fecha_inicial = date("Y-m-d", strtotime($request['fecha_inicial']));
        $proyecto->fecha_final = date("Y-m-d", strtotime($request['fecha_final']));
        $proyecto->estatus='A';
        $proyecto->usuario_ini_id=1;
        $proyecto->usuario_act_id=1;
        $proyecto->updated_at=date('Y-m-d H:i:s');
        $proyecto->created_at=date('Y-m-d H:i:s');

        //$proyecto->precio_desde=$precio_desde;
        //$proyecto->precio_hasta=$precio_hasta;
        //dd($request['logotipo']);
        $proyecto->save();

        foreach ($request['tipo'] as $key => $value){
            $TiposProyectos = new TiposProyectos();
            $TiposProyectos->proyecto_id = $proyecto->id;
            $TiposProyectos->tipo_id=$value;
            $TiposProyectos->updated_at=date('Y-m-d H:i:s');
            $TiposProyectos->created_at=date('Y-m-d H:i:s');
            $TiposProyectos->estatus='A';
            $TiposProyectos->nombre='';
            $TiposProyectos->save();
        }

        foreach ($request['caracteristica'] as $key => $value){
            $caracteristicae_proyecto = new CaracteristicasProyecto();
            $caracteristicae_proyecto->proyecto_id = $proyecto->id;
            $caracteristicae_proyecto->caracteristica_id=$value;
            $caracteristicae_proyecto->updated_at=date('Y-m-d H:i:s');
            $caracteristicae_proyecto->created_at=date('Y-m-d H:i:s');
            $caracteristicae_proyecto->estatus='A';
            $caracteristicae_proyecto->nombre='';
            $caracteristicae_proyecto->save();
        }
        //dd($request['logotipo']);
        //$request['logotipo']->getClientOriginalName());
        $mensaje="Se ha agregado con éxito el proyecto ".$proyecto->nombre;
        $clase="callout callout-success";
        $descripcion=['mensaje'=>$mensaje,'clase'=>$clase];
        //$request['logotipo']->store('public');
        $tipo=Tipo::where('estatus', 'A')->get();
        $agente=User::where('estatus', 'A')->get();
        $caracteristica=Caracteristica::where('estatus', 'A')->get();
        $sector=Sector::where('estatus', 'A')->get();
        $proyecto=Proyecto::where('estatus', 'A')->get();
        $pais=Paise::where('estatus', 'A')->get();
        $ciudad=Ciudad::where('estatus', 'A')->get();
        return view('proyecto.proyecto_form_registro')->with(array('descripcion'=>$descripcion,'tipo'=>$tipo,'agentes'=>$agente,'caracteristica'=> $caracteristica,'sectores' => $sector,'proyecto'=>$proyecto,'pais'=>$pais,'ciudad'=>$ciudad,'es_agente'=>$es_agente,'usuario'=>$usuario,'usuario_id'=>$usuario_id));

        //$foto_cortar=explode('/',$request['logotipo']->store('public'));
        //
    }
}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $dataForm=Proyecto::find($id);
        $tipo=Tipo::where('estatus', 'A')->get();
        $agente=User::where('estatus', 'A')->get();
        $caracteristica=Caracteristica::where('estatus', 'A')->get();
        $sector=Sector::where('estatus', 'A')->get();
        $pais=Paise::where('estatus', 'A')->get();
        $ciudad=Ciudad::where('estatus', 'A')->get();
        $consultaproyectosCaracteristicas=DB::table('proyectos')->select('proyectos.*','caracteristicas_proyecto.*')->join('caracteristicas_proyecto','proyectos.id','=','caracteristicas_proyecto.proyecto_id')->where([
            ['proyectos.estatus','=','A'],
            ['proyectos.estatus_invermaste','=','A'],
            ['proyectos.id','=',$id],
            ])->get();
        $consultaproyectosTipos=DB::table('proyectos')->select('proyectos.*','tipos_proyecto.*')->join('tipos_proyecto','proyectos.id','=','tipos_proyecto.proyecto_id')->where([
            ['proyectos.estatus','=','A'],
            ['proyectos.id','=',$id],
            ['proyectos.estatus_invermaste','=','A'],
            ])->get();
        //dd($consultaproyectosCaracteristicas);
      //dd($dataForm);
      return view('proyecto.proyecto_form_view')->with(['dataForm'=> $dataForm,'tipo'=>$tipo,'agentes'=>$agente,'caracteristica'=>$caracteristica,'sector'=>$sector,'pais'=>$pais,'ciudad'=>$ciudad,'consultaproyectosCaracteristicas'=>$consultaproyectosCaracteristicas,'consultaproyectosTipos'=>$consultaproyectosTipos]);
    }

public function detaleProyecto($id){
    $vistaProyecto= new vistaProyecto();
    $vistaProyecto->proyecto_id=$id;
    $vistaProyecto->updated_at=date('Y-m-d H:i:s');
    $vistaProyecto->created_at=date('Y-m-d H:i:s');
    $vistaProyecto->estatus='A';
    $vistaProyecto->save();
    $tipoConsulta=Tipo::where('estatus', 'A')->get();
    $agenteConsulta=User::where('estatus', 'A')->get();
    $caracteristicaConsulta=Caracteristica::where('estatus', 'A')->get();
    $operaciones=Operaciones::where('estatus', 'A')->get();
    $sectorConsulta=Sector::where('estatus', 'A')->get();
    $ciudad=Ciudad::where('estatus', 'A')->get();
    $consultaMaximoPrecio=DB::table('proyectos')->select(DB::raw('MAX(proyectos.precio_hasta_en_dolares) AS precio_maximo'))->get();
    $consultaMinimoPrecio=DB::table('proyectos')->select(DB::raw('MIN(proyectos.precio_desde_en_dolares) AS precio_minimo'))->get();
    $consultaEmpresa=DB::table('empresas')->select('empresas.*')->get();
    $consultaProyectoPrincipal=DB::table('proyectos')->select('proyectos.nombre as nombre_proyecto','galerias.nombre as nombre_galeria', 'proyectos.id as proyecto_id','usuarios.nombre as nombre_usuario','usuarios.apellido as apellido_usuario','usuarios.telefono_local','proyectos.sector_id as sector_id','usuarios.telefono_celular','usuarios.correo','usuarios.foto','paises.nombre as nombre_pais', 'ciudades.nombre as nombre_ciudad','sectores.nombre as nombre_sector', 'galerias.id','proyectos.precio_desde_en_dolares as precio_desde','proyectos.latitud','proyectos.longitud','proyectos.precio_hasta_en_dolares as precio_hasta','proyectos.ubicacion','proyectos.fecha_publicacion','proyectos.construccion','proyectos.fecha_inicial','proyectos.fecha_final','proyectos.descripcion_breve','sectores.nombre as nombre_sector','proyectos.video','proyectos.descripcion','proyectos.logotipo')->join('galerias','proyectos.id','=','galerias.proyecto_id')->join('sectores','proyectos.sector_id','=','sectores.id')->join('paises','proyectos.pais_id','=','paises.id')->join('ciudades','proyectos.ciudad_id','=','ciudades.id')->join('usuarios','proyectos.agente_id','=','usuarios.id')->
        where([
        ['proyectos.id','=',$id],
        ['destacada','=','SI'],
        ['proyectos.estatus_invermaste','=','A']
        ])
        ->get();

        if($consultaProyectoPrincipal[0]->sector_id){
            $sector_id=$consultaProyectoPrincipal[0]->sector_id;
        }

    $consultaProyectoAll=DB::table('proyectos')->select('proyectos.nombre as nombre_proyecto' ,'galerias.nombre as nombre_galeria', 'galerias.id','proyectos.precio_desde_en_dolares as precio_desde','proyectos.precio_hasta_en_dolares as precio_hasta','proyectos.descripcion_breve','proyectos.descripcion','proyectos.video','proyectos.logotipo')->join('galerias','proyectos.id','=','galerias.proyecto_id')->where([
        ['proyectos.id','=',$id],
        ['proyectos.estatus_invermaste','=','A']])->get();

    $consultaCaracteristicaProyecto=DB::table('proyectos')->select('proyectos.nombre as nombre_proyecto' ,'caracteristicas.nombre as nombre_caracteristica','caracteristicas.icono')->join('caracteristicas_proyecto','proyectos.id','=','caracteristicas_proyecto.proyecto_id')->join('caracteristicas','caracteristicas.id','=','caracteristicas_proyecto.caracteristica_id')->where(
        [
        ['proyectos.id','=',$id],
        ['proyectos.estatus_invermaste','=','A']
        ])->get();

    $consultaTipoProyecto=DB::table('proyectos')->select('tipos.nombre as nombre_tipo')->join('tipos_proyecto','proyectos.id','=','tipos_proyecto.proyecto_id')->join('tipos','tipos.id','=','tipos_proyecto.tipo_id')->where(
        [
        ['proyectos.id','=',$id],['proyectos.estatus_invermaste','=','A']])->get();
   $consultaProyectoAllDestacadaSector= \DB::select("CALL proyectos_recientes('$sector_id')");

    $consultaCantidadProyectosVistos=DB::table('vistas_proyectos')->select(DB::raw('COUNT(vistas_proyectos.proyecto_id) AS cantidad_vistas'),'vistas_proyectos.proyecto_id as proyecto_id')->groupBy('vistas_proyectos.proyecto_id')->where([
        ['estatus','=','A'],
        ['vistas_proyectos.proyecto_id','=',$id]
        ])->get();

    $consultaCantidadProyectosVistosContactados=DB::table('contactar_agentes')->select(DB::raw('COUNT(contactar_agentes.proyecto_id) AS cantidad_vistas'),'contactar_agentes.proyecto_id as proyecto_id')->groupBy('contactar_agentes.proyecto_id')->where([
        ['estatus','=','A'],
        ['contactar_agentes.proyecto_id','=',$id]
        ])->get();
    /*$consultaProyectoAllDestacadaSector=DB::table('proyectos')->select('proyectos.nombre as nombre_proyecto' ,'galerias.nombre as nombre_galeria', 'galerias.id','proyectos.precio_desde','proyectos.precio_hasta','proyectos.descripcion_breve','proyectos.descripcion','proyectos.video','proyectos.logotipo')->join('galerias','proyectos.id','=','galerias.proyecto_id')->join('vistas_proyectos','vistas_proyectos.proyecto_id','=','proyectos.id')->where([
           ['proyectos.sector_id','=',$sector_id],
           ['destacada','=','SI']
           ])->groupBy('proyectos.nombre','galerias.nombre','galerias.id','proyectos.precio_desde','proyectos.precio_hasta','proyectos.descripcion_breve','proyectos.descripcion','proyectos.video','proyectos.logotipo')->orderBy('vistas_proyectos.created_at','ASC')->limit(3)->get();*/

    return view('plantillaCliente.plantillaProyecto')->with(array('consultaProyectoPrincipal'=>$consultaProyectoPrincipal,'consultaProyectoAll'=>$consultaProyectoAll,'tipo'=>$tipoConsulta,'agente'=>$agenteConsulta,'sector'=>$sectorConsulta,'caracteristica'=>$caracteristicaConsulta,'operaciones'=>$operaciones,'ciudad'=>$ciudad,'consultaCaracteristicaProyecto'=>$consultaCaracteristicaProyecto,'consultaTipoProyecto'=>$consultaTipoProyecto,'consultaMaximoPrecio'=>$consultaMaximoPrecio,'consultaMinimoPrecio'=>$consultaMinimoPrecio,'consultaEmpresa'=>$consultaEmpresa,'consultaProyectoAllDestacadaSector'=>$consultaProyectoAllDestacadaSector,'consultaCantidadProyectosVistos'=>$consultaCantidadProyectosVistos,'consultaCantidadProyectosVistosContactados'=>$consultaCantidadProyectosVistosContactados));

    //

 }

  public function detallePropiedad($id){
    $nombre_sector="";
    $tipoConsulta=Tipo::where('estatus', 'A')->get();
    $vistaPropiedad= new vistaPropiedad();
    $vistaPropiedad->propiedad_id=$id;
    $vistaPropiedad->updated_at=date('Y-m-d H:i:s');
    $vistaPropiedad->created_at=date('Y-m-d H:i:s');
    $vistaPropiedad->estatus='A';
    $vistaPropiedad->save();
    $agenteConsulta=User::where('estatus', 'A')->get();
    $caracteristicaConsulta=Caracteristica::where('estatus', 'A')->get();
    $sectorConsulta=Sector::where('estatus', 'A')->get();
    $operaciones=Operaciones::where('estatus', 'A')->get();
    $ciudad=Ciudad::where('estatus', 'A')->get();
    $propiedad=Propiedad::where('estatus', 'A')->get();
    $consultaEmpresa=DB::table('empresas')->select('empresas.*')->get();
    $consultaMaximoPrecio=DB::table('propiedades')->select(DB::raw('MAX(propiedades.precio_en_dolares) AS precio_maximo'))->get();
    $consultaPropiedadPrincipal=DB::table('propiedades')->select('propiedades.nombre as nombre_proyecto' , 'sectores.id as sector_id', 'galerias_propiedades.nombre as nombre_galeria', 'propiedades.id', 'propiedades.latitud','propiedades.longitud', 'paises.nombre as nombre_pais','usuarios.nombre as nombre_usuario','usuarios.apellido as apellido_usuario','usuarios.telefono_local','usuarios.telefono_celular','usuarios.correo','usuarios.foto','paises.nombre as nombre_pais','ciudades.nombre as nombre_ciudad','sectores.nombre as nombre_sector','galerias_propiedades.id as galeria_id','propiedades.precio_en_dolares as precio','propiedades.ubicacion','propiedades.fecha_publicacion','propiedades.construccion','propiedades.fecha_inicial','propiedades.fecha_final','propiedades.descripcion_breve','tipos.nombre as nombre_tipo','sectores.nombre as nombre_sector','propiedades.video','propiedades.descripcion')->join('galerias_propiedades','propiedades.id','=','galerias_propiedades.propiedad_id')->join('tipos','propiedades.tipo_id','=','tipos.id')->join('sectores','propiedades.sector_id','=','sectores.id')->join('paises','propiedades.pais_id','=','paises.id')->join('ciudades','propiedades.ciudad_id','=','ciudades.id')->join('usuarios','propiedades.agente_id','=','usuarios.id')->
        where([
        ['propiedades.id','=',$id],
        ['destacada','=','SI'],
        ['propiedades.estatus_invermaste','=','A']
        ])->get();
        if($consultaPropiedadPrincipal[0]->sector_id){
            $sector_id=$consultaPropiedadPrincipal[0]->sector_id;
        }

    $consultaMinimoPrecio=DB::table('propiedades')->select(DB::raw('MIN(propiedades.precio_en_dolares) AS precio_minimo'))->get();

    $consultaPropiedadAll=DB::table('propiedades')->select('propiedades.nombre as nombre_proyecto' ,'galerias_propiedades.nombre as nombre_galeria', 'galerias_propiedades.id as galeria_id','propiedades.precio_en_dolares as precio','propiedades.descripcion_breve','propiedades.descripcion','propiedades.video')->join('galerias_propiedades','propiedades.id','=','galerias_propiedades.propiedad_id')->where([
        ['propiedades.id','=',$id],
        ['propiedades.estatus_invermaste','=','A']])->get();

       /*$consultaPropiedadAllDestacadaSector=DB::table('propiedades')->select('propiedades.nombre as nombre_proyecto' ,'galerias_propiedades.nombre as nombre_galeria','propiedades.fecha_publicacion', 'galerias_propiedades.id as galeria_id','propiedades.precio','propiedades.precio','propiedades.descripcion_breve','propiedades.descripcion','propiedades.video')->join('galerias_propiedades','propiedades.id','=','galerias_propiedades.propiedad_id')->where([
           ['propiedades.sector_id','=',$sector_id],
           ['destacada','=','SI']
           ])->limit(3)->get();*/
        $consultaPropiedadAllDestacadaSector= \DB::select("CALL propiedades_recientes('$sector_id')");


    $consultaCaracteristicaPropiedad=DB::table('propiedades')->select('propiedades.nombre as nombre_proyecto' ,'caracteristicas.nombre as nombre_caracteristica','caracteristicas.icono')->join('caracteristicas_propiedades','propiedades.id','=','caracteristicas_propiedades.propiedad_id')->join('caracteristicas','caracteristicas.id','=','caracteristicas_propiedades.caracteristica_id')->where([
        ['propiedades.id','=',$id],
        ['propiedades.estatus_invermaste','=','A']])->get();

    $consultaOperacionesPropiedad=DB::table('propiedades')->select('propiedades.nombre as nombre_propiedad' ,'operaciones.nombre as nombre_operacion')->join('operaciones_propiedades','propiedades.id','=','operaciones_propiedades.propiedad_id')->join('operaciones','operaciones.id','=','operaciones_propiedades.operacion_id')->where([
        ['propiedades.id','=',$id],
        ['propiedades.estatus_invermaste','=','A']])->get();


    $consultaCantidadPropiedadesVistas=DB::table('vistas_propiedades')->select(DB::raw('COUNT(vistas_propiedades.propiedad_id) AS cantidad_vistas'),'vistas_propiedades.propiedad_id as propiedad_id')->groupBy('vistas_propiedades.propiedad_id')->where([
        ['estatus','=','A'],
        ['vistas_propiedades.propiedad_id','=',$id]
        ])->get();

    $consultaCantidadPropiedadesVistasContactadas=DB::table('contactar_agentes')->select(DB::raw('COUNT(contactar_agentes.propiedad_id) AS cantidad_vistas'),'contactar_agentes.propiedad_id as propiedad_id')->groupBy('contactar_agentes.propiedad_id')->where([
        ['estatus','=','A'],
        ['contactar_agentes.propiedad_id','=',$id]
        ])->get();
    $parametro_seguridad=ParametroSeguridades::where('estatus', 'A')->get();

    //dd($consultaCaracteristicaPropiedad);

    return view('plantillaCliente.plantillaPropiedad')->with(array('consultaPropiedadPrincipal'=>$consultaPropiedadPrincipal,'consultaPropiedadAll'=>$consultaPropiedadAll,'tipo'=>$tipoConsulta,'agente'=>$agenteConsulta,'sector'=>$sectorConsulta,'caracteristica'=>$caracteristicaConsulta,'consultaCaracteristicaPropiedad'=>$consultaCaracteristicaPropiedad,'operaciones'=>$operaciones,'ciudad'=>$ciudad,'propiedad'=>$propiedad,'consultaMaximoPrecio'=>$consultaMaximoPrecio,'consultaEmpresa'=>$consultaEmpresa,'consultaPropiedadAllDestacadaSector'=>$consultaPropiedadAllDestacadaSector,'consultaCantidadPropiedadesVistas'=>$consultaCantidadPropiedadesVistas,'consultaMinimoPrecio'=>$consultaMinimoPrecio,'parametro_seguridad'=>$parametro_seguridad,'consultaCantidadPropiedadesVistasContactadas'=>$consultaCantidadPropiedadesVistasContactadas,'consultaOperacionesPropiedad'=>$consultaOperacionesPropiedad));


        //
    }

    public function postGaleriaPro(Request $request){
        $upload_handler='';
        $contadorPrincipal=0;
        $contadorPromocional=0;
        $destacado='';
        $promocional='';
        $filePost="";
        $filePostPro="";
        if($request->isMethod('post')){
            $upload_handler = new UploadHandler();
            if(isset($_REQUEST['destacado'])){
                $filePost=$_REQUEST['destacado'];
              $filaName=explode('(',$upload_handler['response']['files'][0]->name);
              if(isset($filaName[1])){
                $filaName=$filaName[1];
                $filePostExplode=explode('.',$_REQUEST['destacado']);
                $filePost=$filePostExplode[0].' ('.$filaName[0].')'.'.'.$filePostExplode[1];
              }
                  if($filePost==$upload_handler['response']['files'][0]->name){
                    $destacado="SI";
                    if(isset($_REQUEST['edit'])){
                        $dataImagenPrincipal=Galeria::where([
                            ['destacada','=','SI'],
                            ['proyecto_id','=',$_REQUEST['proyecto']]])->get();
                        if(count($dataImagenPrincipal)>0){
                          $dataImagenPrincipal=Galeria::find($dataImagenPrincipal[0]->id);
                          $dataImagenPrincipal->destacada='';
                          $dataImagenPrincipal->save();
                        }
                    }
                  }
            }
            if(isset($_REQUEST['destacadoPromocionl'])){
                  $filePostPro=$_REQUEST['destacadoPromocionl'];
                  $filaNamePro=explode('(',$upload_handler['response']['files'][0]->name);
                  if(isset($filaNamePro[1])){
                    $filaNamePro=$filaNamePro[1];
                    $filePostExplode=explode('.',$_REQUEST['destacadoPromocionl']);
                    $filePostPro=$filePostExplode[0].' ('.$filaNamePro[0].')'.'.'.$filePostExplode[1];
                   }
                       if($filePostPro==$upload_handler['response']['files'][0]->name){
                            $promocional="SI";
                            if(isset($_REQUEST['edit'])){
                                $dataImagenPrincipal=$dataImagenPrincipal=Galeria::where([
                            ['promocional','=','SI'],
                            ['proyecto_id','=',$_REQUEST['proyecto']]])->get();
                                if(count($dataImagenPrincipal)>0){
                                  $dataImagenPrincipal=Galeria::find($dataImagenPrincipal[0]->id);
                                  $dataImagenPrincipal->promocional='';
                                  $dataImagenPrincipal->save();
                                }
                            }
                        }
            }
            $galeria = new Galeria();
            $galeria->updated_at=date('Y-m-d H:i:s');
            $galeria->created_at=date('Y-m-d H:i:s');
            $galeria->usuario_ini_id=1;
            $galeria->usuario_act_id=1;
            $galeria->estatus='A';
            $galeria->proyecto_id=$_REQUEST['proyecto'];
            $galeria->destacada=$destacado;
            $galeria->promocional=$promocional;
            $galeria->nombre='upload/server/php/files/'.$upload_handler['response']['files'][0]->name;
            $galeria->save();
        }else{
            error_reporting(E_ALL | E_STRICT);
        }

    }

    public function propiedadGenerale(){
          $tipoConsulta=Tipo::where('estatus', 'A')->get();
            $agenteConsulta=User::where('estatus', 'A')->get();
            $caracteristicaConsulta=Caracteristica::where('estatus', 'A')->get();
            $sectorConsulta=Sector::where('estatus', 'A')->get();
            $operaciones=Operaciones::where('estatus', 'A')->get();
            $ciudad=Ciudad::where('estatus', 'A')->get();
            $propiedad=Propiedad::where('estatus', 'A')->get();
            $consultaProyecto=[];
            $consultaEmpresa=DB::table('empresas')->select('empresas.*')->get();
            $consultaMinimoPrecio=DB::table('propiedades')->select(DB::raw('MIN(propiedades.precio_en_dolares) AS precio_minimo'))->where('propiedades.estatus_invermaste','=','A')->get();


            $consultaCantidadPropiedadesVistas=DB::table('vistas_propiedades')->select('vistas_propiedades.propiedad_id as propiedad_id')->where('estatus','=','A')->get();
            //dd($consultaCantidadPropiedadesVistas);

            $consultaMaximoPrecio=DB::table('propiedades')->select(DB::raw('MAX(propiedades.precio_en_dolares) AS precio_maximo'))->get();
        $consultaPropiedades=DB::table('propiedades')->select('propiedades.nombre as nombre_propiedad' ,'galerias_propiedades.nombre as nombre_galeria','sectores.nombre as nombre_sector', 'propiedades.id', 'propiedades.precio_en_dolares as precio_desde','propiedades.descripcion_breve','propiedades.metros_cuadrado', 'propiedades.dormitorio', 'propiedades.banios', 'propiedades.ubicacion')->join('galerias_propiedades','propiedades.id','=','galerias_propiedades.propiedad_id')->join('sectores','propiedades.sector_id','=','sectores.id')->where([
            ['promocional','SI'],
            ['propiedades.estatus_invermaste','=','A']])->paginate(4);
        //dd($consultaPropiedades);


        return view('plantillaCliente.plantillaPropiedadNueva')->with(['consultaProyecto'=>$consultaProyecto,'tipo'=>$tipoConsulta,'consultaPropiedades'=>$consultaPropiedades,'agente'=>$agenteConsulta,'sector'=>$sectorConsulta,'caracteristica'=>$caracteristicaConsulta,'operaciones'=>$operaciones,'ciudad'=>$ciudad,'propiedad'=>$propiedad,'consultaMaximoPrecio'=>$consultaMaximoPrecio,'consultaEmpresa'=>$consultaEmpresa,'consultaCantidadPropiedadesVistas'=>$consultaCantidadPropiedadesVistas,'consultaMinimoPrecio'=>$consultaMinimoPrecio]);
        //return view('plantillaCliente.plantillaPropiedadNueva')->with(['tipo'=>$tipoConsulta,'consultaPropiedades'=>$consultaPropiedades,'agente'=>$agenteConsulta,'sector'=>$sectorConsulta,'caracteristica'=>$caracteristicaConsulta]);
    }

        public function proyectoGeneral(){
            $tipoConsulta=Tipo::where('estatus', 'A')->get();
            $agenteConsulta=User::where('estatus', 'A')->get();
            $caracteristicaConsulta=Caracteristica::where('estatus', 'A')->get();
            $sectorConsulta=Ciudad::where('estatus', 'A')->get();
            $consultaEmpresa=DB::table('empresas')->select('empresas.*')->get();

            $consultaTipoProyecto=DB::table('proyectos')->select('tipos.nombre as nombre_tipo','proyectos.id as proyecto_id')->join('tipos_proyecto','proyectos.id','=','tipos_proyecto.proyecto_id')->join('tipos','tipos.id','=','tipos_proyecto.tipo_id')->get();

            $consultaMaximoPrecio=DB::table('proyectos')->select(DB::raw('MAX(proyectos.precio_hasta_en_dolares) AS precio_maximo'))->get();
            $consultaMinimoPrecio=DB::table('proyectos')->select(DB::raw('MIN(proyectos.precio_desde_en_dolares) AS precio_minimo'))->get();
            //dd($consultaTipoProyecto);
            //$Proyecto = new Proyecto();
            //dd($Proyecto->tipo());
            //foreach ($Proyecto->tipo as $value){
                //echo $value;
                # code...
            //}
            //$proyecto = Proyecto::find(107); //La materia en la se inscribió;
            //$tipos=$Proyecto->Tipo()->attach(1);
            //$tipos=$tipo->tiposTipo();
            //$tipoGeneral=$proyectoB->$tipos;
            //dd($proyectoB);
                //$consultaTipoProyecto=DB::table('proyectos')->select('tipos.nombre as nombre_tipo')->join('tipos_proyecto','proyectos.id','=','tipos_proyecto.proyecto_id')->join('tipos','tipos.id','=','tipos_proyecto.tipo_id')->where('proyectos.id','=',$id)->get();
            if($_POST){
                $precio_desde=str_replace(",", "", $_REQUEST['precio_desde']);
                $precio_hasta=str_replace(",", "", $_REQUEST['precio_hasta']);

                $precio_hasta=str_replace(",", "", $_REQUEST['precio_hasta']);
                if($_REQUEST['sector']==''){
                    $consultaProyecto=DB::table('proyectos')->select('proyectos.nombre as nombre_proyecto' ,'galerias.nombre as nombre_galeria', 'proyectos.id','proyectos.precio_desde_en_dolares as precio_desde','proyectos.precio_hasta_en_dolares as precio_hasta','proyectos.descripcion_breve','proyectos.logotipo')->join('galerias','proyectos.id','=','galerias.proyecto_id')->where([
                    ['proyectos.precio_desde_en_dolares','>=',$precio_desde],
                    ['proyectos.precio_hasta_en_dolares','>=',$precio_desde],
                    ['promocional','SI']])->paginate(6);
                }else{
                    $consultaProyecto=DB::table('proyectos')->select('proyectos.nombre as nombre_proyecto' ,'galerias.nombre as nombre_galeria', 'proyectos.id','proyectos.precio_desde_en_dolares as precio_desde','proyectos.precio_hasta_en_dolares as precio_hasta','proyectos.descripcion_breve','proyectos.logotipo')->join('galerias','proyectos.id','=','galerias.proyecto_id')->where([
                        ['proyectos.precio_desde_en_dolares','>=',$precio_desde],
                        ['proyectos.ciudad_id','=',$_REQUEST['sector']],
                        ['proyectos.precio_hasta_en_dolares','>=',$precio_desde],
                        ['promocional','SI']])->paginate(6);
                }

            }else{
            $consultaProyecto=DB::table('proyectos')->select('proyectos.nombre as nombre_proyecto' ,'galerias.nombre as nombre_galeria', 'proyectos.id','proyectos.precio_desde_en_dolares as precio_desde','proyectos.precio_hasta_en_dolares as precio_hasta','proyectos.ubicacion','proyectos.descripcion_breve','proyectos.logotipo')->join('galerias','proyectos.id','=','galerias.proyecto_id')->where('promocional','SI')->paginate(6);
            }


        return view('plantillaCliente.plantillaProyectoNueva')->with(['tipo'=>$tipoConsulta,'consultaProyecto'=>$consultaProyecto,'agente'=>$agenteConsulta,'sector'=>$sectorConsulta,'caracteristica'=>$caracteristicaConsulta,'consultaTipoProyecto'=>$consultaTipoProyecto,'consultaMaximoPrecio'=>$consultaMaximoPrecio,'consultaMinimoPrecio'=>$consultaMinimoPrecio,'consultaEmpresa'=>$consultaEmpresa]);
    }


    public function equipoGenerale(){
        $consultaEmpresa=DB::table('empresas')->select('empresas.*')->get();
        $consultaEquipo=DB::table('usuarios')->select('usuarios.*')->where([
        ['usuarios.visible','=',"SI"],['usuarios.estatus','=',"A"]
        ])->get();
        return view('plantillaCliente.plantillaEquipo')->with(['consultaEquipo'=>$consultaEquipo,'consultaEmpresa'=>$consultaEmpresa]);
    }

    public function contactoEmpresa(){
        $consultaEmpresa=DB::table('empresas')->select('empresas.*')->get();
        $consultaEquipo=DB::table('usuarios')->select('usuarios.*')->where([
        ['usuarios.visible','=',"SI"],['usuarios.estatus','=',"A"]
        ])->get();
        return view('plantillaCliente.plantillaContacto')->with(['consultaEquipo'=>$consultaEquipo,'consultaEmpresa'=>$consultaEmpresa]);
    }

    public function quienesSomos(){
        $consultaEmpresa=DB::table('empresas')->select('empresas.*')->get();
        return view('plantillaCliente.plantillaQuinesSomos')->with(['consultaEmpresa'=>$consultaEmpresa]);
    }

    

    public function contacto(){
        $agente = new contactarAgente();
        $agente->estatus_id=1;
        $agente->nombre=$_REQUEST['nombre'];
        $agente->correo=$_REQUEST['email'];
        $agente->telefono=$_REQUEST['telefono'];
        $agente->mensaje=$_REQUEST['mensaje'];
        $agente->created_at=date('Y-m-d H:i:s');
        $agente->updated_at=date('Y-m-d H:i:s');
        $agente->estatus="A";
        $compobarSolicitud="";
        $compobarSolicitud="";
        $usuario_propiedad_contacto=0;
        $_REQUEST['administracion']="frontend";
        $contacto="";
        if($_REQUEST['tipo']=="proyecto"){
            $compobarSolicitud="Admin";
            $agente->proyecto_id=$_REQUEST['proyecto_id'];
            $usuario_propiedad_contacto=$agente->proyecto_id;
        }else if($_REQUEST['tipo']=="usuario"){
            $compobarSolicitud="Agente";
            $agente->equipo_id=$agente->equipo_id;
            $usuario_propiedad_contacto=$agente->proyecto_id;
        }else if($_REQUEST['tipo']=="propiedad"){
            $compobarSolicitud="Admin";
            $agente->propiedad_id=$_REQUEST['propiedad_id'];
            $usuario_propiedad_contacto=$agente->propiedad_id;
        }else if($_REQUEST['tipo']=="contacto"){
            $contacto="agenteAdmin";
            $agente->contacto_id=1;
            $usuario_propiedad_contacto=$agente->contacto_id;
        }

        \Mail::send('contacto.correo_envio',['data'=>$_REQUEST],function($msj){
            $msj->subject('Solicitud Invermaster');
            $emails = array("info@invermasterpuntacana.com",$_REQUEST['email']);
            foreach ($emails as $key => $value) {
                $msj->to($value);
            }
        });

        $agente->save();

        if($compobarSolicitud=="Admin" or $compobarSolicitud=="Agente" or $contacto=="agenteAdmin"){

            $consultaCantidadTicketAdmin=DB::table('contactar_agentes')->select(DB::raw('COUNT(contactar_agentes.id) AS cantidad_solicitud'))->join('estatus','estatus.id','=','contactar_agentes.estatus_id')->where('contactar_agentes.estatus_id','=',1)->get();


            Session::put('solicitud_admin', $consultaCantidadTicketAdmin[0]->cantidad_solicitud);
        }
        if($compobarSolicitud=="Agente" or $contacto=="agenteAdmin"){
            $consultaCantidadTicketAgnte=DB::table('contactar_agentes')->select(DB::raw('COUNT(contactar_agentes.id) AS cantidad_solicitud'))->join('estatus','estatus.id','=','contactar_agentes.estatus_id')->where([
          ['contactar_agentes.estatus_id','=',1],
          ['equipo_id','=',$_REQUEST['usuario_id']]])->orWhere([['contacto_id','=',1],['contactar_agentes.estatus_id','=',1]])->get();

             $usuario_id=Session::get('usuario_id');
             if($contacto!='agenteAdmin'){
                 if(isset($usuario_id)){
                    if($_REQUEST['usuario_id']==Session::get('usuario_id')){
                        Session::put('solicitud_agente', $consultaCantidadTicketAgnte[0]->cantidad_solicitud);   
                    }
                }
            }else{
                Session::put('solicitud_agente', $consultaCantidadTicketAgnte[0]->cantidad_solicitud);
            }
        }
        echo json_encode(['statusCode'=>'exito','menaje'=>'Estimado usuario su mensaje fue enviado de forma exitosa, en breves momento sera atendida su solicitud','usuario_propiedad_contacto'=>$usuario_propiedad_contacto]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dataForm=Proyecto::find($id);
        $tipo=Tipo::where('estatus', 'A')->get();
        $agente=User::where('estatus', 'A')->get();
        $caracteristica=Caracteristica::where('estatus', 'A')->get();
        $sector=Sector::where('estatus', 'A')->get();
        $pais=Paise::where('estatus', 'A')->get();
        $ciudad=Ciudad::where('estatus', 'A')->get();
        $consultaproyectosCaracteristicas=DB::table('proyectos')->select('proyectos.*','caracteristicas_proyecto.*')->join('caracteristicas_proyecto','proyectos.id','=','caracteristicas_proyecto.proyecto_id')->where([
            ['proyectos.estatus','=','A'],
            ['proyectos.id','=',$id],
            ])->get();
        //dd($consultaproyectosCaracteristicas);
        $consultaproyectosTipos=DB::table('proyectos')->select('proyectos.*','tipos_proyecto.*')->join('tipos_proyecto','proyectos.id','=','tipos_proyecto.proyecto_id')->where([
            ['proyectos.estatus','=','A'],
            ['proyectos.id','=',$id],
            ])->get();
        //dd($consultaproyectosCaracteristicas);
      //dd($dataForm);
      return view('proyecto.proyecto_form_update')->with(['dataForm'=> $dataForm,'tipo'=>$tipo,'agentes'=>$agente,'caracteristica'=>$caracteristica,'sector'=>$sector,'pais'=>$pais,'ciudad'=>$ciudad,'consultaproyectosCaracteristicas'=>$consultaproyectosCaracteristicas,'consultaproyectosTipos'=>$consultaproyectosTipos]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    $caracteriticas_elimi=DB::table('tipos_proyecto')->where('proyecto_id',$id)->delete();
    if($request['tipo']!=''){
        foreach ($request['tipo'] as $key => $value){
            $tipo_proyecto=TiposProyectos::where([
              ['tipo_id','=',$value],
              ['proyecto_id','=',$id]]
              )->get();
            if(count($tipo_proyecto)<=0){
                $TiposProyectos = new TiposProyectos();
                $TiposProyectos->proyecto_id = $id;
                $TiposProyectos->tipo_id=$value;
                $TiposProyectos->updated_at=date('Y-m-d H:i:s');
                $TiposProyectos->created_at=date('Y-m-d H:i:s');
                $TiposProyectos->estatus='A';
                $TiposProyectos->nombre='';
                $TiposProyectos->save();
            }
        }
    }

    $caracteriticas_elimi=DB::table('caracteristicas_proyecto')->where('proyecto_id',$id)->delete();


      if($request['caracteristica']!=''){
        foreach ($request['caracteristica'] as $key => $value){
            $caracteriticas=CaracteristicasProyecto::where([
              ['caracteristica_id','=',$value],
              ['proyecto_id','=',$id]]
              )->get();
            if(count($caracteriticas)<=0){
              $caracteristicae_proyecto = new CaracteristicasProyecto();
              $caracteristicae_proyecto->proyecto_id = $id;
              $caracteristicae_proyecto->caracteristica_id=$value;
              $caracteristicae_proyecto->updated_at=date('Y-m-d H:i:s');
              $caracteristicae_proyecto->created_at=date('Y-m-d H:i:s');
              $caracteristicae_proyecto->estatus='A';
              $caracteristicae_proyecto->nombre="";
              $caracteristicae_proyecto->save();
            }
        }
      }
        $dataForm=Proyecto::find($id);
        if($request['logotipo']!=''){
            $request['logotipo']->store('public');
            $foto_cortar=explode('/',$request['logotipo']->store('public'));
            //$request['logotipo']->setPath('/hola/mundo');
        }

        $dataForm->updated_at=date('Y-m-d H:i:s');
        $dataForm->usuario_act_id=Session::get('usuario_id');
        $request['fecha_publicacion'] = date("Y-m-d", strtotime($request['fecha_publicacion']));
        $request['fecha_inicial'] = date("Y-m-d", strtotime($request['fecha_inicial']));
        $request['fecha_final'] = date("Y-m-d", strtotime($request['fecha_final']));

        $precio_desde=str_replace(".", "", $request['precio_desde']);
        $precio_desde=str_replace(",", ".", $precio_desde);
        $precio_desde=str_replace("USD ", "", $precio_desde);

        $precio_hasta=str_replace(".", "", $request['precio_hasta']);
        $precio_hasta=str_replace(",", ".", $precio_hasta);

        $precio_hasta=str_replace("USD ", "", $precio_hasta);


        $precio_desde_dolares=str_replace(".", "", $request['precio_desde_en_dolares']);

        $precio_desde_dolares=str_replace(",", ".", $precio_desde_dolares);

        $precio_desde_dolares=str_replace("USD ", "", $precio_desde_dolares);

        $precio_hasta_dolares=str_replace(".", "", $request['precio_hasta_en_dolares']);
        $precio_hasta_dolares=str_replace(",", ".", $precio_hasta_dolares);

        $precio_hasta_dolares=str_replace("USD ", "", $precio_hasta_dolares);


        $request['precio_desde']=$precio_desde;
        $request['precio_hasta']=$precio_hasta;

        $request['precio_desde_en_dolares']=$precio_desde_dolares;
        $request['precio_hasta_en_dolares']=$precio_hasta_dolares;

        if($dataForm->update($request->all())){
            if($request['logotipo']!=''){
                $dataForm->logotipo='storage/'.$foto_cortar[1];
            }
            $dataForm->save();
            return redirect('admin/proyecto/');
        }
        //
    }

    public function showGaleriaProyecto($id)
    {
      $dataForm=Proyecto::find($id);
      $dataFormGaleria=Galeria::where([
        ['estatus','=','A'],
        ['proyecto_id','=',$id]
        ])->orderBy('destacada','promocional','desc')->paginate(6);
      return view('proyecto.proyecto_view_galeria')->with(['dataForm'=>$dataForm,'dataFormGaleria'=>$dataFormGaleria]);
        //
    }

    public function eliminarFoto(Request $request)
    {
      $dataImagenPrincipal=Galeria::find($request['campoId']);
      $fileContenido=$dataImagenPrincipal->id.'-'.$request['FileInput'][0]->getClientOriginalName();
        $archivoEliminado=unlink($request['campoFoto']);
        //$request['FileInput'][0]->store('public');
        if($archivoEliminado){
        \Storage::disk('galeria')->put($fileContenido,  file_get_contents($request['FileInput'][0]->getRealPath()));
        $dataImagenPrincipal->nombre="upload/server/php/files/".$fileContenido;
        $dataImagenPrincipal->save();
        }
          echo json_encode(['statusCode'=>'Exitoso','id'=>$dataImagenPrincipal->proyecto_id]);
    }


        public function eliminarImagen(Request $request)
    {
      $eliminarImagen=Galeria::find($request['id']);
      $archivoEliminado=unlink($eliminarImagen->nombre);
      $eliminarImagen->delete();
      echo json_encode(['statusCode'=>'Exitoso','id'=>$eliminarImagen->proyecto_id]);
        //dd($request['FileInput'][0]->getClientOriginalName());

      }
    public function cambiarImagenPrincipalProyecto()
    {
        $proyecto_id=$_REQUEST['proyecto_id'];
        $id=$_REQUEST['id'];
      $dataImagenPrincipal=Galeria::where([
        ['destacada','=','SI'],
        ['proyecto_id','=',$proyecto_id]])->get();
      $dataImagenPrincipal=Galeria::find($dataImagenPrincipal[0]->id);
      $dataImagenPrincipal->destacada='';
      if($dataImagenPrincipal->save()){
        $dataImagenPrincipal=Galeria::find($id);
        $dataImagenPrincipal->destacada="SI";
        $dataImagenPrincipal->save();
        echo json_encode(['statusCode'=>'Exitoso','id'=>$proyecto_id]);
      }
    }

    public function cambiarImagenPromocionalProyecto()
    {
        $proyecto_id=$_REQUEST['proyecto_id'];
        $id=$_REQUEST['id'];
      $dataImagenPrincipal=Galeria::where([
        ['promocional','=','SI'],
        ['proyecto_id','=',$proyecto_id]])->get();
      $dataImagenPrincipal=Galeria::find($dataImagenPrincipal[0]->id);
      $dataImagenPrincipal->promocional='';
      if($dataImagenPrincipal->save()){
        $dataImagenPrincipal=Galeria::find($id);
        $dataImagenPrincipal->promocional="SI";
        $dataImagenPrincipal->save();
        echo json_encode(['statusCode'=>'Exitoso','id'=>$proyecto_id]);
      }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $idRequest=explode('-',$id);
        $dataForm=Tipo::find($idRequest[0]);
        $dataForm->updated_at=date('Y-m-d H:i:s');
        if($idRequest[1]=="e"){
            $dataForm->estatus='E';    
        }else{
            $dataForm->estatus='A';   
        }
        
        $dataForm->usuario_act_id=Session::get('usuario_id');
        if($dataForm->save()){
            $mensaje="El registro se ha guardado de forma exitosa";
            $clase="callout callout-success";
            $descripcion=['mensaje'=>$mensaje,'clase'=>$clase];
            echo json_encode(['statusCode'=>'Exito']);
            //return redirect('/admin/tipo/');
        }
    }
}