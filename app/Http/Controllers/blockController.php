<?php

namespace laravelPrueba\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use laravelPrueba\post;

class blockController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $consultaEmpresa=DB::table('empresas')->select('empresas.*')->get();
        $consultaPost=DB::table('post')->select('post.nombre as nombre_post','post.descripcion','post.descripcion_breve','post.id','post.foto','categoria.nombre as nombre_categoria')->join('categoria','categoria.id','=','post.categoria_id')->where([['post.estatus','=','A']])->paginate(6);
        return view('plantillaCliente.plantillaBloc1')->with(['consultaEmpresa'=>$consultaEmpresa,'consultaPost'=>$consultaPost]);

        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $consultaEmpresa=DB::table('empresas')->select('empresas.*')->get();


        $posTags=DB::table('tags')->select('tags.*','post_tag.*')->join('post_tag','tags.id','=','post_tag.tags_id')->where([
            ['tags.estatus','=','A'],
            ['post_tag.post_id','=',$id],
            ])->get();
        $postUsuario=DB::table('post')->select('post.nombre as nombre_post','categoria.nombre as nombre_categoria','post.descripcion_breve','post.descripcion', 'post.foto as foto_post','usuarios.nombre as nombre_usuario','usuarios.correo','usuarios.telefono_celular','usuarios.apellido as apellido_usuario','usuarios.foto as foto_usuario')->join('usuarios','usuarios.id','=','post.usuario_ini_id')->join('categoria','categoria.id','=','post.categoria_id')->where([
            ['post.estatus','=','A'],
            ['post.id','=',$id],
            ])->get();


        return view('plantillaCliente.plantillaBlock2')->with(['consultaEmpresa'=>$consultaEmpresa,'posTags'=>$posTags,'postUsuario'=>$postUsuario]);
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
