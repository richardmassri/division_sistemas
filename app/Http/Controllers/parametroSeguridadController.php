<?php

namespace laravelPrueba\Http\Controllers;

use Illuminate\Http\Request;
use laravelPrueba\ParametroSeguridades;
use laravelPrueba\User;

use Validator;
use Session;

class parametroSeguridadController extends Controller
{

    protected $validationRules=[
          'nombre' => 'required',
          'cantidad' => 'required',
    ];

      protected $validationMessages = [

        'nombre.required' => 'Debe ingresar el nombre',
        'cantidad.required' => 'Debe ingresar la cantidad'
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $consultarParametroSeguridad=ParametroSeguridades::all();
      return view('parametroSeguridad.index')->with('consultarParametroSeguridad', $consultarParametroSeguridad);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $dataForm=ParametroSeguridades::find($id);
      //dd($dataForm);
      return view('parametroSeguridad.parametroSeguridad_form_view')->with('dataForm', $dataForm);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
     $dataForm=ParametroSeguridades::find($id);
      //dd($dataForm);
      return view('parametroSeguridad.parametroSeguridad_form_update')->with('dataForm', $dataForm);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $descripcion=[];
      $v = Validator::make($request->all(), $this->validationRules,$this->validationMessages);
      if ($v->fails())
      {
        return redirect('admin/tipo/create')->withInput()->withErrors($v);
      // The given data did not pass validation
      }else{
        $dataForm=ParametroSeguridades::find($id);
        $dataForm->updated_at=date('Y-m-d H:i:s');
        $dataForm->usuario_act_id=Session::get('usuario_id');
        $dataForm->nombre=$request['nombre'];
        $dataForm->cantidad=$request['cantidad'];
        if($dataForm->save()){
            $mensaje="El registro se ha guardado de forma exitosa";
            $clase="callout callout-success";
            $descripcion=['mensaje'=>$mensaje,'clase'=>$clase];
            return redirect('admin/parametroSeguridad/');
        }
    }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
