<?php

namespace laravelPrueba\Http\Controllers;

use Illuminate\Http\Request;
use laravelPrueba\Caracteristica;
use laravelPrueba\User;

use Validator;
use Session;

class caracteristicaController extends Controller
{

         protected $validationRules=[
          'nombre' => 'required|unique:caracteristicas',
            'descripcion' => 'required',
            'aplica_proyecto' => 'required',
            'aplica_propiedad' => 'required'
    ];

      protected $validationMessages = [

        'nombre.required' => 'Debe ingresar el nombre',
        'nombre.unique' => 'El nombre es de la caracteristica es unico'
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $consultarCaracteristica=Caracteristica::all();
      return view('caracteristica.index')->with('consultarCaracteristica', $consultarCaracteristica);
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('caracteristica.caracteristica_form_registro');
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $validationRules=[
          'nombre' => 'required',
            'descripcion' => 'required',
            'aplica_proyecto' => 'required',
            'aplica_propiedad' => 'required',
            'aplica_propiedad' => 'required',
            'aplica_proyecto' => 'required'
    ];

       $validationMessages = [

        'nombre.required' => 'Debe ingresar el nombre',
        'descripcion.required' => 'Debe ingresar una descripcion',
        'aplica_propiedad.required' => 'Debe ingresar si aplica propiedad',
        'aplica_proyecto.required' => 'Debe ingresar si aplica proyecto'
    ];
    if($request['icono']!=""){

          $validationRules=[
          'nombre' => 'required',
            'descripcion' => 'required',
            'aplica_proyecto' => 'required',
            'aplica_propiedad' => 'required',
            'icono' => 'required|image',
            'aplica_propiedad' => 'required',
            'aplica_proyecto' => 'required'
    ];

       $validationMessages = [

        'nombre.required' => 'Debe ingresar el nombre',
        'descripcion.required' => 'Debe ingresar una descripcion',
        'icono.required' => 'Debe ingresar la foto',
        'icono.image' => 'Debe ingresar el formato correcto de la imagen',
        'aplica_propiedad.required' => 'Debe ingresar si aplica propiedad',
        'aplica_proyecto.required' => 'Debe ingresar si aplica proyecto'
    ];

    }
        if(isset($request['aplica_propiedad'])){
              $validationRules=[
              'nombre' => 'required|unique:caracteristicas',
                //'descripcion' => 'required',
                //'aplica_proyecto' => 'required',
                'aplica_propiedad' => 'required'
            ];
                $validationMessages = [
                'nombre.required' => 'Debe ingresar el nombre',
                'aplica_propiedad' => 'Debe ingresar si aplica propiedad',
                'nombre.unique' => 'El nombre de la caracteristica es unico'
            ];
        }else if(isset($request['aplica_proyecto'])){
             $validationRules=[
                'nombre' => 'required|unique:caracteristicas',
                'aplica_proyecto' => 'required'
                //'aplica_propiedad' => 'required',
            ];
             $validationMessages = [
                'nombre.required' => 'Debe ingresar el nombre',
                'descripcion.required' => 'Debe ingresar una descripcion',
                'aplica_proyecto' => 'Debe ingresar si aplica proyecto',
                'nombre.unique' => 'El nombre de la caracteristica es unico'
            ];
        }
      $caracteristica=new Caracteristica($request->all());
      $v = Validator::make($request->all(), $validationRules,$validationMessages);
      if ($v->fails())
      {
        return redirect('admin/caracteristica/create')->withInput()->withErrors($v);
      // The given data did not pass validation
      }else{
        $caracteristica->estatus= 'A';

        if(isset($request['aplica_proyecto'])){
            $caracteristica->aplica_proyecto=$request['aplica_proyecto'];
        }else{
          $caracteristica->aplica_proyecto='NO';
        }
        if(isset($request['aplica_propiedad'])){
            $caracteristica->aplica_propiedad=$request['aplica_propiedad'];
        }else{
          $caracteristica->aplica_propiedad='NO';
        }
    $caracteristica->updated_at=date('Y-m-d H:i:s');
    $caracteristica->created_at=date('Y-m-d H:i:s');
    $caracteristica->usuario_ini_id=Session::get('usuario_id');
    $caracteristica->usuario_act_id=Session::get('usuario_id');
    $clase="callout callout-success";
    $mensaje="Se ha agregado con éxito la característica ".$caracteristica->nombre;
    $descripcion=['mensaje'=>$mensaje,'clase'=>$clase];
    if(isset($request['icono']) and !empty($request['icono'])){
      $request['icono']->store('public');
      $foto_cortar=explode('/',$request['icono']->store('public'));
      $caracteristica->icono='storage/'.$foto_cortar[1];
    }else{
      $caracteristica->icono="";
    }
    $caracteristica->save();

      #return redirect()->withInput('error', 'Something went wrong.');
      return view('caracteristica.caracteristica_form_registro')->with('descripcion',$descripcion);
    }
  }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $dataForm=caracteristica::find($id);
      //dd($dataForm);
      return view('caracteristica.caracteristica_form_view')->with('dataForm', $dataForm);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Responseq
     */
    public function edit($id)
    {
        $dataForm=Caracteristica::find($id);
      //dd($dataForm);
      return view('caracteristica.caracteristica_form_update')->with('dataForm', $dataForm);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

         $validationRules=[
          'nombre' => 'required',
            //'descripcion' => 'required',
            //'icono' => 'required|image',
            'aplica_propiedad' => 'required',
            'aplica_proyecto' => 'required'
    ];

       $validationMessages = [

        'nombre.required' => 'Debe ingresar el nombre',
        //'descripcion.required' => 'Debe ingresar una descripcion',
        //'icono.required' => 'Debe ingresar la foto',
        //'icono.image' => 'Debe ingresar el formato correcto de la imagen',
        'aplica_propiedad' => 'Debe ingresar si aplica propiedad',
        'aplica_proyecto' => 'Debe ingresar si aplica proyecto'
    ];
        if(isset($request['aplica_propiedad'])){
              $validationRules=[
              'nombre' => 'required',
                //'descripcion' => 'required',
                //'aplica_proyecto' => 'required',
                'aplica_propiedad' => 'required',
                //'icono' => 'required|image'
            ];
                $validationMessages = [
                'nombre.required' => 'Debe ingresar el nombre',
                //'descripcion.required' => 'Debe ingresar una descripcion',
                //'icono.required' => 'Debe ingresar la foto',
                //'icono.image' => 'Debe ingresar el formato correcto de la imagen',
                'aplica_propiedad' => 'Debe ingresar si aplica propiedad'
            ];
        }else if(isset($request['aplica_proyecto'])){
             $validationRules=[
                'nombre' => 'required',
                //'descripcion' => 'required',
                'aplica_proyecto' => 'required',
                //'aplica_propiedad' => 'required',
                //'icono' => 'required|image'
            ];
             $validationMessages = [
                'nombre.required' => 'Debe ingresar el nombre',
                //'descripcion.required' => 'Debe ingresar una descripcion',
                //'icono.required' => 'Debe ingresar la foto',
                //'icono.image' => 'Debe ingresar el formato correcto de la imagen',
                'aplica_proyecto' => 'Debe ingresar si aplica proyecto'
            ];
        }
        if($request['icono']!=""){
            $validationRules['icono']='required|image';
            //$validationMessages[]
        }
      $descripcion=[];
      $v = Validator::make($request->all(), $validationRules,$validationMessages);
      if ($v->fails())
      {
        return redirect('admin/caracteristica/'.$reuest['id'].'/edit')->withInput()->withErrors($v);
      // The given data did not pass validation
      }else{
        $dataForm=Caracteristica::find($id);
        $dataForm->updated_at=date('Y-m-d H:i:s');
        $dataForm->usuario_act_id=Session::get('usuario_id');
        $dataForm->nombre=$request['nombre'];
        $dataForm->descripcion=$request['descripcion'];
        if(isset($request['aplica_proyecto'])){
            $dataForm->aplica_proyecto=$request['aplica_proyecto'];
        }else{
          $dataForm->aplica_proyecto='NO';
        }
        if(isset($request['aplica_propiedad'])){
            $dataForm->aplica_propiedad=$request['aplica_propiedad'];
        }else{
          $dataForm->aplica_propiedad='NO';
        }
        if($request['icono']!=""){
            $dataForm->icono=$request['icono'];
        }
        if($dataForm->save()){
            $mensaje="El registro se ha guardado de forma exitosa";
            $clase="callout callout-success";
            $descripcion=['mensaje'=>$mensaje,'clase'=>$clase];
            return redirect('admin/caracteristica/');
        }
    }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $idRequest=explode('-',$id);
        $dataForm=Caracteristica::find($idRequest[0]);
        $dataForm->updated_at=date('Y-m-d H:i:s');
        if($idRequest[1]=="e"){
            $dataForm->estatus='E';    
        }else{
            $dataForm->estatus='A';   
        }
        $dataForm->usuario_act_id=Session::get('usuario_id');
        if($dataForm->save()){
            $mensaje="El registro se ha guardado de forma exitosa";
            $clase="callout callout-success";
            $descripcion=['mensaje'=>$mensaje,'clase'=>$clase];
            echo json_encode(['statusCode'=>'Exito']);
            //return redirect('/admin/tipo/');
        }
    }
}
