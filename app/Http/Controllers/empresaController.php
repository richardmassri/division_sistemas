<?php

namespace laravelPrueba\Http\Controllers;

use Illuminate\Http\Request;

use laravelPrueba\ParametroSeguridades;
use laravelPrueba\galeriasEmpresas;
use laravelPrueba\empresa;

use Validator;
use Session;
use laravelPrueba\uploadHandler;
use Storage;

class empresaController extends Controller
{

    protected $validationRules=[
          'nombre' => 'required',
          'correo' => 'required',
          'direcccion' => 'required',
          'telefono' => 'required',
          'quines_somos' => 'required',
          'vision' => 'required',
          'telefono' => 'required',
          'facebook' => 'required',
          'twitter' => 'required',
          'instagram'=> 'required'
    ];

      protected $validationMessages = [

        'nombre.required' => 'Debe ingresar el nombre',
        'correo.required' => 'Debe ingresar el correo'
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $consultarEmpresa=Empresa::all();
      return view('empresa.index')->with('consultarEmpresa', $consultarEmpresa);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

      $dataForm=Empresa::find($id);
      //dd($dataForm);
      return view('empresa.empresa_form_view')->with('dataForm', $dataForm);
        //
    }

    public function showGaleria($id)
    {
      $dataForm=Empresa::find($id);
      $dataFormGaleria=galeriasEmpresas::where('estatus','=','A')->orderBy('destacada','desc')->get();
      return view('empresa.empresa_view_galeria')->with(['dataForm'=>$dataForm,'dataFormGaleria'=>$dataFormGaleria]);
        //
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $dataForm=Empresa::find($id);
      return view('empresa.empresa_form_update')->with('dataForm', $dataForm);
    }

    public function cambiarImagenPrincipalEmpresa()
    {
      $dataImagenPrincipal=galeriasEmpresas::where('destacada','=','SI')->get();
      $dataImagenPrincipal=galeriasEmpresas::find($dataImagenPrincipal[0]->id);
      $dataImagenPrincipal->destacada='';
      if($dataImagenPrincipal->save()){
        $dataImagenPrincipal=galeriasEmpresas::find($_REQUEST['id']);
        $dataImagenPrincipal->destacada="SI";
        $dataImagenPrincipal->save();
        echo json_encode(['statusCode'=>'Exitoso','id'=>$dataImagenPrincipal->empresa_id]);
      }
    }

    public function eliminarFoto(Request $request)
    {
      $dataImagenPrincipal=galeriasEmpresas::find($request['campoId']);
        $fileContenido=$dataImagenPrincipal->id.'-'.$request['FileInput'][0]->getClientOriginalName();
        $archivoEliminado=unlink($request['campoFoto']);
        //$request['FileInput'][0]->store('public');
        if($archivoEliminado){
        \Storage::disk('galeriaEmpresa')->put($fileContenido,  file_get_contents($request['FileInput'][0]->getRealPath()));
        $dataImagenPrincipal->nombre="galeriaEmpresa/".$fileContenido;
        $dataImagenPrincipal->save();
        }
          echo json_encode(['statusCode'=>'Exitoso','id'=>$dataImagenPrincipal->empresa_id]);
    }

        public function eliminarImagen(Request $request)
    {
      $eliminarImagen=galeriasEmpresas::find($request['id']);
      $archivoEliminado=unlink($eliminarImagen->nombre);
      $eliminarImagen->delete();
      echo json_encode(['statusCode'=>'Exitoso','id'=>$eliminarImagen->mpresa_id]);
        //dd($request['FileInput'][0]->getClientOriginalName());

      }

    

    
    public function postGaleriaEmpresa()
    {
        $contadorPrincipal=0;
        $contadorPromocional=0;
        $destacado='';
        $promocional='';
        $filePost="";
        if($_POST){
            $upload_handler = new UploadHandler();
            //dd($upload_handler);
            if(isset($_REQUEST['destacado'])){
              $filePost=$_REQUEST['destacado'];
              $filaName=explode('(',$upload_handler['response']['files'][0]->name);
              if(isset($filaName[1])){
                $filaName=$filaName[1];
                $filePostExplode=explode('.',$_REQUEST['destacado']);
                $filePost=$filePostExplode[0].' ('.$filaName[0].')'.'.'.$filePostExplode[1];
              }
              if($filePost==$upload_handler['response']['files'][0]-> name){
                $destacado="SI";
                $dataImagenPrincipal=galeriasEmpresas::where('destacada','=','SI')->get();
                if(count($dataImagenPrincipal)>0){
                  $dataImagenPrincipal=galeriasEmpresas::find($dataImagenPrincipal[0]->id);
                  $dataImagenPrincipal->destacada='';
                  $dataImagenPrincipal->save();
                }
              }
            }

            $galeria = new galeriasEmpresas();
            $galeria->updated_at=date('Y-m-d H:i:s');
            $galeria->created_at=date('Y-m-d H:i:s');
            $galeria->estatus='A';
            $galeria->destacada=$destacado;
            $galeria->promocional=$promocional;
            $galeria->empresa_id=$_REQUEST['empresa_id'];
            //dd($upload_handler['response']['files'][0]->name);
            $galeria->nombre='upload/server/php/files/'.$upload_handler['response']['files'][0]->name;
            $galeria->save();
        }else{
            error_reporting(E_ALL | E_STRICT);
        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $id_request=$request['id'];
      $descripcion=[];
      $v = Validator::make($request->all(), $this->validationRules,$this->validationMessages);
      if ($v->fails())
      {
        return redirect('admin/empresa/'.$id_request.'/edit')->withInput()->withErrors($v);
      // The given data did not pass validation
      }else{
        $dataForm=Empresa::find($id);

        $dataForm->updated_at=date('Y-m-d H:i:s');
        $dataForm->usuario_act_id=Session::get('usuario_id');
        $dataForm->nombre=$request['nombre'];
        $dataForm->correo=$request['correo'];
        $dataForm->direcccion=$request['direcccion'];
        $dataForm->telefono=$request['telefono'];
        $dataForm->vision=$request['vision'];
        $dataForm->facebook=$request['facebook'];
        $dataForm->twitter=$request['twitter'];
        $dataForm->instagram=$request['instagram'];
        $dataForm->latitud=$request['latitud'];
        $dataForm->longitud=$request['longitud'];
        $dataForm->ubicacion=$request['ubicacion'];
        $dataForm->quines_somos=$request['quines_somos'];
        $dataForm->mision=$request['mision'];
        $dataForm->direccion1=$request['direccion1'];

        if($request['logotipo']!=''){
            $request['logotipo']->store('public');
            $foto_cortar=explode('/',$request['logotipo']->store('public'));
            $dataForm->logotipo='storage/'.$foto_cortar[1];
        }
        if($dataForm->save()){
            $mensaje="El registro se ha guardado de forma exitosa";
            $clase="callout callout-success";
            $descripcion=['mensaje'=>$mensaje,'clase'=>$clase];
            return redirect('admin/empresa/'.$dataForm->id.'/edit');
        }
    }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
