<?php

namespace laravelPrueba;

use Illuminate\Database\Eloquent\Model;

class galeriasEmpresas extends Model
{
    protected $table = 'galerias_empresas';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'nombre', 'estatus', 'updated_at', 'created_at','empresa_id','destacada','promocional'
    ];

public function empresa() {
    return $this->belongsTo('laravelPrueba\galeriasEmpresas','empresa_id','id');
}
    //
}
