<?php

namespace laravelPrueba;

use Illuminate\Database\Eloquent\Model;

class CaracteristicasProyecto extends Model
{
       protected $table = 'caracteristicas_proyecto';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'nombre', 'estatus', 'updated_at', 'created_at','proyecto_id','caracteristica_id'
    ];

public function proyecto() {
    return $this->belongsTo('laravelPrueba\Proyecto');
}
}
