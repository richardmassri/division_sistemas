<?php

namespace laravelPrueba;

use Illuminate\Database\Eloquent\Model;

class postTags extends Model
{
       protected $table = 'post_tag';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'estatus', 'updated_at', 'created_at','post_id','tags_id'
    ];

}
