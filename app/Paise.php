<?php

namespace laravelPrueba;

use Illuminate\Database\Eloquent\Model;

class Paise extends Model
{

	protected $table = 'paises';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'nombre','updated_at', 'created_at'
    ];

public function ciudad() {
    return $this->hasOne('laravelPrueba\Ciudad');
}

public function metodo_usuario_act_id() {
    return $this->belongsTo('laravelPrueba\User','usuario_act_id','id');
    
}
public function metodo_usuario_ini_id() {
    return $this->belongsTo('laravelPrueba\User','usuario_ini_id','id');
}
    //
}
