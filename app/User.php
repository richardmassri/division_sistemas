<?php

namespace laravelPrueba;

use Illuminate\Database\Eloquent\Model;


class User extends Model
{
    protected $table = 'usuarios';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'correo', 'nombre', 'apellido', 'telefono_local', 'foto', 'telefono_celular', 'contrasenia','contrasenia_nueva','updated_at', 'created_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

}
