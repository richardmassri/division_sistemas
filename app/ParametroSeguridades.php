<?php

namespace laravelPrueba;

use Illuminate\Database\Eloquent\Model;

class ParametroSeguridades extends Model
{

	protected $table = 'parametros_seguridades';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'nombre','cantidad','updated_at', 'created_at'
    ];


public function metodo_usuario_act_id() {
    return $this->belongsTo('laravelPrueba\User','usuario_act_id','id');
    
}
public function metodo_usuario_ini_id() {
    return $this->belongsTo('laravelPrueba\User','usuario_ini_id','id');
    
}
    //
}
