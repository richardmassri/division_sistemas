<?php

namespace laravelPrueba;

use Illuminate\Database\Eloquent\Model;

class Galeria extends Model
{

	protected $table = 'galerias';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'nombre', 'estatus', 'updated_at', 'created_at','proyecto_id','destacada','promocional'
    ];

public function proyecto() {
    return $this->belongsTo('laravelPrueba\Proyecto');
}



    //
}
