<?php

namespace laravelPrueba;

use Illuminate\Database\Eloquent\Model;

class CaracteristicasPropiedades extends Model
{
   protected $table = 'caracteristicas_propiedades';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'nombre', 'estatus', 'updated_at', 'created_at','caracteristica_id','operacion_id'
    ];

public function proyecto() {
    return $this->belongsTo('laravelPrueba\Proyecto');
}
}
