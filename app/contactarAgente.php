<?php

namespace laravelPrueba;

use Illuminate\Database\Eloquent\Model;

class contactarAgente extends Model
{
        protected $table = 'contactar_agentes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
            'nombre','correo', 'telefono', 'mensaje', 'proyecto_id', 'propiedad_id', 'equipo_id','estatus_id','descripcion','contacto_id','created_at','updated_at','estatus','usuario_act_id'
    ];


public function metodo_proyecto() {
    return $this->belongsTo('laravelPrueba\Proyecto','proyecto_id','id');
    
}
public function metodo_propiedad() {
    return $this->belongsTo('laravelPrueba\Propiedad','propiedad_id','id');
    
}

public function metodo_contacto() {
    return $this->belongsTo('laravelPrueba\User','equipo_id','id');
    
}

public function metodo_usuario_act_id() {
    return $this->belongsTo('laravelPrueba\User','usuario_act_id','id');
    
}

public function metodo_estatus() {
    return $this->belongsTo('laravelPrueba\estatu','estatus_id','id');
    
}

}
