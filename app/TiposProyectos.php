<?php

namespace laravelPrueba;

use Illuminate\Database\Eloquent\Model;

class TiposProyectos extends Model
{
	protected $table = 'tipos_proyecto';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'nombre', 'estatus', 'updated_at', 'created_at','proyecto_id','tipo_id'
    ];

    //
}
