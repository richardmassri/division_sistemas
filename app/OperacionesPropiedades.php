<?php

namespace laravelPrueba;

use Illuminate\Database\Eloquent\Model;

class OperacionesPropiedades extends Model
{

	protected $table = 'operaciones_propiedades';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'nombre', 'estatus', 'updated_at', 'created_at','propiedad_id','operacion_id'
    ];

public function proyecto() {
    return $this->belongsTo('laravelPrueba\Proyecto');
}
    //
}
