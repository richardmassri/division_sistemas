<?php

namespace laravelPrueba;

use Illuminate\Database\Eloquent\Model;

class empresa extends Model
{
     protected $table = 'empresas';

    protected $fillable = [
         'correo', 'nombre', 'direcccion','direccion1', 'telefono', 'quines_somos', 'vision', 'mision', 'latitud','longitud', 'correo','facebook','twitter', 'instagram','logotipo','updated_at', 'ubicacion','created_at','usuario_ini_id','usuario_act_id', 
    ];

   public function metodo_usuario_act_id() {
    return $this->belongsTo('laravelPrueba\User','usuario_act_id','id');
    
}
public function metodo_usuario_ini_id() {
    return $this->belongsTo('laravelPrueba\User','usuario_ini_id','id');
    
}

public function galeriaEmpresa() {
    return $this->hasOne('laravelPrueba\GaleriasEmpresas','id');
}

public function empresa() {
    return $this->belongsTo('laravelPrueba\galeriasEmpresas','id','empresa_id');
}


}
